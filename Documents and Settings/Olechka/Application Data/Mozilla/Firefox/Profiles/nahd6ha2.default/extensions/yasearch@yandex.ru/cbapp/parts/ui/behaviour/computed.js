UI.Behaviour.Computed = UI.Behaviour.extend({
    $name: "XB_UI_Computed",
    
    name: "computed",
    
    constructor: function XBUI_Computed() {
        this.base.apply(this, arguments);
        this.elementId = this.element.getUserData(UI._consts.STR_VAL_REF_ID_KEY_NAME);
        this.builder.collection.put(this.root.widgetInstanceId, this.elementId, this);
    },
    
    readValue: function XBUI_Computed_readValue() {
        if (this.elementId) {
            this.value = this.builder._windowEngine.getValue(this.root.widgetInstanceId, this.elementId);
        }
        else {
            this.logger.warn("No value reference ID found.");
            this.logger.debug(UI._XMLSerializer.serializeToString(this.element));
        }
    },
    outerNode: function XBUI_Computed_outerNode() {
        var children = this.childrenEx(),
            container = this.parent.innerNode();
        for (let i = 0, length = children.length; i < length; i++) {
            let child = children[i],
                node = child.outerNode();
            if (node && node.parentNode == container)
                return node;
        }
        return null;
    },
    
    attach: function XBUI_Computed_attach(child) {
        this.parent.attach(child, this);
    },
    
    detach: function XBUI_Computed_detach(child) {
        this.parent.detach(child, this);
    },
    
    update: function XBUI_Computed_update() {
        this.readValue();
        
        for each (let child in this.children())
            child.destroy();
        
        this._animateChildren(this._processValue());
        
        this.change();
    },
    
    append: function XBUI_Computed_append() {
    },
    
    build: function XBUI_Computed_build() {
        this.update();
    },
    
    destroy: function XBUI_Computed_destroy() {
        this.builder.windowEngine.valueNotNeeded(this.root.widgetInstanceId, this.elementId);
        this.builder.collection.remove(this.root.widgetInstanceId, this.elementId);
        
        this.base();
    },
    
    _processValue: function XBUI_Computed__processValue() {
        let value = this.value;
        if (typeof value == "undefined")
            return [];
        
        if (XB._base.runtime.isException(value)) {
            this.logger.debug("Got exception value: " + value.toString());
            return [];
        }
        
        let elements = null;
        
        if (XB._base.runtime.isXML(value)) {
            elements = Array.prototype.slice.apply(value.childNodes);
            elements = elements.concat(Array.prototype.slice.apply(value.attributes));
        } else {
            let text = "";
            try {
                text = XB._base.runtime.xToString(value);
            }
            catch (e) {
                this.logger.error("Failed converting XValue into a string. " + strutils.formatError(e));
            }
            
            elements = [this.document.createTextNode(text)];
        }
        return this._translateElements(elements);
    }
});
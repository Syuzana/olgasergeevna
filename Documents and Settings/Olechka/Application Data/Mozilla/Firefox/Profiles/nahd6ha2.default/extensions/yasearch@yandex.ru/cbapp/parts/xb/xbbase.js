
XB._Cc = Components.classes;
XB._Ci = Components.interfaces;
XB._Cr = Components.results;
XB._Cu = Components.utils;

XB._base = {
    application: null,
    consts: {
        STR_FUNCTIONAL_NS: "http://bar.yandex.ru/dev/functional",
        STR_UI_NS: "http://bar.yandex.ru/dev/gui",
        
        STR_VAL_REF_ELEM_NAME: "__value__",
        STR_VAL_REF_ID_KEY_NAME: "uid",
        
        ERR_XML_PARSE_ERROR: "XML Parse error",
        ERR_INVALID_DOC_STRUC: "Invalid document structure",
        ERR_UID_NOT_FOUND: "Node uid attribute not found",
        ERR_DOM_DOC_EXPECTED: "DOM document expected",
        ERR_DOM_NODES_EXPECTED: "DOM node or node list expected",
        ERR_NUMBER_EXPECTED: "Number expected",
        ERR_STRING_EXPECTED: "String expected",
        ERR_NO_RUNTIME_FUNCTION: "Function is not defined",
        ERR_UID_REQUIRED: "UID required",
        ERR_RUNTIME_ERROR: "Runtime error occured",
        ERR_UNIT_REQUIRED: "XB unit required",
        ERR_WINST_REQUIRED: "Widget instance required",
        ERR_DATA_UNDEFINED: "No data, variable or setting with this name was defined",
        ERR_PROC_NODE_EXPECTED: "ProcNode instance expected",
        ERR_DISPOSED_XML: "Disposed XML object"
    },
    loggersRoot: undefined,
    logger: null,
    domParser: XB._Cc["@mozilla.org/xmlextras/domparser;1"].getService(XB._Ci.nsIDOMParser),
    getLogger: function XBBase_getLogger(name) {
        return XB._application.core.Lib.Log4Moz.repository.getLogger(this.loggersRoot + "." + name);
    }
};

XB._base.toolkitDataProvider = {
    QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports, Ci.nsIXBDataProvider, Ci.nsIXBPackage]),
    get UUID() {
        return "toolkit";
    },
    
    newChannel: function XBPToolkitDataProvider_newChannel(aURI) {
        const ioService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
        let channel = ioService.newChannel(this._rootPath + aURI.path, null, null);
        channel.originalURI = aURI;
        return channel;
    },
    findFile: function XBPToolkitDataProvider_findFile(path) {
        let file = this._pkgsFile.clone().QueryInterface(Ci.nsILocalFile);
        path.split("/").forEach(function appendPart(part) {
            if (!path) return;
            file.append(part);
        });
        return file;
    },
    
    get _rootPath() {
        let path = "chrome://" + XB._base.application.name + "/content/custombar/packages/toolkit";
        delete this._rootPath;
        return this._rootPath = path;
    },
    
    get _pkgsFile() {
        let rootFile = XB._base.application.core.extensionPathFile.QueryInterface(Ci.nsILocalFile);
        ["chrome", "content", "custombar", "packages", "toolkit"].forEach(function appendPart(part) {
            rootFile.append(part);
        });
        delete this._pkgsFile;
        return this._pkgsFile = rootFile;
    }
};

XB._base.runtime = {
    finalize: function XBRuntime_finalize() {
        this._tempDoc = null;
    },
    
    xToString: function XBRuntime_xToString(fromXVal) {
        if (sysutils.isString(fromXVal))
            return fromXVal;
        if (fromXVal == undefined || fromXVal == XB.types.empty)
            return "";
        if (this.isXML(fromXVal))
            return fromXVal.textContent;
        if (this.isRequestData(fromXVal))
            return fromXVal.url;
        if (this.isException(fromXVal))
            throw new XB._base.runtime.ETypeCast(fromXVal, "String");
        return fromXVal.toString();
    },
    
    xToNumber: function XBRuntime_xToNumber(fromXVal) {
        if (sysutils.isNumber(fromXVal)) {
            if (isNaN(fromXVal))
                throw new XB._base.runtime.ETypeCast(fromXVal, "Number");
            return fromXVal;
        }
        if (this.isException(fromXVal))
            throw new XB._base.runtime.ETypeCast(fromXVal, "Number");
        if (sysutils.isBool(fromXVal))
            return Number(fromXVal);
        let trimmedStrVal = strutils.trimAll( this.xToString(fromXVal) );
        if (!trimmedStrVal)
            return 0;
        if (!this._numPattern.test(trimmedStrVal))
            throw new XB._base.runtime.ETypeCast(fromXVal, "Number");
        let num = parseFloat(trimmedStrVal);
        if (isNaN(num))
            throw new XB._base.runtime.ETypeCast(fromXVal, "Number");
        return num;
    },
    
    xToBool: function XBRuntime_xToBool(fromXVal) {
        if (sysutils.isBool(fromXVal))
            return fromXVal;
        if (sysutils.isNumber(fromXVal))
            return fromXVal != 0;
        if (this.isException(fromXVal))
            throw new XB._base.runtime.ETypeCast(fromXVal, "Bool");
        let strVal = strutils.trimSpaces( this.xToString(fromXVal) );
        if (strVal == "" || strVal == "false")
            return false;
        if (this._numPattern.test(strVal)) {
            let numVal = Number(strVal);
            if (!isNaN(numVal))
                return numVal != 0;
        }
        return true;
    },
    
    xToXML: function XBRuntime_xToXML(fromXVal) {
        if (this.isXML(fromXVal))
            return fromXVal;
        let node = this._tempDoc.createTextNode(this.xToString(fromXVal));
        return new XB.types.XML(node);
    },
    
    xToRequestData: function XBRuntime_xToRequestData(fromXVal) {
        if (this.isRequestData(fromXVal))
            return fromXVal;
        const monthTimeRange = 60 * 60 * 24 * 30;
        return new XB.types.RequestData(this.xToString(fromXVal), "GET", monthTimeRange, monthTimeRange * 2,
                                        XB.types.RequestData.Format.FMT_XML, {start:100, end:399});
    },
    
    isXML: function XBRuntime_isXML(xVal) {
        return (xVal instanceof XB.types.XML);
    },
    
    isException: function XBRuntime_isException(xVal) {
        return xVal instanceof XB.types.Exception;
    },
    
    isRequestData: function XBRuntime_isRequestData(xVal) {
        return xVal instanceof XB.types.RequestData;
    },

    compareValues: function XBRuntime_compareValues(left, right, mode, castFails) {
        if (left === right)
            return 0;
        let [value1, value2, rank] = this._checkTypes(left, right, mode, castFails);
        switch (rank) {
            case this._ranksMap.Empty:
                return 0;
            case this._ranksMap.Bool:
                return Number(value1) - Number(value2);
            case this._ranksMap.Number:
                return value1 - value2;
            case this._ranksMap.String:
                return strutils.compareStrings(value1, value2);
            case this._ranksMap.XML:
            case this._ranksMap.RequestData:
            case this._ranksMap.Exception:
                return value1.equalsTo(value2) ? 0 : NaN;
            default:
                return NaN;
        }
    },
    
    serializeXML: function XBRuntime_serializeXML(node) {
        return this._XMLSerializer.serializeToString(node);
    },
    
    get tempXMLDoc() {
        return this._tempDoc;
    },
    
    queryXMLDoc: function XBRuntime_queryXMLDoc(expr, contextNode, extNSResolver) {
        try {
            let result = misc.queryXMLDoc(expr, contextNode, extNSResolver);
            if ((typeof result == "number") && isNaN(result))
                throw new this.EXPATHNaN(expr);
            if (result === undefined)
                throw new this.EXPATHUnexpectedResult(expr, typeof result);
            
            XB._base.logger.trace("XPATH (" + expr + ") result is \"" + result + "\"");
            return result;
        }
        catch (e) {
            if (!(e instanceof this.EXPATHNaN))
                XB._base.logger.warn(this._consts.ERR_XPATH_QUERY_FAILED + ". " + strutils.formatError(e));
            throw e;
        }
    },
    
    transformXML: function XBRuntime_transformXML(nodeOrDoc, stylesheet) {
        try {
            this._xsltProcessor.reset();
            this._xsltProcessor.importStylesheet(stylesheet);
            return this._xsltProcessor.transformToFragment(nodeOrDoc, this._tempDoc);
        }
        finally {
            this._xsltProcessor.reset();
        }
    },
    
    makeXML: function XBRuntime_makeXML(xmlJson) {
        let doc = this._tempDoc;
        
        function makeElement(elementJson, asDoc) {
            let element;
            if (!asDoc)
                element = doc.createElement(elementJson.name);
            else {
                doc = doc.implementation.createDocument("", elementJson.name, null);
                element = doc.documentElement;
            }
            
            for (let attrName in elementJson.attributes)
                element.setAttribute(attrName, elementJson.attributes[attrName]);
            append(elementJson.children, element);
            return asDoc ? doc : element;
        }
        
        function append(children, parent) {
            for each (let elementJson in children)
                parent.appendChild(makeElement(elementJson));
        }
        
        if (!sysutils.isArray(xmlJson))
            return new XB.types.XML(makeElement(xmlJson, true));
        
        let result = new XB.types.XML();
        append(xmlJson, result);
        return result;
    },
    createXBExceptionFromRTError: function XBRuntime_XBEFromRTE(nid, error) {
        let XBE = new XB.types.Exception(nid, XB.types.Exception.types.E_RUNTIME, error.message);
        XBE.name = error.name;
        XBE.fileName = error.fileName;
        XBE.lineNumber = error.lineNumber;
        
        return XBE;
    },
    describeValue: function XBRuntime_describeValue(xValue) {
        let valueRank = this.rankValue(xValue);
        let descr = this._typeNames[valueRank];
        if (valueRank > this._ranksMap.Empty && valueRank < this._ranksMap.XML)
            descr += "(" + this.xToString(xValue) + ")";
        if (valueRank == this._ranksMap.Exception)
            descr += "(" + xValue.toString() + ")";
        return descr;
    },
    ensureValueTypeIs: function XBRuntime_ensureValueTypeIs(xValue, xType) {
        let expectedRank = this._ranksMap[xType];
        if (expectedRank === undefined)
            throw new Error("Unknown type name");
        let valueRank = this.rankValue(xValue);
        if (expectedRank != valueRank)
            throw new TypeError(xType + " expected, got " + this._typeNames[valueRank]);
    },
    
    _consts: {
        ERR_XPATH_QUERY_FAILED: "XPATH query failed",
        ERR_UNKNOWN_TYPE: "Unknown type ",
        ERR_NO_CAST_FUNC: "No casting function for this rank"
    },
    
    _tempDoc: XB._Cc["@mozilla.org/xml/xml-document;1"].createInstance(XB._Ci.nsIDOMDocument).implementation.createDocument("", "xbruntime", null),
    _XMLSerializer: XB._Cc["@mozilla.org/xmlextras/xmlserializer;1"].getService(XB._Ci.nsIDOMSerializer),
    _xpathEvaluator: XB._Cc["@mozilla.org/dom/xpath-evaluator;1"].getService(XB._Ci.nsIDOMXPathEvaluator),
    _xsltProcessor: XB._Cc["@mozilla.org/document-transformer;1?type=xslt"].createInstance(XB._Ci.nsIXSLTProcessor),
    _numPattern: /^[+\-]?(\d+\.?|\d*\.\d+)$/,

    _typeNames: {
        0: "Empty",
        1: "Bool",
        2: "Number",
        3: "String",
        4: "XML",
        5: "RequestData",
        6: "Exception"
    },
    
    _ranksMap: {
        Empty: 0,
        Bool: 1,
        Number: 2,
        String: 3,
        XML: 4,
        RequestData: 5,
        Exception: 6
    },
    
    rankValue: function XBRuntime__rankValue(value) {
        if (value === XB.types.empty)
            return this._ranksMap.Empty;
        if (sysutils.isBool(value))
            return this._ranksMap.Bool;
        if (sysutils.isNumber(value))
            return this._ranksMap.Number;
        if (sysutils.isString(value))
            return this._ranksMap.String;
        if (this.isXML(value))
            return this._ranksMap.XML;
        if (this.isRequestData(value))
            return this._ranksMap.RequestData;
        if (this.isException(value))
            return this._ranksMap.Exception;
        
        throw new TypeError(this._consts.ERR_UNKNOWN_TYPE + typeof value);
    },
    
    _castToRank: function XBRuntime__castToRank(value, rank) {
        let destType = this._typeNames[rank];
        let castFunc = this["xTo" + destType];
        if (castFunc == undefined)
            throw new Error(this._consts.ERR_NO_CAST_FUNC);
        return castFunc.call(this, value);
    },
    
    _checkTypes: function XBRuntime__checkTypes(value1, value2, mode, castFails) {
        let r1 = this.rankValue(value1);
        let r2 = this.rankValue(value2);
        
        if (r1 == r2)
            return [value1, value2, r1];
        
        if (mode == XB._base.runtime.cmpModes.CMP_STRICT ||
            r1 > this._ranksMap.RequestData || r2 > this._ranksMap.RequestData)
            return [value1, value2, NaN];
        
        let maxRank = Math.max(r1, r2);
        let commonRank = (mode == this.cmpModes.CMP_SMART ? Math.min(this._ranksMap.String, maxRank) : maxRank);
        try {
            return [this._castToRank(value1, commonRank), this._castToRank(value2, commonRank), commonRank]
        }
        catch (e) {
            if (castFails)
                throw e;
            return [value1, value2, NaN];
        }
    }
};
XB._base.runtime.cmpModes = {
    CMP_STRICT: 0,
    CMP_FREE: 1,
    CMP_SMART: 2
};

XB._base.runtime.EXPATH = CustomErrors.ECustom.extend({
    $name: "EXPATH",
    
    constructor: function XBRuntimeEXPATH(expression) {
        this.base();
        this._expr = expression.toString();
    },
    
    _message: "XPATH query error",
    _expr: undefined,
    
    get _details() {
        return [this._expr];
    }
});

XB._base.runtime.EXPATHNaN = XB._base.runtime.EXPATH.extend({
    $name: "EXPATHNaN",
    
    _message: "XPATH 'number' expression returned NaN"
});

XB._base.runtime.EXPATHUnexpectedResult = XB._base.runtime.EXPATH.extend({
    $name: "EXPATHUnexpectedResult",
    
    constructor: function XBRuntimeEXPATHUnexpReslt(expression, resultType) {
        this.base(expression);
        this._resType = resultType.toString();
    },
    
    _message: "XPATH query error",
    _resType: undefined,
    
    get _details() {
        return [this._expr, this._resType];
    }
});

XB._base.runtime.ETypeCast = CustomErrors.ECustom.extend({
    $name: "ETypeCast",
    
    constructor: function XBRuntimeETypeCast(originalValue, destTypeName) {
        this.base();
        this._origValueDescr = XB._base.runtime.describeValue(originalValue);
        this._destTypeName = destTypeName.toString();
    },
    
    _message: "This value conversion is impossible",
    _origValueDescr: undefined,
    _destTypeName: undefined,
    
    get _details() {
        return [this._origValueDescr, this._destTypeName];
    }
});

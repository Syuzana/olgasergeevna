UI.Behaviour.Image = UI.Behaviour.extend({
    $name: "XB_UI_Image",
    
    name: "image",
    nodeName: "image",
    
    attach: function XBUI_Image_attach() {
    },
    
    change: function XBUI_Image_change() {
        this.node.setAttribute("src", this.url());
    },
    
    url: function XBUI_Image_url() {
        return this.builder.resolveURI(this.text(), this.root.widgetInstanceId);
    }
});
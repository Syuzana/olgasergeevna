let EXPORTED_SYMBOLS = ["JSON", "JSON2XML"];
if (typeof(JSON) === "undefined") {
    var JSON = {
        _nativeJSON: Components.classes["@mozilla.org/dom/json;1"]
                               .createInstance(Components.interfaces.nsIJSON),
        
        _getGlobalObject: function JSON__getGlobalObject(aObjectName) {
            let top = this.parse.caller || this;
            
            do {
                top = top.__parent__;
            } while (top.__parent__)
            
            return top[aObjectName];
        },
        
        parse: function JSON_parse(aJSONString) {
            let result = this._nativeJSON.decode(aJSONString);
            
            if (result) {
                if (isObject(result)) {
                    let obj = this._getGlobalObject("Object");
                    if (!(result instanceof obj))
                        result.__proto__ = obj.prototype;
                } else if (isArray(result)) {
                    let arr = this._getGlobalObject("Array");
                    if (!(result instanceof arr))
                        result.__proto__ = arr.prototype;
                }
            }
            
            return result;
        },
        
        stringify: function JSON_stringify(aJSObject) {
            return this._nativeJSON.encode(aJSObject);
        }
    }
}


function isArray(aObject)
    Object.prototype.toString.call(aObject) === "[object Array]";

function isObject(aObject)
    Object.prototype.toString.call(aObject) === "[object Object]";

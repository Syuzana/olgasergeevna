const EXPORTED_SYMBOLS = ["addonFS"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

const addonFS = {
    
    init: function AddonFS_init(application) {
        this._CHROME_APP_PATH = "chrome://" + application.name + "/";
    },
    
    getStreamFromPath: function AddonFS_getStreamFromPath(aPath) {
        let chromeURI = this._convertPathToURI(aPath);
        
        let inputStream;
        
        switch (chromeURI.scheme) {
            case "jar": {
                chromeURI.QueryInterface(Ci.nsIJARURI);
                
                let fileURI = chromeURI.JARFile;
                fileURI.QueryInterface(Ci.nsIFileURL);
                
                let chromeFile = fileURI.file;
                let jarEntry = chromeURI.JAREntry;
                
                if (jarEntry) {
                    let zipReader = Cc["@mozilla.org/libjar/zip-reader;1"].createInstance(Ci.nsIZipReader);
                    zipReader.open(chromeFile);
                    
                    try {
                        if (zipReader.hasEntry(jarEntry))
                            inputStream = zipReader.getInputStream(jarEntry);
                    } finally {
                        zipReader.close();
                    }
                }
                
                break;
            }
            
            case "file": {
                let protocolHandler = Cc["@mozilla.org/network/protocol;1?name=file"]
                                          .createInstance(Ci.nsIFileProtocolHandler);
                let file = protocolHandler.getFileFromURLSpec(chromeURI.spec);
                
                if (file instanceof Ci.nsILocalFile) {
                    inputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
                    inputStream.init(file, 0x01, 0, inputStream.CLOSE_ON_EOF);
                }
                
                break;
            }
            default:
                throw new Error("'" + chromeURI.scheme + "' not yet impl");
                break;
            
        }
        
        return inputStream;
    },
    
    copySource: function AddonFS_copySource(aSource, aDestination, aNewName, aPermissions) {
        if (!(aDestination instanceof Ci.nsIFile))
            throw new TypeError("nsIFile required");
        
        if (typeof aSource == "string") {
            let chromeURI = this._convertPathToURI(aSource);
            switch (chromeURI.scheme) {
                case "jar": {
                    chromeURI.QueryInterface(Ci.nsIJARURI);
                    
                    let fileURI = chromeURI.JARFile;
                    fileURI.QueryInterface(Ci.nsIFileURL);
                    
                    let chromeFile = fileURI.file;
                    let jarEntry = chromeURI.JAREntry;
                    
                    if (jarEntry) {
                        let zipReader = Cc["@mozilla.org/libjar/zip-reader;1"].createInstance(Ci.nsIZipReader);
                        zipReader.open(chromeFile);
                        
                        try {
                            let hasEntry = zipReader.hasEntry(jarEntry);
                            if (!hasEntry) {
                                jarEntry = jarEntry.replace(/(\/?)$/, "/");
                                hasEntry = zipReader.hasEntry(jarEntry);
                            }
                            
                            if (hasEntry) {
                                
                                function getTargetFile(aEntry) {
                                    let target = destinationDir.clone();
                                    
                                    aEntry.replace(jarEntry, "")
                                          .split("/")
                                          .forEach(function(aPart) target.append(aPart));
                                    
                                    return target;
                                }
                                
                                let nsILocalFile = Ci.nsILocalFile;
                                let destinationDir = aDestination.clone();
                                let realEntry = zipReader.getEntry(jarEntry);
                                if (!realEntry.isDirectory) {
                                    let entryForTarget = jarEntry + "/" + (aNewName || jarEntry.split("/").pop());
                                    let target = getTargetFile(entryForTarget);
                                    
                                    if (!target.exists()) {
                                        target.create(nsILocalFile.FILE_TYPE, aPermissions || aDestination.permissions);
                                    } else if (aPermissions) {
                                        target.permissions = aPermissions;
                                    }
                                    
                                    zipReader.extract(jarEntry, target);
                                    return;
                                }
                                destinationDir.append(aNewName || jarEntry.split("/").slice(-2,-1)[0] || "");
                                let entries = zipReader.findEntries(jarEntry);
                                while (entries.hasMore()) {
                                    let entryName = entries.getNext();
                                    let target = getTargetFile(entryName);
                                    if (!target.exists())
                                        target.create(nsILocalFile.DIRECTORY_TYPE, aPermissions || aDestination.permissions);
                                }
                                entries = zipReader.findEntries(jarEntry + "*");
                                while (entries.hasMore()) {
                                    let entryName = entries.getNext();
                                    let target = getTargetFile(entryName);
                                    
                                    if (target.exists())
                                        continue;
                                    
                                    target.create(nsILocalFile.DIRECTORY_TYPE, aPermissions || aDestination.permissions);
                                    zipReader.extract(entryName, target);
                                }
                            }
                        } finally {
                            zipReader.close();
                        }
                    }
                    
                    break;
                }
                
                case "file": {
                    let protocolHandler = Cc["@mozilla.org/network/protocol;1?name=file"]
                                              .createInstance(Ci.nsIFileProtocolHandler);
                    let file = protocolHandler.getFileFromURLSpec(chromeURI.spec);
                    
                    if (file instanceof Ci.nsILocalFile) {
                        file.copyTo(aDestination, aNewName || "");
                        
                        if (aPermissions)
                            file.permissions = aPermissions;
                    }
                    
                    break;
                }
                default:
                    throw new Error("'" + chromeURI.scheme + "' not yet impl");
                    break;
                
            }
        }
    },
    _CHROME_APP_PATH: undefined,
    _IO_SERVICE: Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService),
    _CHROME_REGISTRY_SERVICE: Cc["@mozilla.org/chrome/chrome-registry;1"].getService(Ci.nsIChromeRegistry),
    
    _convertPathToURI: function AddonFS__convertPathToURI(aPath) {
        if (!(typeof aPath == "string"))
            throw new TypeError("String required");
        
        let path = aPath.replace(/^\$custombar\//, this._CHROME_APP_PATH + "content/custombar/")
                        .replace(/^\$(content|locale|skin)\//, this._CHROME_APP_PATH + "$1/")
                        .replace(/^\$chrome\//, this._CHROME_APP_PATH);
        
        let uri = this._IO_SERVICE.newURI(path, undefined, null);
        if (uri.scheme !== "chrome")
            throw new Error("Not a chrome URI");
        
        return this._CHROME_REGISTRY_SERVICE.convertChromeURL(uri);
    }
};

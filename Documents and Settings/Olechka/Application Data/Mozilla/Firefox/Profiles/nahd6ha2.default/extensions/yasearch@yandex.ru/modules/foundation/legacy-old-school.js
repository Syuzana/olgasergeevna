/*
	http://github.com/shergin/legacy/
	Special version for Firefox only.
*/

function ClassOldSchool($super, $members, $statics) {
    if ($members.constructor && ($members.constructor.name !== "Object")) {
		$members.$constructor = $members.constructor;
        delete $members.constructor;
    }
    
    function $class() {
        if ("$constructor" in this)
            this.$constructor.apply(this, arguments);
    }
    
    var prototype = {};
    
    if ($super) {
        prototype.__proto__ = $super.prototype;
        prototype.constructor = $class;
    }
    
    $class.$super = $super;
    $class.prototype = prototype;
    
    prototype.$class = $class;
    prototype.$super = $super ? $super.prototype : null;
    prototype.$name = $members.$name || ($members.$constructor ? $members.$constructor.name : '');
    
    ClassOldSchool.$implement($class, $members, $statics);
    
    return $class;
}

ClassOldSchool.$empty = function() {};

function wrapMethod(method) {
	if (method.toString().indexOf('.base') > -1) {
        var baseMethod = method.$class.$super.prototype[method.$name];
        if (!baseMethod)
            return method;
        
        var wrapper = function() {
			var savedBaseMethod = this.base;
            this.base = baseMethod;
			try {
				return method.apply(this, arguments);
			}
			finally {
				this.base = savedBaseMethod;
			}
        };
        
        wrapper.$class = method.$class;
        wrapper.$name = method.$name;
        return wrapper;
    }
    else {
        return method;
    }
}

ClassOldSchool.$copy = function($source, $target, $class) {
    const specials = ["$class", "$name", "$super"];
    
    for (let name in $source) {
        if (specials.indexOf(name) >= 0)
            continue;
        
        let getter = $source.__lookupGetter__(name),
            setter = $source.__lookupSetter__(name);
        
        if (getter || setter) {
            if (getter) {
                if (getter.$class)
                    getter = eval(getter.toString());
                getter.$class = $class;
                getter.$name = name;
                $target.__defineGetter__(name, wrapMethod(getter));
            }
            
            if (setter) {
                if (setter.$class)
                    setter = eval(setter.toString());
                setter.$class = $class;
                setter.$name = name;
                $target.__defineSetter__(name, wrapMethod(setter));
            }
        }
        else {
            let member = $source[name];
            if (typeof member == "function") {
                member.$class = $class;
                member.$name = name;
                $target[name] = wrapMethod(member);
            }
            else
                $target[name] = member;
        }
    }
};

ClassOldSchool.$implement = function $implement($class, $members, $statics) {
    ClassOldSchool.$copy($members, $class.prototype, $class);
    
    if ($statics)
        ClassOldSchool.$copy($statics, $class, null);
};
var Base = ClassOldSchool(null, {
        extend: function($members) {
            ClassOldSchool.$copy($members, this, this.$class);
            return this;
        },
        base: function() {
        }
    }, {
        ancestorOf: function($class) {
            while ($class) {
                $class = $class.$super;
                if ($class == this)
                    return true;
            }
            return false;
        },
        
        inherits: function($class) {
            return $class.ancestorOf(this);
        },
        extend: function $extend(members, statics) {
            statics = statics || {};
            statics.extend = this.extend;
            statics.implement = this.implement;
            statics.ancestorOf = this.ancestorOf;
            statics.inherits = this.inherits;
            var $class = ClassOldSchool(this, members || {}, statics);
            return $class;
        },
        implement: function(members, statics) {
            if (members.prototype)
                ClassOldSchool.$implement(this, members.prototype, members);
            else
                ClassOldSchool.$implement(this, members, statics);
            return this;
        }
    }
);

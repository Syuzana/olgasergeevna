
BarPlatform.WidgetPackage = function WidgetPackage(rootDir, id) {
    if ( !(rootDir instanceof Ci.nsIFile) )
        throw new CustomErrors.EArgType("rootDir", "nsIFile", rootDir);
    if ( !rootDir.isDirectory() )
        throw new CustomErrors.EArgRange("rootDir", "nsIFile(Directory)", rootDir);
    if (!sysutils.isString(id))
        throw new CustomErrors.EArgType("id", "String", id);
    
    this._rootDir = rootDir.clone();
    this._rootDir.normalize();
    
    this._id = id;
    
    let uuid = Cc["@mozilla.org/uuid-generator;1"].getService(Ci.nsIUUIDGenerator).generateUUID().toString();
    this._uuid = uuid.replace(/[^\w\-]/g, "") + "." + BarPlatform._application.name;
    
    this._files = {__proto__: null};
    
    this._logger = BarPlatform._getLogger("Package_" + this._uuid);
    
    let protocolHandler = BarPlatform._application.core.xbProtocol;
    protocolHandler.setDataProvider(this._uuid, this);
    this._uri = protocolHandler.newURI(protocolHandler.scheme + "://" + this._uuid + "/", null, null);
    
    this._persistPath = BarPlatform._application.core.xbWidgetsPrefsPath + this._id;
    this._settings = {};
    this._units = {};
};

BarPlatform.WidgetPackage.prototype = {
    constructor: BarPlatform.WidgetPackage,
    get id() {
        return this._id;
    },
    getUnit: function XBPkg_getUnit(unitName) {
        let unit = this._units[unitName];
        if (!unit) {
            let unitFile = this.findFile(unitName + ".xml");
            if (!unitFile)
                throw new Error(this._consts.ERR_FILE_NOT_FOUND + " \"" + unitName + ".xml\"");
            
            unit = new BarPlatform.Unit(unitFile.leafName, this, unitName);
            this._units[unitName] = unit;
        }
        return unit;
    },
    
    newChannelFromPath: function XBPkg_newChannelFromPath(path) {
        return this.newChannel(this._ioService.newURI(this.resolvePath(path), null, null));
    },
    resolvePath: function XBPkg_resolvePath(path) {
        if (!sysutils.isString(path))
            throw new CustomErrors.EArgType("path", "String", path);
        
        return this._uri.resolve(path);
    },
    
    finalize: function XBPkg_finalize() {
        for (let unitName in this._units)
            try {
                this._units[unitName].finalize();
            }
            catch (e) {
                this._logger.error("Couldn't clear loaded unit " + unitName + ". " + strutils.formatError(e));
            }
        this._units = null;
        this._settings = null;
        
        BarPlatform._application.core.xbProtocol.setDataProvider(this._uuid, null);
    },
    
    addSettingUser: function XBPkg_addSettingUser(settingName, componentID) {
        let users = this._settings[settingName] || (this._settings[settingName] = []);
        if (users.indexOf(componentID) > -1) return;
        users.push(componentID);
    },
    
    removeSettingUser: function XBPkg_removeSettingUser(settingName, componentID) {
        if (!settingName in this._settings) return;
        let users = this._settings[settingName];
        let userIndex = users.indexOf(componentID);
        if (userIndex < 0) return;
        users.splice(userIndex, 1);
    },
    
    getSettingUsers: function XBPkg_getSettingUsers(settingName) {
        let users = this._settings[settingName];
        return users ? users.slice(0) : [];
    },
    QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports, Ci.nsIXBDataProvider, Ci.nsIXBPackage]),
    
    get UUID() {
        return this._uuid;
    },
    
    get _chromeChannelPrincipal() {
        let chromeChannel = this._ioService.newChannel("chrome://" + BarPlatform._application.name + "/content/", null, null);
        systemPrincipal = chromeChannel.owner;
        chromeChannel.cancel(Components.results.NS_BINDING_ABORTED);
        
        this.__defineGetter__("_chromeChannelPrincipal", function() systemPrincipal);
        return this._chromeChannelPrincipal;
    },
    
    newChannel: function XBPkg_newChannel(aURI) {
        let file = this.findFile(aURI.QueryInterface(Ci.nsIURL).filePath);
        if (!file)
            throw new Error(this._consts.ERR_FILE_NOT_FOUND + " " + aURI.path);
        
        let filesStream = Cc["@mozilla.org/network/file-input-stream;1"]
            .createInstance(Ci.nsIFileInputStream);
        filesStream.init(file, 1, 0, filesStream.CLOSE_ON_EOF);
        
        let channel = Cc["@mozilla.org/network/input-stream-channel;1"]
                          .createInstance(Ci.nsIInputStreamChannel)
                          .QueryInterface(Ci.nsIChannel);
        channel.setURI(aURI);
        channel.originalURI = aURI;
        channel.contentStream = filesStream;
        channel.owner = this._chromeChannelPrincipal;
        
        return channel;
    },
    findFile: function XBPkg_findFile(path) {
        if (!sysutils.isString(path))
            throw new CustomErrors.EArgType("path", "String", path);
        
        path = this._suppressRelativePathReference(path);
        
        if (this._files[path])
            return this._files[path];
        
        let root = this._rootDir.clone(),
            locales = this._locales(),
            components = path.split("/"),
            file = null;
        
        if (components[components.length - 1][0] == ".")
            return this._files[path] = null;
        
        for (let i = locales.length; i--;) {
            let localeName = locales[i].name;
            let candidate = root.clone();
            if (localeName != "") {
                candidate.append("locale");
                candidate.append(localeName);
            }
            
            for (let j = 0, len = components.length; j < len; j++)
                candidate.append(components[j]);
            
            if (!candidate.exists())
                continue;
            
            try {
                candidate.normalize();
                if (candidate.isReadable() && !candidate.isDirectory() && this._rootDir.contains(candidate, true)) {
                    file = candidate;
                    break;
                }
            } catch(e) {
                this._logger.error("Error while searching file. " + strutils.formatError(e));
                continue;
            }
        }
        
        return this._files[path] = file;
    },
    
    _consts: {
        ERR_NO_UNIT: "No such unit",
        ERR_NO_WIDGET: "Unit does not describe a widget",
        ERR_FILE_NOT_FOUND: "File not found",
        ERR_ACCESS_DENIED: "Attempt to access a file outside the package directory"
    },
    _id: undefined,
    _uuid: undefined,
    _rootDir: null,
    _units: null,
    _logRoot: undefined,
    _name: undefined,
    _bestLocaleName: undefined,
    _logger: null,
    _persistPath: undefined,
    
    _ioService: Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService),
    
    _locales: function XBPkg__locales() {
        if (this._localesCache)
            return this._localesCache;
        
        const weights = {
            language: 32,
            root: 16,
            ru: 8,
            en: 4,
            country: 2,
            region: 1
        };
        
        let locales = [];
        locales.push({
            name: "",
            weight: weights.root,
            components: null
        });
        
        let localeDir = this._rootDir.clone();
        localeDir.append("locale");
        if (!localeDir.exists())
            return this._localesCache = locales;
        
        let appLocale = misc.parseLocale(BarPlatform._application.localeString);
        
        let entries = localeDir.directoryEntries;
        while (entries.hasMoreElements()) {
            let file = entries.getNext().QueryInterface(Ci.nsIFile);
            if (file.isDirectory()) {
                let name = file.leafName;
                let components = misc.parseLocale(name);
                
                if (!components)
                    continue;
                
                let weight = 0;
                for (let space in weights) {
                    let component = components[space];
                    if (component === undefined)
                        continue;
                    
                    if (space == "language")
                        if (component in weights)
                            weight += weights[component];
                    
                    if (component === appLocale[space])
                        weight += weights[space];
                }
                
                locales.push({
                    name: name,
                    weight: weight,
                    components: components
                });
                
            }
        }
        
        locales.sort(function XBPkg__locales_sort(a, b) a.weight - b.weight);
        
        return this._localesCache = locales;
    },
    
    _suppressRelativePathReference: function XBPkg__suppressRelativePathReference(path) {
        let re = [
            /\/\//g,
            /\/\.\//g,
            /\/[^\/]+\/\.\.\//g,
            /\/\.\.\//g
        ];
        
        for (let i = 0, len = re.length; i < len; i++)
            while (re[i].test(path))
                path = path.replace(re[i], "/");
        
        return path;
    }
};

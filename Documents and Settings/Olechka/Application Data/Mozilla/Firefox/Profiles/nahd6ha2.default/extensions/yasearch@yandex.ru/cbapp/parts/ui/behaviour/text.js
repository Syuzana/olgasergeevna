UI.Behaviour.Text = UI.Behaviour.extend({
    name: "text",
    
    constructor: function XBUI_Text_constructor() {
        this.base.apply(this, arguments);
        
        let text = this.element.nodeValue;
        text = strutils.normalizeString(text);
        this.value = text;
    },
    create: function XBUI_Text_create() {
        this.logger.trace("Creating node " + this.nodeName);
        this.node = this.document.createTextNode(this.value);
    },
    text: function XBUI_Text_text() {
        return this.value;
    },
    readAttributes: function XBUI_Text_readAttributes() {
    },
    build: function XBUI_Text_build() {
    }
});
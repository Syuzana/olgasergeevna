const EXPORTED_SYMBOLS = [
    "Base",
    "sysutils",
    "strutils",
    "fileutils",
    "misc",
    "CustomErrors",
    "DownloadQueue"
];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

const FOUNDATION_DIR = __LOCATION__.parent;
FOUNDATION_DIR.append("foundation");

const FOUNDATION_PATH = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI(FOUNDATION_DIR).spec;

[
    "sysutils.js",
    "strutils.js",
    "fileutils.js",
    "misc.js",
    "legacy-old-school.js",
    "ecustom.js",
    "dlqueue.js"
].forEach(function load(moduleFileName) {
    this.loadSubScript(FOUNDATION_PATH + moduleFileName);
}, Cc["@mozilla.org/moz/jssubscript-loader;1"].getService(Ci.mozIJSSubScriptLoader));

const EXPORTED_SYMBOLS = ["installer"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

const installer = {
    init: function installer_init(aApplication) {
        this._application = aApplication;
        this.info = {
            __proto__: null,
            addonVersionState: 0,
            addonLastVersion: "0",
            get isFreshAddonInstall() this.addonLastVersion === "0"
        };
        
        this._start();
    },
    get addonDir() {
        return this._application.core.extensionPathFile;
    },
    get addonId() {
        let id = this.AddonManager.getAddonId(this.addonDir);
        this.__defineGetter__("addonId", function addonId() id);
        return this.addonId;
    },
    get addonVersion() {
        let ver = this.AddonManager.getAddonVersion(this.addonDir);
        this.__defineGetter__("addonVersion", function addonVersion() ver);
        return this.addonVersion;
    },
    get isAddonUninstalling() {
        return this.AddonManager.isAddonUninstalling(this.addonId);
    },
    get _addonLastInstalledVersion() {
        return this._application.core.Lib.Preferences.get(this._application.name + ".versions.lastAddon", "0");
    },
    set _addonLastInstalledVersion(aValue) {
        this._application.core.Lib.Preferences.set(this._application.name + ".versions.lastAddon", aValue);
    },
    
    _start: function Installer__start() {
        this.AddonManager = this._application.core.Lib.AddonManager;
        this.AddonManager.watchAddonUninstall(this.addonId);

        Cc["@mozilla.org/observer-service;1"]
            .getService(Ci.nsIObserverService)
            .addObserver(this, "quit-application", false);
        
        this._checkVersions();
    },
    _checkVersions: function Installer__checkVersions() {
        let currentVersion = this.addonVersion;
        let lastVersion = this._addonLastInstalledVersion;
        
        this.info.addonLastVersion = lastVersion;
        
        if (lastVersion == currentVersion)
            return;
        this._addonLastInstalledVersion = currentVersion;
        
        const versionComparator = Cc["@mozilla.org/xpcom/version-comparator;1"].getService(Ci.nsIVersionComparator);
        this.info.addonVersionState = versionComparator.compare(currentVersion, lastVersion);
    },
    
    observe: function Installer_observe(aSubject, aTopic, aData) {
        if (aTopic == "quit-application")
            this._shutdown();
    },
    
    _shutdown: function Installer__shutdown() {
        Cc["@mozilla.org/observer-service;1"]
            .getService(Ci.nsIObserverService)
            .removeObserver(this, "quit-application");
        
        this._application = null;
        this.info = null;
    }
};

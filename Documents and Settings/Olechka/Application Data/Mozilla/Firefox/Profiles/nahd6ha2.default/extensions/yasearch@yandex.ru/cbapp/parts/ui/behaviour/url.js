UI.Behaviour.Url = UI.Behaviour.extend({
    $name: "XB_UI_Url",
    
    name: "url",
    
    append: function XBUI_Url_append() {
    },
    
    action: function XBUI_Url_action(eventInfo) {
        let navData = {
            sourceWindow: this.document.defaultView,
            unsafeURL:    this.text(),
            target:       this.attribute.target,
            eventInfo:    eventInfo
        };
        let actionID = this.attribute.action;
        if (actionID) {
            let widgetProto = this.root.prototype();
            let componentInfo = {id: widgetProto.id, packageID: widgetProto.unit.unitPackage.id};
            this.builder.windowEngine.navigate(navData, actionID, componentInfo);
        }
        else
            this.builder.windowEngine.navigate(navData);
    }
});
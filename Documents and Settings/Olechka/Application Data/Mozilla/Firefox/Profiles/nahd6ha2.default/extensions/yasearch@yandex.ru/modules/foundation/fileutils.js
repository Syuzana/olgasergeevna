const fileutils = {
    MODE_RDONLY:   0x01,
    MODE_WRONLY:   0x02,
    MODE_CREATE:   0x08,
    MODE_APPEND:   0x10,
    MODE_TRUNCATE: 0x20,
    
    PERMS_FILE     : parseInt("0644", 8),
    PERMS_DIRECTORY: parseInt("0755", 8),
    
    readTextFile: function FileUtils_readTextFile(file) {
        let is = this._Cc["@mozilla.org/network/file-input-stream;1"].createInstance(this._Ci.nsIFileInputStream);
        is.init(file, this.MODE_RDONLY, 0, is.CLOSE_ON_EOF);
        try {
            return this.readStringFromStream(is);
        } finally {
            is.close();
        }
    },
    
    writeTextFile: function FileUtils_writeTextFile(file, text, accessRights, modeFlags) {
        let accessRights = accessRights || this.PERMS_FILE;
        let modeFlags = modeFlags || (this.MODE_WRONLY | this.MODE_CREATE | this.MODE_TRUNCATE);
        
        let fileOutStream = this._Cc["@mozilla.org/network/file-output-stream;1"]
                                .createInstance(this._Ci.nsIFileOutputStream);
        fileOutStream.init(file, modeFlags, accessRights, 0);
        
        let converter = this._Cc["@mozilla.org/intl/converter-output-stream;1"]
                            .createInstance(this._Ci.nsIConverterOutputStream);
        converter.init(fileOutStream, "UTF-8", 0, 0);
        converter.writeString(text);
        converter.close();
    },
    
    writeStreamToFile: function FileUtils_writeStreamToFile(inputStream, destFile, accessRights, modeFlags, progressWatcher) {
        if ( !(inputStream instanceof this._Ci.nsIInputStream) )
            throw new TypeError("First argument must be nsIInputStream");
        if ( !(destFile instanceof this._Ci.nsIFile) )
            throw new TypeError("Second argument must be nsIFile");
        
        let fileOutStream = this._Cc["@mozilla.org/network/file-output-stream;1"].createInstance(this._Ci.nsIFileOutputStream);
        try {
            let accessRights = accessRights || this.PERMS_FILE;
            let modeFlags = modeFlags || this.MODE_WRONLY | this.MODE_CREATE | this.MODE_TRUNCATE;
            fileOutStream.init(destFile, modeFlags, accessRights, 0);
            
            let binInputStream = this._Cc["@mozilla.org/binaryinputstream;1"].createInstance(this._Ci.nsIBinaryInputStream);
            binInputStream.setInputStream(inputStream);
            try {
                let size;
                let buf;
                while (size = binInputStream.available()) {
                    buf = binInputStream.readBytes(size);
                    fileOutStream.write(buf, size);
                    
                    if (progressWatcher) {
                        let stop = progressWatcher.onWrite(size);
                        if (stop)
                            break;
                    }
                }
            } finally {
                binInputStream.close();
            }
        }
        finally {
            fileOutStream.close();
        }
    },
    
    xmlDocFromFile: function FileUtils_xmlDocFromFile(file, withSystemPrincipal) {
        file = file.QueryInterface(this._Ci.nsILocalFile);
        let inStream = this._Cc["@mozilla.org/network/file-input-stream;1"].createInstance(this._Ci.nsIFileInputStream);
        inStream.init(file, this.MODE_RDONLY, 0, inStream.CLOSE_ON_EOF);
        return this.xmlDocFromStream(inStream, null, null, withSystemPrincipal);
    },
    
    xmlDocFromStream: function FileUtils_xmlDocFromStream(docStream, docURI,
                                                          baseURI, withSystemPrincipal) {
        docStream = docStream.QueryInterface(this._Ci.nsIInputStream);
        try {
            let domParser = misc.getDOMParser(docURI, baseURI, withSystemPrincipal);
            let XMLDoc = domParser.parseFromStream(docStream, null, docStream.available(), "text/xml");
            if (XMLDoc.documentElement.localName == "parsererror")
                throw new Error(XMLDoc.documentElement.textContent);
            return XMLDoc;
        }
        finally {
            docStream.close();
        }
    },
    
    readStringFromStream: function FileUtils_readStringFromStream(aInputStream) {
        let streamSize = aInputStream.available();
        let convStream = this._Cc["@mozilla.org/intl/converter-input-stream;1"].createInstance(this._Ci.nsIConverterInputStream);
        convStream.init(aInputStream, "UTF-8", streamSize, this._Ci.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER);
        try {
            let data = {};
            convStream.readString(streamSize, data);
            return data.value;
        }
        finally {
            convStream.close();
        }
    },
    
    extractZipArchive: function FileUtils_extractZipArchive(archiveFile, destDirFile) {
        if ( !(archiveFile instanceof this._Ci.nsIFile) || !(destDirFile instanceof this._Ci.nsIFile) )
            throw new TypeError("nsIFile required");
        
        function getItemFile(filePath) {
            let itemLocation = destDirFile.clone();
            filePath.split("/").forEach(function(part) itemLocation.append(part));
            return itemLocation;
        }
        
        let zReader = this._Cc["@mozilla.org/libjar/zip-reader;1"].createInstance(this._Ci.nsIZipReader);
        zReader.open(archiveFile);
        try {
            let entries = zReader.findEntries("*/");
            
            while (entries.hasMore()) {
                let entryName = entries.getNext();
                let target = getItemFile(entryName);
                if (!target.exists())
                    target.create(this._Ci.nsILocalFile.DIRECTORY_TYPE, destDirFile.permissions);
            }
            
            entries = zReader.findEntries(null);
            while (entries.hasMore()) {
                let entryName = entries.getNext();
                let target = getItemFile(entryName);
                if (target.exists()) {
                    if (target.isDirectory())
                        continue;
                    else
                        target.remove();
                }
                
                zReader.extract(entryName, target);
                target.permissions |= this.PERMS_FILE;
            }
        }
        finally {
            zReader.close();
        }
    },
    
    forceDirectories: function FileUtils_forceDirectories(dirFile, perm) {
        perm = perm || this.PERMS_DIRECTORY;
        if (!(dirFile.exists() && dirFile.isDirectory()))
            dirFile.create(this._Ci.nsIFile.DIRECTORY_TYPE, perm);
    },
    
    removeFileSafe: function FileUtils_removeFileSafe(file) {
        if (!file.exists())
            return;
        file = file.clone();
        
        try {
            file.remove(true);
            if (!file.exists())
                return;
        }
        catch(e) {
        }
        
        let trash = this._Cc["@mozilla.org/file/directory_service;1"]
                        .getService(this._Ci.nsIProperties)
                        .get("TmpD", this._Ci.nsIFile);
        
        trash.append("trash");
        trash.createUnique(this._Ci.nsIFile.DIRECTORY_TYPE, this.PERMS_DIRECTORY);
        try {
            file.moveTo(trash, file.leafName);
        }
        catch (e) {
            try {
                file.remove(true);
            }
            catch (e) {
            }
        }
        try {
            trash.remove(true);
        }
        catch (e) {
        }
    },
    
    _getDOMParserSys: function FileUtils__getDOMParserSys(docURI, baseURI) {
        let domParser = this._Cc["@mozilla.org/xmlextras/domparser;1"].createInstance(this._Ci.nsIDOMParser);
        
        if (docURI || baseURI) {
            let systemPrincipal = null;
            try {
                systemPrincipal = Cc["@mozilla.org/scriptsecuritymanager;1"]
                                      .getService(Ci.nsIScriptSecurityManager)
                                      .getSystemPrincipal();
            } catch (ex) {}
            
            domParser.init(systemPrincipal, docURI, baseURI);
            
        } else {
            try {
                domParser.init();
            } catch (ex) {
                domParser = this._Cc["@mozilla.org/xmlextras/domparser;1"].getService(this._Ci.nsIDOMParser);
            }
        }
        
        return domParser;
    },
    
    _Ci: Components.interfaces,
    _Cc: Components.classes
};

pref("extensions.yasearch@yandex.ru.description", "chrome://yasearch/locale/yasearch.properties");
pref("network.IDN.whitelist.xn--p1ai", true);

pref("yasearch.license.hidden", true);
pref("yasearch.license.show", true);
pref("yasearch.license.homepage.checked", true);
pref("yasearch.license.search.checked", true);

pref("yasearch.general.debug.enabled", false);
pref("yasearch.general.lastVersion", "0");

pref("yasearch.default.search", true);

pref("yasearch.users.default", "");

pref("yasearch.general.ui.command.open.tab", false);
pref("yasearch.general.ui.show.site.search", true);
pref("yasearch.general.ui.search.history.enabled", true);
pref("yasearch.general.ui.highlighter.enabled", false);
pref("yasearch.general.ui.highlighter.teleport", true);
pref("yasearch.general.ui.highlighter.serp", true);
pref("yasearch.general.ui.translate.enabled", true);
pref("yasearch.general.ui.translate.key.enabled", false);
pref("yasearch.general.ui.translate.key.shortcut", "control|| ||");
pref("yasearch.general.ui.translate.reverse", false);
pref("yasearch.general.ui.translate.from", "en");
pref("yasearch.general.ui.translate.to", "ru");
pref("yasearch.general.ui.translate.interval.show", 500);
pref("yasearch.general.ui.show.bloggers.value", false);
pref("yasearch.general.ui.show.cy.value", false);
pref("yasearch.general.ui.mybar.login.show", true);
pref("yasearch.general.ui.mybar.logout.noprompt", false);
pref("yasearch.general.ui.mybar.collapsed.services", "");
pref("yasearch.general.ui.bookmarks.action", 2);
pref("yasearch.general.ui.bookmarks.lastfolder", "");
pref("yasearch.general.ui.bookmarks.showaddtofolderontop", true);
pref("yasearch.general.ui.bookmarks.prefs.yaruLiveWindowDisabled", false);
pref("yasearch.general.ui.bookmarks.prefs.yaruBlockExpanded", false);
pref("yasearch.general.ui.bookmarks.prefs.yaruExportChecked", false);
pref("yasearch.general.ui.bookmarks.prefs.yaruAddHelpShowed", 0);
pref("yasearch.general.ui.bookmarks.prefs.yaruPublicVal", 5);
pref("yasearch.general.ui.mail.integration", true);
pref("yasearch.general.ui.mail.integration.helper.show", true);
pref("yasearch.general.ui.mail.mfd.warning.show", true);
pref("yasearch.general.ui.urlbar.corrector.state", 2);
pref("yasearch.general.searchplugin.check", false);
pref("yasearch.general.server.time", "");
pref("yasearch.general.search.first", 0);
pref("yasearch.general.stt", "");
pref("yasearch.general.ftab.enabled", true);
pref("yasearch.general.ftab.compatibility", 1);
pref("yasearch.general.ftab.gridLayout", "[[1,2,3],[4,5,6],[7,8,9]]");
pref("yasearch.general.ftab.backgroundImage", "");
pref("yasearch.general.ftab.backgroundAdjustment", "atleast");
pref("yasearch.general.ftab.backgroundForceResize", true);
pref("yasearch.general.ftab.backgroundAlignVertical", "center");
pref("yasearch.general.ftab.backgroundAlignHorizontal", "center");
pref("yasearch.general.ftab.allowOpacity", false);
pref("yasearch.general.ftab.refreshTimeGlobal", 604800);
pref("yasearch.general.ftab.hideInfoBlock", false);

pref("yasearch.welcomepage.version", "5.2.0");
pref("yasearch.welcomepage.version.introduced", "0");
pref("yasearch.welcomepage.dontshow", true);

pref("yasearch.http.auto.enabled", true);
pref("yasearch.http.update.interval", 5);

pref("yasearch.mail.ui.notification.enabled", true);
pref("yasearch.mail.ui.soundnotification.enabled", false);
pref("yasearch.mail.ui.soundnotification.uri", "");
pref("yasearch.mail.uri.open.new", false);

pref("yasearch.feeds.ui.notification.enabled", true);
pref("yasearch.feeds.ui.soundnotification.enabled", false);
pref("yasearch.feeds.ui.soundnotification.uri", "");

pref("yasearch.defence.homepage.enabled", true);

pref("yasearch.geolocation.enabled", true);

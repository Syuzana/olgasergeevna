const EXPORTED_SYMBOLS = ["XB"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

Cu.import("resource://gre/modules/XPCOMUtils.jsm");

var sysutils,
    fileutils,
    strutils,
    misc,
    CustomErrors,
    Base,
    BarPlatform;

const XB = {
    init: function XB_init(application) {
        this._application = application;
        BarPlatform = application.BarPlatform;
        let (appCoreLib = application.core.Lib) {
            sysutils = appCoreLib.sysutils;
            fileutils = appCoreLib.fileutils;
            strutils = appCoreLib.strutils;
            misc = appCoreLib.misc;
            CustomErrors = appCoreLib.CustomErrors;
            Base = appCoreLib.Base;
        };
        
        this._logger = application.core.Lib.Log4Moz.repository.getLogger(application.name + ".XB");
        this._loadModules();
        
        XB._base.application = application;
        XB._base.loggersRoot = application.name + ".XB";
        XB._base.logger = this._logger;
        
        XB._Parser.init();
        XB.cache.init();
        XB.network.init();
        
        application.core.xbProtocol.setDataProvider("toolkit", this._base.toolkitDataProvider);
    },
    
    finalize: function XB_finalize() {
        this._application.core.xbProtocol.setDataProvider("toolkit", null);
        XB.network.finalize();
        XB.cache.finalize();
        XB._base.runtime.finalize();
        XB._base.logger = null;
        XB._base.domParser = null;
    },
    
    _application: null,
    
    _modules: [
        "xbbase.js", "xbtypes.js", "xbcalcnodes.js", "xbparser.js", "xbwidget.js",
        "xbdb.js", "xbcache.js", "xbnetwork.js", "xbfuncs.js"
    ],
    
    _loadModules: function XB__loadModules() {
        const mozSSLoader = Cc["@mozilla.org/moz/jssubscript-loader;1"].getService(Ci.mozIJSSubScriptLoader);
        const xbDirPath = this._application.partsURL + "xb/";
        this._modules.forEach(function xb_loadModule(moduleFileName) {
            this._logger.debug("  Loading module " + moduleFileName);
            mozSSLoader.loadSubScript(xbDirPath + moduleFileName);
        }, this);
    }
};

UI.Behaviour.Button = UI.Behaviour.WithAction.extend({
    name: "button",
    nodeName: "toolbarbutton",
    
    constructor: function XBUI_Button() {
        this.base.apply(this, arguments);
        this.menu = null;
        this.hasActionOrUrl = false;
    },
    
    action: function XBUI_Button_action(eventInfo) {
        if (eventInfo.type != "command")
            return true;
        
        if (!this.hasActionOrUrl && this.menu) {
            let box = this.node.boxObject,
                menu = this.menu.node;
            if (box)
                menu.openPopup(null, "", box.x, box.y + box.height, false, false);
            else
                menu.openPopup(this.node, "after_start", 0, 0, false, false);
            
            return false;
        }
        
        return true;
    },
    
    change: function XBUI_Button_change() {
        this.menu = null;
        let menus = this.childrenEx("menu");
        for each (let menu in menus)
            if (menu.appended()) {
                this.menu = menu;
                break;
            }
        
        this.hasActionOrUrl = (this.childrenEx(/^action|url$/).length > 0);
        
        if (this.hasActionOrUrl && this.menu)
            this.node.setAttribute("type", "menu-button");
        else
            this.node.removeAttribute("type");
        
        this.base();
    }
});

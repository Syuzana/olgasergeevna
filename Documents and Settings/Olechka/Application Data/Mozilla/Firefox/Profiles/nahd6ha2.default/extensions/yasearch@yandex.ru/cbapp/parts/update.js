const EXPORTED_SYMBOLS = ["updater"];

const { classes: Cc, interfaces: Ci, utils: Cu, results: Cr } = Components;

const updater = {
    init: function Updater_init(application) {
        this._application = application;
        
        (this._timerOne = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer))
            .initWithCallback(this, 24 * 3600 * 1000, Ci.nsITimer.TYPE_REPEATING_SLACK);
        
        let Preferences = this._application.core.Lib.Preferences;
        let now = Math.floor(Date.now() / 1000);
        let PREF_LAST_UPDATE_TIME = this._application.name + ".updates.widgets.lastUpdateTime";
        
        if (Math.abs((Preferences.get(PREF_LAST_UPDATE_TIME) || 0) - now) > 24 * 3600) {
            Preferences.set(PREF_LAST_UPDATE_TIME, now);
            
            (this._timerTwo = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer))
                .initWithCallback(this, 30 * 1000, Ci.nsITimer.TYPE_ONE_SHOT);
        }
    },
    
    notify: function Updater_notify() {
        this._update();
    },
    _comparator: Cc["@mozilla.org/xpcom/version-comparator;1"].getService(Ci.nsIVersionComparator),
    
    _activePresets: function Updater__activePresets() {
        let presets = [];
        let entries = this._application.directories.XBPresets.directoryEntries;
        
        while (entries.hasMoreElements()) {
            let file = entries.getNext().QueryInterface(Ci.nsIFile);
            let address = decodeURIComponent(file.leafName);
            let preset = new this._application.BarPlatform.Preset(file, address);
            
            let used = false;
            for each (let widgetInfo in preset.allEntries)
                if (this._widgetLibrary.isKnownComponent(widgetInfo.componentID)) {
                    used = true;
                    break;
                }
            
            if (used)
                presets.push(preset);
            else
                file.remove(false);
        }
        
        return presets;
    },
    
    _update: function Updater__update() {
        this._presets = [];
        this._manifests = [];
        
        this._packageManager = this._application.packageManager;
        this._widgetLibrary = this._application.widgetLibrary;
        this._lib = this._application.core.Lib;
        
        this._updateStepOne();
    },
    
    _updateStepOne: function Updater__updateStepOne() {
        let knownPresets = this._activePresets();
        
        let items = [];
        
        for each (let preset in knownPresets) {
            items.push({
                url: preset.address,
                preset: preset
            });
        }
        
        let context = this;
        this.queue = new this._lib.DownloadQueue(
            items,
            function (items) { context._updateStepTwo(items); },
            null
        );
    },
    
    _updateStepTwo: function Updater__updateStepTwo(items) {
        let unknownPackageIds = [],
            knownPackageIds = this._packageManager.packageIDs;
        
        knownPackageIds = knownPackageIds.filter(function(id) {
            return !this._application.widgetLibrary.isPreinstalledPackage(id);
        }, this);
        
        for each (let item in items) {
            if (item.status === Cr.NS_OK) {
                try {
                    let preset = new this._application.BarPlatform.Preset(item.file, item.url);
                    if (this._comparator.compare(preset.version, item.preset.version) > 0) {
                        this._presets.push(preset);
                        
                        for each (let info in preset.widgetEntries)
                            if (!this._packageManager.isPackageInstalled(info.packageID) && (unknownPackageIds.indexOf(info.packageID) == -1))
                                unknownPackageIds.push(info.packageID);
                    }
                }
                catch(e) {
                }
            }
        }
        
        this.queue.destroy();
        items = [];
        
        for each (let id in unknownPackageIds) {
            let url = id;
            items.push({
                url: url
            });
        }
        
        for each (let id in knownPackageIds) {
            if (this._widgetLibrary.getComponentsInfo(id).length == 0)
                continue;
            let info = this._packageManager.getPackageInfo(id);
            items.push({
                url: info.uri,
                info: info
            });
        }
        
        let context = this;
        this.queue = new this._lib.DownloadQueue(
            items,
            function (items) { context._updateStepThree(items); },
            null
        );
    },
    
    _updateStepThree: function Updater__updateStepThree(items) {
        for each (let item in items) {
            if (item.status === Cr.NS_OK) {
                try {
                    let manifest = new this._application.BarPlatform.PackageManifest(item.url, item.file);
                    if (item.info) {
                        let [info, ] = this._application.selectBestPackage(manifest);
                        if (info && this._comparator.compare(info.version, item.info.version) > 0)
                            this._manifests.push(manifest);
                    }
                    else
                        this._manifests.push(manifest);
                }
                catch(e) {
                }
            }
        }
        
        this.queue.destroy();
        
        if (this._presets.length == 0 && this._manifests == 0)
            return;
        
        let context = this;
        let window = this._application.core.Lib.misc.openWindow({
                url: "chrome://" + this._application.name +
                     "/content/custombar/dialogs/package-management/notification-update/notification-update.xul",
                features: "__popup__",
                name: "notification-update",
                presets: this._presets,
                manifests: this._manifests,
                application: this._application,
                install: function(simple) { context._openDialog(simple); }
            });
    },
    
    _openDialog: function Updater__openDialog(simple) {
        let window = this._application.core.Lib.misc.openWindow({
                url: "chrome://" + this._application.name +
                     "/content/custombar/dialogs/package-management/update/update.xul",
                features: "__popup__",
                name: "package-management-update",
                mode: "update",
                presets: this._presets,
                manifests: this._manifests,
                application: this._application,
                simple: !!simple
            });
    }
};
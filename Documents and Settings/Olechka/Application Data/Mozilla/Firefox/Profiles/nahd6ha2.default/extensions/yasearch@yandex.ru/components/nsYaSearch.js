"use strict";

const {
  classes: Cc,
  interfaces: Ci,
  results: Cr,
  utils: Cu
} = Components;

const EXT_ID = "yasearch@yandex.ru";
const CHROME_IMAGES = "chrome://yasearch/skin/images/";
const CHROME_CONTENT = "chrome://yasearch/content/";

Cu.import("resource://gre/modules/XPCOMUtils.jsm");

this.__defineGetter__("AddonManager", function addonManagerGetter() {
  delete this.AddonManager;
  Cu.import("resource://yasearch-mod/AddonManager.jsm", this);
  return this.AddonManager;
});

var gSubscriptsForInit = [];

["consts", "utils", "cache_wrapper",
 "ya_auth", "ya_storage", "ya_installer", "ya_defence",
 "ya_bookmarks", "ya_overlay", "ya_searchplugin",
 "ya_uservices", "ya_partner", "ya_sshot", "ya_ftab",
 "ya_mfd", "ya_geolocation"]
.forEach(
  function(aScriptName) this.loadSubScript(CHROME_CONTENT + "sub-scripts/" + aScriptName + ".js"),
  Cc["@mozilla.org/moz/jssubscript-loader;1"].getService(Ci.mozIJSSubScriptLoader)
);

function nsIYaSearch() {
  this.version = "201012150600";
  this.versionBuild = "15617";
  this.VERSION_BUILD = "";

  this.wrappedJSObject = this;
  this.debug = false;

  this._inited = false;

  this.prefs = {
    _showBloggersValuePrefName: "yasearch.general.ui.show.bloggers.value",

    get showBloggersValue() {
      return gYaSearchService.getBoolPref(this._showBloggersValuePrefName);
    },

    set showBloggersValue(val) {
      gYaSearchService.setBoolPref(this._showBloggersValuePrefName, !!val);
      return this.showBloggersValue;
    },

    _showCyValuePrefName: "yasearch.general.ui.show.cy.value",

    get showCyValue() {
      return gYaSearchService.getBoolPref(this._showCyValuePrefName);
    },

    set showCyValue(val) {
      gYaSearchService.setBoolPref(this._showCyValuePrefName, !!val);
      return this.showCyValue;
    },

    _highlighterEnabledPrefName: "yasearch.general.ui.highlighter.enabled",

    get highlighterEnabled() {
      return gYaSearchService.getBoolPref(this._highlighterEnabledPrefName);
    },

    set highlighterEnabled(val) {
      gYaSearchService.setBoolPref(this._highlighterEnabledPrefName, !!val);
      return this.highlighterEnabled;
    },

    _showSiteSearchPrefName: "yasearch.general.ui.show.site.search",

    get showSiteSearch() {
      return gYaSearchService.getBoolPref(this._showSiteSearchPrefName);
    },

    set showSiteSearch(val) {
      gYaSearchService.setBoolPref(this._showSiteSearchPrefName, !!val);
      return this.showSiteSearch;
    },

    _searchHistoryEnabledPrefName: "yasearch.general.ui.search.history.enabled",

    get searchHistoryEnabled() {
      return gYaSearchService.getBoolPref(this._searchHistoryEnabledPrefName);
    },

    set searchHistoryEnabled(val) {
      gYaSearchService.setBoolPref(this._searchHistoryEnabledPrefName, !!val);
      return this.searchHistoryEnabled;
    },

    _commandOpenTabPrefName: "yasearch.general.ui.command.open.tab",

    get commandOpenTab() {
      return gYaSearchService.getBoolPref(this._commandOpenTabPrefName);
    }
  };

  this.updateTimer = {
    "_Guid": null,
    "_GuidRefresh": null,
    "_MailAndFeeds": null,
    "_Bookmarks": null
  };

  this.checkTimeOut = {
    "_Guid": DAY_SECS,
    "_GuidRefresh": 3600010,
    "_MailAndFeeds": 0,
    "_Bookmarks": DAY_SECS
  };

  this.feedsCounter = 0;

  this._runnedNotifications = [];

  this.usersData = { __proto__: null };

  this.maxUnreadInXML = 30;

  this.showLocalWelcomePage = false;
  this.checkKeywordURL = null;
}

nsIYaSearch.prototype = {
  log: function(msg) {
    if (!this.debug)
      return;

    let date = new Date();
    let time = [date.getHours(), date.getMinutes(), date.getSeconds()].join(":") + "." + date.getMilliseconds();

    msg = "[nsIYaSearch (" + time + ")]: " + msg + "\n";
    CONSOLE_SERVICE.logStringMessage(msg);
  },

  get tmpLogOutput() {
    if (!this.__tmpLogOutput) {
      let tmpFile = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIProperties).get("TmpD", Ci.nsIFile);

      tmpFile.append("!!_ya_searchlog_");

      let os = Cc["@mozilla.org/network/file-output-stream;1"].createInstance(Ci.nsIFileOutputStream);
      os.init(tmpFile, 0x02 | 0x08 | 0x20, PERMS_FILE, 0);

      this.__tmpLogOutput = os;
    }

    return this.__tmpLogOutput;
  },

  defaultSearchEngineId: "www",

  settingsFolderName: "yandex",
  xmlServicesFileName: "services.data.xml",
  xmlServicesFile: null,

  _xsltemplates: { __proto__: null },
  xsltImportRegExp: new RegExp(/<xsl:import href="chrome:\/\/yasearch\/content\/xsl\-templ\/xsl\-(.*)\.xsl"\/>/),

  domParser:        Cc["@mozilla.org/xmlextras/domparser;1"].getService(Ci.nsIDOMParser),
  domParserSys:     (function() {
    let domParser = Cc["@mozilla.org/xmlextras/domparser;1"].createInstance(Ci.nsIDOMParser);
    try {
      domParser.init();
    } catch (e) {
      domParser = Cc["@mozilla.org/xmlextras/domparser;1"].getService(Ci.nsIDOMParser);
    }
    return domParser;
  })(),

  xmlSerializer:    Cc["@mozilla.org/xmlextras/xmlserializer;1"].getService(Ci.nsIDOMSerializer),
  xPathEvaluator:   Cc["@mozilla.org/dom/xpath-evaluator;1"].getService(Ci.nsIDOMXPathEvaluator),
  unSnapshotType:   Ci.nsIDOMXPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
  orSnapshotType:   Ci.nsIDOMXPathResult.ORDERED_NODE_SNAPSHOT_TYPE,

  soundService:     Cc["@mozilla.org/sound;1"].createInstance(Ci.nsISound),

  windowMediator:   Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator),
  windowWatcher:    Cc["@mozilla.org/embedcomp/window-watcher;1"].getService(Ci.nsIWindowWatcher),
  promptService:    Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService),

  get customBarApp() {
    return this._customBarApp ||
           (this._customBarApp = Cc["@yandex.ru/custombarcore;yasearch"].getService().wrappedJSObject.application);
  },

  isFirstDOMWinStuffDone: false,

  onBrowserUIStartupComplete: function() {
    if (!this.initialized || this.isFirstDOMWinStuffDone)
      return;

    this.stringBundle = null;

    this.isFirstDOMWinStuffDone = true;

    if (this.checkKeywordURL) {
      if (this.checkKeywordURL === "set") {
        this._setKeywordUrl(true);
      } else if (this.checkKeywordURL === "check") {
        this._checkKeywordUrl();
      }

      this.checkKeywordURL = null;
    }

    if (typeof(gYaSearchPlugin) === "object" && "checkSearchPluginInstall" in gYaSearchPlugin)
      gYaSearchPlugin.checkSearchPluginInstall();
  },

  onSessionstoreWindowsRestored: function() {
    if (!this.initialized)
      return;

    if (this.showLocalWelcomePage) {
      this.showLocalWelcomePage = false;
      new G_Timer(function YS_onSessionstoreWindowsRestored_timed(){
        gYaSearchService.loadURI("chrome://yasearch/locale/first-start/welcome.html", "tab");
      }, 500);
    }

    if (typeof(gYaMFD) === "object" && "init" in gYaMFD)
      gYaMFD.init();

    if (this.isLogin && this.isCountersAutoUpdateEnabled)
      this.refreshHTTPData("mailAndFeeds");
  },

  get initialized() {
    return this._inited;
  },

  init: function() {
    this.debug = this.getBoolPref("yasearch.general.debug.enabled");

    if (!this.getBoolPref("yasearch.license.accepted")) {
      if (!this.getBoolPref("yasearch.license.show")) {
        this.setBoolPref("yasearch.license.accepted", true);
      } else {
        try {
          let setupWin = this.windowWatcher.openWindow(null, "chrome://yasearch/content/first-start/wizard.xul",
                                                       null, "centerscreen,modal,resizable", null);

          if (!this.getBoolPref("yasearch.license.accepted")) {
            let refuseWin = this.windowWatcher.openWindow(null, "chrome://yasearch/content/first-start/license-refuse.xul",
                                                          null, "centerscreen,modal", null);
            AddonManager.disableAddonByID(EXT_ID);
            return;
          }

        } catch(e) {
          this.log(e);
          return;
        }
      }

      this.resetPref("yasearch.license.hidden");
      try {
        let prefService = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefService);
        prefService.savePrefFile(null);
      } catch(e) {
        this.log(e);
      }
    }

    ["cookie-changed",
     "http-on-modify-request",
     "http-on-examine-response"]
    .forEach(function YS_init_addObservers(aTopicName) {
      OBSERVER_SERVICE.addObserver(this, aTopicName, true);
    }, this);
    gSubscriptsForInit.forEach(function(aObject) {
      if (typeof aObject === "object" && "startup" in aObject)
        aObject.startup();
    });

    gSubscriptsForInit = null;

    this.checkTimeOut._MailAndFeeds = this.isCountersAutoUpdateEnabled ?
                                          (this.getIntPref("yasearch.http.update.interval") * MIN_SEC || 0) : 0;

    this.appendUserSessionData("data");

    this.yaAuth.Cookies.checkAuthOnStart();

    this._hackKnownSkins();

    this._inited = true;

    OBSERVER_SERVICE.notifyObservers(null, "yasearch-state-changed", "initialized");
  },

  get buttonsInfo() {
    let enumerator = this.windowMediator.getEnumerator("navigator:browser");
    if (enumerator.hasMoreElements()) {
      try {
        return enumerator.getNext().Ya.buttonsObject;
      } catch(e) {}
    }
    return null;
  },
  get username() {
    return this.yaAuth.username;
  },
  set username(val) {
    return this.yaAuth.username = val;
  },
  get isLogin() {
    return this.yaAuth.isLogin;
  },
  set isLogin(val) {
    this.yaAuth.isLogin = val;
  },

  onYandexLogin: function() {
    this.clearAllTimers();

    if (this.isCountersAutoUpdateEnabled)
      this.refreshHTTPData("mailAndFeeds", {forceRequest: true});

    if (this.bookmarksIsOutOfDate)
      this.refreshHTTPData("bookmarks");
  },

  onYandexLogout: function(aClearCookies, aForgetLogin) {
    this.clearAllTimers();
  },

  clearAllTimers: function() {
    this.clearTimer("_MailAndFeeds");
    this.clearTimer("_Bookmarks");

    this.setNotifyTimer("cancel");
  },

  clearTimer: function(type) {
    if (type && this.updateTimer[type]) {
      this.updateTimer[type].cancel();
      this.updateTimer[type] = null;
    }
  },

  setTimer: function(aType, aTimeout) {
    if (!aType)
      throw "nsIYaSearch::setTimer -- no type";

    aTimeout = aTimeout || this.checkTimeOut[aType] || 0;

    if (this.updateTimer[aType])
      this.updateTimer[aType].cancel();
    else
      this.updateTimer[aType] = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);

    if (aTimeout > 0)
      this.updateTimer[aType].initWithCallback(this._TimerCallbacks[aType], aTimeout, Ci.nsITimer.TYPE_ONE_SHOT);
  },

  _TimerCallbacks: {
    _Login: {
      notify: function YS__TimerCallbacks_Login_notify() gYaSearchService.startLoginConnection()
    },

    _MailAndFeeds: {
      notify: function YS__TimerCallbacks_MailAndFeeds_notify() gYaSearchService.refreshHTTPData("mailAndFeeds")
    },

    _Bookmarks: {
      notify: function YS__TimerCallbacks_Bookmarks_notify() gYaSearchService.refreshHTTPData("bookmarks")
    },

    _Guid: {
      notify: function YS__TimerCallbacks_Guid_notify() gYaSearchService.refreshHTTPData("guid")
    },

    _GuidRefresh: {
      notify: function YS__TimerCallbacks_GuidRefresh_notify() gYaSearchService.clearTimer("_GuidRefresh")
    }
  },

  HTTPDataRequests: {
    _data: { __proto__: null },

    add: function YS_HDReq_add(aURL, aType, aRequest) {
      this._data[aURL] = {
        requestSimpleURL: aURL,

        request: aRequest,
        requestTime: Date.now(),

        isRequestPending: function() {
          return !!(this.request && this.request.channel.isPending());
        },

        cancelRequest: function() {
          this.request.channel.cancel(Cr.NS_BINDING_ABORTED);
        },

        responseTime: 0,
        responseIsOK: null,

        types: { __proto__: null }
      }

      this._data[aURL].types[aType] = true;
    },

    removeRequest: function YS_HDReq_removeRequest(aRequest) {
      for each (let data in this._data) {
        if (data.request === aRequest) {
          data.request = null;
          return data;
        }
      }
    },

    canResendRequest: function YS_HDReq_canResendRequest(aURL, aType, aData) {
      if (aType == "allServices" || (aData && aData.forceRequest))
        return true;

      let data = this._data[aURL];

      if (data) {
        let requestTimeDelta = Math.abs(Date.now() - data.requestTime);

        if (data.isRequestPending()) {
          if (requestTimeDelta > 30000) {
            data.cancelRequest();
            return true;
          }

          data.types[aType] = true;
          return false;
        }

        let responseTimeDelta = Math.abs(Date.now() - data.responseTime);

        if (data.types[aType] && (requestTimeDelta < 1000 || (data.responseIsOK && responseTimeDelta < 5000)))
          return false;

        data.responseIsOK = null;
        data.responseTime = 0;
      }

      return true;
    },

    processDataFromCache: function YS_HDReq_processDataFromCache(aURL, aType, aData) {
      if (aData && aData.forceRequest) {
        G_CacheWrapper.writeData(aURL, null);
        return false;
      }

      let lastDataFromCache = G_CacheWrapper.readData(aURL);

      if (lastDataFromCache) {
        gYaSearchService.processHTTPDataResponse(lastDataFromCache, aType, aData);
        return true;
      }

      return false;
    }
  },

  manualRefreshHTTPData: function(aType, aData, aCallerElementId) {
    if (!aData)
      aData = {};

    aData.manual = true;
    aData.callerElementId = aCallerElementId || ("yasearch-" + aType);

    this.refreshHTTPData(aType, aData);
  },

  __refreshHTTPDataTimers: {
    __proto__: null
  },

  refreshHTTPData: function(aType, aData, aDelay) {
    if (!aData)
      aData = {};

    let currentTimerForType = this.__refreshHTTPDataTimers[aType];
    if (currentTimerForType && currentTimerForType.isRunning)
      currentTimerForType.cancel();

    this.__refreshHTTPDataTimers[aType] = new G_Timer(
      function YS_refreshHTTPData_timed() {
        gYaSearchService.__refreshHTTPData(aType, aData);
      }, aDelay || 10
    )
  },

  __refreshHTTPData: function(aType, aData) {
    if (!aType || typeof(aType) != "string")
      throw "No type given in refreshHTTPData";

    var url,
        appendTimestamp = false;

    aData.isCounters = false;

    switch (aType) {
      case "allServices":
        url = this.Counters.getURLForSID("allServices");

        if (!url)
          return;

        aData.isCounters = true;

        appendTimestamp = true;

        break;

      case "mailAndFeeds":
        this.setTimer("_MailAndFeeds");

        url = this.Counters.getURLForSID("allCounters");

        if (!url)
          return;

        aData.isCounters = true;

        break;

      case "mail":
        if (aData.manual && this.yaMFD)
          this.yaMFD.updateDataForAllAccounts();

      case "lenta":
      case "money":
      case "fotki":
      case "yaru":
      case "moikrug":
        url = this.Counters.getURLForSID([aType]);

        if (!url)
          return;

        aData.isCounters = true;

        break;

      case "mailList":
        url = "http://mail.yandex.ru/api/barlist";
        break;

      case "mfd-counter":
      case "mfd-list":
        url = aData.url;
        appendTimestamp = true;
        break;

      case "bookmarks":
        url = "http://zakladki.yandex.ru/bar/index.xml?newdescr=true";
        appendTimestamp = true;

        break;

      case "guid":
        url = this.generateGUIDStatusURL + this.generateGUIDData;
        break;

      default:
        break;
    }

    if (!url)
      throw new Error("Bad type given in refreshHTTPData");

    this.notifyBusyStateOfRequest(aType, true, !!(aData && aData.manual), aData.callerElementId);

    if (this.HTTPDataRequests.canResendRequest(url, aType, aData)) {
      var fullUrl = url;

      if (appendTimestamp)
        fullUrl += (/\?/.test(fullUrl) ? "&":"?") + "ts=" + Date.now();

      if (!!(aData && aData.manual))
        fullUrl = this.appendStatData2Url(fullUrl, {});

      this.HTTPDataRequests.add(url, aType, this.xmlHttpRequest(fullUrl,
                                                 {callbackFunc: this.refreshHTTPDataCallback.yaBind(this, aData)} ));
    } else {
      this.notifyBusyStateOfRequest(aType, false, !!(aData && aData.manual), aData.callerElementId);
    }
  },

  refreshHTTPDataCallback: function(aReq, aData) {
    var data = this.HTTPDataRequests.removeRequest(aReq.target);

    if (!data)
      return;

    data.responseTime = Date.now();

    var aType = data.types;

    if (!aType)
      throw new Error("No type given in refreshHTTPDataCallback");

    var isCounters = aData.isCounters;

    if (this.isReqError(aReq)) {
      if ("guid" in aType) {
        this.clearTimer("_GuidRefresh");
      }

      data.responseIsOK = false;

    } else {
      G_DateUtils.updateServerTimeValue(aReq);

      var text = this.safeUnicode(aReq.target.responseText);

      if (!("guid" in aType))
        this.checkNeedSendGuid();

      var noAuthError = false;

      if (isCounters) {
        let countersError = this.Counters.getErrorFromInboxString(text);
        if (countersError) {
          if (countersError.type == "noauth") {
            noAuthError = true;
          } else {
            return;
          }
        }
      }

      if (noAuthError) {
        data.responseIsOK = false;
        this.yaAuth.fireAfterLogout();

      } else {
        data.responseIsOK = true;

        if (isCounters)
          this.Counters.setDataFromInboxString(aType, text);

        for (let typeStr in aType)
          this.processHTTPDataResponse(text, typeStr, aData);
      }
    }

    if (data.responseIsOK == false && ("allServices" in aType))
      this.Counters.setAllServicesError(true);

    if (aData && aData.manual) {
      for (let typeStr in aType)
        this.notifyBusyStateOfRequest(typeStr, false, true, aData.callerElementId);
    }
  },

  notifyBusyStateOfRequest: function(aType, aState, aIsManualRequest, aCallerElementId) {
    if (aIsManualRequest)
      OBSERVER_SERVICE.notifyObservers(null, "yasearch-refresh-busy-state", aState + ":" + aCallerElementId);
  },

  processHTTPDataResponse: function(aText, aTypeStr, aData) {
    var isManualRequest = !!(aData && aData.manual);

    switch (aTypeStr) {
      case "guid":
        this.timeGuid = true;
        OBSERVER_SERVICE.notifyObservers(null, "yasearch-guid-response", "SENDED");
        return;

      case "bookmarks":
        var bookmarksResponse = this.yaBookmarks.getServerResponse(aText);
        if (!bookmarksResponse.error)
          this.bookmarksDOMMenu = bookmarksResponse.xml;
        break;

      case "allServices":
        this.Counters.setDataFromInboxString({"allServices": true}, aText);

      case "mailAndFeeds":
        if (this.Counters.error) {
          if (aTypeStr == "mailAndFeeds")
            this.setTimer("_MailAndFeeds", this.Counters.errorTimeout);
          this.Counters.handleError();
        }

      case "yaru":
        if (aTypeStr == "yaru") break;

      case "fotki":
        if (aTypeStr == "fotki") break;

      case "money":
        if (aTypeStr == "money") break;

      case "lenta":
        if (aTypeStr == "lenta") break;

      case "moikrug":
        if (aTypeStr == "moikrug") break;

      case "mail":
        var mCount = this.Counters.getCount("mail");

        if (mCount && (mCount != this.mailCounter || (mCount > 0 && this.mailLastCheckIsOutOfDate))) {
          this.mailCounter = mCount;
          this.refreshHTTPData("mailList", aData);
        } else if (!mCount && mCount != this.mailCounter) {
          this.mailCounter = mCount;
          this.mailDOMMenuDoc = false;
          OBSERVER_SERVICE.notifyObservers(null, "yasearch-refresh-data", "mailList");
        }
        break;

      case "mailList":
        this.mailLastCheckIsOutOfDate = false;

        var msgURLPrefix = "http://" + this.getLocaleDependedUrl("MailHost") + "/message?ids=";
        var listData = this.parseMailListXMLString(aText, this.mailLastMaxId, msgURLPrefix);

        if (!listData)
          return;

        if (listData == "No Auth") {
          this.yaAuth.fireAfterLogout();
          return;
        }

        this.mailDOMMenuDoc = listData.list;

        if (listData.newLastMaxId)
          this.mailLastMaxId = listData.newLastMaxId;

        if (listData.newMailCounter > 0) {
          if (listData.newMailCounter == gYaSearchService.maxUnreadInXML) {
            let tmp = this.mailCounter - this.mailPermCounter;
            listData.newMailCounter = tmp >= gYaSearchService.maxUnreadInXML ? tmp : 0;
          }

          this.notifyAboutNewItems(aTypeStr, listData.newMailCounter,
                                   { from: listData.newLastItem.@from.toString(),
                                     title: listData.newLastItem.@title.toString() });
        }

        this.mailPermCounter = this.mailCounter;

        break;

      case "mfd-counter":
        this.yaMFD.onResponse("mfd-counter", aText, aData);
        break;

      case "mfd-list":
        this.yaMFD.onResponse("mfd-list", aText, aData);
        break;
    }

    OBSERVER_SERVICE.notifyObservers(null, "yasearch-refresh-data", aTypeStr);
  },

  parseMailListXMLString: function(aMailListXMLString, aLastMaxId, aMsgURLPrefix) {
    let barlist = this.safeE4Xml(aMailListXMLString, null, ["yandexmenu", "auther"]);
    if (!barlist)
      return false;

    if (barlist..error.(@reason == "not authenticated").length() > 0)
      return "No Auth";

    let list = <list/>;

    let newMailCounter = 0,
        newLastItem,
        newLastMaxId;

    var increaseNewMailCounter = true;

    let i = 0;

    for each (let folder in barlist.item) {
      for each (let message in folder.item) {
        let id = /\?mesid=(\d+)/.exec(message.@url.toString())[1];
        message.@url = aMsgURLPrefix + id;

        if (increaseNewMailCounter) {
          if (i == 0)
            newLastMaxId = "" + id;

          if (id > aLastMaxId) {
            newMailCounter++;
            newLastItem = newLastItem || message;
          } else {
            increaseNewMailCounter = false;
          }
        }

        i++;
      }

      list.appendChild(folder);
    }

    return {
      list: list,
      newLastItem: newLastItem,
      newMailCounter: newMailCounter,
      newLastMaxId: newLastMaxId
    };
  },

  xmlHttpRequest: function(aUrl, aDetails) {
    let req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Ci.nsIXMLHttpRequest);

    if (aDetails.background)
      req.mozBackgroundRequest = true;

    req.open(aDetails.data ? "POST" : "GET", aUrl, true);
    req.setRequestHeader("Cache-Control", "no-cache");

    if (aDetails.data) {
      req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      req.setRequestHeader("Connection", "close");
    }

    let target = req.QueryInterface(Ci.nsIDOMEventTarget);
    if (aDetails.callbackFunc) {
      target.addEventListener("load", aDetails.callbackFunc, false);
      target.addEventListener("error", aDetails.callbackFunc, false);
    }

    req.send(aDetails.data || null);
    return req;
  },

  isReqError: function(aReq) {
    return !!(!aReq || aReq.type == "error" || !aReq.target || aReq.target.status != 200);
  },

  get Counters() {
    return gYaUServices;
  },

  get yaPartner() {
    return gYaPartner;
  },

  get yaAuth() {
    return gYaAuth;
  },

  get yaMFD() {
    return gYaMFD;
  },

  get yaBookmarks() {
    return gYaBookmarks;
  },

  get yaOverlay() {
    return gYaOverlay;
  },

  get yaDefence() {
    return gYaDefence;
  },

  get yaFTab() {
    return gYaFTab;
  },

  get yaURLInfo() {
    return gYaURLInfo;
  },

  get yaGeolocation() {
    return gYaGeolocation;
  },

  getFeedsGroups: function(aCallback) {
    this.xmlHttpRequest("http://mail.yandex.ru/lenta/bar/count",
                         { callbackFunc: this.getFeedsGroupsCallback.yaBind(this, aCallback) });
  },

  getFeedsGroupsCallback: function(aReq, aCallback) {
    var mlist = false,
        doc = false;

    if (!this.isReqError(aReq))
      doc = aReq.target.responseXML;

    if (doc) {
      var _doc = this.domParserSys.parseFromString("<empty/>", "text/xml");

      function createElement(aElementName) {
        return _doc.createElementNS(XULNS, aElementName);
      }

      var groups = doc.getElementsByTagName("group");
      var mpopup = createElement("menupopup");

      var last_group_id = this.feedsLastGroupId;
      var selected_item = 0;

      for (var i = 0; i < groups.length; i++) {
        var mi = createElement("menuitem");
        mi.setAttribute("label", groups.item(i).getAttribute("title"));

        var group_id = groups.item(i).getAttribute("id");
        if (group_id == last_group_id)
          selected_item = i;

        mi.setAttribute("group-id", group_id);
        mpopup.appendChild(mi);
      }

      mpopup.selectedIndex = selected_item;

      mlist = createElement("menulist");

      mlist.appendChild(mpopup);
      mlist.setAttribute("id", "yasearch-feeds-select-group");

      if (groups.length == 0) {
        mlist.setAttribute("disabled", "true");
        mpopup.appendChild(createElement("menuitem"));
      }
    }

    if (aCallback)
      aCallback(mlist);
  },

  get feedsCounter() {
    return this.isLogin ? (this.usersData[this.username]._feedsCounter || 0) : 0;
  },

  set feedsCounter(val) {
    if (this.isLogin)
      this.usersData[this.username]._feedsCounter = Math.max(val, 0);
  },

  get feedsLastGroupId() {
    return this.isLogin ? (this.usersData[this.username]._feedsLastGroupId || 0) : 0;
  },

  set feedsLastGroupId(val) {
    if (this.isLogin)
      this.usersData[this.username]._feedsLastGroupId = val;
  },

  feedsInsertNewItem: function(aData) {
    if (aData.group_id) {
      this.feedsLastGroupId = aData.group_id;
      this.xmlHttpRequest("http://mail.yandex.ru/lenta/bar/feed_add",
                          { callbackFunc: this.feedsInsertNewItemCallback.yaBind(this, aData),
                            data: "url=" + encodeURIComponent(aData.url) +
                                  "&group_id=" + aData.group_id +
                                  "&yasoft=" + encodeURIComponent(this.AppInfo.yasoftStr)
                          });
    } else if (aData.title && aData.title != "") {
      this.xmlHttpRequest("http://mail.yandex.ru/lenta/bar/group_add",
                          { callbackFunc: this.feedsInsertNewGroupCallback.yaBind(this, aData),
                            data: "title=" + encodeURIComponent(aData.title) +
                                  "&yasoft=" + encodeURIComponent(this.AppInfo.yasoftStr)
                          });
    }
  },

  feedsInsertNewGroupCallback: function(aReq, aData) {
    let error;

    if (this.isReqError(aReq)) {
      error = "errorNewGroup1";
    } else {
      let group_id;

      let pageXml = gYaSearchService.safeE4Xml(aReq.target.responseText, null, "page");
      if (pageXml)
        group_id = pageXml.status.added.@id.toString();

      if (group_id)
        aData.group_id = group_id;
      else
        error = "errorNewGroup2";
    }

    return error ? aData.callback(error) : this.feedsInsertNewItem(aData);
  },

  feedsInsertNewItemCallback: function(aReq, aData) {
    this.feedsLastGroupId = aData.group_id;

    let error;

    if (this.isReqError(aReq)) {
      error = "errorNewItem1";
    } else {
      let pageXml = gYaSearchService.safeE4Xml(aReq.target.responseText, null, "page");
      if (!(pageXml && pageXml.ok.length()))
        error = "errorNewItem2";
    }

    return aData.callback(error);
  },

  get moneyCounter() {
    return this.isLogin ? (this.usersData[this.username]._moneyCounter || 0) : 0;
  },

  set moneyCounter(val) {
    if (this.isLogin)
      this.usersData[this.username]._moneyCounter = Math.max(val, 0);
  },

  get mailCounter() {
    return this.isLogin ? (this.usersData[this.username]._mailCounter || 0) : 0;
  },

  set mailCounter(val) {
    if (this.isLogin)
      this.usersData[this.username]._mailCounter = Math.max(val, 0);
  },

  get newMailCounter() {
    return this.isLogin ? (this.usersData[this.username]._newMailCounter || 0) : 0;
  },

  set newMailCounter(val) {
    if (this.isLogin)
      this.usersData[this.username]._newMailCounter = Math.max(val, 0);
  },

  get mailPermCounter() {
    return this.isLogin ? (this.usersData[this.username]._mailPermCounter || 0) : 0;
  },

  set mailPermCounter(val) {
    if (this.isLogin)
      this.usersData[this.username]._mailPermCounter = Math.max(val, 0);
  },

  get mailDOMMenuDoc() {
    if (!this.isLogin)
      return false;

    let doc = this.usersData[this.username]._mailDOMMenuDoc;
    if (!doc) {
      doc = <list boxname={this.username + "@yandex.ru"} type="yandex"/>;
      this.usersData[this.username]._mailDOMMenuDoc = doc;
    }

    return this.usersData[this.username]._mailDOMMenuDoc;
  },

  set mailDOMMenuDoc(aXMLDoc) {
    if (this.isLogin) {
      let doc = this.usersData[this.username]._mailDOMMenuDoc = (aXMLDoc || <list/>);
      doc.@boxname = this.username + "@yandex.ru";
      doc.@type = "yandex";
    }
  },

  get mailData() {
    let xmlDataForAllAccounts = <lists/>;

    let yandexList = this.mailDOMMenuDoc;
    if (yandexList) {
      yandexList.@counter = this.mailCounter;
      xmlDataForAllAccounts.appendChild(yandexList);
    }

    let mfdData = this.yaMFD.getDataForAllUsers();
    for each (let list in mfdData.lists)
      xmlDataForAllAccounts.appendChild(list);

    let counterValue = this.mailCounter + mfdData.counter;
    let mfdListLength = mfdData.lists.list.length();

    let mfdWarning = !!(counterValue && this.isLogin && !this.mailCounter);
    let type = mfdWarning ? "mfd" : "yandex";

    return {
      "count": counterValue,
      "permCount": this.mailPermCounter,
      "nodes": this.getDOMDocContent2("xsl-templ/xsl-mail-items.xsl", xmlDataForAllAccounts),
      "lastmaxid": this.mailLastMaxId,
      "type": mfdWarning ? "mfd" : "yandex"
    };
  },

  get mailLastMaxId() {
    if (!this.isLogin || !this.usersData[this.username]._mailLastMaxId)
      return "0";

    return this.usersData[this.username]._mailLastMaxId;
  },

  set mailLastMaxId(id) {
    if (this.isLogin)
      this.usersData[this.username]._mailLastMaxId = id.toString();
  },

  get mailLastCheckIsOutOfDate() {
    if (!this.isLogin)
      return false;

    let lastCheckTime = this.usersData[this.username]._mailLastCheckTime || 0;

    return Math.abs(lastCheckTime - Date.now()) > (15 * MIN_SEC);
  },

  set mailLastCheckIsOutOfDate(val) {
    if (this.isLogin)
      this.usersData[this.username]._mailLastCheckTime = val ? 0 : Date.now();
  },

  bookmarksPrepeareDialog: function(aCallback) {
    this.xmlHttpRequest("http://zakladki.yandex.ru/bar/struct.xml?newdescr=true&" + getNCRndStr(),
                             { callbackFunc: this.bookmarksPrepeareDialogCallback.yaBind(this, aCallback) });
  },

  bookmarksPrepeareDialogCallback: function(aReq, aCallback) {
    var res;

    if (this.isReqError(aReq)) {
      res = "lost_connection";
    } else if (!this.isLogin) {
      res = "auth_required";
    } else {
      var bookmarksResponse = this.yaBookmarks.getServerResponse(aReq.target.responseText);
      if (bookmarksResponse.error && bookmarksResponse.error !== "reg_required") {
        res = bookmarksResponse.error;
      } else {
        var data = bookmarksResponse.error ? "<err/>" : bookmarksResponse.xml.toString();
        if (res = this.getDOMDocContent("bar-bookmarks-folders", this.domParser.parseFromString(data, "text/xml")))
          res.setAttribute("disabled", "true");
        else
          res = "service";
      }
    }

    if (aCallback)
      aCallback(res);
  },

  bookmarksGetItemById: function(aId) {
    let item = this.bookmarksRawXml.bookmarks..*.(function::attribute("id") == aId)[0];

    return {
      id:       item.@id.toString(),
      type:     item.localName().toString(),
      url:      item.@url.toString(),
      name:     item.@name.toString(),
      descr:    item.descr.toString(),
      tags:     item.@tags.toString(),
      parentId: item.@folder_id.toString() || item.@parent_id.toString()
    };
  },

  bookmarksEditItem: function(aItem) {
    if (!aItem.id)
      throw "bookmarksEditItem: no ID";

    if (aItem.nfolder != "") {
      var url = "http://zakladki.yandex.ru/bar/addfolder.xml?" + getNCRndStr();
      url = this.appendStatData2Url(url,{});

      var data = "name=" + encodeURIComponent(aItem.nfolder) + "&parent_id=" + aItem.folder;

      this.xmlHttpRequest(url, {data: data, callbackFunc: this.bookmarksInsertNewFolderInXML.yaBind(this, aItem)});

    } else {

      var old = aItem._old_info;

      aItem.tags = this.yaBookmarks.formatTagsString(aItem.tags);

      if (old.name != aItem.name || old.descr != aItem.descr || old.url != aItem.url || old.tags != aItem.tags) {

        var type = aItem.type;

        var url = "http://zakladki.yandex.ru/bar/update" + type + ".xml?" + getNCRndStr();
        url = this.appendStatData2Url(url,{});

        var data = type + "_id=" + aItem.id +
                    "&name="  + encodeURIComponent(aItem.name) +
                    "&descr=" + encodeURIComponent(aItem.descr) +
                    "&tags="  + encodeURIComponent(aItem.tags);

        if (type == "link")
          data += "&url=" + encodeURIComponent(aItem.url);

        this.xmlHttpRequest(url, { data: data, callbackFunc: this.bookmarksEditItemInXML.yaBind(this, aItem) });

      } else if (aItem.folderOld != aItem.folder) {
        this.bookmarksMoveItem(aItem);

      } else {
        return aItem.callback();
      }
    }
  },

  bookmarksEditItemInXML: function(aReq, aItem) {
    var res;

    if (this.isReqError(aReq)) {
      res = "lost_connection";
    } else if (!this.isLogin) {
      res = "auth_required";
    } else {
      var bookmarksResponse = this.yaBookmarks.getServerResponse(aReq.target.responseText);
      if (bookmarksResponse.error) {
        res = bookmarksResponse.error;
      } else {
        if (!bookmarksResponse.xml.ok.length()) {
          res = "service";
        } else {
          var bookmarks = this.usersData[this.username]._bookmarksRawXml.bookmarks;
          var elem = (aItem.type == "folder" ? bookmarks..folder : bookmarks..link).(@id == aItem.id)[0];

          elem.@name  = aItem.name;
          elem.@url   = aItem.url;
          elem.descr  = aItem.descr;
          elem.@tags  = this.yaBookmarks.formatTagsString(aItem.tags);

          this.yaBookmarks.refreshBookmarksDOMMenu();

          if (aItem.folderOld != aItem.folder) {
            this.bookmarksMoveItem(aItem);
            return;
          }
        }
      }
    }

    return aItem.callback(res);
  },

  bookmarksMoveItem: function(aItem) {
    if (!aItem.id)
      throw "bookmarksMoveItem: no ID";

    var url = "http://zakladki.yandex.ru/bar/move.xml?" + getNCRndStr();
    url = this.appendStatData2Url(url,{});

    var data =  "folder_id=" + aItem.folder + "&" + (aItem.type == "folder" ? "fid" : "link_id") + "=" + aItem.id;

    this.xmlHttpRequest(url, { data: data, callbackFunc: this.bookmarksMoveItemInXML.yaBind(this, aItem) });
  },

  bookmarksMoveItemInXML: function(aReq, aItem) {
    var res;

    if (this.isReqError(aReq)) {
      res = "lost_connection";
    } else if (!this.isLogin) {
      res = "auth_required";
    } else {
      var bookmarksResponse = this.yaBookmarks.getServerResponse(aReq.target.responseText);
      if (bookmarksResponse.error) {
        res = bookmarksResponse.error;
      } else {
        if (!bookmarksResponse.xml.ok.length()) {
          res = "service";
        } else {
          var bookmarks = this.usersData[this.username]._bookmarksRawXml.bookmarks;
          var elem = (aItem.type == "folder" ? bookmarks..folder : bookmarks..link).(@id == aItem.id);

          if (elem[0]) {
            var clone = new XML(elem[0]);
            var inFolder = bookmarks..folder.(@id == aItem.folder), inFolderId = inFolder.@id.toString();
            if (inFolder.@id.toString() == "") {
              inFolder = bookmarks;
              inFolderId = "0";
            }

            var type = aItem.type;

            switch (type) {
              case "folder":
                clone.@parent_id = inFolderId;

                var len = inFolder.folder.(@name < aItem.name).length()-1;
                if (len == -1)
                  inFolder.folder = clone + inFolder.folder;
                else
                 inFolder.folder[len] += clone;
                break;

              case "link":
                clone.@folder_id = inFolderId;

                var len = inFolder.links.link.(@name < aItem.name).length()-1;
                if (len == -1)
                  inFolder.links.link = clone + inFolder.links.link;
                else
                  inFolder.links.link[len] += clone;
                break;
            }

            delete elem[0];
          }
        }
      }
    }

    this.yaBookmarks.refreshBookmarksDOMMenu();

    return aItem.callback(res);
  },

  bookmarksDeleteItem: function(aItem) {
    if (!aItem.id)
      throw "bookmarksDeleteItem: no ID";

    var url = "http://zakladki.yandex.ru/bar/del.xml?" + getNCRndStr();
    url = this.appendStatData2Url(url, {});

    var data = (aItem.type == "folder" ? "fid" : "link_id") + "=" + encodeURIComponent(aItem.id);

    this.xmlHttpRequest(url, {data: data, callbackFunc: this.bookmarksDeleteItemInXML.yaBind(this, aItem)});
  },

  bookmarksDeleteItemInXML: function(aReq, aItem) {
    var res;

    if (this.isReqError(aReq)) {
      res = "lost_connection";
    } else if (!this.isLogin) {
      res = "auth_required";
    } else {
      var bookmarksResponse = this.yaBookmarks.getServerResponse(aReq.target.responseText);
      if (bookmarksResponse.error) {
        res = bookmarksResponse.error;
      } else {
        if (!bookmarksResponse.xml.ok.length()) {
          res = "service";
        } else {
          var bookmarks = this.usersData[this.username]._bookmarksRawXml.bookmarks;
          var elem = (aItem.type == "folder" ? bookmarks..folder : bookmarks..link).(@id == aItem.id);

          if (elem[0]) {
            delete elem[0];

            this.yaBookmarks.refreshBookmarksDOMMenu();
          }
        }
      }
    }

    return aItem.callback(res);
  },

  bookmarksSubscribeCallback: function(aReq, aItem) {
    this.bookmarksInsertNewItem(aItem);
  },

  bookmarksInsertNewItem: function(aItem) {
    if (this.yaBookmarks.isRegRequired) {
      this.yaBookmarks.isRegRequired = null;
      if (!this.yaBookmarks.isRegRequired) {
        this.xmlHttpRequest("http://passport.yandex.ru/passport?mode=subscribe&from=zakladki",
                             { callbackFunc: this.bookmarksSubscribeCallback.yaBind(this, aItem) });
        return;
      }
    }

    if (aItem.nfolder) {
      var url = "http://zakladki.yandex.ru/bar/addfolder.xml?" + getNCRndStr();
      url = this.appendStatData2Url(url, {});

      var data = "name=" + encodeURIComponent(aItem.nfolder) + "&parent_id=" + aItem.folder;

      this.xmlHttpRequest(url, {data: data, callbackFunc: this.bookmarksInsertNewFolderInXML.yaBind(this, aItem)});

    } else {
      var url = "http://zakladki.yandex.ru/bar/addlink.xml?" + getNCRndStr();
      url = this.appendStatData2Url(url, {});

      var yaruDataAppend = (aItem.yaru && ("feed_id" in aItem.yaru) && ("status" in aItem.yaru)) ?
                               ("&feed_id=" + encodeURIComponent(aItem.yaru.feed_id) +
                                "&status=" + encodeURIComponent(aItem.yaru.status)) : "";

      var data = "name="        + encodeURIComponent(aItem.name)  +
                  "&url="        + encodeURIComponent(aItem.url)   +
                  "&descr="      + encodeURIComponent(aItem.descr) +
                  "&tags="       + encodeURIComponent(aItem.tags)  +
                  "&folder_id="  + (aItem.folder || 0) +
                  "&newfolder="  +
                  yaruDataAppend +
                  "&from=barff";

      this.xmlHttpRequest(url, {data: data, callbackFunc: this.bookmarksInsertNewLinkInXML.yaBind(this, aItem)});
    }
  },

  bookmarksInsertNewFolderInXML: function(aReq, aItem) {
    var res;

    if (this.isReqError(aReq)) {
      res = "lost_connection";
    } else if (!this.isLogin) {
      res = "auth_required";
    } else {
      var bookmarksResponse = this.yaBookmarks.getServerResponse(aReq.target.responseText);
      if (bookmarksResponse.error) {
        res = bookmarksResponse.error;
      } else {
        var newFolderId = bookmarksResponse.xml.ok[0].@id.toString();
        if (!newFolderId) {
          res = "service";
        } else {
          var bookmarks = this.usersData[this.username]._bookmarksRawXml.bookmarks;
          var elem = bookmarks..folder.(@id == aItem.folder);
          var parent = elem.@id.toString() == "" ? bookmarks : elem;

          var parentId = parent.@id.toString() == "" ? "0" : parent.@id.toString();

          if (bookmarks..folder.(@id == newFolderId).length() == 0) {
            var newItem = new XML('<folder id="' + newFolderId + '" parent_id="' + parentId + '"><links/></folder>');
            newItem.@name = aItem.nfolder;

            var len = parent.folder.(@name < aItem.nfolder).length()-1;
            if (len == -1)
              parent.folder = newItem + parent.folder;
            else
              parent.folder[len] += newItem;

            this.yaBookmarks.refreshBookmarksDOMMenu();
          }

          aItem.nfolder = "";
          aItem.folder = newFolderId;

          this.yaBookmarks.lastUsedFolder = newFolderId;

          if (aItem.folderOld) {
            this.bookmarksEditItem(aItem);
          } else {
            this.bookmarksInsertNewItem(aItem);
          }
        }
      }
    }

    if (res)
      return aItem.callback(res);
  },

  bookmarksInsertNewLinkInXML: function(aReq, aItem) {
    var res;

    if (this.isReqError(aReq)) {
      res = "lost_connection";
    } else if (!this.isLogin) {
      res = "auth_required";
    } else {
      var bookmarksResponse = this.yaBookmarks.getServerResponse(aReq.target.responseText);
      if (bookmarksResponse.error) {
        res = bookmarksResponse.error;
      } else {
        var newLinkId = bookmarksResponse.xml.ok[0].@id.toString();
        if (!newLinkId || newLinkId == "0") {
          res = "service";
        } else {
          if (bookmarksResponse.xml.ok[0].@yaru_link.toString()) {
            res = this.getDOMDocContent2("bookmarks/xsl-yaru-post-props.xsl",
                                          this.domParser.parseFromString(bookmarksResponse.xml.toSource(), "text/xml"));
          } else {
            res = null;
          }

          try {
            var _uri = this.makeURI(aItem.url, null, null);
            aItem.url = "hostPort" in _uri ?
                        [_uri.scheme, "://", (_uri.scheme == "file" ? "/" : ""), _uri.hostPort, _uri.path].join("") :
                        _uri.spec;
          } catch(e) {
            aItem.url = "http://" + aItem.url;
          }

          var bookmarks = this.usersData[this.username]._bookmarksRawXml.bookmarks;
          var elem = bookmarks..folder.(@id == aItem.folder);
          var parent = (elem.@id.toString() == "" ? bookmarks : elem).links;

          var duplicate = !!(bookmarksResponse.xml.ok[0].@duplicate.toString() > "");
          duplicate = duplicate && parent.link.(@id == newLinkId).length();

          if (!duplicate) {
            var newItem = new XML('<link id="' + newLinkId + '" folder_id="' + aItem.folder + '"/>');
            newItem.@name  = aItem.name;
            newItem.@url   = aItem.url;
            newItem.descr = aItem.descr;
            newItem.@tags  = aItem.tags;

            var len = parent.link.(@name < aItem.name).length()-1;
            if (len == -1)
              parent.link = newItem + parent.link;
            else
              parent.link[len] += newItem;
          }

          this.yaBookmarks.refreshBookmarksDOMMenu();
        }
      }
    }

    return aItem.callback(res);
  },

  get bookmarksDOMMenu() {
    if (!this.isLogin)
      return this.bookmarksDOMMenuDefault;

    if (!this.usersData[this.username].bookmarksDOMMenu)
      this.bookmarksDOMMenu = false;

    return this.usersData[this.username].bookmarksDOMMenu.cloneNode(true);
  },

  get bookmarksDOMMenuDefault() {
    if (!this._bookmarksDOMMenuDefault)
      this._bookmarksDOMMenuDefault = this.getDOMDocContent("bar-bookmarks",
                                      this.domParser.parseFromString("<page><bookmarks/></page>", "text/xml"));

    return this._bookmarksDOMMenuDefault.cloneNode(true);
  },

  set bookmarksDOMMenu(aBookmarksXML) {
    if (!this.isLogin)
      return false;

    this.usersData[this.username]._bookmarksRawXml = aBookmarksXML || this.bookmarksRawXml;

    if (aBookmarksXML)
      this.usersData[this.username]._bookmarksRawXml.@ts = Date.now();

    this.usersData[this.username].bookmarksDOMMenu =
        this.getDOMDocContent2("xsl-templ/xsl-bar-bookmarks.xsl",
            this.domParser.parseFromString(this.usersData[this.username]._bookmarksRawXml, "text/xml"),
            { addToFolderOnTop: this.getBoolPref("yasearch.general.ui.bookmarks.showaddtofolderontop") });

    this.yaBookmarks.bookmarksCache.clear(true);
  },

  get bookmarksRawXml() {
    return (this.isLogin && this.usersData[this.username]._bookmarksRawXml) ?
                    this.usersData[this.username]._bookmarksRawXml :
                    new XML("<page><bookmarks/></page>");
  },

  bookmarksGetLinksInFolder: function(aFolderId) {
    let urlArray = [];
    let folder = aFolderId ? this.bookmarksRawXml.bookmarks..folder.(@id == aFolderId) : this.bookmarksRawXml.bookmarks;

    for each (let link in folder.links.link)
      urlArray.push(link.@url.toString());

    return urlArray;
  },

  get bookmarksIsOutOfDate() {
    if (this.isLogin && !this.usersData[this.username]._bookmarksRawXml)
      return true;

    let lastCheckTime = this.parseIntFromStr(this.bookmarksRawXml.@ts);
    return !!(lastCheckTime && (Math.abs(Date.now() - lastCheckTime) > DAY_SECS))
  },

  setNotifyTimer: function(cancel) {
    if (!this._notifyTimer) {
      this._notifyTimer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
      this._notifyTimer.initWithCallback(this, 1000, Ci.nsITimer.TYPE_REPEATING_SLACK);
    } else if (cancel) {
      this._runnedNotifications = [];
      this._notifyTimer.cancel();
      this._notifyTimer = null;
    }
  },

  notify: function(aTimer) {
    if (!this._runnedNotifications || this._runnedNotifications.length < 1)
      return this.setNotifyTimer("cancel");

    var notif = this._runnedNotifications[0];
    if (notif.isShowing == true)
      return;

    if (this.getBoolPref("yasearch." + notif.type + ".ui.notification.enabled")) {
      notif.isShowing = true;

      var msg = [(notif.type == "mail" && notif.nmb == 0) ?
                   this.getFormattedString("mailNotificationMsgPlus", [this.maxUnreadInXML]) :
                   this.getFormattedStringL18End(notif.type + "NotificationMsg", [notif.nmb])];

      let typeData = { type: notif.type };

      if (notif.type == "mail") {
        if (notif.nmb != 1)
          msg[0] += this.getString("mailNotificationMsgLast");

        msg.push([notif.mdata.from, notif.mdata.title]);

        if (notif.mdata.mfdDomain)
          typeData.mfdDomain = notif.mdata.mfdDomain;
      }

      this.showAlert(this.getString(notif.type + "NotificationTitle"), msg, typeData);
    } else {
      this._runnedNotifications.shift();
    }

    if (this.getBoolPref("yasearch." + notif.type + ".ui.soundnotification.enabled"))
      this.playSoundURL(this.getComplexValue("yasearch." + notif.type + ".ui.soundnotification.uri"));
  },

  playSoundURL: function(aSoundUrl) {
    if (!aSoundUrl || !aSoundUrl.length)
      return;

    try {
      let uri;

      if (aSoundUrl.indexOf("file://") == -1) {
        let tempLocalFile = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
        tempLocalFile.initWithPath(aSoundUrl);
        uri = this.makeFileURI(tempLocalFile);
      } else {
        uri = this.makeURI(aSoundUrl);
      }

      this.soundService.play(uri);

    } catch(e) {}
  },

  notifyAboutNewItems: function(aType, aNewNum, aMailData) {
    let notificationType;

    switch (aType) {
      case "lenta":
        notificationType = "feeds";
        break;

      case "mailList":
        notificationType = "mail";
        break;

      default:
        return;
    }

    if (!this.getBoolPref("yasearch." + notificationType + ".ui.notification.enabled") &&
        !this.getBoolPref("yasearch." + notificationType + ".ui.soundnotification.enabled"))
      return;

    let browserButtons = this.browserButtons;
    if (browserButtons && !(browserButtons[notificationType] || browserButtons[aType]))
      return;

    this._runnedNotifications.push({
      type: notificationType,
      nmb: aNewNum,
      mdata: aMailData,
      isShowing: false
    });

    this.setNotifyTimer();
  },

  _showAlertWithGrowl: function(title, msg, aTypeData) {
    if (this.AppInfo.OS.isMacOS) {
      try {
        let listener = {
          observe: function YS__showAlertWithGrowl_observe(aSubject, aTopic, aData) {
          	if (aTopic == "alertclickcallback") {
          	  let action;

          	  switch (aData) {
          	    case "mfd":
          	    case "mail":
          	      action = 1040;
          	      break;
          	    case "feeds":
          	      action = 1120;
          	      break;
          	    default:
          	      return;
          	  }

          	  gYaSearchService.loadConditionalURI(this._condURIType, "tab", {action: action});
          	}
          },

          _condURIType: aTypeData.mfdDomain || aTypeData.type
        };

      	const alertsService = Cc["@mozilla.org/alerts-service;1"].getService(Ci.nsIAlertsService);

      	alertsService.showAlertNotification(CHROME_IMAGES + "bar-logo.png",
            title.split(":")[0], msg, false,
            aTypeData.mfdDomain ? "mfd" : aTypeData.type,
            listener);

        return true;

      } catch(e) {}
    }

    return false;
  },

  showAlert: function(title, msg, aTypeData) {
  	if (this._showAlertWithGrowl(title, msg, aTypeData)) {
      this._runnedNotifications.shift();
  	  return;
  	}

    let flags = "chrome,popup=yes";
    if (!this.AppInfo.browser.isGreaterThenFx35)
      flags += ",titlebar=no";

    let url = (aTypeData && aTypeData.type) ?
                  CHROME_CONTENT + "alerts/timed.alert.xul" :
                  "chrome://global/content/alerts/alert.xul";

    let alertWin = this.windowWatcher.openWindow(null, url, "_blank", flags, null);
    alertWin.arguments = [CHROME_IMAGES + "bar-logo.png", title, msg, aTypeData, this.timedAlertListener];
  },

  timedAlertListener: {
    observe: function YS_timedAlertListener_observe(aSubject, aTopic, aData) {
      if (aTopic == "alertfinished" && gYaSearchService._runnedNotifications)
        gYaSearchService._runnedNotifications.shift();
    }
  },

  decMailCounter: function(aMailId) {
    let item = this.mailDOMMenuDoc ? this.mailDOMMenuDoc.item.item.(@url == aMailId)[0] : null;
    if (item) {
      let parent = item.parent();

      delete parent.item[item.childIndex()];

      if (!parent.item.length())
        delete parent.parent().item[parent.childIndex()];

      this.mailCounter--;
      this.mailPermCounter--;

      new G_Timer(
        function YS_decMailCounter_timed() {
          OBSERVER_SERVICE.notifyObservers(null, "yasearch-refresh-data", "mailList");
        }, 1);

      this.mailLastCheckIsOutOfDate = true;
      this.refreshHTTPData("mail", null, 5000);
    } else {
      this.yaMFD.decMailCounter(aMailId);
    }
  },

  KeyCorrector: {
    STATE_DISABLED: 0,
    STATE_RUNNED:   1,
    STATE_STOPPED:  2,

    _statePrefName: "yasearch.general.ui.urlbar.corrector.state",

    _checkStateValue: function(aValue) {
      if (aValue > this.STATE_STOPPED || aValue < this.STATE_DISABLED)
        aValue = this.STATE_STOPPED;

      return aValue;
    },

    get currentState() {
      return this._checkStateValue(gYaSearchService.getIntPref(this._statePrefName));
    },

    set currentState(val) {
      gYaSearchService.setIntPref(this._checkStateValue(this._statePrefName), val);
      return val;
    },

    get keyConvTable() {
      return gYaSearchService.windowsOS ?
        { withoutShift: { 192: 96, 49: 49, 50: 50, 51: 51, 52: 52, 53: 53, 54: 54, 55: 55, 56: 56, 57: 57, 48: 48, 109: 45, 61: 61, 81: 113, 87: 119, 69: 101, 82: 114, 84: 116, 89: 121, 85: 117, 73: 105, 79: 111, 80: 112, 219: 91, 221: 93, 220: 92, 65: 97, 83: 115, 68: 100, 70: 102, 71: 103, 72: 104, 74: 106, 75: 107, 76: 108, 59: 59, 222: 39, 90: 122, 88: 120, 67: 99, 86: 118, 66: 98, 78: 110, 77: 109, 188: 44, 190: 46, 191: 47 },
          withShift: { 192: 126, 49: 33, 50: 64, 51: 35, 52: 36, 53: 37, 54: 94, 55: 38, 56: 42, 57: 40, 48: 41, 109: 95, 61: 43, 81: 81, 87: 87, 69: 69, 82: 82, 84: 84, 89: 89, 85: 85, 73: 73, 79: 79, 80: 80, 219: 123, 221: 125, 220: 124, 65: 65, 83: 83, 68: 68, 70: 70, 71: 71, 72: 72, 74: 74, 75: 75, 76: 76, 59: 58, 222: 34, 90: 90, 88: 88, 67: 67, 86: 86, 66: 66, 78: 78, 77: 77, 188: 60, 190: 62, 191: 63 }
        } : null;
    },

    __charConvTable: null,

    get charConvTable() {
      if (!this.__charConvTable) {
        let withoutShift = { __proto__: null };
        let withShift = { __proto__: null };

        const cirChars = UConverter.ConvertToUnicode(
                         "йцукенгшщзхъфывапролджэячсмитьбюёЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁіІ");
        const latChars = "qwertyuiop[]asdfghjkl;'zxcvbnm,.`QWERTYUIOP[]ASDFGHJKL;'ZXCVBNM,.`sS";

        const cirCharsShift = UConverter.ConvertToUnicode('хъжэбюё"*,.;№');
        const latCharsShift = '{}:"<>~@$^&*#';

        for each (let [i, ch] in Iterator(cirChars))
          withoutShift[ch] = latChars[i];

        for each (let [i, ch] in Iterator(cirCharsShift))
          withShift[ch] = latCharsShift[i];

        this.__charConvTable = {
          __proto__: null,
          withoutShift: withoutShift,
          withShift: withShift
        };
      }

      return this.__charConvTable;
    }
  },
  Bloggers: {
    cachedTabs: { __proto__: null },

    _tabTimer: null,

    _setTimer: function(aTabData, aVal) {
      if (this._tabTimer)
        this._tabTimer.cancel();
      else if (aVal)
        this._tabTimer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);

      if (!aVal)
        return;

      this._tabTimer.initWithCallback({
        tab: aVal.tab,
        url: aVal.url,

        notify: function(aTimer) {
          let bloggers = gYaURLInfo.getBloggers(this.url);
          if (!bloggers)
            return;

          var _self = gYaSearchService.Bloggers;
          if (bloggers.windowState & gYaURLInfo.BLOGGERS_STATE_TIMED_REQUEST) {
            bloggers.windowState = gYaURLInfo.BLOGGERS_STATE_REQUEST;

            _self.setBloggersData(false, this.tab, this.url);

            let _url = "http://blogs.yandex.ru/bar.xml?text=" +
                        encodeURIComponent('!link="' + this.url + '"') +
                        "&count=" + bloggers.value +
                        (this.manual ? "&" + getNCRndStr() : "");

            gYaSearchService.xmlHttpRequest(
                _url,
                {callbackFunc: _self.setBloggersData.yaBind(_self, this.tab, this.url)}
            );
          }
        }
      }, aTabData._timeout, Ci.nsITimer.TYPE_ONE_SHOT);
    },

    clearTabData: function(aTabId) {
      delete this.cachedTabs[aTabId];
    },

    getBloggersData: function(aTab, aSkipCache, aTimeout, aManual) {
      if (!(aTab && aTab.selected))
        return;

      let bloggers = gYaURLInfo.getBloggers(aTab.linkedBrowser.currentURI);
      if (!bloggers)
        return;

      let url = bloggers.url,
          aTabId = aTab.linkedPanel,
          tData = this.cachedTabs[aTabId];

      if (!tData)
        tData = this.cachedTabs[aTabId] = { visible: false };

      tData.url = url;
      tData._timeout = aTimeout ? 5 : 5000;

      this._setTimer();

      if ((bloggers.buttonState === gYaURLInfo.BLOGGERS_STATE_UNKNOWN) ||
          (aSkipCache && !(bloggers.buttonState & gYaURLInfo.BLOGGERS_STATE_REQUEST)))
      {
        bloggers.buttonState = gYaURLInfo.BLOGGERS_STATE_UNKNOWN;

        if (aManual) {
          bloggers.manual = true;
          gYaSearchService.notifyBusyStateOfRequest(null, true, true, "yasearch-bloggers");
        }

        gYaSearchService.getCY(url, aTab.linkedBrowser.webProgress, false);

      } else if (bloggers.value > 0 && tData && this.isWindowVisible(aTab) &&
                  ((bloggers.windowState & gYaURLInfo.BLOGGERS_STATE_UNKNOWN) ||
                   (bloggers.windowState & gYaURLInfo.BLOGGERS_STATE_TIMED_REQUEST) ||
                  (aSkipCache && !(bloggers.windowState & gYaURLInfo.BLOGGERS_STATE_REQUEST))))
      {
        bloggers.windowState = gYaURLInfo.BLOGGERS_STATE_TIMED_REQUEST;
        this._setTimer(tData, {tab: aTab, url: url});
      }

      this.setBloggersData(false, aTab, url);
    },

    persistScrollPosition: function(aURL, aYPos) {
      let bloggers = gYaURLInfo.getBloggers(aURL);
      if (bloggers)
        bloggers.scroll = aYPos;
    },

    switchWindowMode: function(aMode) {
      this._fullMode = aMode === null ? !this._fullMode : aMode;
      return this._fullMode;
    },

    hideTopModeWindow: function() {
      if (this._fullMode)
        for each (let tabData in this.cachedTabs)
          if (tabData.visible)
            tabData.visible = false;
    },

    persistWindowVisible: function(aTab, aVisible) {
      let tData = this.cachedTabs[aTab.linkedPanel];
      if (!tData)
        tData = this.cachedTabs[aTab.linkedPanel] = {};

      tData.visible = aVisible;
    },

    isWindowVisible: function(aTab) {
      if (this._fullMode)
        return true;

      if (!aTab.selected)
        return false;

      let tData = this.cachedTabs[aTab.linkedPanel];
      return !!(tData && tData.visible);
    },

    setBloggersData: function(aReq, aTab, aUrl) {
      let bloggers = gYaURLInfo.getBloggers(aUrl);
      if (!bloggers)
        return;

      if (bloggers.manual) {
        bloggers.manual = false;
        gYaSearchService.notifyBusyStateOfRequest(null, false, true, "yasearch-bloggers");
      }

      if (aReq != false) {
        bloggers.windowState = gYaURLInfo.BLOGGERS_STATE_ERROR;

        if (!gYaSearchService.isReqError(aReq)) {
          let serverDate;

          try {
            serverDate = new Date(aReq.target.getResponseHeader("Date"));

            if (serverDate != "Invalid Date")
              serverDate = new Date(serverDate.getTime() +
                                    serverDate.getTimezoneOffset() * 60 * 1000 +
                                    3 * 60 * 60 * 1000);

            if (serverDate == "Invalid Date")
              serverDate = null;
          } catch(e) {}

          let doc = gYaSearchService.getDOMDocContent2("xsl-templ/xsl-bloggers-data.xsl",
                                     gYaSearchService.domParser.parseFromString(aReq.target.responseText, "text/xml"));

          if (doc && doc.localName == "vbox") {
            bloggers.windowState = gYaURLInfo.BLOGGERS_STATE_RESPONSE;

            doc.firstChild.setAttribute("location", bloggers.url);

            if (serverDate) {
              Array.forEach(doc.getElementsByTagName("label"), function(aLabel) {
                if (aLabel.getAttribute("class") == "ya-blogger-time") {
                  var postDate = this.getDateDiffString(serverDate, this.getDateFromString(aLabel.getAttribute("value")));
                  if (postDate)
                    aLabel.setAttribute("value", postDate);
                }
              }, this);
            }

            Array.forEach(doc.getElementsByTagName("description"), function(aDescription) {
              var node = aDescription.firstChild;
              if (node && node.nodeName == "#text")
                node.nodeValue = node.nodeValue.replace(/([\/\-&\?\.])/g, "$1\u200B")
                                               .replace(/(\S{15})(\S{15})/g, "$1\u200B$2");
            })


            bloggers.content = doc;
          }
        } else {
          gYaSearchService.log("setBloggersData got bad request result");
        }
      }

      try {
        if (aTab.selected)
          aTab.ownerDocument.defaultView.Ya.Bloggers.setData(bloggers);
      } catch(e) {}
    },

    getDateFromString: function(aString) {
      let res = aString.match(/(\d{1,2})\.(\d{1,2})\.(\d{4})(?:\s+(\d{1,2})\:(\d{1,2}))?/) ?
                    new Date(RegExp.$3, (RegExp.$2 * 1) - 1, RegExp.$1, RegExp.$4, RegExp.$5) :
                    null;

      return (!res && res == "Invalid Date") ? null : res;
    },

    _getFormatedTime: function(aDate) {
      return [aDate.getHours(), ("0" + aDate.getMinutes()).slice(-2)].join(":");
    },

    getDateDiffString: function(aCurrentDate, aDate) {
      if (!(aCurrentDate instanceof Date) || !(aDate instanceof Date))
        return null;

      let diff = aCurrentDate.getTime() - aDate.getTime();
      if (diff < 0)
        return null;

      let res = [],
          strType = "year";

      if (diff < DAY_SECS)
        strType = "today";

      if (aCurrentDate.getDate() != aDate.getDate())
        strType = "yesterday";

      if (diff >= DAY_SECS * 2)
        strType = "month";

      if (aCurrentDate.getYear() != aDate.getYear())
        strType = "year";

      switch (strType) {
        case "year":
          res = [aDate.getDate(), this.dateStrings.months[aDate.getMonth()], aDate.getFullYear()];
          break;

        case "month":
          res = [aDate.getDate(), this.dateStrings.months[aDate.getMonth()]];
          res.push(this._getFormatedTime(aDate));
          break;

        case "today":
        case "yesterday":
          res = [this.dateStrings[strType]];
          res.push(this._getFormatedTime(aDate));
          break;
      }

      return res.join(" ");
    },

    _dateStrings: null,

    get dateStrings() {
      if (!this._dateStrings) {
        this._dateStrings = {
          months: gYaSearchService.getString("dateMonths").toLowerCase().split(","),
          today: gYaSearchService.getString("dateToday"),
          yesterday: gYaSearchService.getString("dateYesterday")
        }
      }

      return this._dateStrings;
    }
  },
  _browserButtons: null,

  get browserButtons() {
    if (!this._browserButtons)
      this._browserButtons = this.buttonsInfo || {};

    return this._browserButtons;
  },

  set browserButtons(val) {
    this._browserButtons = val;
  },

  webProgressListener: {
    _servicesRe: /^https?:\/\/(?:((?:web)?mail|lenta(?:\-ng)?|money|fotki)\.yandex|[^\/]*\.(ya)|(moikrug))\.(?:ru|ua|kz|by|com)\/(.*)/i,

    _checkNeedRefreshData: function(aURL, aButtons, aTimeout) {
      aURL = "" + aURL;

      if (!aURL.match(this._servicesRe))
        return;

      var service = (RegExp.$1 || RegExp.$2 || RegExp.$3).toLowerCase();
      var path    = RegExp.$4.toLowerCase();

      var timeoutDelay = aTimeout || 5000;

      if (service == "mail" && /^lenta/.test(path)) {
        service = "lenta";
        path = path.replace(/^lenta\/?/, "");
      }

      if (service == "mail") {
        if (aButtons.mail) {
          if (/(^|\/)neo2\/#/.test(path) || /(^|\/)(modern|classic|neo)\/message/.test(path)) {
            if (!gYaSearchService.yaMFD.checkNeedRefreshDataOnLocation(path, timeoutDelay)) {
              if (gYaSearchService.isLogin && gYaSearchService.Counters.getCount("mail") > 0)
                gYaSearchService.refreshHTTPData("mail", null, 70000);
            }
          }
        }

        return;
      }

      if (!gYaSearchService.isLogin)
        return;

      switch (service) {
        case "lenta-ng":
        case "lenta":
          if (aButtons.lenta && /^(unread.xml|read)/.test(path) &&
              gYaSearchService.Counters.getCount("lenta") > 0)
            gYaSearchService.refreshHTTPData("lenta", null, timeoutDelay);

          break;

        case "fotki":
          if (aButtons.fotki) {
            var needRefresh = false;

            if (/\/favorites$/.test(path)) {
              if (gYaSearchService.Counters.getCount("fotki") > 0)
                needRefresh = true;
            } else if (/\/comments$/.test(path)) {
              if (gYaSearchService.Counters.getCount("fotki", "comments") > 0)
                needRefresh = true;
            }

            if (needRefresh)
              gYaSearchService.refreshHTTPData("fotki", null, timeoutDelay);
          }
          break;

        case "money":
          if (path == "" || path == "prepaid.xml" || path == "shops.xml")
            gYaSearchService.refreshHTTPData("money", null, timeoutDelay);
          break;

        case "ya":
          if (path && path.indexOf("replies_history_unread.xml") == 0)
            gYaSearchService.refreshHTTPData("yaru", null, timeoutDelay);
          break;

        case "moikrug":
          if (path && (/^threads\/?$/.test(path) || /^threads\/\?ncrnd/.test(path)) &&
              gYaSearchService.Counters.getCount("moikrug") > 0) {
            gYaSearchService.refreshHTTPData("moikrug", null, timeoutDelay);
          }
          break;

        default:
          break;
      }
    },

    _getUrlFromLocation: function(aLocation) {
      let url = gYaURLInfo.getURL(aLocation);
      return url ? url.toString() : false;
    },

    _checkIsTopWindowRequested: function(aWebProgress, aRequest) {
      let reqWindow = false,
          topWindow = false;

      try {
        reqWindow = aWebProgress.DOMWindow;
        topWindow = reqWindow.top;
      } catch(e) {}

      return (topWindow && topWindow === reqWindow &&
               (arguments.length == 1 || topWindow.location.toString() === aRequest.name.toString())
              );
    },

    _getDataSumm: function(aWebProgress) {
      let dataSumm = {};

      if (aWebProgress && !aWebProgress.isLoadingDocument) {
        let httpStatus = false;
        try {
          if ("currentDocumentChannel" in aWebProgress) {
            httpStatus = aWebProgress.currentDocumentChannel
                                     .QueryInterface(Ci.nsIHttpChannel)
                                     .responseStatus;
          }
        } catch(e) {}

        dataSumm = (aWebProgress.chromeEventHandler.yaSearchTHandler || {}).dataSumm || {};
        dataSumm.httpStatus = httpStatus;
      }

      return dataSumm;
    },

    onPageShowInBackground: function(aWebProgress, aButtons) {
      let url = this._getUrlFromLocation(aWebProgress.currentURI);

      if (url)
        this._checkNeedRefreshData(url, aButtons);
    },

    onLocationChange: function(aWebProgress, aButtons) {
      gYaSearchService.browserButtons = aButtons;

      if (!aButtons.navigElements)
        return;
      if (!this._checkIsTopWindowRequested(aWebProgress))
        return;

      let url = this._getUrlFromLocation(aWebProgress.currentURI);
      gYaSearchService.getCY(url, aWebProgress, true, null, this._getDataSumm(aWebProgress));
    },

    onPageStateStart: function(aWebProgress, aRequest) {
      if (!gYaSearchService.browserButtons.navigElements)
        return;

      let url = this._getUrlFromLocation(aRequest.URI);

      if (!url)
        return;

      let cyDataToSet = { post: aRequest.requestMethod === "POST" };

      let wpCurrentURI = aWebProgress.currentURI;
      if (aWebProgress.loadType & 0x800000 && wpCurrentURI && wpCurrentURI.spec && url != wpCurrentURI.spec) {
        cyDataToSet.original = this._getUrlFromLocation(aWebProgress.currentURI);
        cyDataToSet.referring = this._getUrlFromLocation(aWebProgress.referringURI);
      }

      gYaURLInfo.setCY(aRequest.URI, cyDataToSet);
    },

    onPageStateStop: function(aWebProgress, aRequest) {
      if (!this._checkIsTopWindowRequested(aWebProgress, aRequest))
        return;

      let url = this._getUrlFromLocation(aWebProgress.currentURI);

      if (!url)
        return;

      this._checkNeedRefreshData(url, gYaSearchService.browserButtons);

      if (!gYaSearchService.browserButtons.navigElements)
        return;

      let originalURL = this._getUrlFromLocation(aRequest.originalURI);
      try {
        if (originalURL == aRequest.URI.spec)
          originalURL = false;
      } catch(e) {}

      gYaSearchService.getCY(url, aWebProgress, false, originalURL, this._getDataSumm(aWebProgress));
    }
  },

  confirmCYSpam: function(aUrl, aElement) {
    this.xmlHttpRequest(this.appendStatData2Url(aUrl,{}),
                         { callbackFunc: this.confirmedCYSpam.yaBind(this, aUrl, aElement) });
  },

  confirmedCYSpam: function(aReq, aUrl, aElement) {
    let browser;

    if (aElement && aElement.localName) {
      if ("menuitem" == aElement.localName)
        aElement = aElement.parentNode.parentNode;

      if ("toolbarbutton" == aElement.localName && aElement.hasAttribute("oncommand"))
        browser = aElement.ownerDocument.defaultView;
    }

    if (!browser)
      return;

    if (this.isReqError(aReq)) {
      browser.alert(this.getString("spamError"));
    } else {
      let url = browser.gBrowser.selectedBrowser.currentURI.spec;
      if (url && url.length > 9
          && aElement.getAttribute("oncommand")
                     .indexOf("('http://bar-compl.yandex.ru/c?url=" + encodeURIComponent(url)) > 0) {

        let cy = gYaURLInfo.getCY(url);
        if (cy)
          cy.spam = false;

        aElement.disabled = true;
      }
    }
  },

  getCY: function(aURL, aWebProgress, aFromCache, aOriginalURL, aDataSumm) {
    let cy = gYaURLInfo.setCY(aURL);
    let bloggers = gYaURLInfo.setBloggers(aURL);

    aURL = aURL || "undefined";

    var originalURL = null;

    if (cy && cy.spam === null) {
      originalURL = aOriginalURL || cy.original || false;
      var referrer = aWebProgress.referringURI ? aWebProgress.referringURI.spec : (originalURL ? cy.referrer : null);

      cy.spam = (aURL == "undefined") ?
                    false :
                    "http://bar-compl.yandex.ru/c?url=" + encodeURIComponent(aURL) +
                    (referrer ? ("&referer=" + encodeURIComponent(referrer)) : "") +
                    (originalURL ? "&oldurl=" + encodeURIComponent(originalURL) : "") +
                    "&login=" + (encodeURIComponent(this.username || ""));
    }

    let browser = aWebProgress.chromeEventHandler;
    this.setCY(false, browser, aURL);

    if (!cy || (aFromCache && aWebProgress.isLoadingDocument))
      return;

    let urlinfo = 0;

    if (this.browserButtons.cy)
      urlinfo |= 1;

    if (this.browserButtons.bloggers)
      urlinfo |= 2;

    if (!urlinfo) {
      this.customBarApp.statLogger.readActions();
      return;
    }

    if (
      !!((urlinfo & 1) && (cy.state & gYaURLInfo.CY_STATE_UNKNOWN)) ||
      !!((urlinfo & 2) && (bloggers.buttonState & gYaURLInfo.BLOGGERS_STATE_UNKNOWN)) ||
      !!(aDataSumm && (aDataSumm.time || aDataSumm.action)))
    {
      if (urlinfo & 1)
        cy.state = gYaURLInfo.CY_STATE_REQUEST;

      if (urlinfo & 2)
        bloggers.buttonState = gYaURLInfo.BLOGGERS_STATE_REQUEST;

      originalURL = originalURL || aOriginalURL || cy.original || false;

      var post = cy.post;

      let tSumm = "";
      let actions = [];

      if (aDataSumm) {
        if (aDataSumm.time) {
          tSumm += "&tv=" + aDataSumm.time.tv + "&t=" + aDataSumm.time.t;

          if (aDataSumm.time.yamm)
            tSumm += "&yamm=" + encodeURIComponent(aDataSumm.time.yamm);
        }

        if (aDataSumm.action)
          actions.push(aDataSumm.action);

        if (aDataSumm.httpStatus)
          tSumm += "&httpstatus=" + aDataSumm.httpStatus;
      }

      let customBarActions = this.customBarApp.statLogger.readActions();
      if (customBarActions)
        actions.push(customBarActions);

      if (actions.length)
        tSumm += "&action=" + actions.join(",");

      let ui = this.guidString;
      if (ui) {
        ui = "&ui=" + encodeURIComponent(ui.replace(/^\{/, "").replace(/\}$/, ""));
        let r1 = this.barnavigR1String;
        if (r1)
          ui += "&r1=" + encodeURIComponent(r1);
      }

      let params = ["ver=" + this.barExtensionVersionWithLocale +
                    tSumm + "&" + this.getAppendStatData2Url({clid:4}) +
                    (ui || "") +
                    "&urlinfo=" + urlinfo +
                    "&url=" + encodeURIComponent(aURL) +
                    "&show=1&post=" + (post ? 1 : 0) +
                    (aWebProgress.referringURI && aWebProgress.referringURI.userPass == "" ?
                    "&referer=" + encodeURIComponent(aWebProgress.referringURI.spec) : "") +
                    (originalURL ? "&oldurl=" + encodeURIComponent(originalURL) : ""),
                    "",//hip
                    (browser.contentTitle ? ("&title=" + encodeURIComponent(("" + browser.contentTitle).substr(0,1000))) : "")
                   ];

      gYaURLInfo.asyncGetIPsForURL(aURL, this.sendCY.yaBind(this, params, browser, aURL, urlinfo));
    }
  },

  sendCY: function(aIPs, aParams, aBrowser, aURL, aUrlinfo) {
    if (aIPs) {
      aIPs.some(function YS_sendCY_makeHIP(aIP) {
        let parts = aIP ? aIP.match(/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/) : null;
        if (!parts)
          return false;

        aParams[1] = "&hip=" + ((parts[1] * 16777216) + (parts[2] * 65536) + (parts[3] * 256) + (parts[4] * 1));
        return true;
      });
    }

    let postStr = aParams.join("");
    this.xmlHttpRequest("http://bar-navig.yandex.ru/u",
                        { data: postStr, callbackFunc: this.setCY.yaBind(this, aBrowser, aURL, aUrlinfo, false, postStr) });
  },

  setCY: function(aReq, aBrowser, aURL, aUrlinfo, aDontResend, aPostStr) {
    let [cy, bloggers] = gYaURLInfo.getCYAndBloggers(aURL);

    let checkBloggers = false;

    if (aReq) {
      if (!(cy && bloggers)) {
        this.log("No CY or Bloggers data");
        return;
      }

      if (this.isReqError(aReq)) {
        if (!aDontResend) {
          try {
            var reqURL = aReq.target.channel.name;
            var nameReg = /^(http:\/\/)(bar\-navig\.yandex\.ru)/;

            if (reqURL && nameReg.test(reqURL)) {
              var status = 0;
              try {
                status = parseInt(aReq.target.status, 10) || 0;
              } catch(ex) {}

              if (status != 200) {
                this.xmlHttpRequest(reqURL.replace(nameReg, "$1backup-$2") + "?pstatus=" + status,
                                   { data: aPostStr,
                                     callbackFunc: this.setCY.yaBind(this, aBrowser, aURL, aUrlinfo, true)
                                   });
                return;
              }
            }
          } catch(e) {}
        }

        if (aUrlinfo & 1)
          cy.state = gYaURLInfo.CY_STATE_ERROR;

        if (aUrlinfo & 2)
          bloggers.buttonState = gYaURLInfo.BLOGGERS_STATE_ERROR;

      } else {
        this.checkNeedSendGuid();

        let urlinfoXml = this.safeE4Xml(aReq.target.responseText, null, "urlinfo");

        if (urlinfoXml) {
          if (aUrlinfo & 1) {
            cy.state = gYaURLInfo.CY_STATE_RESPONSE;

            const REGION_REG = new RegExp(UConverter.ConvertToUnicode("Регион: (.*)"), "g");
            const THEME_REG = new RegExp(UConverter.ConvertToUnicode("^\\s*Тема:\\s*"), "");

            let cyData = {};
            cyData.domain = urlinfoXml.yaca.@url.toString();
            cyData.value = parseInt(urlinfoXml.tcy.@value.toString(), 10) || 0;
            cyData.rang = parseInt(urlinfoXml.tcy.@rang.toString(), 10) || 0;

            let titles = [];
            for each (let topic in urlinfoXml.topics.topic) {
              let title = topic.@title.toString().replace(THEME_REG, "");
              if (title)
                titles.push(title);
            }

            cyData.theme = titles.length ? titles.join(", ") : this.getString("cyNoTheme");

            let region = REGION_REG.exec(urlinfoXml.textinfo.toString());
            cyData.region = (region && region[1]) ? region[1] : this.getString("cyNoRegion");

            if (urlinfoXml.r1.length() == 1) {
              let r1 = urlinfoXml.r1.toString();
              if (r1) {
                this.barnavigR1String = r1;
              }
            }

            gYaURLInfo.setCY(aURL, cyData);
          }

          if (aUrlinfo & 2) {
            if ((bloggers.buttonState & gYaURLInfo.BLOGGERS_STATE_REQUEST) || urlinfoXml.page.length() == 1) {
              bloggers.value = parseInt(urlinfoXml.page.@count, 10) || 0;
              if (bloggers.value)
                checkBloggers = true;
            }

            bloggers.buttonState = gYaURLInfo.BLOGGERS_STATE_RESPONSE;
          }
        } else {
          if (aUrlinfo & 1)
            cy.state = gYaURLInfo.CY_STATE_ERROR;

          if (aUrlinfo & 2)
            bloggers.buttonState = gYaURLInfo.BLOGGERS_STATE_ERROR;
        }
      }
    }

    try {
      let browser = aBrowser.boxObject.element.ownerDocument.defaultView;
      if (aBrowser == browser.gBrowser.selectedBrowser &&
          ((aURL == "undefined") || (aBrowser.webProgress.currentURI.spec == aURL)))
      {
        if (!aUrlinfo || (aUrlinfo & 1))
          browser.Ya.setCY(cy);

        if (!aUrlinfo || (aUrlinfo & 2))
          browser.Ya.Bloggers.setData(bloggers, checkBloggers);
      }
    } catch(e) {}
  },

  refreshCYInAllBrowsers: function() {
    this.browserButtons = null;

    gYaURLInfo.clear();

    for each (let browser in this.getWindows("navigator:browser")) {
      try {
        let yaButtons = browser.Ya.buttonsObject;

        let webProgress = browser.gBrowser.selectedBrowser.webProgress;
        webProgress.QueryInterface(Ci.nsIWebNavigation);
        webProgress.QueryInterface(Ci.nsIDocShell);

        let me = this;

        new G_Timer(function YS_refreshCYInAllBrowsers_timed() {
          me.webProgressListener.onLocationChange(webProgress, yaButtons);
        }, 0);

      } catch(e) {}
    }
  },

  getWindow: function(aWindowType) {
    return this.windowMediator.getMostRecentWindow(aWindowType);
  },

  getWindows: function(aWindowType) {
    let windows = [],
        enumerator = this.windowMediator.getEnumerator(aWindowType);

    while (enumerator.hasMoreElements())
      windows.push(enumerator.getNext());

    return windows;
  },

  getInstallDir: function YS_getInstallDir() {
    return __LOCATION__.parent.parent;
  },

  getDefaultsDir: function YS_getDefaultsDir() {
    let dir = this.getInstallDir().clone();
    dir.append("defaults");
    return dir;
  },

  getYandexDir: function YS_getYandexDir() {
    let file = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIProperties).get("ProfD", Ci.nsIFile);
    file.append(this.settingsFolderName);

    if (!file.exists())
      file.create(Ci.nsIFile.DIRECTORY_TYPE, PERMS_DIRECTORY);

    if (!file.exists())
      throw "Can't create '" + this.settingsFolderName + "' folder in profile";

    return file;
  },

  getServicesData: function() {
    return this.readFile("$content/services/services.data.xml");
  },

  getUserDataFile: function() {
    let file = this.getYandexDir();
    file.append(this.xmlServicesFileName);

    if (!file.exists() || !file.isFile())
      file.create(Ci.nsIFile.NORMAL_FILE_TYPE, PERMS_FILE);

    if (!file.exists() || !file.isFile())
      throw "Can't create '" + this.xmlServicesFileName + "' in profile";

    if (!file.isReadable())
      throw this.xmlServicesFileName + " has type or permission problems";

    return file;
  },

  get xmlServices() {
    if (!this._xmlServices) {
      this._xmlServices = this.domParser.parseFromString(this.getServicesData(), "text/xml");
      this.appendUserSessionData("services");
    }
    return this._xmlServices;
  },

  appendUserSessionData: function(aType) {
    let userDataFile = this.getUserDataFile();
    if (!userDataFile)
      return;

    let userData;
    try {
      userData = new XML(this.readFile(userDataFile));
    } catch(e) {
      return;
    }

    switch (aType) {
      case "services":
        let timeNow = Date.now();
        for each (let serv in userData.timestamps.serv) {
          let elem = this.getServiceById(serv.@id);
          if (elem)
            elem.setAttribute("service-timestamp", timeNow--);
        }

        break;

      case "data":
        for each (let user in userData.users.user) {
          if (user.@login && user.@login.toString() > "") {
            let login = user.@login.toString();

            if ("undefined" == typeof this.usersData[login])
              this.usersData[login] = {};

            this.usersData[login]._mailLastMaxId = user.mail.@last.toString().replace(/\D/g, "") || "0";

            this.usersData[login]._mailPermCounter = this.parseIntFromStr(user.mail.@counter.toString());

            this.Counters.appendUserSessionData(login, user.counters, user.yaru);

            this.usersData[login]._bookmarksRawXml = this.safeE4Xml(user.bookmarks.toString(), "<page><bookmarks/></page>", "page");
          }
        }

        if (this.yaMFD && userData.mfd.length() == 1)
          this.yaMFD.appendSessionData(userData.mfd[0]);

        break;

      default:
        break;
    }
  },

  flushUserData: function() {
    if (!(this.xmlServices && this.xmlServices.documentElement))
      return;

    let userDataFile = this.getUserDataFile();

    if (userDataFile && userDataFile.isWritable()) {
      let userData = new XML('<data><version value="' + this.version + '"/><timestamps/><users/><lastengine/></data>');

      let xpathStr = "//xul:toolbarbutton/xul:menupopup/xul:menuitem[starts-with(@id,'yasearchMenuIdPrefix-')]";
      let elems = this.xPathEvaluator.evaluate(xpathStr,
                                               this.getServicesDOMNode(),
                                               function() XULNS,
                                               this.orSnapshotType,
                                               null);

      for (let i = 0, obj; (obj = elems.snapshotItem(i)); i++) {
        let len = userData.timestamps.serv.length();
        let serv = userData.timestamps.serv[len] = <serv/>;
        serv.@id = obj.getAttribute("id").split("yasearchMenuIdPrefix-")[1];
      }

      for (var data in this.usersData) {
        let len = userData.users.user.length();

        let user = userData.users.user[len] = <user/>;
        user.@login = data;

        user.mail = <mail/>;

        if (this.usersData[data]._mailLastMaxId > "0")
          user.mail.@last = this.usersData[data]._mailLastMaxId;

        if (this.usersData[data]._mailPermCounter > 0)
          user.mail.@counter = this.usersData[data]._mailPermCounter;

        if (this.usersData[data]._bookmarksRawXml) {
          user.bookmarks = <bookmarks/>;
          user.bookmarks = this.usersData[data]._bookmarksRawXml.toString();
        }

        let [countersData, yaruData] = this.Counters.getUserSessionDataForFlush(data);
        user.counters = countersData;
        user.yaru = yaruData;
      }

      if (this.yaMFD) {
        userData.appendChild(this.yaMFD.getSessionDataForFlush());
      }

      this.writeFile(userDataFile, userData);
    }
  },

  getXSLTemplate: function(fName) {
    if (!this._xsltemplates[fName]) {
      let content = this.readFile("$content/xsl-templ/xsl-" + fName + ".xsl");

      let include = this.xsltImportRegExp.exec(content);
      if (include && include[1]) {
        include = this.getInludeXSLTemplate(include[1]);
        content = content.replace(this.xsltImportRegExp, include);
      }

      this._xsltemplates[fName] = content;
    }

    return this.domParser.parseFromString(this._xsltemplates[fName], "text/xml");
  },

  getInludeXSLTemplate: function(fName) {
    let incHashName = "include-" + fName;

    if (!this._xsltemplates[incHashName]) {
      let content = this.readFile("$content/xsl-templ/xsl-" + fName + ".xsl");
      content = content.split("<xsl:output method=\"xml\" encoding=\"UTF-8\" indent=\"no\"/>")[1];
      content = content.split("</xsl:stylesheet>")[0];
      this._xsltemplates[incHashName] = content;
    }

    return this._xsltemplates[incHashName];
  },

  getDOMDocContent: function(xslName, aDataSource, aParameters) {
    if (aDataSource && aDataSource.firstChild.localName == "parsererror")
      return null;

    let parameters = aParameters || {};

    if (!("localeTld" in parameters))
      parameters.localeTld = this.localeTld;

    if (!("localeLang" in parameters))
      parameters.localeLang = this.localeLang;

    let xsltProcessor = Cc["@mozilla.org/document-transformer;1?type=xslt"].createInstance(Ci.nsIXSLTProcessor);

    for (let [paramName, paramValue] in Iterator(parameters))
      xsltProcessor.setParameter(null, paramName, paramValue);

    xsltProcessor.importStylesheet(this.getXSLTemplate(xslName));

    return xsltProcessor.transformToDocument(aDataSource || this.xmlServices).firstChild;
  },

  getDOMDocContent2: function(xslFilePath, aDataSource, aParameters) {
    switch (typeof aDataSource) {
      case "string"://filepath
        aDataSource = this.getXSLTemplate2(aDataSource);
        break;

      case "xml":
        aDataSource = this.domParser.parseFromString(aDataSource.toXMLString(), "text/xml");
        if (aDataSource.firstChild.localName == "parsererror")
          return null;
        break;

      default:
        break;
    }

    let parameters = aParameters || {};

    if (!("localeTld" in parameters))
      parameters.localeTld = this.localeTld;

    if (!("localeLang" in parameters))
      parameters.localeLang = this.localeLang;

    let xsltProcessor = Cc["@mozilla.org/document-transformer;1?type=xslt"].createInstance(Ci.nsIXSLTProcessor);

    for (let [paramName, paramValue] in Iterator(parameters))
      xsltProcessor.setParameter(null, paramName, paramValue);

    xsltProcessor.importStylesheet(this.getXSLTemplate2(xslFilePath));

    return xsltProcessor.transformToDocument(aDataSource).firstChild;
  },

  getXSLTemplate2: function(aFilePath) {
    return this.domParser.parseFromString(this.readFile("$content/" + aFilePath), "text/xml");
  },

  getServicesDOMNode: function() {
    return this.getDOMDocContent("bar-services", null,
                                 { homePageHost: this.getLocaleDependedUrl("YandexWwwHost") });
  },

  getServiceById: function(id) {
    function YS_getServiceById_nsResolver() "urn:data";

    let elem = this.xPathEvaluator.evaluate("//xmlns:*[@id='" + id + "']",
                                            this.xmlServices.documentElement,
                                            YS_getServiceById_nsResolver,
                                            this.unSnapshotType,
                                            null);

    return elem && elem.snapshotLength ? elem.snapshotItem(0) : null;
  },

  setServiceTimestamp: function(id) {
    let elem = this.getServiceById(id);
    if (!elem)
      return null;

    elem.setAttribute("service-timestamp", Date.now());
    OBSERVER_SERVICE.notifyObservers(null, "yasearch-refresh-services", false);

    return elem;
  },

  getDefaultSearchEngine: function() {
    function YS_getDefaultSearchEngine_nsResolver() "urn:data";

    let elem = this.xPathEvaluator.evaluate("//xmlns:*[@search-url]",
                                            this.xmlServices.documentElement,
                                            YS_getDefaultSearchEngine_nsResolver,
                                            this.unSnapshotType,
                                            null);

    return elem && elem.snapshotLength ? elem.snapshotItem(0).getAttribute("id") : null;
  },

  get searchService() {
    if (!this._searchService)
      this._searchService = Cc["@mozilla.org/browser/search-service;1"].getService(Ci.nsIBrowserSearchService);

    return this._searchService;
  },

  get searchEngineSuggestURL() {
    return this._searchEngineSuggestURL ||
           (this._searchEngineSuggestURL = "http://" + this.getLocaleDependedUrl("YandexSuggestHost") + "/suggest-ff.cgi?part=");
  },
  get defaultYandexSearchEngine() {
    return {
      _self: this,

      supportsResponseType: function(aType) {
        return aType == "application/x-suggestions+json";
      },

      getSubmission: function(aData, aResponseType) {
        return {
          postData: null,
          uri: this._self.makeURI(this._self.searchEngineSuggestURL + encodeURIComponent(aData))
        }
      }
    };
  },

  get currentSearchEngine() {
    if (!this._yaCurrentEngine.isYandex)
      return this.searchService.getEngineByName(this._yaCurrentEngine.name);

    return {
      _self: this,

      _currentEngine: this._yaCurrentEngine,

      name: "___ya___" + this._yaCurrentEngine.name,

      supportsResponseType: function(aType) {
        return aType == "application/x-suggestions+json";
      },

      getSubmission: function(aData, aResponseType) {
        return {
          postData: null,
          uri: this._self.makeURI(this._self.searchEngineSuggestURL + encodeURIComponent(aData))
        }
      }
    };
  },

  getSearchEngineUrl: function(aId, aText, aHost, aStatData) {
    let engine,
        result = { isYandexSearch: false },
        id = aId.replace(/^__/, "");

    if (id == aId) {
      result.isYandexSearch = true;

      engine = this.getServiceById(aId);

      if (engine) {
        let appendix = aHost && engine.hasAttribute("search-site") ?
                           "&" + engine.getAttribute("search-site") + "=" + encodeURIComponent(aHost) :
                           "";

        let searchURL = engine.getAttribute("search-url") +
                        (encodeURIComponent(aText).replace(/%20/g, "+")) +
                        appendix;

        result.url = this.appendStatData2Url(searchURL, (typeof aStatData == "undefined") ? {clid:2} : aStatData);

        let action = engine.hasAttribute("search-action") ?
                         engine.getAttribute("search-action") :
                         (engine.hasAttribute("action") ? engine.getAttribute("action") : null);

        result.statData = action ? {action: action} : null;

        result.postData = null;
      }
    } else {
      engine = this.searchService.getEngineByName(id);

      if (engine) {
        let submission = engine.getSubmission(aText, null);

        if (submission) {
          result.url = submission.uri.spec;
          result.statData = {action: "4300"};
          result.postData = submission.postData;
        }
      }
    }

    return result.url ? result : null;
  },

  setCurrentSearchEngine: function(aId) {
    var elem,
        result = {},
        id = aId.replace(/^__/, "");

    this._yaCurrentEngine = {isYandex: false, name: id};

    if (id == aId) {
      this._yaCurrentEngine.isYandex = true;

      function YS_setCurrentSearchEngine_nsResolver() "urn:data";

      var obj,
          elems = this.xPathEvaluator.evaluate("//xmlns:*[@last-engine]",
                                               this.xmlServices.documentElement,
                                               YS_setCurrentSearchEngine_nsResolver,
                                               this.unSnapshotType,
                                               null);

      for (let i = 0; (obj = elems.snapshotItem(i)); i++)
        obj.removeAttribute("last-engine");

      elem = this.getServiceById(aId);

      if (elem) {
        elem.setAttribute("last-engine", "true");
        result.searchUrl = elem.getAttribute("search-url");
        result.searchSite = elem.getAttribute("search-site");
        result.label = elem.getAttribute("label2") || elem.getAttribute("label");
        result.image = CHROME_IMAGES + elem.getAttribute("image") + ".png";
      }
    } else {
      elem = this.searchService.getEngineByName(id);
      if (elem) {
        result.label = id;
        result.image = elem.iconURI ? elem.iconURI.spec : "";
        result.searchUrl = elem.uri;
      }
    }

    return result.label ? result : null;
  },
  
  uninit: function() {
    if (this._inited) {
      this._inited = false;

      OBSERVER_SERVICE.notifyObservers(null, "yasearch-state-changed", "before-finalized");

      ["cookie-changed",
       "http-on-modify-request",
       "http-on-examine-response"]
      .forEach(function YS_uninit_removeObservers(aTopicName) {
        OBSERVER_SERVICE.removeObserver(this, aTopicName, true);
      }, this);
      gYaAuth.shutdown();

      this.clearAllTimers();

      if (gYaInstaller && !gYaInstaller.isBarUninstalled)
        this.flushUserData();

      this._browserButtons = null;

      OBSERVER_SERVICE.notifyObservers(null, "yasearch-state-changed", "finalized");
    }

    this._searchService = null;
  },

  handlePrefChanges: function(aData) {
    switch (aData.split("yasearch.")[1]) {
      case "general.debug.enabled":
        this.debug = this.getBoolPref(aData);
        break;

      case "general.ui.show.cy.value":
        this.refreshCYInAllBrowsers();
        break;

      case "general.ui.urlbar.corrector.state":
        OBSERVER_SERVICE.notifyObservers(null, "yasearch-refresh-data", "urlbar-corrector");
        break;

      case "http.update.interval":
        this.updateHttpTimers("mailandfeeds");
        break;

      case "http.auto.enabled":
        this.updateHttpTimers();
        break;

      case "general.ui.mail.integration":
        OBSERVER_SERVICE.notifyObservers(null, "yasearch-refresh-data", "mail-integration");
        break;
    }
  },

  get isCountersAutoUpdateEnabled() {
    return this.getBoolPref("yasearch.http.auto.enabled");
  },

  updateHttpTimers: function(aTimerType) {
    var autoUpdateEnabled = this.isCountersAutoUpdateEnabled;

    if (!aTimerType || aTimerType == "mailandfeeds") {
      this.checkTimeOut._MailAndFeeds = autoUpdateEnabled ? (this.getIntPref("yasearch.http.update.interval") * MIN_SEC || 0) : 0;
      this.isLogin && this.checkTimeOut._MailAndFeeds > 0 ?
          this.setTimer("_MailAndFeeds") : this.clearTimer("_MailAndFeeds");
    }

    if (this.yaMFD && (!aTimerType || aTimerType == "mailandfeeds"))
      this.yaMFD.setUpdateTimer();
  },

  get barPref() {
    if (!this._barPref)
      this._barPref = " YB/" + this.barExtensionVersionWithLocale;

    return this._barPref;
  },

  get barPrefReg() {
    if (!this._barPrefReg)
      this._barPrefReg = new RegExp(this.barPref.replace(/(\.|\-)/g, "\\\$1"));

    return this._barPrefReg;
  },

  __localeTld: null,
  get localeTld() {
    if (!this.__localeTld)
      this.__localeTld = this.getString("locale.tld");

    return this.__localeTld;
  },

  __localeLang: null,
  get localeLang() {
    if (!this.__localeLang)
      this.__localeLang = this.getString("locale.lang");

    return this.__localeLang;
  },

  isYandexHost: function(aHost) {
    return /(^|\.)(yandex\.(ru|ua|by|kz|net|com)|(ya|narod|moikrug)\.ru)$/i.test(aHost);
  },

  _getDOMWindowForRequest: function(aRequest) {
    try {
      return aRequest.loadGroup.groupObserver.QueryInterface(Ci.nsIWebProgress).DOMWindow;
    } catch(e) {}

    return null;
  },

  _getTabForDOMWindow: function(aDOMWindow) {
    let tab = null;

    let docShellTree = aDOMWindow.QueryInterface(Ci.nsIInterfaceRequestor)
                                 .getInterface(Ci.nsIWebNavigation)
                                 .QueryInterface(Ci.nsIDocShellTreeItem);

    if (docShellTree.itemType == Ci.nsIDocShellTreeItem.typeContent) {
      try {
        let chromeWindow = docShellTree.rootTreeItem
                          .QueryInterface(Ci.nsIInterfaceRequestor)
                          .getInterface(Ci.nsIDOMWindow)
                          .wrappedJSObject;

        if (!chromeWindow)
          return null;

        tab = chromeWindow.getBrowser()
                          .getBrowserForDocument(aDOMWindow.document);

        if (!tab)
          return null;

        if (!tab.yaSearchTHandler) {
          let YaProgressListener = chromeWindow.YaProgressListener;
          if (YaProgressListener)
            YaProgressListener.addTabListener(tab);
        }

        if (tab.yaSearchTHandler)
          return tab;

      } catch(e) {}
    }

    return null;
  },

  _getYaSearchTHandlerForRequest: function(aRequest) {
    let win = this._getDOMWindowForRequest(aRequest);
    if (win && win === win.parent) {
      let tab = this._getTabForDOMWindow(win);

      if (tab)
        return tab.yaSearchTHandler;
    }

    return null;
  },

  observe: function(aSubject, aTopic, aData) {
    switch (aTopic) {
      case "http-on-modify-request":
        aSubject.QueryInterface(Ci.nsIHttpChannel);

        if (this.isYandexHost(aSubject.URI.host)) {
          try {
            var ua = aSubject.getRequestHeader("User-Agent");
            if (!this.barPrefReg.test(ua))
              aSubject.setRequestHeader("User-Agent", ua + this.barPref, false);
          } catch(e) {}
        }

        try {
          if (aSubject.loadFlags & Ci.nsIHttpChannel.LOAD_DOCUMENT_URI) {
            var tabHandler = this._getYaSearchTHandlerForRequest(aSubject);
            if (tabHandler)
              tabHandler.onTransferStart(aSubject.URI.spec);
          }
        } catch(e) {}

        break;

      case "http-on-examine-response":
        aSubject.QueryInterface(Ci.nsIHttpChannel);

        try {
          if (aSubject.loadFlags & Ci.nsIHttpChannel.LOAD_DOCUMENT_URI) {
            var tabHandler = this._getYaSearchTHandlerForRequest(aSubject);
            if (tabHandler)
              tabHandler.onTransferStop();
          }
        } catch(e) {}

        break;

      case "cookie-changed":
        this.yaAuth.onCookieChanged(aSubject, aTopic, aData);
        break;

      case "nsPref:changed":
        this.handlePrefChanges(aData);
        break;

      case "profile-after-change":
        OBSERVER_SERVICE.addObserver(this, "browser-ui-startup-complete", true);
        OBSERVER_SERVICE.addObserver(this, "sessionstore-windows-restored", true);
        OBSERVER_SERVICE.addObserver(this, "profile-before-change", true);
        OBSERVER_SERVICE.addObserver(this, "quit-application", true);

        this.init();

        if (this.initialized) {
          let prefInternal = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch2);
          prefInternal.addObserver("yasearch", this, true);
        }
        break;

      case "profile-before-change":
        if (this.initialized) {
          let prefInternal = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch2);
          prefInternal.removeObserver("yasearch", this, true);
        }

        this.uninit();
        break;

      case "app-startup":
        OBSERVER_SERVICE.addObserver(this, "profile-after-change", true);
        break;

      case "browser-ui-startup-complete":
        this.onBrowserUIStartupComplete();
        break;

      case "sessionstore-windows-restored":
        this.onSessionstoreWindowsRestored();
        break;

      case "quit-application":
        break;
    }
  },

  _hackKnownSkins: function YS__hackKnownSkins() {
    let selectedSkin = this.getCharPref("extensions.lastSelectedSkin") ||
                       this.getCharPref("general.skins.selectedSkin");

    if (!selectedSkin)
      return;

    if (selectedSkin == "classic/1.0")
      selectedSkin = "classic";

    let skinPaths = [selectedSkin];

    let platformName = this.AppInfo.OS.platformName;
    if (platformName)
      skinPaths.push(platformName + "/" + selectedSkin);

    const SS_SERVICE = Cc["@mozilla.org/content/style-sheet-service;1"].getService(Ci.nsIStyleSheetService);
    const USER_SHEET = SS_SERVICE.USER_SHEET;

    skinPaths.forEach(function YS__hackKnownSkins_register(aSkinPath) {
      let uri = this.makeURI("chrome://yasearch/skin/hacks/themes/" + aSkinPath + ".css");

      if (uri && !SS_SERVICE.sheetRegistered(uri, USER_SHEET)) {
        try {
          SS_SERVICE.loadAndRegisterSheet(uri, USER_SHEET);
        } catch (e) {}
      }
    }, this);
  },

  parseIntFromStr: function(aStr) {
    let res = aStr ? parseInt(aStr.toString().replace(/\D/g,""), 10) : 0;
    return isNaN(res) ? 0 : res;
  },

  safeUnicode: function(aString) {
    if (!/[^\r\n\x9\xA\xD\x20-\uD7FF\uE000-\uFFFD\u10000-\u10FFFF]/.test(aString))
      return aString;

    return aString.replace(/[^\r\n\x9\xA\xD\x20-\uD7FF\uE000-\uFFFD\u10000-\u10FFFF]/g, "");
  },

  getStringBundle: function(aLocaleFilePath) {
    let strBundleService = Cc["@mozilla.org/intl/stringbundle;1"].createInstance(Ci.nsIStringBundleService);

    if (aLocaleFilePath)
      return strBundleService.createBundle("chrome://yasearch/locale/" + aLocaleFilePath);

    if (!this.stringBundle)
      this.stringBundle = strBundleService.createBundle("chrome://yasearch/locale/yasearch.properties");

    return this.stringBundle;
  },

  getString: function(aName) {
    return this.getStringBundle().GetStringFromName(aName);
  },

  getFormattedString: function(aName, aStrArray) {
    return this.getStringBundle().formatStringFromName(aName, aStrArray, aStrArray.length);
  },

  getFormattedStringL18End: function(aName, aStrArray) {
    let n = aStrArray[0];
    let _cases = [2,0,1,1,1,2,2,2,2,2];
    let l18End = aName + "L18End" + ((n%100>10)&&(n%100<20) ? 2 : _cases[n%10]);
    return this.getFormattedString(aName, aStrArray) + this.getString(l18End);
  },

  __localeDependedStrBundleService: null,
  getLocaleDependedUrl: function(aUrl) {
    if (!this.__localeDependedStrBundleService) {
      let strBundleService = Cc["@mozilla.org/intl/stringbundle;1"].createInstance(Ci.nsIStringBundleService);
      this.__localeDependedStrBundleService = strBundleService.createBundle("chrome://yasearch/locale/links/links.properties");
    }

    return this.__localeDependedStrBundleService.GetStringFromName(aUrl);
  },

  utils: {
    get G_Timer() {
      return G_Timer;
    }
  },

  writeFile: function(aFile, aData) {
    let stream = Cc["@mozilla.org/network/file-output-stream;1"].createInstance(Ci.nsIFileOutputStream);
    try {
      stream.init(aFile, 0x02 | 0x08 | 0x20, PERMS_FILE, 0);
      let writer = Cc["@mozilla.org/intl/converter-output-stream;1"].createInstance(Ci.nsIConverterOutputStream);
      try {
        writer.init(stream, "UTF-8", 0, 0x0000);
        writer.writeString(aData);
      } catch (ex) {
        Cu.reportError(ex);
      } finally {
        writer.close();
      }
    } catch (ex) {
      Cu.reportError(ex);
    } finally {
      stream.close();
    }
  },

  _getBytesFromStream: function(aInputStream) {
    let byteArray = null;

    try {
      let binaryStream = Cc["@mozilla.org/binaryinputstream;1"].createInstance(Ci.nsIBinaryInputStream);
      binaryStream.setInputStream(aInputStream);
      byteArray = binaryStream.readByteArray(binaryStream.available());
      binaryStream.close();
    } catch (ex) {
      Cu.reportError(ex);
    }

    return byteArray;
  },

  _getStringFromStream: function(aInputStream) {
    let content = "";

    try {
      let fileSize = aInputStream.available();
      let cvstream = Cc["@mozilla.org/intl/converter-input-stream;1"].createInstance(Ci.nsIConverterInputStream);
      cvstream.init(aInputStream, "UTF-8", fileSize, Ci.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER);
      let data = {};
      cvstream.readString(fileSize, data);
      content = data.value;
      cvstream.close();
    } catch (ex) {
      Cu.reportError(ex);
    }

    return content;
  },

  readFile: function(aSource, aBinaryMode) {
    if (!aSource)
      return null;

    let inputStream;
    let content = "";

    if (typeof aSource == "string") {
      const CHROME_APP_PATH = "chrome://yasearch/";

      aSource = aSource.replace(/^\$(content|locale|skin)\//, CHROME_APP_PATH + "$1/")
                       .replace(/^\$chrome\//, CHROME_APP_PATH);

      let chromeReg = Cc["@mozilla.org/chrome/chrome-registry;1"].getService(Ci.nsIChromeRegistry);

      let chromeURI;

      let uri = this.makeURI(aSource);

      try {
        chromeURI = chromeReg.convertChromeURL(uri);
      } catch (ex) {}

      if (chromeURI) {
        switch (chromeURI.scheme) {
          case "jar":
            chromeURI.QueryInterface(Ci.nsIJARURI);

            let fileURI = chromeURI.JARFile;
            fileURI.QueryInterface(Ci.nsIFileURL);

            let chromeFile = fileURI.file;
            let jarEntry = chromeURI.JAREntry;

            let zipreader = Cc["@mozilla.org/libjar/zip-reader;1"].createInstance(Ci.nsIZipReader);
            zipreader.open(chromeFile);

            if (zipreader.hasEntry(jarEntry))
              inputStream = zipreader.getInputStream(jarEntry);

            zipreader.close();

            break;

          case "file":
            let protocolHandler = Cc["@mozilla.org/network/protocol;1?name=file"].createInstance(Ci.nsIFileProtocolHandler);
            aSource = protocolHandler.getFileFromURLSpec(chromeURI.spec);
            break;
          default:
            throw new Error("'" + chromeURI.scheme + "' not yet impl");
            break;

        }
      }
    }

    if (!inputStream && (aSource instanceof Ci.nsILocalFile)) {
      inputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
      inputStream.init(aSource, 0x01, 0, inputStream.CLOSE_ON_EOF);
    }

    if (inputStream) {
      content = aBinaryMode ? this._getBytesFromStream(inputStream)
                            : this._getStringFromStream(inputStream);
      inputStream.close();
    }

    return content;
  },

  removeFile: function(aFile) {
    if (!(aFile instanceof Ci.nsILocalFile))
      throw new Error("nsYaSearch.removeFile: not nsILocalFile.");

    if (!aFile.exists())
      return true;

    if (!aFile.isFile() && !aFile.isDirectory())
      throw new Error("nsYaSearch.removeFile: not a file or directory.");
    try {
      aFile.remove(false);
      return true;
    } catch (e) {}

    let tmpDir = Cc["@mozilla.org/file/directory_service;1"]
                     .getService(Ci.nsIProperties)
                     .get("TmpD", Ci.nsILocalFile);

    let tmpFile = tmpDir.clone();
    tmpFile.append(aFile.leafName);

    if (aFile.isFile())
      tmpFile.createUnique(tmpFile.NORMAL_FILE_TYPE, PERMS_FILE);
    else
      tmpFile.createUnique(tmpFile.DIRECTORY_TYPE, PERMS_DIRECTORY);

    aFile.moveTo(tmpDir, tmpFile.leafName);

    return true;
  },

  prefBranchInternal: Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch2),
  prefBranch:         Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch),

  setBoolPref: function(aName, aValue) {
    this.prefBranch.setBoolPref(aName, aValue);
  },

  getBoolPref: function(aName) {
    let rv = null;
    try {
      rv = this.prefBranch.getBoolPref(aName);
    } catch(e) {}

    return rv;
  },

  setIntPref: function(aName, aValue) {
    this.prefBranch.setIntPref(aName, aValue);
  },

  getIntPref: function(aName) {
    let rv = null;
    try {
      rv = this.prefBranch.getIntPref(aName);
      if (rv < 0)
        rv = 0;
    } catch(e) {}

    return rv;
  },

  setCharPref: function(aName, aValue) {
    this.prefBranch.setCharPref(aName, aValue);
  },

  getCharPref: function(aName) {
    let rv = null;
    try {
      rv = this.prefBranch.getCharPref(aName);
    } catch(e) {}

    return rv;
  },

  getComplexValue: function(aName) {
    let rv = null;
    try {
      rv = this.prefBranch.getComplexValue(aName, Ci.nsIPrefLocalizedString).data;
    } catch(e) {}

    return rv;
  },

  setComplexValue: function(aName, aValue) {
    try {
      let str = Cc["@mozilla.org/supports-string;1"].createInstance(Ci.nsISupportsString);
      str.data = aValue;
      this.prefBranch.setComplexValue(aName, Ci.nsISupportsString, str);
    } catch(e) {}
  },

  resetPrefBranch: function(aPrefBranchName) {
    if (!aPrefBranchName)
      return;

    if (!/\.$/.test(aPrefBranchName))
      aPrefBranchName += ".";

    this.prefBranch.getChildList(aPrefBranchName, {}).forEach(function(aPrefName) {
      try {
        this.resetPref(aPrefName);
      } catch(e) {}
    }, this);
  },

  resetPref: function(aPrefName) {
    try {
      this.prefBranch.clearUserPref(aPrefName);
    } catch(e) {}
  },

  getBrowserHomePage: function() {
    let url;
    try {
      url = this.getComplexValue("browser.startup.homepage");
    } catch(e) {}

    if (!url) {
      const SBS = Cc["@mozilla.org/intl/stringbundle;1"].getService(Ci.nsIStringBundleService);

      let propertiesPath = this.AppInfo.browser.isGreaterThenFx36 ?
                               "chrome://branding/locale/browserconfig.properties" :
                               "resource:/browserconfig.properties";

      let configBundle = SBS.createBundle(propertiesPath);
      url = configBundle.GetStringFromName("browser.startup.homepage");
    }

    return url;
  },

  setBrowserHomePage: function(aValue) {
    if (this.yaDefence)
      this.yaDefence.protectedHomepage = aValue;

    this.setComplexValue("browser.startup.homepage", aValue);
  },

  getBrowserKeywordURL: function() {
    let url;
    try {
      url = this.getComplexValue("keyword.URL") || this.getCharPref("keyword.URL");
    } catch(e) {}

    if (!url) {
      try {
        const SBS = Cc["@mozilla.org/intl/stringbundle;1"].getService(Ci.nsIStringBundleService);
        let configBundle = SBS.createBundle("chrome://browser-region/locale/region.properties");
        url = configBundle.GetStringFromName("keyword.URL");
      } catch (ex) {}
    }

    return url;
  },

  setBrowserKeywordURL: function(aValue) {
    this.setComplexValue("keyword.URL", aValue);
  },

  get appOS() {
    var gApp  = Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULAppInfo).QueryInterface(Ci.nsIXULRuntime);
    return gApp.OS;
  },

  get windowsOS() {
    return ("nsIWindowsRegKey" in Ci) ? true : false;
  },

  getYaOnlineProgramDir: function() {
    return this.WinReg.read("HKCU", "Software\\Yandex\\Online", "ProgramDir");
  },

  get isYaOnlineInstalled() {
    return !!this.getYaOnlineProgramDir();
  },

  WinReg: {
    _convertName2Key: function(aKeyName) {
      aKeyName = aKeyName.replace(/^(HKEY_)/i, "").toUpperCase();

      const keyNamesHash = {
        "CURRENT_USER":  ["HKCU", "USER", "CURRENT_USER"],
        "CLASSES_ROOT":  ["HKCR", "ROOT", "CLASSES_ROOT"],
        "LOCAL_MACHINE": ["HKLM", "MACHINE", "LOCAL_MACHINE"]
      };

      let keyName;
      for (let kName in keyNamesHash) {
        if (keyNamesHash[kName].indexOf(aKeyName) !== -1) {
          keyName = kName;
          break;
        }
      }

      if (!keyName)
        throw new TypeError("nsIYaSearch.WinReg: wrong key name");

      return Ci.nsIWindowsRegKey["ROOT_KEY_" + keyName];
    },

    _readValue: function(wrk, value) {
      if (wrk.hasValue(value)) {
        switch (wrk.getValueType(value)) {
          case wrk.TYPE_STRING:
            return wrk.readStringValue(value);
          case wrk.TYPE_BINARY:
            return wrk.readBinaryValue(value);
          case wrk.TYPE_INT:
            return wrk.readIntValue(value);
          case wrk.TYPE_INT64:
            return wrk.readInt64Value(value);
          default:
            break;
        }
      }
      return null;
    },

    _writeValue: function(wrk, name, value, type) {
      type = type.toLowerCase();

      switch (type) {
        case "string":
          wrk.writeStringValue(name, value);
          break;
        case "binary":
          wrk.writeBinaryValue(name, value);
          break;
        case "int":
          wrk.writeIntValue(name, value);
          break;
        case "int64":
          wrk.writeInt64Value(name, value);
          break;
        default:
          throw new TypeError("nsIYaSearch.WinReg: wrong key type");
          break;
      }
    },

    _removeChildren: function(wrk) {
      for (let i = wrk.childCount - 1; i >= 0; i--) {
        let name = wrk.getChildName(i);
        let subkey = wrk.openChild(name, wrk.ACCESS_ALL);
        this._removeChildren(subkey);
        subkey.close();
        wrk.removeChild(name);
      }
    },

    _getWrk: function() {
      return Cc["@mozilla.org/windows-registry-key;1"].createInstance(Ci.nsIWindowsRegKey);
    },

    read: function(aKey, aPath, aName) {
      let result = null;

      if (gYaSearchService.windowsOS) {
        let key = this._convertName2Key(aKey);

        let wrk = this._getWrk();
        try {
          wrk.open(key, aPath, wrk.ACCESS_READ);
          result = this._readValue(wrk, aName);
        } catch(e) {}

        try {
          wrk.close();
        } catch(e) {}
      }

      return result;
    },

    write: function(aKey, aPath, aName, aValue, aValueType) {
      if (gYaSearchService.windowsOS) {
        let key = this._convertName2Key(aKey);

        let wrk = this._getWrk();
        try {
          wrk.create(key, aPath, wrk.ACCESS_WRITE);
          this._writeValue(wrk, aName, aValue, aValueType);
        } catch(e) {}

        try {
          wrk.close();
        } catch(e) {}
      }
    },

    remove: function(aKey, aPath, aName) {
    }
  },

  get barExtensionVersion() {
    if (!this._barExtensionVersion)
      this._barExtensionVersion = this.barExtensionMajorVersion + this.VERSION_BUILD;

    return this._barExtensionVersion;
  },

  get barExtensionMajorVersion() {
    if (!this._barExtensionMajorVersion)
      this._barExtensionMajorVersion = AddonManager.getAddonVersion(__LOCATION__.parent.parent);

    return this._barExtensionMajorVersion;
  },

  get barExtensionVersionWithLocale() {
    if (!this._barExtensionVersionWithLocale)
      this._barExtensionVersionWithLocale = this.barExtensionVersion + this.versionLocaleAppend;

    return this._barExtensionVersionWithLocale;
  },

  get versionLocaleAppend() {
    let verLocaleAppend = this.getString("locale.lang");
    return (verLocaleAppend && verLocaleAppend != "ru") ? ("-" + verLocaleAppend) : "";
  },

  get generateGUIDStatusURL() {
    if (!this._generateGUIDStatusURL) {
      try {
        let vendorURL = this.getCharPref("yasearch.vendor.guid.url");
        if (vendorURL) {
          let URIFixup = Cc["@mozilla.org/docshell/urifixup;1"].getService(Ci.nsIURIFixup);
          vendorURL = URIFixup.createFixupURI(vendorURL, Ci.nsIURIFixup.FIXUP_FLAG_NONE);

          if ((vendorURL.scheme == "http" || vendorURL.scheme == "https") && /yandex\.ru$/i.test(vendorURL.host)) {
            vendorURL = vendorURL.spec;
            vendorURL += /\?/.test(vendorURL) ? (/&$/.test(vendorURL) ? "" : "&") : "?";
            this._generateGUIDStatusURL = vendorURL;
          }
        }
      } catch (ex) {}

      if (!this._generateGUIDStatusURL)
        this._generateGUIDStatusURL = "http://soft.export.yandex.ru/status.xml?";
    }
    return this._generateGUIDStatusURL;
  },

  get generateGUID() {
    return Cc["@mozilla.org/uuid-generator;1"].createInstance(Ci.nsIUUIDGenerator).generateUUID();
  },

  getYaAppDir: function() {
    var appDir;

    if (this.AppBarType == "barffport") {
      appDir = this.getPortableDir();
    } else {
      try {
        appDir = Cc["@mozilla.org/file/directory_service;1"]
                     .getService(Ci.nsIProperties)
                     .get(this.windowsOS ? "AppData" : "Home", Ci.nsIFile);
      } catch(e) {}
    }

    if (appDir && appDir.exists() && appDir.isDirectory()) {
      appDir.append(this.windowsOS ? "Yandex" : ".yandex");

      if (!appDir.exists() || !appDir.isDirectory()) {
        try {
          appDir.create(Ci.nsIFile.DIRECTORY_TYPE, PERMS_DIRECTORY);
        } catch(e) {}
      }

      if (appDir.exists() && appDir.isDirectory())
        return appDir;
    }

    return false;
  },

  getPortableDir: function() {
    let curProcDir,
        badStructure = true;

    try {
      curProcDir = Cc["@mozilla.org/file/directory_service;1"]
                       .getService(Ci.nsIProperties)
                       .get("CurProcD", Ci.nsIFile)
                       .parent;

      badStructure = ["AppInfo", "DefaultData", "Firefox"].some(function(aDirName) {
        let dir = curProcDir.clone();
        dir.append(aDirName);
        return !(dir.exists() && dir.isDirectory());
      });

    } catch(e) {}

    return badStructure ? null : curProcDir;
  },

  _AppBarType: null,
  get AppBarType() {
    if (!this._AppBarType) {
      let prefName = "yasearch.general.app.bar.type";
      let barType = this.getCharPref(prefName);

      switch (barType) {
        case "barff":
        case "barffport":
          break;

        default:
          barType = !!this.getPortableDir() ? "barffport" : "barff";
          this.setCharPref(prefName, barType);
          break;
      }

      this._AppBarType = barType;
    }

    return this._AppBarType;
  },

  get vendorFileName() {
    return "clids-" + this.AppBarType + ".xml";
  },

  getYaPrefsFile: function(aFileName, aCreate) {
    function _isFileExists(aFile) {
      return !!(aFile.exists() && aFile.isFile());
    }

    var appDir = this.getYaAppDir();
    if (appDir) {
      var file = appDir.clone();
      file.append(aFileName);

      if (aFileName == this.vendorFileName && !_isFileExists(file)) {
        var venFile = appDir.clone();
        venFile.append("clids-barie.xml");

        if (_isFileExists(venFile)) {
          try {
            venFile.copyTo(appDir, aFileName);
            venFile.permissions = PERMS_FILE;
          } catch(e) {}
        }

        if (!_isFileExists(file)) {
          venFile = appDir.clone();
          venFile.append("vendor.xml");

          if (_isFileExists(venFile)) {
            try {
              venFile.copyTo(appDir, aFileName);
              venFile.permissions = PERMS_FILE;
            } catch(e) {}
          }

          if (!_isFileExists(file)) {

            let distContent = this.readFile("$content/services/vendor.xml");
            if (distContent)
              this.writeFile(file, distContent);

          }
        }
      }

      if (aCreate == true && !_isFileExists(file))
        file.create(Ci.nsIFile.NORMAL_FILE_TYPE, PERMS_FILE);

      if (_isFileExists(file) && file.isReadable())
        return file;
    }

    return false;
  },

  getGuidDataFromString: function(aGuidStr) {
    let guid = aGuidStr ? aGuidStr.toString() : false;
    return (guid && /^\{[0-9a-f]{2,8}\-[0-9a-f]{2,4}\-[0-9a-f]{2,4}\-[0-9a-f]{1,4}\-[0-9a-f]{2,12}\}$/i.test(guid)) ? guid : false;
  },

  __guidString: null,

  get guidString() {
    if (this.__guidString === null) {
      var guidPrefName = "yasearch.guid.value",
          guidStr = "",
          uiFile = this.getYaPrefsFile("ui");

      if (uiFile) {
        guidStr = this.getGuidDataFromString(this.readFile(uiFile));
      } else {
        var uiWasCreated = false;

        var currentWinUser;
        if (this.AppBarType !== "barffport") {
          currentWinUser = this.WinReg.read("HKCU",
                           "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer", "Logon User Name");

          if (currentWinUser) {
            uiWasCreated = !!this.WinReg.read("HKCU", "Software\\Yandex", "UICreated_" + currentWinUser);
          }
        }

        if (!uiWasCreated) {
          if (this.prefBranch.prefHasUserValue(guidPrefName)) {
            guidStr = this.getGuidDataFromString(this.getCharPref(guidPrefName));
          } else {
            guidStr = this.generateGUID;
            this.setCharPref(guidPrefName, guidStr);
          }

          if (guidStr) {
            uiFile = this.getYaPrefsFile("ui", true);

            if (!uiFile || !uiFile.isWritable()) {
              guidStr = "";

            } else {
              this.writeFile(uiFile, guidStr);

              if (currentWinUser) {
                this.WinReg.write("HKCU", "Software\\Yandex", "UICreated_" + currentWinUser, 1, "int");
              }
            }
          }
        }
      }

      if (guidStr)
        this.setCharPref(guidPrefName, guidStr);
      else
        guidStr = "";

      this.__guidString = "" + (guidStr || "");
    }

    return this.__guidString;
  },

  __barnavigR1String: null,

  get barnavigR1String() {
    if (this.__barnavigR1String === null) {
      let r1 = "",
          r1File = this.getYaPrefsFile("r1-barff", false);

      if (r1File) {
        r1 = this.readFile(r1File);
      }

      this.__barnavigR1String = r1 || "";
    }

    return this.__barnavigR1String;
  },

  set barnavigR1String(val) {
    if (val !== this.barnavigR1String) {
      let r1File = this.getYaPrefsFile("r1-barff", true);
      if (r1File) {
        this.writeFile(r1File, val);
      }

      this.__barnavigR1String = null;
    }

    return this.barnavigR1String;
  },

  vendorDataMergeClids: function() {
    this.__vendorData = null;

    let installedFile = this.getYaPrefsFile(this.vendorFileName, false);
    if (!installedFile)
      return;

    try {
      installedFile.permissions = PERMS_FILE;
    } catch(e) {}

    let installedXml = this.safeE4Xml(this.readFile(installedFile), null, "vendor");
    if (!installedXml) {
      try {
        installedFile.remove(true);
      } catch(e) {}

      return;
    }

    let distribXml = this.safeE4Xml(this.readFile("$content/services/vendor.xml"), null, "vendor");
    if (!distribXml)
      return;

    let writeNewData = false;
    for each (let clid in distribXml.*.(/^clid\d+$/.test(function::name()))) {
      let clidName = clid.name();
      let clidValue = clid.toString();
      if (clidValue && (installedXml[clidName].length() === 0 || !installedXml[clidName].toString())) {
        installedXml[clidName] = clidValue;
        writeNewData = true;
      }
    }

    if (writeNewData) {
      this.writeFile(installedFile, "<?xml version=\"1.0\" encoding=\"windows-1251\"?>\r\n" +
                                    installedXml.toXMLString());
    }
  },

  get vendorData() {
    if (!this.__vendorData) {
      let data = {};
      let vendorFile = this.getYaPrefsFile(this.vendorFileName, false);
      let vendorXml = this.safeE4Xml(vendorFile ? this.readFile(vendorFile) : "", "<vendor/>", "vendor");

      for (let i = 0; i <= 12; i++)
        data["clid" + i] = vendorXml["clid" + i].toString() || false;

      this.__vendorData = data;
    }

    return this.__vendorData;
  },

  get yandexHomePageValue() {
    let clid5 = this.vendorData.clid5;
    return "http://" + this.getLocaleDependedUrl("YandexWwwHost") + "/" + (clid5 ? "?clid=" + clid5 : "");
  },

  isYandexHomePage: function(aHomepageURL) {
    let url = typeof aHomepageURL === "undefined" ? this.getBrowserHomePage() : aHomepageURL;

    return !!(url &&
              (/^http:\/\/(www\.)?yandex\.(ru|ua|kz|by|com)/i.test(url) ||
               /^http:\/\/ru\.start3\.mozilla\.com\/firefox/.test(url))
             );
  },

  setHomePageUrl: function(aOverrideYandex) {
    if (aOverrideYandex || !this.isYandexHomePage())
      this.setBrowserHomePage(this.yandexHomePageValue);

    if (this.getIntPref("browser.startup.page") === 0)
      this.setIntPref("browser.startup.page", 1);
  },

  _checkKeywordUrl: function() {
    if (this.getCharPref("app.distributor") != "yandex")
      return;

    let keywordURL = this.getBrowserKeywordURL();
    if (!keywordURL || this.prefBranch.prefHasUserValue("keyword.URL"))
      return;

    this._setKeywordUrl(false);
  },

  _setKeywordUrl: function(aSetEnabled) {
    let keywordURL = this.getBrowserKeywordURL();

    if (!(/[&?]clid=/.test(keywordURL) && /^http:\/\/yandex\.(ru|ua|kz|by|com)\/yandsearch\?/.test(keywordURL))) {
      let urlParams = [];

      let clid9 = this.vendorData.clid9;
      if (clid9)
        urlParams.push("clid=" + clid9);

      urlParams.push("yasoft=" + encodeURIComponent(this.AppInfo.yasoftStr));
      urlParams.push("text=");

      this.setBrowserKeywordURL(this.getLocaleDependedUrl("KeywordURL") + "?" + urlParams.join("&"));
    }

    if (aSetEnabled)
      this.setBoolPref("keyword.enabled", true);
  },

  appendStatData2Url: function(aURL, aStatData) {
    if (!aStatData || typeof(aStatData) !== "object" || !aURL)
      return aURL;

    if (/[&?]yasoft=/.test(aURL))
      aStatData.yasoft = false;

    if (/[&?]clid=/.test(aURL))
      aStatData.clid = false;

    let appendix = this.getAppendStatData2Url(aStatData);

    if (appendix == "")
      return aURL;

    let url = aURL.split("?");
    url = url[0] + "?" + appendix + (url[1] ? "&" + url[1] : "");

    return url;
  },

  getAppendStatData2Url: function(aStatData) {
    let res = [];

    if (aStatData && !(aStatData.action && !(aStatData.yasoft || aStatData.clid))) {
      if (aStatData.clid) {
        let clid = this.vendorData["clid" + (aStatData.clid || "")];
        if (clid)
          res.push("clid=" + clid);
      }

      if ((aStatData.yasoft || null) !== false)
        res.push("yasoft=" + encodeURIComponent(this.AppInfo.yasoftStr));
    }

    return res.join("&");
  },

  get versionData() {
    let gApp  = Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULAppInfo).QueryInterface(Ci.nsIXULRuntime);

    let versionData = {
      ui: this.guidString,
      v: this.barExtensionVersionWithLocale,
      ver: this.barExtensionVersionWithLocale,
      lang: this.getString("locale.lang"),
      bn: gApp.name,
      bv: gApp.version,
      os: gApp.OS,
      yasoft: this.AppInfo.yasoftStr,
      tl: this.timeGuid
    };

    if (this.vendorData.clid1)
      versionData.clid = this.vendorData.clid1;

    return versionData;
  },

  get generateGUIDData() {
    if (!this._generateGUIDData) {
      var versionData = this.versionData;

      var data2Server = [];
      for (let [propName, propValue] in Iterator(versionData))
        data2Server.push(propName + "=" + encodeURIComponent(propValue));

      this._generateGUIDData = data2Server.join("&");
    }

    var dynamicAppend = "";

    if (this.yaDefence) {
      var yaDefenceTimesData = this.yaDefence.changesTime;
      if (yaDefenceTimesData)
        dynamicAppend += yaDefenceTimesData;

      var yaFSearchData = this.yaDefence.fSearchStatData;
      if (yaFSearchData)
        dynamicAppend += yaFSearchData;
    }

    var _use = "&stat=dayuse";
    if (typeof(gYaInstaller) === "object" && "sttInstall" in gYaInstaller) {
      let stt = gYaInstaller.sttInstall;
      if (stt)
        _use = "&stat=install";
    }

    dynamicAppend += _use;

    return this._generateGUIDData + dynamicAppend;
  },

  __AppInfo: null,
  get AppInfo() {
    if (!this.__AppInfo) {
      const gApp  = Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULAppInfo).QueryInterface(Ci.nsIXULRuntime);
      const os = gApp.OS;

      let verLocaleAppend = this.getString("locale.lang");
      verLocaleAppend = (verLocaleAppend && verLocaleAppend != "ru") ? ("." + verLocaleAppend) : "";

      this.__AppInfo = {
        yasoftStr: "barff" + verLocaleAppend,

        OS: {
          name: os,
          isWindows: /^win/i.test(os),
          isLinux:   /^linux/i.test(os),
          isMacOS:   /^darwin/i.test(os),
          get platformName() {
            if (this.isWindows) return "win";
            if (this.isLinux)   return "unix";
            if (this.isMacOS)   return "mac";

            return "";
          }
        },

        browser: {
          version: gApp.version,
          get isGreaterThenFx30() {
            delete this.isGreaterThenFx30;
            return (this.isGreaterThenFx30 = !!Ci.nsIWorker);
          },
          get isGreaterThenFx35() {
            delete this.isGreaterThenFx35;
            return (this.isGreaterThenFx35 = !!Ci.nsIDOMGeoPositionAddress);
          },
          get isGreaterThenFx36() {
            delete this.isGreaterThenFx36;
            return (this.isGreaterThenFx36 = !!Ci.nsIContentSecurityPolicy);
          }
        }
      };
    }
    return this.__AppInfo;
  },

  get timeGuid() {
    return (new Date(parseInt(this.getCharPref("yasearch.guid.time"), 10))).valueOf() || 0;
  },

  set timeGuid(val) {
    this.setCharPref("yasearch.guid.time", val ? Date.now() : "0");
  },

  checkNeedSendGuid: function() {
    if (this.updateTimer._GuidRefresh)
      return;

    let timeNow = Date.now(),
        timeBefore = this.timeGuid;

    if (timeBefore < (timeNow - DAY_SECS) || timeBefore > timeNow)
      this.setTimer("_Guid", 10);

    this.setTimer("_GuidRefresh");
  },

  loadURI: function(aURL, aEvent, aClidType) {
    let browser = this.getWindow("navigator:browser");
    return (browser && browser.Ya) ? browser.Ya.loadURI(aURL, aEvent, aClidType) : false;
  },

  loadConditionalURI: function(aType, aEvent, aStatData) {
    let browser = this.getWindow("navigator:browser");
    return (browser && browser.Ya) ? browser.Ya.loadConditionalURI(aType, aEvent, aStatData) : false;
  },

  makeURI: function(aURLSpec, aCharset) {
    try {
      return IO_SERVICE.newURI(aURLSpec, aCharset, null);
    } catch(e) {}

    return null;
  },

  makeFileURI: function(aFile) {
    try {
      return IO_SERVICE.newFileURI(aFile);
    } catch(e) {}

    return null;
  },

  safeE4Xml: function(aStr, aDefaultStr, aRootNodeName) {
    if (aStr) {
      if (typeof aStr == "string") {
        aStr = this.safeUnicode(aStr);
      } else {
        if (aStr instanceof Ci.nsIDOMDocument || aStr instanceof Ci.nsIDOMElement)
          aStr = this.xmlSerializer.serializeToString(aStr);
        else if (typeof(aStr) === "xml")
          aStr = aStr.toString();
      }
    }

    if (!aStr || aStr == "")
      aStr = (aDefaultStr || "").toString();

    if (typeof(aStr) != "string")
      return null;

    aStr = aStr.replace(/<\?xml .+\?>[\r\n]*/, "")
               .replace(/(<!DOCTYPE ((.|\r|\n)*?)\]>)[\r\n]*/, "");

    let d,
        rootNodeName;

    try {
      d = new XML(aStr);
      rootNodeName = d.name().localName.toString();
    } catch(e) {
      this.log("'safeE4Xml' error: " + e);
    }

    if (d && rootNodeName) {
      if (!aRootNodeName)
        aRootNodeName = rootNodeName;

      let namesArray = (typeof aRootNodeName == "string") ? [aRootNodeName] : aRootNodeName;

      if (namesArray.indexOf(rootNodeName) != -1)
        return d;
    }

    return aDefaultStr ? this.safeE4Xml(aDefaultStr) : null;
  },

  DOMUtils: {
    evaluateXPath: function(aNode, aExpr) {
      function nsResolver(aPrefix) {
        const ns = {
          "xul": "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul",
          "xhtml": "http://www.w3.org/1999/xhtml"
        };

        return ns[aPrefix] || null;
      }

      let xpEvaluator = Cc["@mozilla.org/dom/xpath-evaluator;1"].getService(Ci.nsIDOMXPathEvaluator);
      let xpathResult = xpEvaluator.evaluate(aExpr, aNode, nsResolver,
                                             Ci.nsIDOMXPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);
      let nextElement,
          result = [];

      while ((nextElement = xpathResult.iterateNext()))
        result.push(nextElement);

      return result;
    },
    importAndRemoveNode: function(aNode, aDocument) {
      if (!aNode)
        return aNode;

      let node = aDocument.importNode(aNode, true);

      try {
        aNode.parentNode.removeChild(aNode);
      } catch(e) {}

      return node;
    },

    adoptNode: function(aNode, aDocument) {
      return this.importAndRemoveNode(aNode, aDocument);
    },

    appendNode: function(aNode, aTarget) {
      let node = this.importAndRemoveNode(aNode, aTarget.ownerDocument);

      if (node)
        aTarget.appendChild(node);

      return node;
    },

    replaceNode: function(aNewNode, aOldNode) {
      let fromDoc = aNewNode.ownerDocument,
          toDoc = aOldNode.ownerDocument;

      let node = (fromDoc === toDoc) ? aNewNode : this.importAndRemoveNode(aNewNode, toDoc);

      if (node)
        aOldNode.parentNode.replaceChild(node, aOldNode);

      return node;
    },

    replaceChildNodes: function(aNode, aTarget) {
      while (aTarget.hasChildNodes())
        aTarget.removeChild(aTarget.firstChild);

      return aNode ? this.appendNode(aNode, aTarget) : null;
    }
  },

  dumpIFaces: function(aObject) {
    if (!this.debug)
      return;

    var dumpTxt = ["Dump interfaces\r\n==============================================="];
    for (var iface in Ci) {
      try {
        aObject.QueryInterface(Ci[iface])
        dumpTxt.push(iface);
      } catch(e) {}
    }

    this.log(dumpTxt.join("\r\n"));
    return aObject;
  },

  dump: function(aObject) {
    if (!this.debug)
      return;

    var dumpTxt = ["Dump properties in Object\r\n==============================================="];
    for (var prop in aObject)
      try { dumpTxt.push(prop + " :: " + aObject[prop]); } catch(e) {}

    if (aObject instanceof Ci.nsIDOMEvent) {
      dumpTxt.push("================= nsIDOMEvent targets ======================================");
      ["target", "currentTarget", "originalTarget", "explicitOriginalTarget"].forEach(function(aType) {
        dumpTxt.push(aType + " :: " + aObject[aType] + " :: " + (aObject[aType] ? aObject[aType].localName : null))
      })
    }

    this.log(dumpTxt.join("\r\n"));
  },
  
  _getToolbarForNode: function(aNode) {
    let toolbar = false,
        node = aNode;

    while (node && !toolbar) {
      node = node.parentNode;
      if (node && node.localName == "toolbar")
        toolbar = node;
    }

    return toolbar;
  },
  checkToolbarSet: function(aNmb, aToolbar) {
    if (aNmb == this.barExtensionMajorVersion)
      return;

    aToolbar.setAttribute("yaNmbSaved", this.barExtensionMajorVersion);
    aToolbar.ownerDocument.persist(aToolbar.id, "yaNmbSaved");

    if (!aNmb)
      return;

    let needPersist = false;
    let overlayController = null;

    try {
      overlayController = aToolbar.ownerDocument.defaultView.yasearchOverlayController;
    } catch (e) {
      this.log(e);
    }

    let gDocument = aToolbar.ownerDocument,
        defaultSet = aToolbar.getAttribute("defaultset");

    let pagetranslatorProtoId = "http://bar.yandex.ru/packages/yandexbar#pagetranslator";

    let addDiff = {
      "3.5.0": [ ["yasearch-fotki", ",yasearch-mail", true],
                 ["yasearch-yaru", ",yasearch-lenta", false] ],

      "4.0.0": [ ["yasearch-spellchecker", ",yasearch-bloggers", true] ],

      "4.2.90": [ ["yasearch-translate", ",yasearch-spellchecker", true] ],

      "5.0.90": [ [pagetranslatorProtoId, ",yasearch-translate", true] ]
    };

    for (let nmb in addDiff) {
      if (aNmb <= nmb) {
        let i = -1,
            _d = addDiff[nmb];

        while (_d[++i]) {
          let beforeElt = null;

          if (_d[i][1]) {
            let defSetCute = (defaultSet.split(_d[i][1])[0] + _d[i][1]).split(","),
                j = defSetCute.length;

            let found = false;

            while (defSetCute[--j] && !found)
              found = gDocument.getElementById(defSetCute[j]);

            if (found && found.nextSibling) {
              let check = true;

              while (check && found.nextSibling) {
                switch ((_d[i][2] ? found : found.nextSibling).localName) {
                  case "toolbaritem":
                  case "toolbarbutton":
                    check = false;
                    break;

                  default:
                    found = found.nextSibling;
                }
              }
            }

            beforeElt = found ? (found.nextSibling ? found.nextSibling : null) :
                                 (aToolbar.firstChild ? aToolbar.firstChild : null);
          }

          for each (let newItemId in _d[i][0].split(",").reverse()) {
            if (newItemId == pagetranslatorProtoId) {
              try {
                beforeElt = overlayController.addWidgetItem(pagetranslatorProtoId, null, beforeElt, true, true);
                if (beforeElt)
                  needPersist = true;
              } catch (e) {
                this.log(e);
              }
            } else {
              if (!gDocument.getElementById(newItemId)) {
                beforeElt = aToolbar.insertItem(newItemId, beforeElt);
                needPersist = true;
              }
            }
          }

        }
      }
    }
    if (aNmb < "4.3.0" && overlayController) {
      try {
        if (overlayController.addWidgetItem("http://bar.yandex.ru/packages/yandexbar#quote", null, null, true, "no"))
          needPersist = true;
      } catch (e) {
        this.log(e);
      }
    }

    if (needPersist)
      this._persistToolbarSet(aToolbar);
  },

  _persistToolbarSet: function(aToolbar) {
    if (aToolbar && aToolbar.localName && aToolbar.localName == "toolbar") {
      aToolbar.setAttribute("currentset", aToolbar.currentSet);
      aToolbar.ownerDocument.persist(aToolbar.id, "currentset");
    }
  },

  classDescription: "nsYaSearch JS component",
  contractID: "@yandex.ru/yasearch;1",
  classID: Components.ID("{3F79261A-508E-47a3-B61C-D1F29E2068F3}"),
  _xpcom_categories: [{ category: "app-startup", service: true }],

  QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports,
                                         Ci.nsIYaSearch,
                                         Ci.nsIObserver,
                                         Ci.nsISupportsWeakReference])
}

if (XPCOMUtils.generateNSGetFactory)
  var NSGetFactory = XPCOMUtils.generateNSGetFactory([nsIYaSearch]);
else
  var NSGetModule = XPCOMUtils.generateNSGetModule([nsIYaSearch]);

this.__defineGetter__("gYaSearchService", function gYaSearchServiceGetter() {
  delete this.gYaSearchService;
  return this.gYaSearchService = Cc["@yandex.ru/yasearch;1"].getService(Ci.nsIYaSearch).wrappedJSObject;
});
if (!!Ci.nsIContentSecurityPolicy) {
nsIYaSearch.prototype.getXSLTemplate = function(fName) {
  if (!this._xsltemplates[fName]) {
    let content = this.readFile("$content/xsl-templ/xsl-" + fName + ".xsl");

    let include = this.xsltImportRegExp.exec(content);
    if (include && include[1]) {
      include = this.getInludeXSLTemplate(include[1]);
      content = content.replace(this.xsltImportRegExp, include);
    }

    this._xsltemplates[fName] = content.replace(/http:\/\/www\.mozilla\.org\/keymaster\/gatekeeper\/there\.is\.only\.xul/g, "http://example.com");
  }

  return this.domParser.parseFromString(this._xsltemplates[fName], "text/xml");
};

nsIYaSearch.prototype.getDOMDocContent = function(xslName, aDataSource, aParameters) {
  if (aDataSource && aDataSource.firstChild.localName == "parsererror")
    return null;

  let parameters = aParameters || {};

  if (!("localeTld" in parameters))
    parameters.localeTld = this.localeTld;

  if (!("localeLang" in parameters))
    parameters.localeLang = this.localeLang;

  let xsltProcessor = Cc["@mozilla.org/document-transformer;1?type=xslt"].createInstance(Ci.nsIXSLTProcessor);

  for (let [paramName, paramValue] in Iterator(parameters))
    xsltProcessor.setParameter(null, paramName, paramValue);

  xsltProcessor.importStylesheet(this.getXSLTemplate(xslName));

  let res = xsltProcessor.transformToDocument(aDataSource || this.xmlServices);
  let resStr = this.xmlSerializer.serializeToString(res).replace(/http:\/\/example\.com/g, "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul");
  return this.domParserSys.parseFromString(resStr, "text/xml").firstChild;
};

nsIYaSearch.prototype.getDOMDocContent2 = function(xslFilePath, aDataSource, aParameters) {
  switch (typeof aDataSource) {
    case "string":
      aDataSource = this.getXSLTemplate2(aDataSource);
      break;

    case "xml":
      aDataSource = this.domParser.parseFromString(aDataSource.toXMLString(), "text/xml");
      if (aDataSource.firstChild.localName == "parsererror")
        return null;
      break;

    default:
      break;
  }

  let parameters = aParameters || {};

  if (!("localeTld" in parameters))
    parameters.localeTld = this.localeTld;

  if (!("localeLang" in parameters))
    parameters.localeLang = this.localeLang;

  let xsltProcessor = Cc["@mozilla.org/document-transformer;1?type=xslt"].createInstance(Ci.nsIXSLTProcessor);

  for (let [paramName, paramValue] in Iterator(parameters))
    xsltProcessor.setParameter(null, paramName, paramValue);

  xsltProcessor.importStylesheet(this.getXSLTemplate2(xslFilePath));

  let res = xsltProcessor.transformToDocument(aDataSource);
  let resStr = this.xmlSerializer.serializeToString(res).replace(/http:\/\/example\.com/g, "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul");
  return this.domParserSys.parseFromString(resStr, "text/xml").firstChild;
};

nsIYaSearch.prototype.getXSLTemplate2 = function(aFilePath) {
  return this.domParser.parseFromString(this.readFile("$content/" + aFilePath)
             .replace(/http:\/\/www\.mozilla\.org\/keymaster\/gatekeeper\/there\.is\.only\.xul/g, "http://example.com"), "text/xml");
};
}

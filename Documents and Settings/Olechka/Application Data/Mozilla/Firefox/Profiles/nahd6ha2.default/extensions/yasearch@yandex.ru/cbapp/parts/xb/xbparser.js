
XB._Parser = {
    init: function XBParser_init() {
        BarPlatform.registerUnitParser("xb", "widget", XB._Parser.widgetProtoParser);
    }
};

XB._Parser.widgetProtoParser = {
    parseFromDoc: function XBWidgetParser_parseFromDoc(unitDoc, unit) {
        if ( !(unitDoc instanceof Ci.nsIDOMDocument) )
            throw new CustomErrors.EArgType("unitDoc", "nsIDOMDocument", unitDoc);
        this._logger.debug("Parsing component from Unit " + unit.name);
        if (unitDoc.documentElement.localName != this._consts.STR_WIDGET_ELEMENT_NAME)
            throw new BarPlatform.Unit.EWidgetSyntax(unitDoc.documentElement.nodeName, this._consts.ERR_ROOT_NAME);
        return this._parseWidgetElement(unitDoc.documentElement, unit);
    },
    
    _consts: {
        STR_WIDGET_ELEMENT_NAME: "widget",
        STR_WIDGET_NAME_ATTR_NAME: "name",
        STR_FUNC_PARAM_ELEM_NAME: "param",
        STR_DEBUGMODE_ATTR_NAME: "__debug",
        
        WARN_UNKNOWN_TOKEN: "Unknown token",
        WARN_DUPLICATE_ARG: "Duplicate argument",
        
        ERR_ROOT_NAME: "Widget unit root element name missmatch",
        ERR_NO_WIDGET_NAME: "No widget name",
        ERR_NO_DATA_NAME: "Unnamed data declaration",
        ERR_NO_VAR_NAME: "Unnamed variable declaration",
        ERR_NO_SETTING_NAME: "Unnamed setting",
        ERR_INVALID_SCOPE: "Invalid scope definition",
        ERR_VALNODE_AMBIGUITY: "Value node has both a value attribute and a child node(s)",
        ERR_UNKNOWN_VALUE_TYPE: "Unknown value type",
        ERR_NO_FUNCTION: "No such function",
        ERR_NO_INC_FILE: "No include file"
    },
    _nodeUID: 1,
    
    get _logger() {
        delete this._logger;
        return (this._logger = XB._base.getLogger("WParser"));
    },
    
    _parseWidgetElement: function XBWidgetParser__parseWidgetElement(widgetElement, unit) {
        let widgetName = widgetElement.getAttribute(this._consts.STR_WIDGET_NAME_ATTR_NAME);
        if (!widgetName)
            throw new BarPlatform.Unit.EWidgetSyntax(widgetElement.nodeName, this._consts.ERR_NO_WIDGET_NAME);
        
        let thisPackage = unit.unitPackage;
        let protoID = thisPackage.id + "#" + unit.name;
        let unique = (widgetElement.getAttribute("unique") != "false");
        let iconPath = widgetElement.getAttribute("icon") || undefined;
        
        let widgetProto = new XB.WidgetPrototype(protoID, widgetName, unique, iconPath, unit);
            
        this._parseTopChildren(widgetProto, widgetElement, unit);
        
        return widgetProto;
    },
    
    _parseTopChildren: function XBWidgetParser__parseTopChildren(widgetProto, DOMNode, unit) {
        let treeWalker = DOMNode.ownerDocument
            .createTreeWalker(DOMNode, XB._Ci.nsIDOMNodeFilter.SHOW_ALL, this._nodeFilter, true);
        
        if (treeWalker.firstChild()) {
            do {
                let subNode = treeWalker.currentNode;
                if (subNode.namespaceURI == XB._base.consts.STR_FUNCTIONAL_NS) {
                    switch (subNode.localName) {
                        case "data":
                            this._parseWidgetData(widgetProto, treeWalker);
                            break;
                        case "variable":
                            this._parseWidgetVar(widgetProto, treeWalker);
                            break;
                        case "setting":
                            this._parseWidgetSetting(widgetProto, treeWalker);
                            break;
                        case "include":
                            this._includeFile(widgetProto, treeWalker.currentNode, unit.unitPackage);
                            break;
                        default:
                            let valRef = this._createFunctionRef(treeWalker, widgetProto);
                            widgetProto.addContentNode(valRef);
                    }
                }
                else {
                    widgetProto.addContentNode(subNode);
                    this._parseElement(treeWalker, null, widgetProto);
                }
            }
            while (treeWalker.nextSibling());
        }
    },
    
    _includeFile: function XBWidgetParser__includeFile(widgetProto, incNode, package_) {
        let filePath = incNode.getAttribute("file");
        let incFile = package_.findFile(filePath);
        if (!incFile)
            throw new BarPlatform.Unit.EWidgetSyntax(incNode.nodeName, this._consts.ERR_NO_INC_FILE + " " + filePath);
        let incDoc = fileutils.xmlDocFromFile(incFile);
        this._parseTopChildren(widgetProto, incDoc.documentElement);
    },
    
    _parseWidgetData: function XBWidgetParser__parseWidgetData(parentWidget, treeWalker) {
        let dataNode = treeWalker.currentNode;
        let dataName = dataNode.getAttribute("name");
        if (!dataName)
            throw new BarPlatform.Unit.EWidgetSyntax(dataNode.nodeName, this._consts.ERR_NO_DATA_NAME);
        this._logger.trace("Found widget scope data " + dataName);
        
        let nodeID = this._genNodeUID();
        let calcNode = new XB._calcNodes.FuncNodeProto(nodeID, XB._functions.Data);
        calcNode.debugMode = dataNode.hasAttribute(this._consts.STR_DEBUGMODE_ATTR_NAME);
        this._parseElement(treeWalker, calcNode, parentWidget);
        
        parentWidget.registerData(dataName, calcNode);
    },
    
    _parseWidgetVar: function XBWidgetParser__parseWidgetVar(parentWidget, treeWalker) {
        let varNode = treeWalker.currentNode;
        let varName = varNode.getAttribute("name");
        if (!varName)
            throw new BarPlatform.Unit.EWidgetSyntax(varNode.nodeName, this._consts.ERR_NO_VAR_NAME);
        this._logger.trace("Found variable " + varName);
        let initialValue = varNode.getAttribute("default") || XB.types.empty;
        
        let varScope;
        try {
            varScope = BarPlatform.Unit.evalScope(varNode.getAttribute("scope") || undefined,
                                                      BarPlatform.Unit.scopes.ENUM_SCOPE_WIDGET);
        }
        catch (e) {
            throw new BarPlatform.Unit.EWidgetSyntax(varNode.nodeName, this._consts.ERR_INVALID_SCOPE);
        }
        let persist = (varNode.getAttribute("persist") == "true");
        parentWidget.registerVariable(varName, varScope, persist, initialValue);
    },
    
    _parseWidgetSetting: function XBWidgetParser__parseWidgetSetting(parentWidget, treeWalker) {
        let settingNode = treeWalker.currentNode;
        let settingData = BarPlatform.Unit.parseSetting(settingNode, BarPlatform.Unit.scopes.ENUM_SCOPE_WIDGET);
        parentWidget.registerSetting(settingData.name, settingData.scope, settingData.defaultValue, settingData.type, settingData.controlElement);
    },
    
    _parseElement: function XBWidgetParser__parseElement(treeWalker, parentCalcNode, parentWidget) {
        this._logger.trace("Checking node " + treeWalker.currentNode.nodeName + " children");
        let calcNodeArgs = [];
        
        if (treeWalker.firstChild()) {
            try {
                do {
                    let widgetSubNode = treeWalker.currentNode;
                    
                    if (parentCalcNode == null && widgetSubNode.namespaceURI != XB._base.consts.STR_FUNCTIONAL_NS) {
                        this._parseElement(treeWalker, null, parentWidget);
                        continue;
                    }
                    
                    if (parentCalcNode == null) {
                        this._createFunctionRef(treeWalker, parentWidget);
                    }
                    else {
                        let argInfo = this._handleFunctionSubNode(treeWalker, parentWidget);
                        calcNodeArgs.push(argInfo);
                    }
                }
                while (treeWalker.nextSibling());
            }
            finally {
                treeWalker.parentNode();
            }
        }
        
        if (parentCalcNode != null) {
            let unnamedArgs = [];
            for each (let argInfo in calcNodeArgs) {
                if (argInfo.name) {
                    if (!parentCalcNode.argumentAttached(argInfo.name))
                        parentCalcNode.attachArgument(argInfo.name, argInfo.calcNode);
                    else
                        this._logger.warn(this._consts.WARN_DUPLICATE_ARG + " '" + argInfo.name + "'");
                }
                else
                    unnamedArgs.push(argInfo);
            }
            for each (let argInfo in unnamedArgs) {
                argInfo.name = parentCalcNode.proposeArgName();
                parentCalcNode.attachArgument(argInfo.name, argInfo.calcNode);
            }
        }
    },
    
    _createFunctionRef: function XBWidgetParser__createFuncRef(treeWalker, parentWidget) {
        let srcXMLNode = treeWalker.currentNode;
        
        let calcNode;
        if (srcXMLNode.localName == "value"){
            calcNode = this._createValNode(srcXMLNode);
        }
        else {
            let funcName = srcXMLNode.localName;
            let calcNodeConstructor = XB._functions["CN_" + funcName];
            if ( !(XB._calcNodes.FuncNode.ancestorOf(calcNodeConstructor) ||
                   XB._calcNodes.ProcNode.ancestorOf(calcNodeConstructor)))
                throw new BarPlatform.Unit.EWidgetSyntax(srcXMLNode.nodeName, this._consts.ERR_NO_FUNCTION);
            calcNode = new XB._calcNodes.FuncNodeProto(this._genNodeUID(), calcNodeConstructor);
        }
        let refID = calcNode.baseID;
        parentWidget.registerReference(refID, calcNode);
        
        this._logger.trace("Replacing functional element " + srcXMLNode.localName + " with a reference " + refID);
        let valRefElement = srcXMLNode.ownerDocument.createElementNS(
            XB._base.consts.STR_UI_NS,
            XB._base.consts.STR_VAL_REF_ELEM_NAME);
        valRefElement.setUserData(XB._base.consts.STR_VAL_REF_ID_KEY_NAME, refID, this._userDataHandler);
        srcXMLNode.parentNode.replaceChild(valRefElement, srcXMLNode);
        try {
            if ( !(calcNode instanceof XB._calcNodes.ConstNodeProto) ) {
                this._parseFuncNodeAttributes(srcXMLNode.attributes, calcNode);
                this._parseElement(treeWalker, calcNode, parentWidget);
            }
        }
        finally {
            treeWalker.currentNode = valRefElement;
        }
        return valRefElement;
    },
    
    _handleFunctionSubNode: function XBWidgetParser__handleFuncSubNode(treeWalker, parentWidget) {
        let calcNode;
        let argName;
        let argXMLNode = treeWalker.currentNode;
        
        if (argXMLNode.nodeType == argXMLNode.ELEMENT_NODE &&
            argXMLNode.localName == this._consts.STR_FUNC_PARAM_ELEM_NAME &&
            argXMLNode.namespaceURI == XB._base.consts.STR_FUNCTIONAL_NS) {
            
            argName = argXMLNode.getAttribute("name") || undefined;
            this._logger.trace("Found <f:param> element, name is " + argName);
            
            let argsLen = 0;
            if (treeWalker.firstChild()) {
                try {
                    do {
                        argsLen++;
                    }
                    while (treeWalker.nextSibling());
                    
                    if (argsLen == 1)
                        calcNode = this._handleArgumentNode(treeWalker, parentWidget);
                }
                finally {
                    treeWalker.parentNode();
                }
            }
            if (argsLen > 1) {
                let nodeID = this._genNodeUID();
                calcNode = new XB._calcNodes.FuncNodeProto(nodeID, XB._functions.CN_concat);
                this._parseElement(treeWalker, calcNode, parentWidget);
            }
            else
                if (argsLen == 0)
                    calcNode = new XB._calcNodes.ConstNodeProto(this._genNodeUID(), "");
        }
        else
            calcNode = this._handleArgumentNode(treeWalker, parentWidget);
        
        return {name: argName, calcNode: calcNode};
    },
    
    _handleArgumentNode: function XBWidgetParser__handleArgNode(treeWalker, parentWidget) {
        let calcNode;
        
        let argXMLNode = treeWalker.currentNode;
        if (argXMLNode.nodeType == argXMLNode.ELEMENT_NODE &&
            argXMLNode.namespaceURI == XB._base.consts.STR_FUNCTIONAL_NS) {
            this._logger.trace("Found functional parameter. Subfunction is \"" + argXMLNode.localName + "\"");
            
            if (argXMLNode.localName == "value"){
                calcNode = this._createValNode(argXMLNode);
            }
            else {
                let funcName = argXMLNode.localName;
                let calcNodeConstructor = XB._functions["CN_" + funcName];
                if ( !(XB._calcNodes.FuncNode.ancestorOf(calcNodeConstructor) ||
                       XB._calcNodes.ProcNode.ancestorOf(calcNodeConstructor)))
                    throw new BarPlatform.Unit.EWidgetSyntax(argXMLNode.nodeName, this._consts.ERR_NO_FUNCTION);
                calcNode = new XB._calcNodes.FuncNodeProto(this._genNodeUID(), calcNodeConstructor);
                
                this._parseFuncNodeAttributes(argXMLNode.attributes, calcNode);
                this._parseElement(treeWalker, calcNode, parentWidget);
            }
        }
        else {
            this._logger.trace("Found XML parameter. Node name is \"" + argXMLNode.nodeName + "\"");
            if (argXMLNode.nodeType == argXMLNode.TEXT_NODE) {
                let text = strutils.trimSpaces(argXMLNode.nodeValue);
                calcNode = new XB._calcNodes.ConstNodeProto(this._genNodeUID(), text);
            }
            else {
                this._parseElement(treeWalker, null, parentWidget);
                calcNode = new XB._calcNodes.ConstNodeProto(this._genNodeUID(), new XB.types.XML(argXMLNode));
            }
        }
        
        return calcNode;
    },
    
    _parseFuncNodeAttributes: function XBWidgetParser__parseFuncNodeAttrs(attrsNodeMap, calcNode) {
        for (let attrIdx = 0, len = attrsNodeMap.length; attrIdx < len; attrIdx++) {
            let attrNode = attrsNodeMap.item(attrIdx);
            let argName = attrNode.localName;
            if (argName == this._consts.STR_DEBUGMODE_ATTR_NAME) {
                calcNode.debugMode = true;
                continue;
            }
            let arg;
            
            let text = attrNode.nodeValue;
            let match = text.match(/^\$(.+)$/);
            if (match) {
                let refName = match[1];
                let refNameNode = new XB._calcNodes.ConstNodeProto(this._genNodeUID(), refName);
                arg = new XB._calcNodes.FuncNodeProto(this._genNodeUID(), XB._functions["CN_value-of"]);
                arg.attachArgument("name", refNameNode);
            }
            else {
                arg = new XB._calcNodes.ConstNodeProto(this._genNodeUID(), text);
            }
            
            if (!calcNode.argumentAttached(argName))
                calcNode.attachArgument(argName, arg);
            else
                this._logger.warn(this._consts.WARN_DUPLICATE_ARG + " '" + argName + "'");
        }
    },
    
    _createValNode: function XBWidgetParser__createValNode(srcXMLNode) {
        if (srcXMLNode.hasAttribute("value") && srcXMLNode.hasChildNodes())
            throw new BarPlatform.Unit.EWidgetSyntax(srcXMLNode.nodeName, this._consts.ERR_VALNODE_AMBIGUITY);
        
        let valueType = srcXMLNode.getAttribute("type") || "string";
        let raw;
        if (valueType == "xml") {
            raw = srcXMLNode.childNodes;
        } else {
            if (srcXMLNode.hasAttribute("value")) {
                raw = srcXMLNode.getAttribute("value");
            } else {
                raw = (srcXMLNode.getAttribute("xml:space") == "preserve") ? srcXMLNode.textContent :
                    strutils.trimSpaces(srcXMLNode.textContent);
            }
        }
        
        let val;
        switch (valueType) {
            case "string":
                val = raw;
                break;
            case "xml":
                val = new XB.types.XML(raw);
                break;
            case "number":
                val = XB._base.runtime.xToNumber(raw);
                break;
            case "bool":
                val = XB._base.runtime.xToBool(raw);
                break;
            case "empty":
                val = XB.types.empty;
                break;
            default:
                throw new BarPlatform.Unit.EWidgetSyntax(srcXMLNode.nodeName, this._consts.ERR_UNKNOWN_VALUE_TYPE);
        }
        
        return new XB._calcNodes.ConstNodeProto(this._genNodeUID(), val);
    },
    
    _genNodeUID: function XBWidgetParser__genNodeUID() {
        return this._nodeUID++;
    },
    
    _nodeFilter: {
        acceptNode : function XBWidgetParser__acceptNode(node) {
            if ( (node.nodeType == node.COMMENT_NODE) ||
                 ((node.nodeType == node.TEXT_NODE) && !this._emptyTextRE.test(node.nodeValue)) )
                return XB._Ci.nsIDOMNodeFilter.FILTER_REJECT;
            return XB._Ci.nsIDOMNodeFilter.FILTER_ACCEPT;
        },
        
        _emptyTextRE: /[^\t\n\r ]/
    },
    _userDataHandler: {
        handle: function XBWidgetParser__handleUserData(operation, key, data, srcNode, dstNode) {
            try {
                dstNode.setUserData(key, data, this);
            }
            catch (e) {
                XB._base.logger.error("Failed setting userData " + strutils.formatError(e));
            }
        }
    }
};

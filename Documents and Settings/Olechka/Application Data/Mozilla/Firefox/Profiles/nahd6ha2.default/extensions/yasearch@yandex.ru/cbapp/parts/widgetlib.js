const EXPORTED_SYMBOLS = ["widgetLibrary"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

const widgetLibrary = {
    init: function WidgetLibrary_init(application) {
        this._barApp = application;
        this._barCore = application.core;
        this._barPlatform = application.BarPlatform;
        this._logger = application.core.Lib.Log4Moz.repository.getLogger(application.name + ".WLib");
        
        this._loadBundleLists();
        this._checkPreinstalledPackages();
        this._loadComponentsInfo();
    },
    
    finalize: function WidgetLibrary_finalize() {
        this._trySaveWidgetsList();
        this._trySavePluginsList();
        this.clear();
        this._logger = null;
    },
    
    clear: function WidgetLibrary_clear() {
        this.flushPlugins();
        this._packages = {};
        this._knownWidgets = {};
        this._pluginsData = {};
    },
    activatePlugins: function WidgetLibrary_activatePlugins() {
        this._activatePlugins(undefined, this._prevPluginsState || this._defaultActivationMap);
    },
    setDefaultPluginsState: function WidgetLibrary_setDefaultPluginsState() {
        this._activatePlugins(undefined, this._defaultActivationMap);
    },
    setPluginsState: function WidgetLibrary_setPluginsState(activationMap) {
        this._activatePlugins(undefined, activationMap);
    },
    getComponentInfo: function WidgetLibrary_getComponentInfo(componentID) {
        let componentData = this._knownWidgets[componentID] || this._pluginsData[componentID];
        if (!componentData)
            throw new Error(this._barCore.Lib.strutils.formatString("No such component (%1)", [componentID]));
        let componentInfo = componentData.info || (componentData.info = this._loadComponentInfo(componentID));
        return this._barCore.Lib.sysutils.copyObj(componentInfo);
    },
    
    getComponentsInfo: function WidgetLibrary_getComponentsInfo(fromPackageID) {
        return this.getWidgetsInfo(fromPackageID).concat(this.getPluginsInfo(fromPackageID));
    },
    
    acquaintWithWidgets: function WidgetLibrary_acquaintWithWidgets(protoIDsArray, dontFail) {
        for each (let protoID in protoIDsArray) {
            if (!this._knownWidgets[protoID]) {
                try {
                    this._knownWidgets[protoID] = {prototype: null, info: this._loadComponentInfo(protoID)};
                }
                catch (e) {
                    if (dontFail) {
                        this._logger.warn("Could not get widget info for \"" + protoID + "\". " +
                                          this._barApp.core.Lib.strutils.formatError(e));
                        this._logger.debug(e.stack);
                    }
                    else
                        throw e;
                }
            }
        }
        this._trySaveWidgetsList();
    },
    
    acquaintWithPlugins: function WidgetLibrary_acquaintWithPlugins(pluginIDsArray, dontFail) {
        for each (let pluginID in pluginIDsArray) {
            if (!this._pluginsData[pluginID]) {
                try {
                    this._pluginsData[pluginID] = {plugin: null, info: this._loadComponentInfo(pluginID)};
                }
                catch (e) {
                    if (dontFail) {
                        this._logger.warn("Could not get plugin info for \"" + pluginID + "\". " +
                                          this._barApp.core.Lib.strutils.formatError(e));
                    }
                    else
                        throw e;
                }
            }
        }
        this._trySavePluginsList();
    },
    
    forgetWidgets: function WidgetLibrary_forgetWidget(protoIDsArray) {
        for each (let protoID in protoIDsArray)
            delete this._knownWidgets[protoID];
        this._trySaveWidgetsList();
    },
    
    forgetPlugins: function WidgetLibrary_forgetPlugins(pluginIDsArray) {
        for each (let pluginID in pluginIDsArray) {
            delete this._pluginsData[pluginID];
        }
        this._trySavePluginsList();
    },
    
    get currentPluginsState() {
        return this._pluginActivationMap;
    },
    
    getPlugin: function WidgetLibrary_getPlugin(pluginID) {
        if (!(pluginID in this._pluginsData))
            throw new Error(this._barApp.core.Lib.strutils.formatString("No such plugin (%1).", [pluginID]));
        let pluginData = this._pluginsData[pluginID];
        return (pluginData? pluginData.plugin : null) || this._loadPlugin(pluginID);
    },
    
    getPlugins: function WidgetLibrary_getPlugins(fromPackageID, activeOnly) {
        return this._forEachKnownPlugin(fromPackageID, function id2plugin(pluginID) {
            let plugin = this.getPlugin(pluginID);
            if (activeOnly)
                return plugin.enabled ? plugin : null;
            return plugin;
        });
    },
    
    getPluginInfo: function WidgetLibrary_getPluginInfo(pluginID) {
        if (!(pluginID in this._pluginsData))
            throw new Error(this._barApp.core.Lib.strutils.formatString("No such plugin (%1).", [pluginID]));
        
        let pluginData = this._pluginsData[pluginID];
        let pluginInfo = pluginData.info || (pluginData.info = this._loadComponentInfo(pluginID));
        return this._barCore.Lib.sysutils.copyObj(pluginInfo);
    },
    
    getPluginsInfo: function WidgetLibrary_getPluginsInfo(fromPackageID) {
        return this._forEachKnownPlugin(fromPackageID, function id2info(pluginID) this.getPluginInfo(pluginID));
    },
    
    pluginEnabled: function WidgetLibrary_pluginEnabled(pluginID) {
        if (!(pluginID in this._pluginsData))
            throw new Error(this._barApp.core.Lib.strutils.formatString("No such plugin (%1).", [pluginID]));
        
        let pluginData = this._pluginsData[pluginID];
        return pluginData ? pluginData.plugin.enabled : false;
    },
    
    flushPlugins: function WidgetLibrary_flushPlugins(packageID) {
        this._forEachKnownPlugin(packageID, function __flushPlugin(pluginID) {
            try {
                let plugin = this._pluginsData[pluginID].plugin;
                if (plugin)
                    plugin.finalize();
            }
            catch (e) {
                this._logger.error("Could not turn plugin off. " + this._barCore.Lib.strutils.formatError(e));
                this._logger.debug(e.stack);
            }
            finally {
                this._pluginsData[pluginID] = {info: null, plugin: null};
            }
        }, this);
    },
    
    getWidgetInfo: function WidgetLibrary_getWidgetInfo(protoID) {
        if (!(protoID in this._knownWidgets))
            throw new Error(this._barApp.core.Lib.strutils.formatString(this._consts.ERR_NO_SUCH_WIDGET, [protoID]));
        
        let widgetData = this._knownWidgets[protoID];
        let widgetInfo = widgetData.info || (widgetData.info = this._loadComponentInfo(protoID));
        return this._barCore.Lib.sysutils.copyObj(widgetInfo);
    },
    
    getWidgetsInfo: function WidgetLibrary_getWidgetsInfo(fromPackageID) {
        return this._forEachKnownWidget(fromPackageID, function id2info(protoID) this.getWidgetInfo(protoID));
    },
    
    widgetProtoInstantiated: function WidgetLibrary_widgetProtoInstantiated(protoID) {
        if (!(protoID in this._knownWidgets))
            throw new Error(this._barApp.core.Lib.strutils.formatString(this._consts.ERR_NO_SUCH_WIDGET, [protoID]));
        let widgetProto = this._knownWidgets[protoID].prototype;
        return widgetProto? (widgetProto.spawnedIDs.length > 0) : false;
    },
    
    getWidgetProto: function WidgetLibrary_getWidgetProto(protoID) {
        if (!(protoID in this._knownWidgets))
            throw new Error(this._barApp.core.Lib.strutils.formatString(this._consts.ERR_NO_SUCH_WIDGET, [protoID]));
        
        let widgetData = this._knownWidgets[protoID];
        return widgetData.prototype || (widgetData.prototype = this._loadWidgetProto(protoID));
    },
    getWidgetProtos: function WidgetLibrary_getWidgetProtos(fromPackageID) {
        return this._forEachKnownWidget(fromPackageID, function id2proto(protoID) this.getWidgetProto(protoID));
    },
    getWidgetProtoIDs: function WidgetLibrary_getWidgetProtoIDs(fromPackageID) {
        return this._forEachKnownWidget(fromPackageID, function id2id(protoID) protoID);
    },
    get nativeWidgets() {
        return this._barCore.Lib.sysutils.copyObj(this._nativeWidgetsMap);
    },
    
    get permanentWidgets() {
        return this._barCore.Lib.sysutils.copyObj(this._permanentWidgetsInfo);
    },
    isKnownWidget: function WidgetLibrary_isKnownWidget(protoID) (protoID in this._knownWidgets),
    
    isKnownPlugin: function WidgetLibrary_isKnownPlugin(pluginID) (pluginID in this._pluginsData),
    
    isKnownComponent: function WidgetLibrary_isKnownWidget(componentID) {
        return this.isKnownWidget(componentID) || this.isKnownPlugin(componentID);
    },
    
    isPreinstalledPackage: function WidgetLibrary_isPreinstalledPackage(packageID) {
        return (packageID in this._internalPackagesInfo);
    },
    
    isPreinstalledWidget: function WidgetLibrary_isPreinstalledWidget(protoID) {
        let [packageID, widgetName] = this._barPlatform.parseComponentID(protoID);
        let packageInfo = this._internalPackagesInfo[packageID];
        let widgetNames = packageInfo && packageInfo.widgets;
        if (!widgetNames)
            return false;
        return (widgetNames.indexOf(widgetName) > -1);
    },
    
    isPreinstalledPlugin: function WidgetLibrary_isPreinstalledPlugin(pluginID) {
        let [packageID, pluginName] = this._barPlatform.parseComponentID(pluginID);
        let packageInfo = this._internalPackagesInfo[packageID];
        let pluginNames = packageInfo && packageInfo.plugins;
        if (!pluginNames)
            return false;
        return (pluginNames.indexOf(pluginName) > -1);
    },
    
    persist: function WidgetLibrary_persist() {
        this._saveWidgetsList();
        this._savePluginsList();
    },
    
    flushWidgets: function WidgetLibrary_flushWidgets(packageID) {
        this._forEachKnownWidget(packageID, function flushWidget(protoID) {
            this._knownWidgets[protoID] = {info: null, prototype: null};
        }, this);
    },
    
    _consts: {
        STR_KNOWN_WIDGETS_FILE_NAME: "known_widgets.json",
        STR_KNOWN_PLUGINS_FILE_NAME: "plugins.json",
        STR_INTERNAL_PKGS_DATA_PATH: "$custombar/internal_packages.json",
        
        ERR_UNIT_HAS_NO_WIDGET: "Unit does not define a widget",
        ERR_NO_COMP_INFO: "Could not get component info from unit",
        ERR_COULDNT_WRITE_WLIST: "Could not write known widgets list. ",
        ERR_COULDNT_WRITE_PLIST: "Could not write known plugins list. ",
        ERR_INVALID_WIDGET_ID: "Invalid widget ID",
        ERR_NO_SUCH_WIDGET: "No such widget (%1)"
    },
    _barApp: null,
    _logger: null,
    _packages: {},
    _knownWidgets: {},
    _prevPluginsState: undefined,
    _pluginsData: {},
    
    _nativeWidgetsMap: {},
    _internalPackagesInfo: {},
    _permanentWidgetsInfo: {toolbarItems: {}, XBIDs: {}},
    
    _nativeWidgetsMapPath: "$custombar/native_widgets.json",
    _permanentWidgetsInfoPath: "$custombar/permanent_widgets.json",
    
    _loadBundleLists: function WidgetLibrary__loadBundleLists() {
        let {
            fileutils: fileutils,
            strutils: strutils,
            sysutils: sysutils,
            JSON: JSONParser
        } = this._barCore.Lib;
        let addonFS = this._barApp.addonFS;
        try {
            this._nativeWidgetsMap = JSONParser.parse(
                fileutils.readStringFromStream( addonFS.getStreamFromPath(this._nativeWidgetsMapPath) ));
        }
        catch (e) {
            this._logger.error("Could not load native widgets info. " + strutils.formatError(e));
        }
        try {
            this._internalPackagesInfo = JSONParser.parse(
                fileutils.readStringFromStream( addonFS.getStreamFromPath(this._consts.STR_INTERNAL_PKGS_DATA_PATH) ));
        }
        catch (e) {
            this._logger.error("Could not load preinstalled widgets info. " + strutils.formatError(e));
        }
        try {
            this._permanentWidgetsInfo = JSONParser.parse(
                fileutils.readStringFromStream( addonFS.getStreamFromPath(this._permanentWidgetsInfoPath) ));
        }
        catch (e) {
            this._logger.error("Could not load permanent widgets info. " + strutils.formatError(e));
        }
    },
    
    _checkPreinstalledPackages: function WidgetLibrary__checkPreinstalledPackages() {
        try {
            let oldCommon = this._barApp.directories.XBPackages.clone();
            oldCommon.append("common");
            if (oldCommon.exists())
                oldCommon.remove(true);
        } catch (e) {
            this._logger.error("Can't remove old 'common' package." + this._barCore.Lib.strutils.formatError(e));
        }
        if (this._barApp._parts.installer.info.addonVersionState != 0) {
            for (let pkgID in this._internalPackagesInfo) {
                if (this._barApp.packageManager.isPackageInstalled(pkgID)) {
                    this._barApp.packageManager.uninstallPackage(pkgID);
                }
            }
            this._extractBuiltinPackages(this._barCore.Lib.misc.mapKeysToArray(this._internalPackagesInfo));
            this._barApp.packageManager.rescanPackages();
        }
        else {
            let missingPackages = [];
            for (let pkgID in this._internalPackagesInfo) {
                this._logger.debug("  Checking package " + pkgID);
                if (!this._barApp.packageManager.isPackageInstalled(pkgID)) {
                    missingPackages.push(pkgID);
                    continue;
                }
            }
            
            if (missingPackages.length > 0) {
                this._logger.warn("  Missing packages: " + missingPackages + ". I'll try to restore.");
                this._extractBuiltinPackages(missingPackages);
                this._barApp.packageManager.rescanPackages();
            }
        }
    },
    
    _extractBuiltinPackages: function WidgetLibrary_extractBuiltinPackages(packageIDs) {
        let packagesDir = this._barApp.directories.XBPackages;
        for each (let packageID in packageIDs) {
            try {
                let packageDirName = this._internalPackagesInfo[packageID].dirName;
                let destPackageDirName = this._barApp.packageManager._makePkgDirName();
                let packageDir = packagesDir.clone();
                packageDir.append(destPackageDirName);
                if (packageDir.exists())
                    packageDir.remove(true);
                this._barApp.addonFS.copySource("$custombar/packages/" + packageDirName, packagesDir, destPackageDirName);
            }
            catch (e) {
                this._logger.error("Couldn't extract package \"" + packageID + "\". " +
                                   this._barCore.Lib.strutils.formatError(e));
                let stackTace = e.stack;
                if (stackTace)
                    this._logger.debug(stackTace);
            }
        }
    },
    
    _forEachKnownWidget: function WidgetLibrary__forEachKnownWidget(fromPackageID, callback) {
        return this._forEachComponentID(this._knownWidgets, fromPackageID, callback);
    },
    
    _forEachKnownPlugin: function WidgetLibrary__forEachKnownPlugin(fromPackageID, callback) {
        return this._forEachComponentID(this._pluginsData, fromPackageID, callback);
    },
    
    _forEachComponentID: function WidgetLibrary__forEachComponentID(componentsMap, fromPackageID, callback) {
        let result = [];
        for (let componentID in componentsMap) {
            if (fromPackageID) {
                let [packageID, ] = this._barPlatform.parseComponentID(componentID);
                if (packageID !== fromPackageID)
                    continue;
            }
            let component = callback.call(this, componentID);
            if (component != null)
                result.push(component);
        }
        return result;
    },
    
    _loadComponentsInfo: function WidgetLibrary__loadComponentsInfo() {
        let {
            fileutils: fileutils,
            sysutils: sysutils,
            strutils: strutils,
            misc: misc,
            JSON: JSONParser
        } = this._barCore.Lib;
        let preinstWidgetIDs = [];
        let preinstPluginIDs = [];
        
        for (let pkgID in this._internalPackagesInfo) {
            function cn2wid(compName) (this._makeComponentID(pkgID, compName))
            
            let packageInfo = this._internalPackagesInfo[pkgID];
            let widgetNames = packageInfo.widgets;
            if (widgetNames)
                preinstWidgetIDs = preinstWidgetIDs.concat(widgetNames.map(cn2wid, this));
            
            let pluginNames = packageInfo.plugins;
            if (pluginNames)
                preinstPluginIDs = preinstPluginIDs.concat(pluginNames.map(cn2wid, this));
        }
        let knownWidgetIDs = [];
        try {
            let dataFile = this._barApp.directories.XBRoot;
            dataFile.append(this._consts.STR_KNOWN_WIDGETS_FILE_NAME);
            if (dataFile.exists() && dataFile.isFile() && dataFile.isReadable()) {
                knownWidgetIDs = JSONParser.parse(fileutils.readTextFile(dataFile));
            }
        }
        catch (e) {
            this._logger.error("An error occured while reading known widgets list. " + strutils.formatError(e));
        }
        
        this.acquaintWithWidgets(preinstWidgetIDs.concat(knownWidgetIDs), true);
        try {
            let dataFile = this._barApp.directories.XBRoot;
            dataFile.append(this._consts.STR_KNOWN_PLUGINS_FILE_NAME);
            this._prevPluginsState = JSONParser.parse(fileutils.readTextFile(dataFile));
        }
        catch (e) {
            this._logger.error("Could not load active plugins list. " + strutils.formatError(e));
        }
        let knownPluginIDs = this._prevPluginsState ?
            preinstPluginIDs.concat(misc.mapKeysToArray(this._prevPluginsState)) :
            preinstPluginIDs;
        this.acquaintWithPlugins(knownPluginIDs, true);
    },
    
    _makeComponentID: function WidgetLibrary__makeComponentID(packageID, componentName) {
        return (packageID + "#" + componentName);
    },
    
    _loadWidgetProto: function WidgetLibrary__loadWidgetProto(protoID) {
        let [packageID, widgetName] = this._barPlatform.parseComponentID(protoID);
        let package_ = this._barApp.packageManager.getPackage(packageID);
        let proto = package_.getUnit(widgetName).widgetProto;
        if (!proto)
            throw new Error(this._consts.ERR_UNIT_HAS_NO_WIDGET);
        return proto;
    },
    
    _loadComponentInfo: function WidgetLibrary__loadComponentInfo(protoID) {
        let [packageID, componentName] = this._barPlatform.parseComponentID(protoID);
        let package_ = this._barApp.packageManager.getPackage(packageID);
        let info = package_.getUnit(componentName).componentInfo;
        if (!info)
            throw new Error(this._consts.ERR_NO_COMP_INFO);
        return info;
    },
    
    _activatePlugins: function WidgetLibrary__activatePlugins(packageID, activationMap) {
        let {
            strutils: strutils,
            sysutils: sysutils
        } = this._barCore.Lib;
        
        let failedPlugins = [];
        this._forEachKnownPlugin(packageID, function __loadPlugin(pluginID) {
            try {
                let plugin = this.getPlugin(pluginID);
                try {
                    let active, settings;
                    let activationRec = activationMap[pluginID];
                    if (sysutils.isArray(activationRec)) {
                        active = activationRec[0];
                        settings = activationRec[1];
                    }
                    else
                        active = !!activationRec;
                    
                    if (settings)
                        plugin.applySettings(settings);
                    plugin.enabled = active;
                }
                catch (e) {
                    this._logger.error( strutils.formatString("Could not turn plugin (%1) on. %2",
                                                              [pluginID, strutils.formatError(e)]) );
                }
            }
            catch (e) {
                failedPlugins.push(pluginID);
                this._logger.error( strutils.formatString("Could not create plugin %1. %2",
                                                          [pluginID, strutils.formatError(e)]) );
            }
        });
        failedPlugins.forEach(function __removeFailed(pluginID) {
            delete this._pluginsData[pluginID];
        }, this);
    },
    
    _loadPlugin: function WidgetLibrary__loadPlugin(pluginID) {
        let [packageID, pluginName] = this._barPlatform.parseComponentID(pluginID);
        let unit = this._barApp.packageManager.getPackage(packageID).getUnit(pluginName),
            componentInfo = unit.componentInfo,
            plugin = unit.plugin;
        
        this._pluginsData[pluginID] = {info: componentInfo, plugin: plugin};
        return plugin;
    },
    
    get _pluginActivationMap() {
        let result = {};
        for (let pluginID in this._pluginsData) {
            let plugin = this._pluginsData[pluginID].plugin;
            result[pluginID] = plugin ? plugin.enabled : false;
        }
        return result;
    },
    
    get _defaultActivationMap() {
        let result = {};
        for each (let pluginEntry in this._barApp.defaultPluginEntries)
            result[pluginEntry.componentID] = [pluginEntry.enabled, pluginEntry.settings];
        return result;
    },
    
    _trySaveWidgetsList: function WidgetLibrary__trySavePluginsList() {
        try {
            this._saveWidgetsList();
        }
        catch (e) {
            this._logger.error(this._consts.ERR_COULDNT_WRITE_WLIST + this._barApp.core.Lib.strutils.formatError(e));
        }
    },
    
    _trySavePluginsList: function WidgetLibrary__trySavePluginsList(stateMap) {
        try {
            this._savePluginsList(stateMap);
        }
        catch (e) {
            this._logger.error(this._consts.ERR_COULDNT_WRITE_PLIST + this._barApp.core.Lib.strutils.formatError(e));
        }
    },
    
    _saveWidgetsList: function WidgetLibrary__saveWidgetsList() {
        let dataFile = this._barApp.directories.XBRoot;
        dataFile.append(this._consts.STR_KNOWN_WIDGETS_FILE_NAME);
        this._barApp.core.Lib.
            fileutils.writeTextFile(dataFile, this._barApp.core.Lib.JSON.stringify(this.getWidgetProtoIDs()));
    },
    
    _savePluginsList: function WidgetLibrary__savePluginsList(stateMap) {
        let dataFile = this._barApp.directories.XBRoot;
        dataFile.append(this._consts.STR_KNOWN_PLUGINS_FILE_NAME);
        this._barApp.core.Lib.fileutils.writeTextFile(dataFile,
            this._barApp.core.Lib.JSON.stringify(stateMap || this._pluginActivationMap));
    }
};

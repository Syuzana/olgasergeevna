Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

const Cc = Components.classes;
const Ci = Components.interfaces;
const nsIAboutModule = Ci.nsIAboutModule;
const nsIIOService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);

function FTabAboutHandler() {
  this.yaFTab = Cc["@yandex.ru/yasearch;1"]
                       .getService(Ci.nsIYaSearch)
                       .wrappedJSObject
                       .yaFTab;
  
  this.urlRE = new RegExp("^about:yandex-tabs\\?sec=" + this.yaFTab.secURLParam + "&image=(.*)$");
}

FTabAboutHandler.prototype = {
  _getURLString: function(aURL) {
    let [, url] = (aURL || "").match(this.urlRE) || [];
    return (url && /^(https?|file)/.test(url)) ? url : null;
  },
  
  _getURLData: function(aURL) {
    let url = this._getURLString(aURL);
    let data = url ? this.yaFTab.getImageForURL(url) : null;
    return data || null;
  },
  
  newChannel: function(aURI) {
    let data = this._getURLData(aURI.spec) || "data:image/png,";
    
    let channel = nsIIOService.newChannel(data, null, null);
    channel.originalURI = aURI;
    
    return channel;
  },
  
  getURIFlags: function(aURI) {
    let spec = aURI.spec;
    
    if (this.urlRE.test(spec) || spec === "about:yandex-tabs?sec={$secURLParam}&image={$pg/@url}") {
      return nsIAboutModule.URI_SAFE_FOR_UNTRUSTED_CONTENT;
    }
    
    return 0;
  },
  
  classDescription: "About Yandex Tabs",
  classID: Components.ID("d18f02ba-3d4c-11de-a33b-0785b984f2d8"),
  contractID: "@mozilla.org/network/protocol/about;1?what=yandex-tabs",
  QueryInterface: XPCOMUtils.generateQI([nsIAboutModule, Ci.nsISupports]),
  
  _xpcom_categories: [
    { category: "app-startup", service: true }
  ]
}

if (XPCOMUtils.generateNSGetFactory)
  var NSGetFactory = XPCOMUtils.generateNSGetFactory([FTabAboutHandler]);
else
  var NSGetModule = XPCOMUtils.generateNSGetModule([FTabAboutHandler]);

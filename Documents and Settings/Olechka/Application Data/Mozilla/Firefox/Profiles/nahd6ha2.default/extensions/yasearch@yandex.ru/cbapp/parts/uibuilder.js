const EXPORTED_SYMBOLS = ["UI"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

var sysutils,
    fileutils,
    strutils,
    misc,
    CustomErrors,
    Base,
    XB;

const UI = {
    init: function UI_init(application) {
        this._application = application;
        this._loggersRoot = application.name + ".UI";
        let (appCoreLib = application.core.Lib) {
            sysutils = appCoreLib.sysutils;
            fileutils = appCoreLib.fileutils;
            strutils = appCoreLib.strutils;
            misc = appCoreLib.misc;
            CustomErrors = appCoreLib.CustomErrors;
            Base = appCoreLib.Base;
        };
        XB = application.XB;
        
        this._logger = application.core.Lib.Log4Moz.repository.getLogger(this._loggersRoot);
        this._loadModules();
    },
    
    finalize: function UI_finalize() {
        
    },
    
    getLogger: function UI_getLogger(name) {
        return this._application.core.Lib.Log4Moz.repository.getLogger(this._loggersRoot + "." + name);
    },
    
    _XMLSerializer: Cc["@mozilla.org/xmlextras/xmlserializer;1"].getService(Ci.nsIDOMSerializer),
    _application: null,
    _loggersRoot: undefined,
    
    _modules: [
        "ui/event-listener.js",
        "ui/behaviour.js",
            "ui/behaviour/widget.js",
            "ui/behaviour/text.js",
            "ui/behaviour/computed.js",
            "ui/behaviour/action.js",
            "ui/behaviour/url.js",
            "ui/behaviour/attribute.js",
            "ui/behaviour/extra-text.js",
            "ui/behaviour/menu.js",
            "ui/behaviour/tooltip.js",
            "ui/behaviour/style.js",
            "ui/behaviour/button.js",
            "ui/behaviour/checkbox.js",
            "ui/behaviour/enabled.js",
            "ui/behaviour/checked.js",
            "ui/behaviour/image.js",
            "ui/behaviour/grid.js",
            "ui/behaviour/xml.js",
        "ui/elements.js"
    ],
    
    _loadModules: function UI__loadModules() {
        const mozSSLoader = Cc["@mozilla.org/moz/jssubscript-loader;1"].getService(Ci.mozIJSSubScriptLoader);
        const partsDirPath = this._application.partsURL;
        this._modules.forEach(function ui_loadModule(moduleFileName) {
            this._logger.debug("  Loading module " + moduleFileName);
            mozSSLoader.loadSubScript(partsDirPath + moduleFileName);
        }, this);
    },
    
    
    _genBuilderID: function UI__genBuilderID() {
        return this._newXBID++;
    },
    
    _newXBID: 0
};

UI._consts = {
        STR_XUL_NS:     "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul",
        STR_HTML_NS:    "http://www.w3.org/1999/xhtml",
        
        STR_VAL_REF_ELEM_NAME: "__value__",
        STR_VAL_REF_ID_KEY_NAME: "uid"
};
UI.ElementsCollection = function UIElementsCollection() {
    this._widgets = {__proto__: null};
}

UI.ElementsCollection.prototype = {
    put: function UIElementsCollection_put(widgetInstanceId, elementId, behaviour) {
        let widget = this._widgets[widgetInstanceId] || (this._widgets[widgetInstanceId] = {__proto__: null});
        widget[elementId] = behaviour;
    },
    
    get: function UIElementsCollection_get(widgetInstanceId, elementId) {
        return this._widgets[widgetInstanceId][elementId];
    },
    
    remove: function UIElementsCollection_remove(widgetInstanceId, elementId) {
        delete this._widgets[widgetInstanceId][elementId];
    },
    
    finalize: function UIElementsCollection_finalize() {
        for (let instance in this._widgets) {
            let widget = this._widgets[instance];
            for (let element in widget)
                delete widget[element];
            delete this._widgets[instance];
        }
    },
    
    _widgets: null
};

UI.Builder = function UIBuilder(windowEngine, overlayController) {
    this._xid = UI._genBuilderID();
    this._logger = UI.getLogger("B" + this._xid);
    this._windowEngine = this.windowEngine = windowEngine;
    this._collection = this.collection = new UI.ElementsCollection();
    this._overlayController = overlayController;
    this._fullWidgetItemIDPattern = new RegExp(XB._base.application.name + "\\.xb\\-(.+)\\-inst\\-(.+)");
    this._behaviours = {};
    
    this._logger.debug("Constructing");
}

UI.Builder.prototype = {
    makeWidget: function UIBuilder_makeWidget(WIID, widgetPrototype, toolbarElement) {
        let behaviour = new UI.Behaviour.Widget(widgetPrototype, toolbarElement, WIID, this);
        this._behaviours[WIID] = behaviour;
    },
    
    destroyWidget: function UIBuilder_destroyWidget(WIID) {
        if (WIID in this._behaviours) {
            this._behaviours[WIID].destroy();
            delete this._behaviours[WIID];
        }
    },
    registerExtActionsHelper: function UIBuilder_registerExtActionsHelper(handler) {
        if ( handler && (typeof handler.handleXBAction == "function") )
            this._extActionHandler = handler;
        else
            throw new TypeError("Provided helper does not support needed interface");
    },
    
    unregisterExtActionsHelper: function UIBuilder_registerExtActionsHelper(handler) {
        if (handler && this._extActionHandler === handler)
            this._extActionHandler = null;
    },
    
    finalize: function UIBuilder_finalize() {
        for (let WIID in this._behaviours)
            this.destroyWidget(WIID);
        this._collection.finalize();
    },
    
    handleDataChange: function UIBuilder_handleDataChange(widgetInstanceId, elementId, value) {
        this._logger.trace("handleDataChange: " + [widgetInstanceId, elementId, "" + value]);
        
        let behaviour = this._collection.get(widgetInstanceId, elementId);
        behaviour.update();
    },
    
    dispatchCommands: function UIBuilder_dispatchCommands(WIID, commandsArray, eventInfo) {
        for each (let command in commandsArray) {
            try {
                this._windowEngine.perform(WIID, command, eventInfo);
            }
            catch (e) {
                try {
                    let ENoActionHandler = this._windowEngine.constructor.ENoActionHandler;
                    if (e instanceof ENoActionHandler) {
                        if (!this._extActionHandler)
                            throw new ENoActionHandler("Extened actions handler is not registered");
                        this._extActionHandler.handleXBAction(WIID, command);
                    }
                    else
                        throw e;
                }
                catch (e) {
                    this._logger.error("Error running action " + [WIID, command].join(":") + ". " +
                                       strutils.formatError(e) );
                    this._logger.debug(e.stack);
                    break;
                }
            }
        }
    },
    
    cleanNode: function UIBuilder_cleanNode(node) {
        if (!node)
            return;
        let child = node.firstChild;
        while (child) {
            let next = child.nextSibling;
            this.cleanNode(child);
            this.removeNode(child);
            child = next;
        }
    },
    
    removeNode: function UIBuilder_removeNode(node) {
        if (node && node.parentNode)
            node.parentNode.removeChild(node);
    },
    
    resolveURI: function UIBuilder_resolveURI(spec, widgetInstanceId) {
        if (!spec)
            return "";
        
        if (!(/^http(s)?:\/\//i.test(spec) || spec.indexOf("data:image/") == 0))
            spec = this._windowEngine.getWidget(widgetInstanceId).prototype.unit.unitPackage.resolvePath(spec);
        
        let uri = misc.createFixupURIFromString(spec);
        return uri ? uri.spec : "";
    },
    
    get controller() {
        return this._overlayController;
    },
    _xid: undefined,
    _logger: null,
    _windowEngine: null,
    _collection: null,
    _overlayController: null,
    _fullWidgetItemIDPattern: null,
    _extActionHandler: null,
    _behaviours: null
};

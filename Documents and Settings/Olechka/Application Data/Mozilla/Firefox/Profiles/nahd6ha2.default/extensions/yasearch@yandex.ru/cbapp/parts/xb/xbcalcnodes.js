
XB._calcNodes = {};

XB._calcNodes.finalizeNodesInMap = function XBCN_finalizeNodesInMap(map) {
    for each (let node in map) {
        try {
            node.finalize();
        }
        catch (e) {
            XB._base.logger.error("Error while finalizing calc node. " + strutils.formatError(e));
            XB._base.logger.debug(e.stack);
        }
    }
};


XB._calcNodes.NodeBase = Base.extend({
    $name: "NodeBase",
    
    constructor: function NodeBase(baseUID) {
        if (!baseUID)
            throw new Error(XB._base.consts.ERR_UID_REQUIRED);
        this._baseUID = baseUID;
    },
    
    get baseID() {
        return this._baseUID;
    },
    
    set debugMode(value) {
        this._debugMode = !!value;
    },
    
    _baseUID: undefined,
    _storedValue: undefined,
    _debugMode: false
});


XB._calcNodes.ConstNodeProto = XB._calcNodes.NodeBase.extend({
    $name: "ConstNodeProto",
    
    constructor: function ConstNodeProto(baseUID, initVal) {
        this.base(baseUID);
        this._storedValue = initVal;
    },
    
    createInstance: function ConstNodeProto_createInstance(widgetInstance) {
        return new XB._calcNodes.ConstNode(this._baseUID, widgetInstance, this._storedValue);
    }
});

XB._calcNodes.FuncNodeProto = XB._calcNodes.NodeBase.extend({
    $name: "FuncNodeProto",
    
    constructor: function FuncNodeProto(baseUID, instanceClass) {
        if ( !instanceClass.inherits(XB._calcNodes.FuncNode) && !instanceClass.inherits(XB._calcNodes.ProcNode) )
            throw new CustomErrors.EArgType("instanceClass", "class(FuncNode|ProcNode)", instanceClass);
        this.base(baseUID);
        this._instanceClass = instanceClass;
        this._argsMap = { __proto__: null };
    },
    
    proposeArgName: function FuncNodeProto_proposeArgName() {
        let expectedArgNames = this._instanceClass.prototype.expectedArgNames;
        
        if (expectedArgNames) {
            for each (let argName in expectedArgNames) {
                if (!this.argumentAttached(argName))
                    return argName;
            }
        }
        
        return "param" + this._argsCount;
    },
    
    attachArgument: function FuncNodeProto_attachArgument(argName, argProto) {
        if ( !(argProto instanceof XB._calcNodes.NodeBase) )
            throw new CustomErrors.EArgType("argProto", "NodeBase", argProto);
        if (typeof argProto.createInstance != "function")
            throw new CustomErrors.EArgRange("argProto", "function createInstance", argProto.createInstance);
        this._argsMap[argName] = argProto;
        this._argsCount++;
    },
    
    argumentAttached: function FuncnodeProto_argumentAttached(argName) {
        return !!this._argsMap[argName];
    },
    
    createInstance: function FuncNodeProto_createInstance(widgetInstance) {
        return new this._instanceClass(this._baseUID, widgetInstance, this._argsMap, this._debugMode);
    },
    
    _argsMap: null,
    _argsCount: 0,
    _instanceClass: null
});

XB._calcNodes.BoundNode = XB._calcNodes.NodeBase.extend({
    $name: "BoundNode",
    
    constructor: function BoundNode(baseUID, owner) {
        if ( !("effectiveID" in owner ||
               "logger" in owner) )
            throw new TypeError("No CalcNodeOwner interface");
        this.base(baseUID);
        this._owner = owner;
        this._logger = this._owner.logger;
    },
    
    get effectiveID() {
        if (!this._effectiveID) {
            this._effectiveID = [this._owner.effectiveID, this._baseUID].join("_");
        }
        return this._effectiveID;
    },
    
    get owner() {
        return this._owner;
    },
    hasSubscribers: function BoundNode_hasSubscribers() {
        return false;
    },
    
    finalize: function BoundNode_finalize() {
        
    },
    
    _owner: null,
    _logger: null,
    
    _formatRuntimeError: function BoundNode__formatRuntimeError(e) {
        return XB._base.consts.ERR_RUNTIME_ERROR + " in node " + this._getHumanReadableID() + ". " + strutils.formatError(e);
    },
    
    _getHumanReadableID: function FuncNode__getHumanReadableID() {
        return this.$name + "(" + this.effectiveID + ")";
    }
});
XB._calcNodes.ConstNode = XB._calcNodes.BoundNode.extend({
    $name: "ConstNode",
    
    constructor: function ConstNode(baseUID, widget, initVal) {
        this.base(baseUID, widget);
        this._storedValue = initVal;
        if (XB._base.runtime.isXML(initVal))
            initVal.owner = this;
    },
    
    getValue: function ConstNode_getValue() {
        return this._storedValue;
    },
    
    unsubscribe: function ConstNode_unsubscribe() {
    },
    
    finalize: function ConstNode_finalize() {
        if (XB._base.runtime.isXML(this._storedValue))
            this._storedValue.dispose();
        this._storedValue = undefined;
    },

    freeze: function ConstNode_freeze() {
        throw new Error(XB._calcNodes.ConstNode.ERR_UNSUPPORTED_ACTION);
    },
    
    melt: function ConstNode_melt() {
        throw new Error(XB._calcNodes.ConstNode.ERR_UNSUPPORTED_ACTION);
    }
}, {
    ERR_UNSUPPORTED_ACTION: "ConstNode does not support this method"
});
XB._calcNodes.DynNode = XB._calcNodes.ConstNode.extend({
    $name: "DynNode",
    
    constructor: function DynNode(baseUID, widget, initVal) {
        this.base.apply(this, arguments);
        this._dependants = {};
    },
    
    getValue: function DynNode_getValue(subscriber) {
        if (subscriber)
            this._subscribe(subscriber);
        return this.base();
    },
    unsubscribe: function DynNode_unsubscribe(subscriber) {
        let subscriberID = subscriber.effectiveID;
        if ( !(subscriberID in this._dependants) )
            return;
        
        delete this._dependants[subscriberID];
        
        if (!this.hasSubscribers()) {
            if (this._debugMode) {
                let subID = (typeof subscriber._getHumanReadableID == "function")?
                    subscriber._getHumanReadableID(): subscriber.effectiveID;
                this._logger.debug("Node " + this._getHumanReadableID() + " lost all subscribers. Last one was " + subID);
            }
            try {
                this._notNeeded();
            }
            catch (e) {
                this._logger.error("Node " + this._getHumanReadableID() + " failed in _notNeeded. " +
                                                this._formatRuntimeError(e));
                this._logger.debug(e.stack);
            }
        }
    },
    
    hasSubscribers: function DynNode_hasSubscribers() {
        return !sysutils.isEmptyObject(this._dependants);
    },
    
    finalize: function DynNode_finalize() {
        try {
            this._dependants = {};
            this._notNeeded();
            this._setNewVal(XB.types.empty);
        }
        finally {
            this.base();
        }
    },
    
    _setNewVal: function DynNode__setNewVal(newVal) {
        let valuesDiffer;
        if (newVal !== undefined && this._storedValue !== undefined) {
            valuesDiffer = (XB._base.runtime.compareValues(newVal, this._storedValue,
                                                           XB._base.runtime.cmpModes.CMP_STRICT) != 0);
        }
        else
            valuesDiffer = (newVal !== this._storedValue);
        
        if (this._debugMode && (this._logger.level <= XB._base.application.core.Lib.Log4Moz.Level.Debug)) {
            this._logger.debug("Node " + this._getHumanReadableID() +
                " _setNewVal from " + (this._storedValue === undefined ?
                    "undefined" : XB._base.runtime.describeValue(this._storedValue)) +
                " to " + (newVal === undefined ?
                    "undefined" : XB._base.runtime.describeValue(newVal))  +
                ", differ: " + valuesDiffer);
            if (XB._base.runtime.isXML(newVal))
                this._logger.debug("new value is:" + newVal.toString());
        }
        
        if (valuesDiffer || newVal === undefined || this._storedValue === undefined) {
            if (XB._base.runtime.isXML(newVal)) {
                if (newVal.disposed && this._debugMode)
                    this._logger.warn(this._getHumanReadableID() + " got disposed XML");
            }
            if (XB._base.runtime.isXML(this._storedValue) && (this._storedValue.owner === this))
                this._storedValue.dispose();
            this._storedValue = newVal;
            if (XB._base.runtime.isXML(newVal) && !newVal.owner)
                newVal.owner = this;
        }
        else {
            if ( XB._base.runtime.isXML(newVal) && (newVal !== this._storedValue) ) {
                newVal.dispose();
            }
        }
        return valuesDiffer;
    },
    _subscribe: function DynNode__subscribe(subscriber) {
        this._dependants[subscriber.effectiveID] = subscriber;
    },
    
    _notifyDeps: function DynNode__notifyDeps() {
        try {
            this._freezeDeps();
        }
        finally {
            this._meltDeps(true);
        }
    },
    
    _freezeDeps: function DynNode__freezeDeps() {
        for each (let dependant in this._dependants)
            dependant.freeze();
    },
    
    _meltDeps: function DynNode__meltDeps(iChanged) {
        for each (let dependant in this._dependants) {
            try {
                dependant.melt(iChanged? this: null);
            }
            catch (e) {
                let depID = (typeof dependant._getHumanReadableID == "function")?
                    dependant._getHumanReadableID(): dependant.effectiveID;
                this._logger.error(this._getHumanReadableID() + " failed melting dependant node " +
                                   depID + ". " + strutils.formatError(e));
                this._logger.debug(e.stack);
            }
        }
    },
    
    _notNeeded: function DynNode__notNeeded() {
    },
    
    _dependants: null
});
XB._calcNodes.IVariable = {
    $name: "IVariable",
    
    setValue: function IVariable_setValue(newValue) {
        if (this._setNewVal(newValue)) {
            if (XB._base.runtime.isXML(newValue))
                newValue.owner = this;
            this._notifyDeps();
        }
    }
};
XB._calcNodes.IHasArguments = {
    constructor: function HasArgs(baseUID, widget, argsMap) {
        this.base(baseUID, widget);
        if (!(widget instanceof XB.WidgetPrototype || widget instanceof XB.WidgetInstance))
            throw new CustomErrors.EArgType("widget", "WidgetPrototype or WidgetInstance", widget);
        this._parentWidget = widget;
        
        this._argManager = new XB._calcNodes.FuncNode.ArgManager(this);
        for (let argName in argsMap) {
            let argProto = argsMap[argName];
            this._argManager.attachArgument(argName, argProto.createInstance(widget));
        }
    },
    
    unsubscribe: function HasArgs_unsubscribe(subscriber) {
        try {
            this.base(subscriber);
        }
        finally {
            if (!this.hasSubscribers())
                this._argManager.freeAll();
        }
    },
    
    finalize: function HasArgs_finalize() {
        try {
            this.base();
        }
        finally {
            this._argManager.freeAll();
        }
    },
    
    _parentWidget: null,
    _argManager: null
};
XB._calcNodes.ProcNodeBase = XB._calcNodes.ConstNode.extend(XB._calcNodes.IHasArguments);

XB._calcNodes.ProcNode = XB._calcNodes.ProcNodeBase.extend({
    $name: "ProcNode",
    
    perform: function ProcNode_perform(eventInfo) {
        this._proc(eventInfo);
    },
    
    getValue: function ProcNode_getValue() {
        return this._proc();
    }
});

XB._calcNodes.FuncNodeBase = XB._calcNodes.DynNode.extend(XB._calcNodes.IHasArguments);

XB._calcNodes.FuncNode = XB._calcNodes.FuncNodeBase.extend({
    $name: "FuncNode",
    
    constructor: function FuncNode(baseUID, widget, argsMap, debugMode) {
        this.base(baseUID, widget, argsMap);
        this._changedArgs = [];
        this._debugMode = !!debugMode;
    },
    
    freeze: function FuncNode_freeze() {
        if (!this.hasSubscribers()) {
            XB._base.logger.warn("Attempt to freeze " + this._getHumanReadableID() + ", which has no subscribers.");
            return;
        }
        this._freezeLevel++;
        this._freezeDeps();
    },
    
    melt: function FuncNode_melt(changedArgNode) {
        if (this._freezeLevel == 0) return;
        this._freezeLevel = Math.max(0, this._freezeLevel - 1);
        let hasSubscribers = this.hasSubscribers();
        if (changedArgNode)
            this._changedArgs.push(changedArgNode);
        let iChanged = false;
        try {
            if ((this._freezeLevel == 0) && (this._changedArgs.length > 0) && hasSubscribers) {
                let newVal = this._calculateSafely();
                this._changedArgs = [];
                iChanged = this._setNewVal(newVal);
            }
        }
        finally {
            this._meltDeps(iChanged);
        }
    },
    
    unsubscribe: function FuncNode_unsubscribe(subscriber) {
        if ( !(subscriber.effectiveID in this._dependants) ) return;
        try {
            this.base(subscriber);
            if (!this.hasSubscribers()) {
                this._freezeLevel = 0;
                this._setNewVal(undefined);
            }
        }
        finally {
            for (let i = this._freezeLevel; i > 0; i--)
                subscriber.melt(null);
        }
    },
    
    
    getValue: function FuncNode_getValue(subscriber) {
        let prevValue = this.base(subscriber);
        if (prevValue === undefined) {
            let newVal = this._calculateSafely();
            if (this.hasSubscribers())
                this._setNewVal(newVal);
            return newVal;
        }
        return prevValue;
    },
    _freezeLevel: 0,
    _changedArgs: null,
    _debugMode: false,
    
    _subscribe: function FuncNode__subscribe(subscriber) {
        try {
            this.base(subscriber);
        }
        finally {
            for (let i = this._freezeLevel; i > 0; i--)
                subscriber.freeze();
        }
    },
    
    _calculateSafely: function FuncNode__calculateSafely() {
        let val;
        this._argManager.resetUseStat();
        try {
            val = this._calculate(this._changedArgs);
        }
        catch (e) {
            if (e instanceof XB.types.Exception)
                val = e;
            else {
                val = XB._base.runtime.createXBExceptionFromRTError(this.effectiveID, e);
                if (this._debugMode) {
                    this._logger.debug(this._formatRuntimeError(e));
                    this._logger.debug(e.stack);
                }
            }
        }
        finally {
            this._argManager.freeUnused();
        }
        return val;
    }
});


XB._calcNodes.FuncNode.ArgManager = function FNArgManager(managedNode) {
    if ( !(managedNode instanceof XB._calcNodes.FuncNode || managedNode instanceof XB._calcNodes.ProcNode) )
        throw new CustomErrors.EArgType("managedNode", "FuncNode|ProcNode", managedNode);
    this._managedNode = managedNode;
    this._namedArgs = {};
    this._orderedArgs = [];
};

XB._calcNodes.FuncNode.ArgManager.prototype = {
    constructor: XB._calcNodes.FuncNode.ArgManager,
    
    attachArgument: function ArgMan_attachArgument(argName, argNode) {
        if (!argName)
            throw new CustomErrors.EArgRange("argName", "/.+/", argName);
        if ( !(argNode instanceof XB._calcNodes.BoundNode) )
            throw new CustomErrors.EArgType("argNode", "BoundNode", argNode);
        
        let argInfo = {
            node: argNode,
            used: false
        };
        this._argsCount = this._orderedArgs.push(argInfo);
        this._namedArgs[argName] = argInfo;
    },
    
    detachArgument: function ArgMan_detachArgument(argName) {
        let argInfo = this._getArgInfoByName(argName);
        argInfo.used = false;
        argInfo.node.unsubscribe(this._managedNode);
        delete this._namedArgs[argName];
    },
    
    resetUseStat: function ArgMan_resetUseStat() {
        for each (let argInfo in this._namedArgs)
            argInfo.used = false;
    },
    
    freeUnused: function ArgMan_freeUnused() {
        for each (let argInfo in this._namedArgs) {
            if (!argInfo.used)
                argInfo.node.unsubscribe(this._managedNode);
        }
    },
    
    freeAll: function ArgMan_freeAll() {
        for each (let argInfo in this._namedArgs) {
            argInfo.used = false;
            argInfo.node.unsubscribe(this._managedNode);
        }
    },
    
    argExists: function ArgMan_argExists(argName) {
        return (argName in this._namedArgs) && (typeof this._namedArgs[argName] == "object");
    },
    
    get argsNames() {
        return [argName for (argName in this._namedArgs)];
    },
    
    getValByName: function ArgMan_getValByName(argName, preferedType) {
        return this._processArgInfo(this._getArgInfoByName(argName), preferedType);
    },
    
    getValsByClass: function AtgMan_getValsByClass(className, preferedType) {
        let namePattern = new RegExp("^" + className + "\\.(.+)$");
        let retVal = {};
        for (let argName in this._namedArgs) {
            let match = argName.match(namePattern);
            if (match)
                retVal[match[1]] = this._processArgInfo(this._namedArgs[argName], preferedType);
        }
        return retVal;
    },
    
    getValByIndex: function ArgMan_getValByIndex(argIndex, preferedType) {
        return this._processArgInfo(this._getArgInfoByIndex(argIndex), preferedType);
    },
    
    getValByNameDef: function ArgMan_getValByNameDef(argName, preferedType, defaultValue) {
        if ( !this.argExists(argName) )
            return defaultValue;
        return this._processArgInfo(this._getArgInfoByName(argName), preferedType);
    },
    
    getValByIndexDef: function ArgMan_getValByIndexDef(argIndex, preferedType, defaultValue) {
        if ( !(argIndex in this._orderedArgs) )
            return defaultValue;
        return this._processArgInfo(this._getArgInfoByIndex(argIndex), preferedType);
    },
    findNodeByName: function ArgMan_findNodeByName(argName) {
        let argInfo = this._namedArgs[argName];
        if (argInfo) {
            argInfo.used = true;
            return argInfo.node;
        }
        return null;
    },
    findNodeByIndex: function ArgMan_findNodeByIndex(argIndex) {
        let argInfo = this._orderedArgs[argIndex];
        if (argInfo) {
            argInfo.used = true;
            return argInfo.node;
        }
        return null;
    },
    
    get argsCount() {
        return this._argsCount;
    },
    
    argInArray: function ArgMan_argInArray(argName, array) {
        if (!sysutils.isArray(array))
            throw new TypeError("Array expected");
        
        let argNode = this.findNodeByName(argName);
        if (!argNode)
            return false;
        
        for each (let item in array) {
            if (item === argNode)
                return true;
        }
        
        return false;
    },
    
    _consts: {
        ERR_NO_ARG: "No such argument"
    },
    _managedNode: null,
    _namedArgs: null,
    _orderedArgs: null,
    _argsCount: 0,
    
    _getArgInfoByName: function ArgMan__getArgInfoByName(argName) {
        if ( !(argName in this._namedArgs) )
            throw new Error(this._consts.ERR_NO_ARG + ": \"" + argName + "\"");
        return this._namedArgs[argName];
    },
    
    _getArgInfoByIndex: function ArgMan__getArgInfoByIndex(argIndex) {
        if ( !(argIndex in this._orderedArgs) )
            throw new Error(this._consts.ERR_NO_ARG + ": " + argIndex);
        return this._orderedArgs[argIndex];
    },
    
    _processArgInfo: function ArgMan__processArgInfo(argInfo, preferedType) {
        if (argInfo == undefined)
            throw new Error(this._consts.ERR_NO_ARG);
        argInfo.used = true;
        let argVal = argInfo.node.getValue(this._managedNode.hasSubscribers()? this._managedNode: null);
        if (argVal instanceof XB.types.Exception)
            throw argVal;
        
        switch (preferedType) {
            case "Number":
                return XB._base.runtime.xToNumber(argVal);
            case "String":
                return XB._base.runtime.xToString(argVal);
            case "Bool":
                return XB._base.runtime.xToBool(argVal);
            case "XML":
                return XB._base.runtime.xToXML(argVal);
            case "RequestData":
                return XB._base.runtime.xToRequestData(argVal);
            default:
                return argVal;
        }
    }
};

XB._calcNodes.Persistent = XB._calcNodes.DynNode.extend({
    $name: "Persistent",
    
    constructor: function FPersistentNode(baseUID, owner, defaultValue, persistSettings) {
        this._defaultValue = defaultValue || XB.types.empty;
        this.base(baseUID, owner, this._defaultValue);
        this._prefsModule = XB._base.application.core.Lib.Preferences;
        
        if (!persistSettings)
            return;
        if (!persistSettings.path || !persistSettings.category || !persistSettings.key)
            throw new CustomErrors.EArgRange("persistSettings", "{path, category, key}", sysutils.dump(persistSettings));
        this._prefFullPath = [persistSettings.path, persistSettings.category, persistSettings.key].join(".");
        
        let storedVal = this._prefsModule.get(this._prefFullPath);
        if (storedVal !== undefined) {
            this._dontWrite = true;
            try {
                this._setNewVal(storedVal);
            }
            finally {
                this._dontWrite = false;
            }
        }
        this._prefsModule.observe(this._prefFullPath, this);
    },
    
    finalize: function FPersistentNode_finalize() {
        if (this._prefFullPath)
            this._prefsModule.ignore(this._prefFullPath, this);
        this._dontWrite = true;
        this.base();
    },
    
    get defaultValue() {
        return this._defaultValue;
    },
    
    erase: function FPersistentNode_erase() {
        if (!this._prefFullPath)
            throw new Error("Persist settings are undefined");
        this._erasing = true;
        try {
            this._prefsModule.reset(this._prefFullPath);
        }
        finally {
            this._erasing = false;
        }
    },
    observe: function FPersistentNode_observe(subject, topic, data) {
        if (this._erasing) return;
        let prefPath = data;
        this._logger.debug(this._getHumanReadableID() + " observes " + topic);
        let value = this._prefsModule.get(prefPath, this._defaultValue);
        this._dontWrite = true;
        try {
            if (this._setNewVal(value))
                this._notifyDeps();
        }
        finally {
            this._dontWrite = false;
        }
    },
    
    _prefsModule: null,
    _prefFullPath: undefined,
    _dontWrite: false,
    _erasing: false,
    
    _setNewVal: function FPersistentNode__setNewVal(value) {
        if (value === undefined)
            return undefined;
        let changed = this.base(value);
        if (changed && !this._dontWrite && this._prefFullPath) {
            this._logger.debug("Persistent node " + this.effectiveID + " writes new value " + value);
            this._prefsModule.ignore(this._prefFullPath, this);
            try {
                if (!XB._base.runtime.isXML(value))
                    this._prefsModule.overwrite(this._prefFullPath, value);
            }
            finally {
                this._prefsModule.observe(this._prefFullPath, this);
            }
        }
        return changed;
    }
});

XB._calcNodes.Persistent.implement(XB._calcNodes.IVariable);

XB._calcNodes.SettingNode = XB._calcNodes.Persistent.extend({
    constructor: function FSettingNode(baseUID, owner, persistPath, name, defaultValue) {
        this.base(baseUID, owner, defaultValue, {path: persistPath, category: "settings", key: name});
    }
});
XB._calcNodes.VarNode = XB._calcNodes.Persistent.extend({
    constructor: function FPersistentVarNode(baseUID, widgetInstance, defaultValue, persistSettings) {
        if (persistSettings)
            persistSettings.category = "variables";
        this.base(baseUID, widgetInstance, defaultValue, persistSettings);
    }
});

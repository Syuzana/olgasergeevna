
XB.WidgetPrototype = BarPlatform.WidgetPrototypeBase.extend({
    constructor: function XBWidgetPrototype(protoID, name, unique, iconPath, unit) {
        this.base(protoID, name, unique, iconPath, unit);
        
        let (xbWidgetsPrefsPath = XB._base.application.core.xbWidgetsPrefsPath) {
            this._persistPath = xbWidgetsPrefsPath + this.id + ".all";
            this._pkgsPersistPath = xbWidgetsPrefsPath + this.pkg.id;
        }
        this._logger = XB._base.getLogger(protoID);
        
        this._varsMap = {};
        this._dataMap = {};
        this._settingsMap = {};
        this._refsMap = {};
        
        this._contentNodes = [];
        this._packageSettings = {};
        this._instSettings = {};
        this._instVars = [];
    },
    
    createInstance: function XBWidgetPrototype_createInstance(instanceID, widgetHost, instanceSettings) {
        let instPool = (instanceID in this._spawnedIDs)? this._spawnedIDs[instanceID] : this._makeInstPool(instanceID);
        let instance = new XB.WidgetInstance(instanceID, this, widgetHost, this._refsMap, this._dataMap, instPool.settings, instPool.vars);
        instPool.refCounter++;
        this._spawnedIDs[instanceID] = instPool;
        
        instance.applySettings(instanceSettings, true);
        
        return instance;
    },
    get widgetSettings() {
        let result = {};
        for (let settingName in this._settingsMap) {
            result[settingName] = this._settingsMap[settingName].controlElement;
        }
        return result;
    },
    get packageSettings() {
        let result = {};
        for (let settingName in this._packageSettings) {
            result[settingName] = this._packageSettings[settingName].controlElement;
        }
        return result;
    },
    get widgetSettings2() {
        return sysutils.copyObj(this._settingsMap);
    },
    get packageSettings2() {
        return sysutils.copyObj(this._packageSettings);
    },
    
    finalize: function XBWidgetPrototype_finalize() {
        this._finalizeSettingsMap(this._settingsMap);
        XB._calcNodes.finalizeNodesInMap(this._varsMap);
        
        for (let settingName in this._packageSettings) {
            try {
                this.pkg.removeSettingUser(settingName, this.id);
                this._packageSettings[settingName].node.finalize();
            }
            catch (e) {
                this._logger.error( strutils.formatString("Could not finalize setting \"%1\". %2",
                                                          [settingName, strutils.formatError(e)]) );
            }
        }
        this._packageSettings = null;
        
        this._settingsMap = null;
        this._varsMap = null;
        this._refsMap = null;
        
        this._instSettings = null;
        this._instVars = null;
        
        this._contentNodes = null;
        
        this.base();
    },
    get effectiveID() {
        return this.id;
    },
    
    get contentNodes() {
        return this._contentNodes.slice();
    },
    
    registerReference: function XBWIdgetPrototype_registerReference(refID, calcNode) {
        this._refsMap[refID] = calcNode;
    },
    
    addContentNode: function XBWidgetPrototype_addContentNode(DOMNode) {
        if (!(DOMNode instanceof Ci.nsIDOMNode))
            throw new CustomErrors.EArgType("DOMNode", "nsIDOMNode", DOMNode);
        this._contentNodes.push(DOMNode);
    },
    
    registerVariable: function XBWidgetPrototype_registerVariable(varName, varScope, persistent, initialValue) {
        XB._base.logger.trace("Widget prototype " + this.id + " registers var " + varName + ", scope " + varScope);
        if (this._varsMap[varName])
            XB._base.logger.warn("Widget " + this._id + " " + this._consts.WARN_VAR_REDEFINED + " " + varName);
        
        switch (varScope) {
            case BarPlatform.Unit.scopes.ENUM_SCOPE_WIDGET:
                let nodeID = this.id + "_var_" + varName;
                this._varsMap[varName] = new XB._calcNodes.VarNode(nodeID, this, initialValue,
                    persistent ? {path: this._persistPath, key: varName} : null);
                break;
            case BarPlatform.Unit.scopes.ENUM_SCOPE_INSTANCE:
                this._instVars.push({name: varName, persistent: persistent, initialValue: initialValue});
                break;
            default:
                throw new CustomErrors.EArgType("varScope", "ENUM_SCOPE_WIDGET | ENUM_SCOPE_INSTANCE", "" + varScope);
        }
    },
    
    findVariable: function XBWidgetPrototype_findVariable(varName) {
        return this._varsMap[varName] || null;
    },
    
    registerData: function XBWidgetPrototype_registerData(dataName, dataNode) {
        if (this._dataMap[dataName])
            XB._base.logger.warn("Widget " + this._id + " " + this._consts.WARN_DATA_REDEFINED + " " + dataName);
        this._dataMap[dataName] = dataNode;
    },
    
    registerSetting: function XBWidgetPrototype_registerSetting(settingName, settingScope, defaultValue, valueType, controlElement) {
        XB._base.logger.trace("Widget prototype " + this.id + " registers setting " + settingName + ", scope " + settingScope);
        
        switch (settingScope) {
            case BarPlatform.Unit.scopes.ENUM_SCOPE_PACKAGE: {
                let nodeID = this.pkg.id + "_setting_" + settingName;
                let settingNode = new XB._calcNodes.SettingNode(nodeID, this, this._pkgsPersistPath, settingName, defaultValue);
                this._packageSettings[settingName] = {node: settingNode, controlElement: controlElement, type: valueType};
                this.pkg.addSettingUser(settingName, this.id);
                break;
            }
            case BarPlatform.Unit.scopes.ENUM_SCOPE_WIDGET: {
                let nodeID = this.id + "_setting_" + settingName;
                let settingNode = new XB._calcNodes.SettingNode(nodeID, this, this._persistPath, settingName, defaultValue);
                this._settingsMap[settingName] = {node: settingNode, controlElement: controlElement, type: valueType};
                break;
            }
            case BarPlatform.Unit.scopes.ENUM_SCOPE_INSTANCE:
                this._instSettings[settingName] = {defaultValue: defaultValue, controlElement: controlElement, type: valueType};
                break;
            default:
                throw new CustomErrors.EArgType("settingScope", "ENUM_SCOPE_PACKAGE | ENUM_SCOPE_WIDGET | ENUM_SCOPE_INSTANCE", "" + settingScope);
        }
    },
    
    findSetting: function XBWidgetPrototype_findSetting(settingName) {
        let setting = this._settingsMap[settingName] || this._packageSettings[settingName];
        return setting? setting.node: null;
    },
    
    instanceFinalized: function XBWidgetPrototype_instanceFinalized(WIID) {
        let instPool = this._spawnedIDs[WIID];
        if (!instPool) {
            this._logger.warn("Somebody said that " + WIID + " instance is finalized but I don't remember this one.");
            return;
        }
        instPool.refCounter--;
        if (instPool.refCounter < 1) {
            XB._calcNodes.finalizeNodesInMap(instPool.vars);
            this._finalizeSettingsMap(instPool.settings);
            delete this._spawnedIDs[WIID];
        }
    },
    
    get logger() {
        return this._logger;
    },
    
    _consts : {
        WARN_VAR_REDEFINED: "Redefined variable",
        WARN_DATA_REDEFINED: "Redefined data"
    },
    
    _logger: null,
    _persistPath: undefined,
    
    _dataMap: null,
    _refsMap: null,
    _varsMap: null,
    _settingsMap: null,
    
    _contentNodes: null,
    
    _packageSettings: null,
    
    _instSettings: null,
    _instVars: null,
    
    _finalizeSettingsMap: function XBWidgetPrototype__finalizeSettingsMap(settingsMap) {
        for each (let setting in settingsMap) {
            try {
                setting.node.finalize();
            }
            catch (e) {
                this._logger.error("Error while finalizing setting node. " + strutils.formatError(e));
            }
        }
    },
    
    _makeInstPool: function XBWidgetPrototype__makeInstPool(instanceID) {
        let instPool = {settings: {}, vars: {}, refCounter: 0};
        let instPersistPath = XB._base.application.core.xbWidgetsPrefsPath + [this.id, instanceID].join(".");
        
        for (let settingName in this._instSettings) {
            let settingInfo = this._instSettings[settingName];
            let nodeID = instanceID + "_setting_" + settingName;
            let settingNode = new XB._calcNodes.SettingNode(nodeID, this, instPersistPath, settingName, settingInfo.defaultValue);
            instPool.settings[settingName] = {node: settingNode, controlElement: settingInfo.controlElement, type: settingInfo.type};
        }
        
        for each (let varInfo in this._instVars) {
            let varName = varInfo.name;
            let nodeID = instanceID + "_var_" + varName;
            instPool.vars[varName] = new XB._calcNodes.VarNode(nodeID, this, varInfo.initialValue,
                varInfo.persistent ? {path: instPersistPath, key: varName} : null);
        }
        
        return instPool;
    }
});
XB.WidgetInstance = Base.extend({
    constructor: function XBWidgetInstance(IID, proto, host, refsMap, dataMap, settingsInfo, varsInfo) {
        if ( !(proto instanceof XB.WidgetPrototype) )
            throw new CustomErrors.EArgType("proto", "XB.WidgetPrototype", proto);
        
        if (!IID)
            throw new CustomErrors.EArgRange("IID", "/.+/", IID);
        
        this._IID = IID;
        this._proto = proto;
        this._host = host;
        
        this._refsMap = {};
        for (let refID in refsMap)
            this._refsMap[refID] = refsMap[refID].createInstance(this);
        
        this._dataMap = {};
        for (let dataName in dataMap)
            this._dataMap[dataName] = dataMap[dataName].createInstance(this);
        
        this._settingsMap = settingsInfo;
        this._varsMap = varsInfo;
    },
    get id() {
        return this._IID;
    },
    
    get prototype() {
        return this._proto;
    },
    
    get host() {
        return this._host;
    },
    
    show: function XBWidgetInstance_show(toolbarElement) {
        this._host.overlayController.guiBuilder.makeWidget(this.id, this.prototype, toolbarElement);
    },
    
    hide: function XBWidgetInstance_hide() {
        this._host.overlayController.guiBuilder.destroyWidget(this.id);
    },
    get instanceSettings() {
        let result = {};
        for (let settingName in this._settingsMap) {
            result[settingName] = this._settingsMap[settingName].controlElement;
        }
        return result;
    },
    get instanceSettings2() {
        return sysutils.copyObj(this._settingsMap);
    },
    
    getSettingValue: function XBWidgetInstance_getSettingValue(settingName) {
        let settingNode = this.findSetting(settingName);
        if (!settingNode)
            throw new Error(strutils.formatString("No such setting (%1)", [settingName]));
        return settingNode.getValue();
    },
    
    applySetting: function XBWidgetInstance_applySetting(name, value) {
        this.findSetting(name).setValue(value);
    },
    
    applySettings: function XBWidgetInstance_applySettings(settingsMap, noFail) {
        for (let name in settingsMap) {
            try {
                this.applySetting(name, settingsMap[name]);
            }
            catch (e) {
                this.logger.error("Couldn't apply widget instance setting. " + strutils.formatError(e));
                if (!noFail)
                    throw e;
            }
        }
    },
    
    eraseSettings: function XBWidgetInstance_eraseSettings() {
        for each (let setting in this._settingsMap) {
            setting.node.erase();
        }
    },
    
    finalize: function XBWidgetInstance_finalize() {
        this.hide();
        
        XB._calcNodes.finalizeNodesInMap(this._refsMap);
        this._refsMap = null;
        XB._calcNodes.finalizeNodesInMap(this._dataMap);
        this._dataMap = null;
        
        this._settingsMap = null;
        this._varsMap = null;
        
        this.base();
        
        let proto = this._proto;
        this._proto = null;
        this._host = null;
        
        proto.instanceFinalized(this.id);
    },
    
    get effectiveID() {
        if (!this._effectiveID) {
            this._effectiveID = [this._host.id, this._IID].join("_");
        }
        return this._effectiveID;
    },
    
    findReference: function XBWidgetInstance_findReference(refID) {
        return this._refsMap[refID] || null;
    },
    
    findData: function XBWidgetInstance_findData(dataName) {
        return this._dataMap[dataName] || null;
    },
    
    findSetting: function XBWidgetInstance_findSetting(settingName) {
        let instSetting = this._settingsMap[settingName];
        return instSetting? instSetting.node: this._proto.findSetting(settingName);
    },
    
    findVariable: function XBWidgetInstance_findVariable(varName) {
        return this._varsMap[varName] || this._proto.findVariable(varName);
    },
    
    get logger() {
        return this.prototype.logger;
    },
    
    _IID: undefined,
    _proto: null,
    _host: null,
    _varsMap: null,
    _dataMap: null,
    _settingsMap: null,
    _refsMap: null
});

const EXPORTED_SYMBOLS = ["BarPlatform"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

Cu.import("resource://gre/modules/XPCOMUtils.jsm");

var sysutils,
    fileutils,
    strutils,
    misc,
    CustomErrors,
    Base;

const BarPlatform = {
    init: function BarPlatform_init(application) {
        this._application = application;
        
        let (appCoreLib = application.core.Lib) {
            sysutils = appCoreLib.sysutils;
            fileutils = appCoreLib.fileutils;
            strutils = appCoreLib.strutils;
            misc = appCoreLib.misc;
            CustomErrors = appCoreLib.CustomErrors;
            Base = appCoreLib.Base;
        };
        this._loggersRoot = application.name + ".CP";
        this._logger = application.core.Lib.Log4Moz.repository.getLogger(this._loggersRoot);
        this._loadModules();
    },
    
    finalize: function BarPlatform_finalize() {
        this._parsers = null;
        this._logger = null;
    },
    
    registerUnitParser: function BarPlatform_registerUnitParser(platformType, componentType, parser) {
        let parserKey = platformType + "#" + componentType;
        if (!parser)
            delete this._parsers[parserKey];
        else
            this._parsers[parserKey] = parser;
    },
    
    
    findPlatformMatch: function BarPlatform_findPlatformMatch(items) {
        if (!sysutils.isArray(items))
            throw new CustomErrors.EArgRange("items", "Array", items);
        
        let platformInfo = sysutils.platformInfo;
        for (let i = 0, len = items.length; i < len; i++) {
            let item = items[i];
            if (
                (("os" in item) && (item.os != platformInfo.os.name)) ||
                (("browser" in item) && (item.browser != platformInfo.browser.name)) ||
                (("architecture" in item) && (item.architecture != platformInfo.browser.simpleArchitecture))
            )
                continue;
            return item;
        }
        return null;
    },
    
    parseComponentID: function BarPlatform_parseComponentID(componentID) {
        let separatorPos = componentID.indexOf("#");
        let packageID = componentID.substring(0, separatorPos);
        if (!packageID)
            throw new Error(this._consts.ERR_INVALID_COMP_ID);
        let compName = componentID.substring(separatorPos + 1);
        if (!compName)
            throw new Error(this._consts.ERR_INVALID_COMP_ID);
        return [packageID, compName];
    },
    
    parsePrefPath: function BarPlatform_parsePrefPath(prefPath, compID) {
        let appCore = this._application.core;
        let [packageID, compName] = this.parseComponentID(compID);
        let partsRE = new RegExp( strutils.escapeRE(appCore.nativesPrefsPath + packageID) + "(\#(.+)\\.(\\w+))?\\.settings\\.(.+)");
        let partsMatch = prefPath.match(partsRE);
        if (!partsMatch)
            throw new Error("Invalid prefPath");
        let isPackagePref = !partsMatch[1],
            scope = partsMatch[3],
            isInstancePref = (!isPackagePref && (scope != "all")),
            settingName = partsMatch[4];
        let prefProps = {
            settingName: settingName,
            isPackagePref: isPackagePref,
            isComponentPref: !(isPackagePref || isInstancePref),
            isInstancePref: isInstancePref,
            instanceID: isInstancePref ? scope : undefined
        };
        return prefProps;
    },
    
    _parsers: {},
    _application: null,
    _loggersRoot: undefined,
    _logger: null,
    _consts: {
        ERR_INVALID_COMP_ID: "Invalid component ID"
    },
    
    _modules: [
        "preset.js", "manifest.js", "preset-with-manifest.js", "package.js", "unit.js"
    ],
    
    _loadModules: function BarPlatform__loadModules() {
        const mozSSLoader = Cc["@mozilla.org/moz/jssubscript-loader;1"].getService(Ci.mozIJSSubScriptLoader);
        const xbDirPath = this._application.partsURL + "platform/";
        this._modules.forEach(function BarPlatform_loadModule(moduleFileName) {
            this._logger.debug("  Loading module " + moduleFileName);
            mozSSLoader.loadSubScript(xbDirPath + moduleFileName);
        }, this);
    },
    
    _getLogger: function BarPlatform__getLogger(name) {
        return this._application.core.Lib.Log4Moz.repository.getLogger(this._loggersRoot + "." + name);
    },
    
    _getParser: function BarPlatform__getParser(platformType, componentType) {
        return this._parsers[platformType + "#" + componentType];
    }
};


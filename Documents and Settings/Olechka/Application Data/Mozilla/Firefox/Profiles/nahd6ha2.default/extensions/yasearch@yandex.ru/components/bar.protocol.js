Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

const ProtocolScheme = "bar";

const Cc = Components.classes;
const Ci = Components.interfaces;

const IOS = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);

function YaResProtocolHandler() {
  Components.utils.import("resource://yasearch-mod/bar.protocol.jsm", this);
  this.ProtocolHandler.scheme = ProtocolScheme;
  
  this._scheme = ProtocolScheme;
}

YaResProtocolHandler.prototype = {
  _scheme: "",
  
  get scheme() {
    return this._scheme;
  },
  
  get protocolFlags() {
    return Ci.nsIProtocolHandler.URI_DANGEROUS_TO_LOAD;
  },
  
  get defaultPort() {
    return -1;
  },
  
  allowPort: function YRPH_allowPort(port, scheme) {
    return false;
  },
  
  newURI: function YRPH_newURI(aSpec, aOriginalCharset, baseURI) {
    let uri = this.ProtocolHandler.newURI(aSpec, aOriginalCharset, baseURI);
    
    if (!uri)
      throw Components.results.NS_ERROR_MALFORMED_URI;
    
    return uri;
  },
  
  newChannel: function YRPH_newChannel(aURI) {
    let channel = this.ProtocolHandler.newChannel(aURI);
    
    if (!channel) {
      channel = IOS.newChannel("data:,", null, null);
    }
    
    channel.originalURI = aURI;
    
    return channel;
  },
  
  classDescription: "Yandex.Bar protocol handler",
  classID: Components.ID("69a91d28-4b81-11de-97de-eb99b362b158"),
  contractID: ("@mozilla.org/network/protocol;1?name=" + ProtocolScheme),
  QueryInterface: XPCOMUtils.generateQI([Ci.nsIProtocolHandler]),
  
  _xpcom_categories: [{
      category: "app-startup",
      service: true
  }]
}

if (XPCOMUtils.generateNSGetFactory)
  var NSGetFactory = XPCOMUtils.generateNSGetFactory([YaResProtocolHandler]);
else
  var NSGetModule = XPCOMUtils.generateNSGetModule([YaResProtocolHandler]);

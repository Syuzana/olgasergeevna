const EXPORTED_SYMBOLS = ["pacMan"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

const pacMan = {
    init: function PacMan_init(barApp) {
        this._logger = barApp.core.Lib.Log4Moz.repository.getLogger(barApp.name + ".PacMan");
        this._barApp = barApp;
        this._packagesInfo = {};
        this._cachedPackages = {};
        
        this.rescanPackages();
    },
    get packageIDs() {
        return [packageID for (packageID in this._packagesInfo)];
    },
    
    rescanPackages: function PacMan_rescanPackages() {
        this._logger.debug("Looking for packages...");
        
        this._unloadPackages();
        this._packagesInfo = {};
        let packagesDir = this._barApp.directories.XBPackages;
        let entries = packagesDir.directoryEntries;
        while (entries.hasMoreElements()) {
            let packageDir = entries.getNext().QueryInterface(Ci.nsIFile);
            if (packageDir.isDirectory()) {
                let packageInfo = this._checkPackageDir(packageDir);
                if (!packageInfo) {
                    this._barApp.core.Lib.fileutils.removeFileSafe(packageDir);
                    continue;
                }
                
                if (packageInfo.id in this._packagesInfo) {
                    this._logger.warn(this._barApp.core.Lib.strutils.formatString(
                        this._consts.MSG_DUPLICATE_PKG,
                        [packageInfo.id, this._packagesInfo[packageInfo.id].installDir.leafName, packageDir.leafName]));
                    this._barApp.core.Lib.fileutils.removeFileSafe(packageDir);
                    continue;
                }
                
                this._logger.debug(this._consts.STR_FOUND_PACKAGE + packageDir.leafName);
                this._packagesInfo[packageInfo.id] = packageInfo;
            }
        }
        if (this._extraPackagesDirs) {
            for each (let packageDir in this._extraPackagesDirs) {
                let packageInfo = this._checkPackageDir(packageDir);
                if (packageInfo) {
                    this._logger.debug(this._consts.STR_FOUND_PACKAGE + packageDir.leafName);
                    this._packagesInfo[packageInfo.id] = packageInfo;
                }
            }
        }
    },
    
    installPackage: function PacMan_installPackage(srcFile, packageInfo) {
        if (!(srcFile instanceof Ci.nsILocalFile))
            throw new this._barApp.core.Lib.CustomErrors.EArgType("srcFile", "nsILocalFile", typeof srcFile);
        
        packageInfo = this._barApp.core.Lib.sysutils.copyObj(packageInfo);
        this._validatePackageInfo(packageInfo);
        
        let packageDir;
        if (srcFile.isDirectory()) {
            packageDir = srcFile.clone();
        }
        else {
            packageDir = srcFile.clone();
            packageDir.createUnique(Ci.nsIFile.DIRECTORY_TYPE, parseInt("0755", 8));
            this._barApp.core.Lib.fileutils.extractZipArchive(srcFile, packageDir);
            srcFile.remove(true);
        }
        
        if (this.isPackageInstalled(packageInfo.id))
            this.uninstallPackage(packageInfo.id);
        
        let packagesDirectory = this._barApp.directories.XBPackages;
        let destDirName = this._makePkgDirName();
        
        let destPackageDir = packagesDirectory.clone();
        destPackageDir.append(destDirName);
        this._barApp.core.Lib.fileutils.removeFileSafe(destPackageDir.clone());
        
        try {
            packageDir.moveTo(packagesDirectory, destDirName);
            let metaInfoFile = destPackageDir.clone();
            metaInfoFile.append(this._consts.STR_PKGINFO_FILE_NAME);
            this._barApp.core.Lib.fileutils
                .writeTextFile(metaInfoFile, this._barApp.core.Lib.JSON.stringify(packageInfo));
        }
        catch (e) {
            this._logger.error("Could not write package files or metadata to its final location. " +
                               this._barApp.core.Lib.strutils.formatError(e));
            this._barApp.core.Lib.fileutils.removeFileSafe(destPackageDir);
            this._barApp.core.Lib.fileutils.removeFileSafe(srcFile);
            throw e;
        }
        packageInfo.installDir = destPackageDir;
        this._packagesInfo[packageInfo.id] = packageInfo;
    },
    
    
    uninstallPackage: function PacMan_uninstallPackage(packageID) {
        this.unloadPackage(packageID);
        this._barApp.core.Lib.fileutils.removeFileSafe(this._packagesInfo[packageID].installDir);
        delete this._packagesInfo[packageID];
    },
    
    isPackageInstalled: function PacMan_isPackageInstalled(packageID) {
        return !!this._packagesInfo[packageID];
    },
    
    getPackage: function PacMan_getPackage(packageID) {
        let pkg = this._cachedPackages[packageID];
        if (pkg)
            return pkg;
        
        let packageInfo = this.getPackageInfo(packageID);
        let package_ = new this._barApp.BarPlatform.WidgetPackage(packageInfo.installDir, packageID);
        this._cachedPackages[packageID] = package_;
        return package_;
    },
    unloadPackage: function PacMan_unloadPackage(packageID) {
        if (!this._packagesInfo[packageID])
            throw new Error(this._consts.ERR_NO_SUCH_PACKAGE + " " + packageID);
        let wl = this._barApp.widgetLibrary;
        if (wl)
            wl.flushWidgets(packageID);
        
        let pkg = this._cachedPackages[packageID];
        if (!pkg) return;
        pkg.finalize();
        delete this._cachedPackages[packageID];
    },
    
    reloadPackage: function PacMan_reloadPackage(packageID) {
        this.unloadPackage(packageID);
        this.getPackage(packageID);
    },
    finalize: function PacMan_finalize() {
        this._unloadPackages();
        this._cachedPackages = null;
        this._packagesInfo = null;
    },
    
    
    getPackageInfo: function PacMan_getPackageInfo(packageID) {
        let packageInfo = this._getPackageInstallInfo(packageID);
        packageInfo = this._barApp.core.Lib.sysutils.copyObj(packageInfo, false);
        packageInfo.installDir = packageInfo.installDir.clone();
        return packageInfo;
    },
    
    _unloadPackages: function PacMan__unloadPackages() {
        for (let packageID in this._cachedPackages) {
            try {
                this.unloadPackage(packageID);
            }
            catch (e) {
                this._logger.error("Error finalizing package " + packageID + ". " +
                                   this._barApp.core.Lib.strutils.formatError(e));
            }
        }
    },
    
    _getPackageInstallInfo: function PacMan__getPackageInstallInfo(packageID) {
        let packageInfo = this._packagesInfo[packageID];
        if (!packageInfo) {
            let errStr = this._consts.ERR_NO_SUCH_PACKAGE + " \"" + packageID + "\"";
            this._logger.warn("PacMan_getPackageInstallInfo: " + errStr);
            throw new Error(errStr);
        }
        return packageInfo;
    },
    
    _checkPackageDir: function PacMan__checkPackageDir(packageDir) {
        let dirName = "";
        
        try {
            dirName = packageDir.leafName;
            this._logger.trace("Checking directory " + dirName);
            
            let pkgInfoFile = packageDir.clone();
            pkgInfoFile.append(this._consts.STR_PKGINFO_FILE_NAME);
            
            if ( !(pkgInfoFile.exists() && pkgInfoFile.isReadable()) ) {
                this._logger.warn("Directory '" + dirName + "' does not contain a valid package");
                return null;
            }
            
            let packageInfo = this._barApp.core.Lib.JSON.parse( this._barApp.core.Lib.fileutils.readTextFile(pkgInfoFile) );
            this._validatePackageInfo(packageInfo);
            
            if (packageInfo.platformMin > this._barApp.core.version) {
                this._logger.warn("Package in directory '" + dirName + "' does not fit this platform");
                return null;
            }
            
            packageInfo.installDir = packageDir;
            return packageInfo;
        }
        catch (e) {
            this._logger.warn("An error occured while checking package directory " + dirName + ". " +
                              this._barApp.core.Lib.strutils.formatError(e));
            this._logger.debug(e.stack);
            return null;
        }
    },
    
    _validatePackageInfo: function PacMan__validatePackageInfo(packageInfo) {
        if (!this._barApp.core.Lib.sysutils.isObject(packageInfo))
            throw new this._barApp.core.Lib.CustomErrors.EArgType("packageInfo", "Object", packageInfo);
            
        for (let propName in this._metainfoPropNames)
            if (!(propName in packageInfo))
                throw new SyntaxError(this._consts.ERR_CORRUPT_PKGINFOFILE + " (" + propName + ")");
        
        for (let propName in packageInfo)
            if (!(propName in this._metainfoPropNames))
                delete packageInfo[propName];
    },
    
    _makePkgDirName: function PacMan__makePkgDirName() {
        return Cc["@mozilla.org/uuid-generator;1"].getService(Ci.nsIUUIDGenerator).generateUUID().toString();
    },
    
    _consts: {
        STR_PKGINFO_FILE_NAME: ".package.json",
        STR_FOUND_PACKAGE: "  Found a package in directory ",
        MSG_DUPLICATE_PKG: "  Package %1 was found in directories %2 and %3. Will remove the later.",
        
        ERR_NO_SUCH_PACKAGE: "No such package",
        ERR_CORRUPT_PKGINFOFILE: "Package information file is corrupt"
    },
    
    _metainfoPropNames: {id: 0, uri: 0, version: 0, platformMin: 0, __proto__: null},
    _barApp: null,
    _logger: null,
    _packagesInfo: null,
    _cachedPackages: null,
    _extraPackagesDirs: undefined
};

UI.Behaviour.Action = UI.Behaviour.extend({
    $name: "XB_UI_Action",
    
    name: "action",
    
    append: function XBUI_Action_append() {
    },
    
    build: function XBUI_Action_build() {
        this.commands = [];
        let command = this.attribute.command || "";
        if (command)
            this.commands.push(command);
        
        let element = this.element.firstChild;
        while (element) {
            if (
                (element.nodeType === this.element.ELEMENT_NODE) && 
                (element.localName === UI._consts.STR_VAL_REF_ELEM_NAME)
            )
                this.commands.push(element.getUserData(UI._consts.STR_VAL_REF_ID_KEY_NAME));
            
            element = element.nextSibling;
        }
    },
    
    action: function XBUI_Action_action(eventInfo) {
        if (this.commands)
            this.builder.dispatchCommands(this.root.widgetInstanceId, this.commands, eventInfo);
    }
});

UI.Behaviour.WithAction = UI.Behaviour.extend({
    create: function XBUI_WithAction_create() {
        this.base();
        let node = this.node;
        this.on(node, "command", this.onCommand, false, this);
        this.on(node, "click", this.onClick, false, this);
    },
    
    onCommand: function XBUI_WithAction_onCommand(event) {
        event.stopPropagation();
        event.preventDefault();
        
        this.onAction(event);
    },
    
    onClick: function XBUI_WithAction_onClick(event) {
        if (!(event.button == 1 && (this._url || this._action)))
            return;
        
        event.stopPropagation();
        event.preventDefault();
        
        this.onAction(event);
    },
    
    onAction: function XBUI_WithAction_onAction(event) {
        let eventInfo = {
            type: event.type,
            keys: {
                shift: event.shiftKey,
                meta: event.metaKey,
                ctrl: event.ctrlKey
            },
            mouse: {
                button: event.button
            }
        };
        
        if (!this.action || this.action(eventInfo)) {
            let order = ["url", "action"],
                listeners = {__proto__: null};
            
            for each (let listener in this.childrenEx(/^action|url$/))
                for each (let type in order)
                    if (!(type in listeners) && listener.name == type)
                        listeners[type] = listener;
            
            for (let type in listeners)
                listeners[type].action(eventInfo);
        }
    }
});
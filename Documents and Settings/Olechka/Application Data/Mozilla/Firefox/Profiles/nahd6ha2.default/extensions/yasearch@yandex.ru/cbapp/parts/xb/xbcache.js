
XB.cache = {
    _database: null,
    
    init: function XBCache_init() {
        this._logger = XB._base.getLogger("Cache");
        this._database = new XB.Database();
        this._database.open("xbcache.sqlite");
        this._database.exec("CREATE TABLE IF NOT EXISTS resources (id TEXT, time INTEGER, value BLOB, PRIMARY KEY(id))");
        this.clean();
    },
    
    finalize: function XBCache_finalize() {
        try {
            if (this._database)
                this._database.close();
        }
        catch (e) {
            this._logger.error("Failed closing database. " + e.message);
        }
        
        this._database = null;
        this._logger = null;
    },
    
    store: function XBCache_store(key, value) {
        if (sysutils.isObject(value))
            value = XB._base.application.core.Lib.JSON.stringify(value);
        let storeTime = this._currentTimestampSecs;
        this._logger.debug("Storing data with key " + key + " at time " + storeTime);
        this._database.exec("INSERT OR REPLACE INTO resources (id, time, value) VALUES (:id, :time, :value)", {
            id: key,
            time: storeTime,
            value: value.toString()
        });
    },
    
    findData: function XBCache_findData(key, notOlderThan, retStoreTime) {
        let treshold = notOlderThan? (this._currentTimestampSecs - notOlderThan): 0;
        this._logger.debug("Looking for data " + key + " not older than " + (new Date(treshold * 1000)) );
        let row = this._database.exec("SELECT * FROM resources WHERE id = :id AND time >= :treshold LIMIT 1", {
            id: key,
            treshold: treshold
        })[0];
        
        if (row) {
            if (sysutils.isObject(retStoreTime))
                retStoreTime.timestamp = row.time;
            return row.value;
        }
        else {
            this._logger.debug("No matching data found");
        }
    },
    
    remove: function XBCache_remove(key) {
        this._logger.debug("Deleting cache record with key " + key);
        this._database.exec("DELETE FROM resources WHERE (id = :id)", {id: key});
    },
    
    clean: function XBCache_clean() {
        this._database.exec("DELETE FROM resources WHERE (time < :time)", {
            time: (this._currentTimestampSecs - 3600 * 24 * 90)
        });
    },
    
    flush: function XBCache_flush() {
        this._database.exec("DELETE FROM resources");
    },
    
    get _currentTimestampSecs() {
        return parseInt(Date.now() / 1000, 10);
    }
};

UI.Behaviour.Widget = UI.XULProducer.extend({
    name: "widget",
    
    constructor: function XBUI_Widget(widgetPrototype, toolbarElement, WIID, builder) {
        this.base(null);
        this.logger = UI.getLogger("Behaviour");
        this._widgetProto = widgetPrototype;
        this.widgetInstanceId = WIID;
        this.builder = builder;
        this.node = toolbarElement;
        this.document = this.node.ownerDocument;
        
        this.node.setAttribute("xb-widget", "true");
        
        this._animateChildren(this._translateElements(this._widgetProto.contentNodes));
        this.built = true;
    },
    
    destroy: function XBUI_Widget_destroy() {
        for each (let child in this._children)
            child.destroy();
        
        this.builder.cleanNode(this.node);
        
        delete this.element;
        delete this.node;
        delete this.builder;
        delete this.document;
        delete this.logger;
    },
    
    change: function XBUI_Widget_change() {
        
    },
    
    prototype: function XBUI_Widget_prototype() {
        return this._widgetProto;
    }
});
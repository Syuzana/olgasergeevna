const EXPORTED_SYMBOLS = ["preinstaller"];

const { classes: Cc, interfaces: Ci, utils: Cu, results: Cr } = Components;

const preinstaller = {
    init: function Preinstaller_init(application) {
        this._application = application;
        this._logger = this._application.core.Lib.Log4Moz.repository.getLogger(this._application.name + ".Preinstaller");
        
        this._directory = this._application.directories.XBRoot;
        this._directory.append("incoming");
        if (!this._directory.exists()) {
            return;
        }
        try {
            this._install();
        }
        catch (e) {
            this._logger.error("Could not install incomming packages. " + e);
        }
    },
    
    addNewWidgets: function Preinstaller_addNewWidgets(controller) {
        if (!this._newComponentEntries)
            return;
        let spring = controller.getToolbarSpring();
        for (let i = this._newComponentEntries.length; i--;) {
            let widgetInfo = this._newComponentEntries[i];
            if (
                widgetInfo.type == "widget" &&
                widgetInfo.visible &&
                controller.getAppWidgetItems(undefined, widgetInfo.componentID).length == 0
            )
                controller.addWidgetItem(widgetInfo.componentID, widgetInfo.settings, spring, false, "all");
        }
    },
    
    get newPlugins() {
        let pluginEntries = [];
        
        if (!this._newComponentEntries)
            return pluginEntries;
        
        for (let i = 0, length = this._newComponentEntries.length; i < length; i++) {
            let entry = this._newComponentEntries[i];
            if (
                entry.type == "plugin" &&
                entry.enabled
            )
                pluginEntries.push(entry);
        }
        return pluginEntries;
    },
    _comparator: Cc["@mozilla.org/xpcom/version-comparator;1"].getService(Ci.nsIVersionComparator),
    
    _install: function Preinstaller__install() {
        let entries = this._directory.directoryEntries;
        let widgetLibrary = this._application.widgetLibrary;
        let first = null;
        
        this._newComponentEntries = [];
        
        while (entries.hasMoreElements()) {
            let file = entries.getNext().QueryInterface(Ci.nsIFile);
            if (!file.isDirectory())
                continue;
            
            if (!first || first.lastModifiedTime > file.lastModifiedTime)
                first = file;
            
            try {
                this._installEntry(file);
            }
            catch (e) {
                this._logger.error("Could not install incomming entry. " + e);
            }
        }
        
        let widgetIds = [],
            pluginIds = [],
            componentsInfo = [];
        for (let i = 0, length = this._newComponentEntries.length; i < length; i++) {
            let entry = this._newComponentEntries[i];
            let id = entry.componentID;
            
            if ((widgetIds.indexOf(id) == -1) && (pluginIds.indexOf(id) == -1)) {
                switch (entry.type) {
                    case "widget":
                        widgetIds.push(id);
                        break;
                    case "plugin":
                        pluginIds.push(id);
                        break;
                }
            }
        }
        
        widgetLibrary.acquaintWithWidgets(widgetIds, true);
        widgetLibrary.acquaintWithPlugins(pluginIds, true);
        
        if (first)
            this._installDefaultPreset(first);
        
        this._application.core.Lib.fileutils.removeFileSafe(this._directory);
    },
    
    _installEntry: function Preinstaller__installEntry(directory) {
        var presetFile = directory.clone();
        presetFile.append("default.xml");
        if (!presetFile.exists()) {
            this._logger.debug("Preinstalled default preset file not found. (Folder: " + directory.path + ")");
            return null;
        }
        
        var defaultPreset;
        try {
            defaultPreset = new this._application.BarPlatform.PresetWithManifest(presetFile);
        }
        catch(e) {
            this._logger.error("Failed parsing preinstalled default preset.\n" + this._application.core.Lib.strutils.formatError(e));
            return;
        }
        
        let packagesInfo = this._filterPackagesInfo(defaultPreset.packagesInfo);
        let packageManager = this._application.packageManager;
        for (let i = 0, length = packagesInfo.length; i < length; i++) {
            let packageInfo = packagesInfo[i];
            let id = packageInfo.id,
                install = true;
            if (packageManager.isPackageInstalled(id)) {
                let installedPackageInfo = packageManager.getPackageInfo(id);
                if (
                    (
                        this._comparator.compare(
                            installedPackageInfo.version,
                            packageInfo.version
                        ) >= 0
                    ) ||
                    (
                        packageInfo.platformMin &&
                        this._application.core.version > packageInfo.platformMin
                    )
                )
                    install = false;
            }
            
            if (install) {
                try {
                    let sourceFile = directory.clone();
                    sourceFile.append(packageInfo.file);
                    packageManager.installPackage(sourceFile, packageInfo);
                }
                catch (e) {
                    this._logger.error("Failed installing package '" + id + "'.\n" + this._application.core.Lib.strutils.formatError(e));
                }
            }
        }
        let widgetLibrary = this._application.widgetLibrary;
        let entries = defaultPreset.allEntries;
        let newComponentIds = [];
        for (let i = 0, length = entries.length; i < length; i++) {
            let entry = entries[i];
            if (!widgetLibrary.isKnownComponent(entry.componentID))
                this._newComponentEntries.push(entry);
        }
    },
    
    _installDefaultPreset: function Preinstaller__installDefaultPreset(directory) {
        
        if (!directory)
            return;
        try {
            var presetsDirectory = this._application.directories.XBPresets,
                fileName = "default.xml",
                presetFile = directory.clone();
            
            presetFile.append(fileName);
            
            var destination = presetsDirectory.clone();
            destination.append(fileName);
            
            if (!destination.exists())
                presetFile.copyTo(presetsDirectory, fileName);
        }
        catch (e) {
            this._logger.error("Failed installing default preset.\n" + this._application.core.Lib.strutils.formatError(e));
        }
    },
    
    _filterPackagesInfo: function Preinstaller__filterPackagesInfo(packagesInfo) {
        let platformInfo = this._application.core.Lib.sysutils.platformInfo;
        let filteredPackagesInfo = [],
            ids = {__proto__: null};
        for (let i = 0, length = packagesInfo.length; i < length; i++) {
            let packageInfo = packagesInfo[i];
            if (
                (packageInfo.browser && (packageInfo.browser != platformInfo.browser.name)) ||
                (packageInfo.os && (packageInfo.os != platformInfo.os.name)) ||
                (packageInfo.architecture && (packageInfo.architecture != platformInfo.browser.architecture))
            )
                continue;
            
            if (packageInfo.id in ids)
                continue;
            ids[packageInfo.id] = 1;
            filteredPackagesInfo.push(packageInfo);
        }
        return filteredPackagesInfo;
    }
};
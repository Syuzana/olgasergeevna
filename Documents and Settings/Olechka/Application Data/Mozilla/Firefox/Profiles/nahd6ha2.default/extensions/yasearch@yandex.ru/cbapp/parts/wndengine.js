const EXPORTED_SYMBOLS = ["WindowEngine"];
function WindowEngine(overlayController) {
    this._overlayController = overlayController;
    this._app = this._overlayController.application;
    this._eid = "WndEngine" + this._app.genWindowEngineID();
    this._logger = this._app.core.Lib.Log4Moz.repository.getLogger(this._app.name + "." + this._eid);
    this._widgetsMap = {__proto__: null};
};

WindowEngine.prototype = {
    constructor: WindowEngine,
    
    get overlayController() {
        return this._overlayController;
    },
    
    navigate: function XBWndEngine_navigate(navigateData, statActionID, componentInfo) {
        navigateData.sourceWindow = this._overlayController.window;
        if ((arguments.length > 1) && statActionID) {
            let packageInfo = this._app.packageManager.getPackageInfo(componentInfo.packageID);
            if (this._app.isTrustedPackageURL(packageInfo.uri))
                this._app.statLogger.logShortAction(statActionID);
        }
        this._app.navigate(navigateData);
    },
    
    setupWidget: function XBWndEngine_setupWidget(widgetInstanceOrToolbarItem) {
        this._app.openSettingsDialog(this._overlayController.window, widgetInstanceOrToolbarItem.id);
    },
    
    addWidget: function XBWndEngine_addWidget(widgetProto, settingsMap, relativeTo, placeAfter) {
        if (!(widgetProto instanceof this._app.XB.WidgetPrototype))
            throw new this._app.core.Lib.CustomErrors.EArgRange("widgetProto", "XB.WidgetPrototype", widgetProto);
        if (widgetProto.isUnique)
            throw new Error(this._app.core.Lib.strutils.formatString("Widget %1 is unique!", [widgetProto.id]));
        let widgetItem = this._overlayController.addWidgetItem(widgetProto.id, settingsMap, relativeTo, placeAfter);
        if (widgetItem && "wdgtInstanceID" in widgetItem)
            this.setupWidget(widgetItem);
    },
    
    removeWidget: function XBWndEngine_removeWidget(WIID) {
        this._overlayController.removeItem(WIID);
    },
    
    clear: function XBWndEngine_clear() {
        for (let WIID in this._widgetsMap) {
            try {
                this.destroyWidget(WIID);
            }
            catch (e) {
                this._logger.error(this._app.core.Lib.strutils.formatError(e));
            }
        }
        this._widgetsMap = {__proto__: null};
        
        this._overlayController = null;
    },
    
    hasWidget: function XBWndEngine_hasWidget(widgetIID) {
        return (widgetIID in this._widgetsMap);
    },
    createWidget: function XBWndEngine_createWidget(protoID, instID, settings, boundToolbarElement) {
        if (!instID)
            throw new this._app.core.Lib.CustomErrors.EArgRange("instID", "/.+/", instID);
        
        let widget;
        if (!this.hasWidget(instID)) {
            let widgetPrototype = this._app.widgetLibrary.getWidgetProto(protoID);
            widget = widgetPrototype.createInstance(instID, this, settings);
            this._widgetsMap[instID] = {widget: widget, toolbarElement: boundToolbarElement};
            widget.show(boundToolbarElement);
        } else {
            this._logger.warn("Widget with this ID (" + instID + ") already exists. Will use the old one.");
            widget = this.getWidget(instID);
            widget.applySettings(settings, true);
        }
        
        return widget;
    },
    
    destroyWidget: function XBWndEngine_destroyWidget(instID, eraseSettingsIfNeeded) {
        let widget = this.getWidget(instID);
        
        try {
            if (eraseSettingsIfNeeded) {
                let widgetProto = widget.prototype;
                if (widgetProto.isUnique && widgetProto.spawnedIDs.length == 1) {
                    try {
                        widget.eraseSettings();
                    }
                    catch (e) {
                        this._logger.error("Could not erase widget setting. " +
                                           this._app.core.Lib.strutils.formatError(e));
                    }
                }
            }
            widget.finalize();
        }
        finally {
            delete this._widgetsMap[instID];
        }
    },
    
    getWidget: function XBWndEngine_getWidget(widgetIID) {
        return this._getWidgetPair(widgetIID).widget;
    },
    
    getToolbarElement: function XBWndEngine_getToolbarElement(widgetIID) {
        return this._getWidgetPair(widgetIID).toolbarElement;
    },
    
    getValueAsString: function XBWndEngine_getValueAsString(widgetIID, valUID) {
        return this._safeGetStrNodeValue(this._getCalcNode(widgetIID, valUID));
    },
    
    getValue: function XBWndEngine_getValue(widgetIID, valUID) {
        return this._getCalcNode(widgetIID, valUID).getValue(this);
    },
    
    perform: function XBWndEngine_perform(widgetIID, valUID, eventInfo) {
        try {
            var procNode = this._getCalcNode(widgetIID, valUID);
        }
        catch (e) {
            throw new WindowEngine.ENoActionHandler(e.message);
        }
        if (procNode instanceof this._app.XB._calcNodes.ProcNode)
            procNode.perform(eventInfo);
        else
            procNode.getValue();
    },
    
    valueNotNeeded: function XBWndEngine_valueNotNeeded(widgetIID, valUID) {
        this._getCalcNode(widgetIID, valUID).unsubscribe(this);
    },
    freeze: function XBWndEngine_freeze() {
        
    },
    melt: function XBWndEngine_melt(changedNode) {
        if (!changedNode) return;
        try {
            this._notifyUIBuilder(changedNode);
        }
        catch (e) {
            this._logger.error("Failed notifying UI builder. " + this._app.core.Lib.strutils.formatError(e));
            if (e.stack)
                this._logger.debug(e.stack);
        }
    },
    
    get effectiveID() {
        return this.id;
    },
    
    get id() {
        return this._eid;
    },
    
    get logger() {
        return this._logger;
    },
    
    _consts: {
        ERR_XF_NOT_FOUND: "Requested value not found",
        ERR_WIDGET_NOT_FOUND: "Couldn't find widget with this ID (%1)",
        ERR_CANT_CONSTRUCT_WIDGET: "Failed creating widget"
    },
    _logger: null,
    _eid: undefined,
    _overlayController: null,
    _widgetsMap: null,
    
    _getHumanReadableID: function XBWndEngine__getHumanReadableID() {
        return this._eid;
    },
    
    _getWidgetPair: function XBWndEngine__getWidgetPair(widgetIID) {
        let widgetPair = this._widgetsMap[widgetIID];
        if (!widgetPair) {
            let errmsg = this._app.core.Lib.strutils.formatString(this._consts.ERR_WIDGET_NOT_FOUND, [widgetIID]);
            try {
                throw new Error(errmsg);
            }
            catch (e) {
                this._logger.error(errmsg);
                this._logger.debug(e.stack);
                throw e;
            }
        }
        return widgetPair;
    },
    
    _getCalcNode: function XBWndEngine__getCalcNode(widgetIID, valUID) {
        let calcNode =  this.getWidget(widgetIID).findReference(valUID);
        if (!calcNode)
            throw new Error(this._consts.ERR_XF_NOT_FOUND + " (" + [widgetIID, valUID] + ")");
        return calcNode;
    },
    
    _safeGetStrNodeValue: function XBWndEngine__safeGetStrNodeValue(node) {
        let strVal = "";
        try {
            strVal = this._app.XB._base.runtime.xToString(node.getValue(this));
        }
        catch (e) {
            this._logger.error("Converting value to string failed. " +
                               this._app.core.Lib.strutils.formatError(e) + ". Will return empty string.");
        }
        return strVal;
    },
    
    _notifyUIBuilder: function XBWndEngine__notifyUIBuilder(changedNode) {
        this._logger.trace("Notifying UI builder");
        this._overlayController.guiBuilder.handleDataChange(changedNode.owner.id,
                                                            changedNode.baseID,
                                                            changedNode.getValue(this));
    }
};

WindowEngine.ENoActionHandler = function XBWE_ENoActionHandler(msg) {
    this.name = "No action handler error";
    this.message = msg;
};

WindowEngine.ENoActionHandler.prototype = new Error();

const EXPORTED_SYMBOLS = ["overlayProvider"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

const overlayProvider = {
    init: function Overlay_init(barApplication) {
        this._application = barApplication;
        this._logger = barApplication.core.Lib.Log4Moz.repository.getLogger(barApplication.name + ".XULOverlay");
        this._commonItemPattern = new RegExp("^" + this._application.name + "\\.cb\\-(\\S+)\\-inst\\-(.+)$");
        this._defaultItemPattern = new RegExp("^" + this._application.name + "\\.cb\\-default\\-(\\d+)$");
        this._defaultSetIDs = {};
        this._unicodeConverter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
        this._unicodeConverter.charset = "UTF-8";
        this._DOMSerializer = Cc["@mozilla.org/xmlextras/xmlserializer;1"].getService(Ci.nsIDOMSerializer);
        this._overlayDocString = '\
            <!DOCTYPE overlay [ \
                <!ENTITY app.name "' + this._application.name + '"> \
                <!ENTITY % barDTD SYSTEM "chrome://' + this._application.name + '/locale/custombar.dtd"> \
                %barDTD; \
            ]> \
            <overlay xmlns="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul"> \
              <script type="application/x-javascript" src="chrome://&app.name;/content/custombar/overlay_ctrl.js"/> \
              <toolbarpalette id="BrowserToolbarPalette"/> \
              <toolbox id="navigator-toolbox"> \
                <toolbar id="&app.name;-bar" \
                         xb-app="&app.name;" \
                         toolbarname="&bar.name;" \
                         class="chromeclass-toolbar" \
                         context="toolbar-context-menu" \
                         hidden="false" \
                         mode="full" iconsize="small" defaulticonsize="small" lockiconsize="true" \
                         customizable="true" \
                         crop="end" \
                         defaultset=""/> \
              </toolbox> \
            </overlay>';
        
        Cc["@mozilla.org/network/protocol;1?name=" + this._application.name]
            .getService(Ci.nsIProtocolHandler)
            .wrappedJSObject
            .addDataProvider(this);
    },
    
    finalize: function Overlay_finalize() {
        this._overlayDoc = null;
    },
    newURI: function Overlay_newURI(aSpec, aOriginalCharset, aBaseURI) {
        if (!aSpec)
            return null;
        
        let content = "";
        try {
            if (aSpec.toLowerCase() == "browser-overlay")
                content = btoa(this._getBrowserOverlay());
        }
        catch (e) {
            this._logger.error(this._application.core.Lib.strutils.formatError(e));
            this._logger.debug(e.stack);
        }
        
        let uri =  Cc["@mozilla.org/network/simple-uri;1"].createInstance(Ci.nsIURI);
        uri.spec = "data:application/vnd.mozilla.xul+xml;base64," + content;
        return uri;
    },
    
    get currentSetIds() {
        if (!this._currentSetIds)
            this._currentSetIds = this._readCurrentSetIds();
        return this._currentSetIds;
    },
    
    parseWidgetItemId: function Overlay_parseWidgetItemId(itemID, fullMode) {
        let match;
        
        if (match = itemID.match(this._commonItemPattern))
            return {
                prototypeID: match[1],
                instanceID: match[2]
            };
        
        if (match = itemID.match(this._defaultItemPattern)) {
            let presetItemIndex = parseInt(match[1], 10);
            let widgetEntry = this._application.defaultWidgetEntries[presetItemIndex];
            if (widgetEntry) {
                let itemInfo = {
                    prototypeID: widgetEntry.componentID,
                    instanceID: 0,
                    isFromDefaultSet: true
                };
                
                if (fullMode) {
                    let instID;
                    if (presetItemIndex in this._defaultSetIDs)
                        itemInfo.instanceID = this._defaultSetIDs[presetItemIndex];
                    else
                        itemInfo.instanceID = this._defaultSetIDs[presetItemIndex] = this._application.getNewWidgetInstanceID();
                    itemInfo.settings = this._application.core.Lib.sysutils.copyObj(widgetEntry.settings);
                }
                return itemInfo;
            }
        }
        
        return null;
    },
    
    compileWidgetItemId: function Overlay_compileWidgetItemId(protoID, instanceID) {
        return this._application.name + ".cb-" + protoID + "-inst-" + instanceID;
    },
    
    widgetItemRemoved: function Overlay_widgetItemRemoved(instID) {
        for (let key in this._defaultSetIDs) {
            if (this._defaultSetIDs[key] == instID) {
                delete this._defaultSetIDs[key];
            }
        }
    },
    makePaletteItem: function Overlay_makePaletteItem(doc, widgetInfo, instanceID) {
        let toolbarItem;
        if (!Ci.nsIWorker) {
            let toolbox = doc.getElementById("navigator-toolbox");
            let palette = toolbox ? toolbox.palette : null;
            
            if (palette) {
                let existItem = palette.getElementsByTagName("toolbaritem")[0];
                if (existItem) {
                    toolbarItem = existItem.cloneNode(false);
                    Array.slice(toolbarItem.attributes).forEach(function(attr) toolbarItem.removeAttribute(attr.name));
                }
            }
        }
        
        if (!toolbarItem)
            toolbarItem = doc.createElementNS(this._consts.STR_XUL_NS, "toolbaritem");
        
        toolbarItem.setAttribute("id", this.compileWidgetItemId(widgetInfo.id, instanceID));
        toolbarItem.setAttribute("xb-proto-id", widgetInfo.id);
        toolbarItem.setAttribute("xb-app", this._application.name);
        toolbarItem.setAttribute("title", widgetInfo.name);
        toolbarItem.setAttribute("image", widgetInfo.iconPath ? widgetInfo.package_.resolvePath(widgetInfo.iconPath) : "");
        
        return toolbarItem;
    },
    
    makeDefaultSetItems: function Overlay_makeDefaultSetItems(document) {
        return this._makeDefaultSetItems(document);
    },
    _commonItemPattern: null,
    _defaultItemPattern: null,
    _defaultSetIDs: null,
    _unicodeConverter: null,
    _DOMSerializer: null,
    _overlayDocString: undefined,
    _overlayDoc: null,
    _logger: null,
    _consts: {
        STR_XUL_NS: "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul",
        ERR_CREATE_ITEM: "Could not add widget palette item"
    },
    
    _getBrowserOverlay: function Overlay__getBrowserOverlay() {
            this._overlayDoc = this._createBrowserOverlay();
        return this._unicodeConverter.ConvertFromUnicode(this._DOMSerializer.serializeToString(this._overlayDoc));
    },
    
    _createBrowserOverlay: function Overlay__createBrowserOverlay() {
        let start = Date.now();
        
        let DOMParser = this._application.core.Lib.misc.getDOMParser(null, null, true);
        
        let overlayDoc = DOMParser.parseFromString(this._overlayDocString, "text/xml"),
            toolbarPalette = overlayDoc.getElementById("BrowserToolbarPalette"),
            toolbar = overlayDoc.getElementById(this._application.name + "-bar");
        
        let [defaultSetItems, defaultSetIDs] = this._makeDefaultSetItems(overlayDoc);
        for (let i = 0, len = defaultSetItems.length; i < len; i++)
            toolbarPalette.appendChild(defaultSetItems[i]);
        
        toolbar.setAttribute("defaultset", defaultSetIDs.join(","));
        
        let paletteItems = this._makePaletteItems(overlayDoc);
        for (let i = 0, len = paletteItems.length; i < len; i++)
            toolbarPalette.appendChild(paletteItems[i]);
        
        this._logger.debug("Overlay created in " + (Date.now() - start) + "ms");
        
        return overlayDoc;
    },
    
    _makePaletteItems: function Overlay__makePaletteItems(overlayDoc) {
        let currentSetIDsData = this._readCurrentSetIds(),
            widgetLibrary = this._application.widgetLibrary,
            paletteItems = [];
        let widgetsInfo = widgetLibrary.getWidgetsInfo();
        for (let i = 0, len = widgetsInfo.length; i < len; i++) {
            let widgetInfo = widgetsInfo[i];
            let isUsed = false;
            
            let protoInstHash = currentSetIDsData[widgetInfo.id] || null;
            if (protoInstHash) {
                isUsed = true;
                for each (let instID in protoInstHash) {
                    paletteItems.push(this.makePaletteItem(overlayDoc, widgetInfo, instID));
                    this._application.getNewWidgetInstanceID();
                    
                    if (widgetInfo.isUnique)
                        break;
                }
            }
            
            if (!widgetInfo.isUnique || !isUsed) {
                paletteItems.push(this.makePaletteItem(overlayDoc, widgetInfo, 0));
            }
        }
        
        return paletteItems;
    },
    
    _makeDefaultSetItems: function Overlay__makeDefaultSetItems(overlayDoc) {
        let widgetLibrary = this._application.widgetLibrary,
            paletteItems = [],
            defaultSetIDs = [];
        
        
        let widgetEntries = this._application.defaultWidgetEntries,
            nativeWidgets = this._application.widgetLibrary.nativeWidgets,
            usedIDs = {};
        
        for (let i = 0, length = widgetEntries.length; i < length; i++) {
            let widgetEntry = widgetEntries[i],
                widgetInfo = null,
                protoID = widgetEntry.componentID,
                itemID;
            
            if (protoID in nativeWidgets) {
                itemID = nativeWidgets[protoID];
            } else {
                try {
                    widgetInfo = widgetLibrary.getWidgetInfo(protoID);
                    if (widgetInfo.isUnique && (protoID in usedIDs))
                        continue;
                }
                catch (e) {
                    this._logger.error(this._consts.ERR_CREATE_ITEM + ". " +
                                       this._application.core.Lib.strutils.formatError(e));
                    continue;
                }
                
                let paletteItem = this.makePaletteItem(overlayDoc, widgetInfo, 0);
                itemID = this._application.name + ".cb-default-" + i;
                paletteItem.setAttribute("id", itemID);
                paletteItem.setAttribute("xb-defset-app", this._application.name);
                paletteItems.push(paletteItem);
                
                usedIDs[protoID] = undefined;
            }
            
            defaultSetIDs.push(itemID);
        }
        
        return [paletteItems, defaultSetIDs];
    },
    _readCurrentSetIds: function Overlay__readCurrentSetIds() {
        let result = {};
        
        let rdfService = Cc["@mozilla.org/rdf/rdf-service;1"].getService(Ci.nsIRDFService);
        let localStoreDataSource = rdfService.GetDataSource("rdf:local-store");
        
        let allResources = localStoreDataSource.GetAllResources();
        let currentSetResource = rdfService.GetResource("currentset");
        while (allResources.hasMoreElements()) {
            let res = allResources.getNext().QueryInterface(Ci.nsIRDFResource);
            let tool = res.Value;
            
            if (tool) {
                let toolbarResource = rdfService.GetResource(tool);
                let currentSetTarget = localStoreDataSource.GetTarget(toolbarResource, currentSetResource, true);
                let currentSetStr = "";
                if (currentSetTarget instanceof Ci.nsIRDFLiteral)
                    currentSetStr = currentSetTarget.Value;
                
                if (currentSetStr) {
                    let currentSetIds = currentSetStr.split(",");
                    for (let i = 0, len = currentSetIds.length; i < len ; i++) {
                        let barWidgetIDMatch = currentSetIds[i].match(this._commonItemPattern);
                        if (barWidgetIDMatch) {
                            let widgetProtoID = barWidgetIDMatch[1];
                            let widgetInstance = barWidgetIDMatch[2];
                            let instArray = result[widgetProtoID];
                            if (!instArray) {
                                instArray = [];
                                result[widgetProtoID] = instArray;
                            }
                            instArray.push(widgetInstance);
                        }
                    }
                }
            }
        }
        return result;
    }
};

BarPlatform.PackageManifest = function XBPackageManifest(srcURL, XMLDocOrFile) {
    if (!sysutils.isString(srcURL))
        throw new CustomErrors.EArgType("srcURL", "String", srcURL);
    this._packageID = srcURL;
    this._baseURI = this._ioService.newURI(this._packageID, null, null);
    this._files = [];
    if (XMLDocOrFile instanceof Ci.nsIFile)
        this._loadFromFile(XMLDocOrFile);
    else if (XMLDocOrFile instanceof Ci.nsIDOMDocument)
        this._loadFromDocument(XMLDocOrFile);
    else
        throw new CustomErrors.EArgType("XMLDocOrFile", "nsIDOMDocument | nsIFile", XMLDocOrFile);
};

BarPlatform.PackageManifest.EPacManifestSyntax = CustomErrors.ECustom.extend({
    $name: "EPacManifestSyntax",
    
    constructor: function EPacManifestSyntax(elementName) {
        this.base("Package manifest parse error");
        this._elementName = elementName.toString();
    },
    
    get _details() {
        return [this._elementName];
    },
    
    _elementName: undefined
});

BarPlatform.PackageManifest.prototype = {
    constructor: BarPlatform.PackageManifest,
    
    get packageID() {
        return this._packageID;
    },
    
    _ioService: Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService),
    get id() {
        return this._packageID;
    },
    
    get packagesInfo() {
        let result = [];
        for each (let packageInfo in this._files) {
            result.push(sysutils.copyObj(packageInfo));
        }
        return result;
    },
    
    _packageID: undefined,
    _files: null,
    
    _loadFromFile: function XBPkgMan__loadFromFile(file) {
        this._loadFromDocument(fileutils.xmlDocFromFile(file));
    },
    
    _resolveUrl: function XBPkgMan__resolveUrl(spec) {
        try {
            var url = this._ioService.newURI(spec, null, this._baseURI);
        } catch(e) {
            return spec;
        }
        return url.spec;
    },
    
    _loadFromDocument: function XBPkgMan__loadFromDocument(document) {
        let root = document.documentElement;
        if (root.localName !== "manifest")
            throw new BarPlatform.PackageManifest.EPacManifestSyntax(root.nodeName);
        
        let children = root.childNodes;
        for (let i = 0, length = children.length; i < length; i++) {
            let packageElement = children[i];
            if ((packageElement.nodeType != packageElement.ELEMENT_NODE) || (packageElement.localName !== "package"))
                continue;
            
            let versionMin = parseInt( packageElement.getAttribute("platform-min"), 10 );
            if (isNaN(versionMin) || versionMin < 0)
                throw new BarPlatform.PackageManifest.EPacManifestSyntax(packageElement.nodeName);
            
            let packageVersion = packageElement.getAttribute("version") || "1.0";
            let packageInfo = {
                id: this._packageID,
                uri: this._packageID,
                version: packageVersion,
                fileURL: this._resolveUrl(packageElement.getAttribute("url")),
                platformMin: versionMin,
                browser: packageElement.getAttribute("browser") || undefined,
                architecture: packageElement.getAttribute("architecture") || undefined,
                os: packageElement.getAttribute("os") || undefined
            };
            
            this._files.push(packageInfo);
        }
    }
};

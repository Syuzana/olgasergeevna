let EXPORTED_SYMBOLS = ["barApplication"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

const barApplication = {
    init: function BarApp_init(core) {
        this._barCore = core;
        this._logger = this._barCore.Lib.Log4Moz.repository.getLogger(core.appName + ".App");
        this._dirs._barApp = this;
        this._defaultWidgets = [];
        this._defaultPlugins = [];
        this._wndControllerName = this.name + "OverlayController";
        
        this._init();
        
        this.widgetLibrary.activatePlugins();
        this._loadIncommingPlugins();
        this._migrateSettings();
        try {
            this._cleanupPreferences();
        }
        catch (e) {
            this._logger.error("Failed cleaning preferences. " + this._barCore.Lib.strutils.formatError(e));
        }
    },
    
    finalize: function BarApp_finalize() {
        let doFinalCleanup = this._parts.installer.isAddonUninstalling;
        this._finalizeParts();
        
        if (doFinalCleanup)
            this._finalCleanup();
        
        this._defaultWidgets = null;
        this._defaultPlugins = null;
        this._logger = null;
        this._barCore = null;
    },
    
    get defaultWidgetEntries() {
        return this._defaultWidgets;
    },
    
    get defaultPluginEntries() {
        return this._defaultPlugins;
    },
    
    get name() {
        return this._barCore.appName;
    },
    
    get defaultPresetURL() {
        return this._barCore.Lib.Preferences.get(this.name + this._consts.PREF_DEFAULT_PRESET_URL, null);
    },
    
    getNewWidgetInstanceID: function barApp_getNewWidgetInstanceID() {
        return "" + Date.now() + this._newWID++;
    },
    
    genWindowEngineID: function BarApp_genWindowEngineID() {
        return this._newWEID++;
    },
    get localeString() {
        if (!this._localeString) {
            let xulChromeReg = Cc["@mozilla.org/chrome/chrome-registry;1"].getService(Ci.nsIXULChromeRegistry);
            try {
                this._localeString = xulChromeReg.getSelectedLocale(this.name);
            } catch (ex) {
                this._localeString = xulChromeReg.getSelectedLocale("global");
            }
        }
        
        return this._localeString || "ru";
    },
    
    get core() {
        return this._barCore;
    },
    
    get directories() {
        return this._dirs;
    },
    
    get partsURL() {
        return "resource://" + this.name + "-app/parts/";
    },
    
    get packageManager() {
        return this._parts.pacMan;
    },
    
    get widgetLibrary() {
        return this._parts.widgetLibrary;
    },
    
    get overlayProvider() {
        return this._parts.overlayProvider;
    },
    
    get addonFS() {
        return this._parts.addonFS;
    },
    
    get BarPlatform() {
        return this._parts.BarPlatform;
    },
    
    get XB() {
        return this._parts.XB;
    },
    
    get UIBuilder() {
        return this._parts.UI.Builder;
    },
    
    get WindowEngine() {
        return this._parts.WindowEngine;
    },
    
    get StringBundle() {
        return this._parts.StringBundlePart.StringBundle;
    },
    
    get NativeComponents() {
        return this._parts.NativeComponents;
    },
    
    get statLogger() {
        return this._parts.statLogger;
    },
    
    restartComponents: function BarApp_restartComponents(packageID) {
        this.switchWidgets(packageID, false);
        let pluginsState = this.widgetLibrary.currentPluginsState;
        this.widgetLibrary.flushPlugins(packageID);
        
        this.packageManager.reloadPackage(packageID);
        
        this.widgetLibrary.setPluginsState(pluginsState);
        this.switchWidgets(packageID, true);
    },
    
    switchWidgets: function BarApp_switchWidgets(packageID, on) {
        this.forEachWindow(
            function(controller) {
                controller.switchWidgets(packageID, on);
            });
    },
    
    forEachWindow: function BarApp_forEachWindow(func, contextObj) {
        if (typeof func !== "function")
            throw new this._barCore.Lib.CustomErrors.EArgType("func", "Function", func);
        
        let browserWindows = this.core.Lib.misc.getBrowserWindows();
        for (let i = browserWindows.length; i--;) {
            let controller = browserWindows[i][this._wndControllerName];
            if (controller)
                func.call(contextObj, controller);
        }
    },
    
    installPreset: function BarApp_installPreset(url) {
        this._barCore.Lib.misc.openWindow({
            url: "chrome://" + this.name + "/content/custombar/dialogs/package-management/install/install.xul",
            features: "__popup__",
            name: "package-management-install",
            mode: "install",
            preset: url,
            application: this
        });
    },
    
    
    openSettingsDialog: function BarApp_openSettingsDialog(navigatorWindow) {
        let chromePath = "chrome://" + this.name + "/content/custombar/preferences/preferences.xul",
            windowClass = this.name + ":Preferences",
            resizeable = true,
            modal = !this._barCore.Lib.Preferences.get("browser.preferences.instantApply", false),
            windowArgs = Array.slice(arguments, 1),
            focusIfOpened = true;
        
        this._openWindow.apply(this, [navigatorWindow, chromePath, windowClass, focusIfOpened, resizeable, modal, windowArgs]);
    },
    
    openAboutDialog: function BarApp_openAboutDialog() {
        let chromePath = "chrome://" + this.name + "/content/about/about.xul",
            windowClass = this.name + ":AboutDialog",
            focusIfOpened = true;
        
        this._openWindow(null, chromePath, windowClass, focusIfOpened);
    },
    
    
    navigate: function BarApp_navigate(aNavigateData) {
        if (typeof aNavigateData != "object")
            throw new Error("Object required.");
        
        let url;
        let unsafe = false;
        
        if ("url" in aNavigateData) {
            url = aNavigateData.url;
        } else if ("unsafeURL" in aNavigateData) {
            url = aNavigateData.unsafeURL;
            unsafe = true;
        }
        if (!url)
            return false;
        let uri = this._barCore.Lib.misc.createFixupURIFromString(url);
        
        if (!uri)
            throw new this._barCore.Lib.CustomErrors.EArgRange("url", "URL", url);
        
        if (unsafe && !/^(http|ftp)s?$/.test(uri.scheme))
            throw new this._barCore.Lib.CustomErrors.ESecurityViolation("application.navigate", "URL=" + url);
        
        url = uri.spec;
        
        let target = ("target" in aNavigateData) ? aNavigateData.target : null;
        
        if (!target) {
            let eventInfo = ("eventInfo" in aNavigateData) ? aNavigateData.eventInfo : null;
            
            if (eventInfo) {
                if (eventInfo instanceof Ci.nsIDOMEvent) {
                    if (eventInfo.ctrlKey || eventInfo.metaKey || eventInfo.button == 1)
                        target = "new tab";
                    else if (eventInfo.shiftKey)
                        target = "new window";
                } else {
                    if (eventInfo.keys.ctrl || eventInfo.keys.meta || eventInfo.mouse.button == 1)
                        target = "new tab";
                    else if (eventInfo.keys.shift)
                        target = "new window";
                }
            }
        }
        
        let postData = ("postData" in aNavigateData) ? aNavigateData.postData : null;
        let referrer = ("referrer" in aNavigateData) ? aNavigateData.referrer : null;
        let sourceWindow = ("sourceWindow" in aNavigateData) ? aNavigateData.sourceWindow : null;
        if (!sourceWindow && target != "new popup") {
            sourceWindow = this.core.Lib.misc.getTopBrowserWindow();
            
            if (!sourceWindow) {
                let sa = Cc["@mozilla.org/supports-array;1"].createInstance(Ci.nsISupportsArray);
                
                let wuri = Cc["@mozilla.org/supports-string;1"].createInstance(Ci.nsISupportsString);
                wuri.data = url;
                
                let allowThirdPartyFixupSupports = Cc["@mozilla.org/supports-PRBool;1"].createInstance(Ci.nsISupportsPRBool);
                allowThirdPartyFixupSupports.data = false;
                
                sa.AppendElement(wuri);
                sa.AppendElement(null);
                sa.AppendElement(referrer);
                sa.AppendElement(postData);
                sa.AppendElement(allowThirdPartyFixupSupports);
                
                let windowWatcher = Cc["@mozilla.org/embedcomp/window-watcher;1"].getService(Ci.nsIWindowWatcher);
                windowWatcher.openWindow(null, "chrome://browser/content/browser.xul", null, "chrome,dialog=no,all", sa);
                return true;
            }
        }
        if (!target)
            target = "none";
        
        if (target != "new popup") {
            sourceWindow.Ya.loadURI(url, target, null, postData);
            return;
        }
        
        switch (target) {
            case "new tab":
                sourceWindow.gBrowser.loadOneTab(url, referrer, null, postData);
                break;
            
            case "new window":
                sourceWindow.openNewWindowWith(url, null, postData, false, referrer);
                break;
            
            case "new popup": {
                let windowProperties = ("windowProperties" in aNavigateData) ? aNavigateData.windowProperties : {};
                
                let title = ("title" in windowProperties) ? windowProperties.title : null;
                let wndWidth = Math.max(parseInt(windowProperties.width, 10) || 300, 50);
                let wndHeight = Math.max(parseInt(windowProperties.height, 10) || 300, 50);
                
                let winFeatures = "chrome,all,dialog=no,resizable,centerscreen,width=" + wndWidth + ",height=" + wndHeight;
                
                let args = {
                    url: url,
                    title: title,
                    postData: postData,
                    referrer: referrer
                };
                args.wrappedJSObject = args;
                
                let popupChromeURL = "chrome://" + this.name + "/content/custombar/dialogs/popup_browser/popup_browser.xul";
                if (sourceWindow) {
                    sourceWindow.openDialog(popupChromeURL, null, winFeatures, args);
                } else {
                    let windowWatcher = Cc["@mozilla.org/embedcomp/window-watcher;1"].getService(Ci.nsIWindowWatcher);
                    windowWatcher.openWindow(null, popupChromeURL, null, winFeatures, args);
                }
                
                break;
            }
            default:
                sourceWindow.gBrowser.loadURI(url, referrer, postData, false);
                break;
        }
        
        return true;
    },
    
    selectBestPackage: function BarApp_selectBestPackage(manifest) {
        if (!(manifest instanceof this.BarPlatform.PackageManifest))
            throw new this._barCore.Lib.CustomErrors.EArgType("manifest", "PackageManifest", manifest);
        
        let platformInfo = this._barCore.Lib.sysutils.platformInfo,
            coreVersion = this._barCore.version,
            bestPackageInfo = null,
            bestVersion = "0",
            bestPackageInfo2 = null,
            bestVersion2 = "0";
        
        const comparator = Components.classes["@mozilla.org/xpcom/version-comparator;1"]
                                     .getService(Components.interfaces.nsIVersionComparator);
        
        for each (let packageInfo in manifest.packagesInfo) {
            if (
                (packageInfo.browser && (packageInfo.browser != platformInfo.browser.name)) ||
                (packageInfo.os && (packageInfo.os != platformInfo.os.name)) ||
                (packageInfo.architecture && (packageInfo.architecture != platformInfo.browser.simpleArchitecture))
            )
                continue;
            
            if (packageInfo.platformMin <= coreVersion) {
                let newerPackage = (comparator.compare(packageInfo.version, bestVersion) > 0),
                    newerPlatform = bestPackageInfo && (packageInfo.platformMin > bestPackageInfo.platformMin);
                if (newerPackage || newerPlatform) {
                    bestPackageInfo = packageInfo;
                    bestVersion = packageInfo.version;
                }
            }
            else {
                if (comparator.compare(packageInfo.version, bestVersion2) > 0) {
                    bestPackageInfo2 = packageInfo;
                    bestVersion2 = packageInfo.version;
                }
            }
        }
        return [bestPackageInfo, bestPackageInfo2];
    },
    
    isYandexHost: function BarApp_isYandexHost(hostName) {
        return this._yandexHostsPattern.test(hostName);
    },
    
    isTrustedPackageURL: function BarApp_isTrustedPackageURL(url) {
        let ioService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
        return this.isTrustedPackageURI(ioService.newURI(url, null, null));
    },
    
    isTrustedPresetUri: function BarApp_isTrustedPresetUri(uri) {
        if (!(uri instanceof Ci.nsIURI))
            throw new this._barCore.Lib.CustomErrors.EArgType("uri", "nsIURI", uri);
        
        if ((uri.scheme != "http") && (uri.scheme != "https"))
            return false;
        
        return ((uri.host == this._consts.WIDGET_LIBRARY_HOST) && (uri.path.indexOf("/presets/approved/") == 0));
    },
    
    isTrustedPackageURI: function BarApp_isTrustedPackageURI(uri) {
        if (!(uri instanceof Ci.nsIURI))
            throw new this._barCore.Lib.CustomErrors.EArgType("uri", "nsIURI", uri);
        
        if ((uri.scheme != "http") && (uri.scheme != "https")) {
            return false;
        }
        
        let trust = ((uri.host == this._consts.WIDGET_LIBRARY_HOST) && (uri.path.indexOf("/packages/approved/") == 0)) ||
                    ((uri.host in this._consts.BAR_HOSTS) && (uri.path.indexOf("/packages/") == 0)) ||
                    (uri.host == this._consts.DOWNLOAD_HOST_NAME);
        

        
        return trust;
    },
    
    onNewBrowserReady: function BarApp_onNewBrowserReady(controller) {
        
        var isFirstWindow = (++this._navigatorID == 1);
        
        try {
            if (isFirstWindow) {
                this._parts.preinstaller.addNewWidgets(controller);
            }
        }
        catch (e) {
            this._logger.error("Could not place preinstalled widgets on toolbar. " + this._barCore.Lib.strutils.formatError(e));
        }
        
        try {
            let autoPresetURL = this._autoPresetURL;
            if (autoPresetURL && isFirstWindow) {
                let cookieManager = Cc["@mozilla.org/cookiemanager;1"].getService(Ci.nsICookieManager);
                cookieManager.remove("." + this._consts.AUTOPRESET_COOKIE_DOMAIN, this._consts.AUTOPRESET_COOKIE_NAME, "/", false);
                this.installPreset(autoPresetURL);
            }
        }
        catch (e) {
            this._logger.error("Could not check autoinstalled preset. " + this._barCore.Lib.strutils.formatError(e));
        }
    },
    
    onDefaultPresetUpdated: function BarApp_onDefaultPresetUpdated(preset) {
        try {
            this._defaultWidgets = preset.widgetEntries.filter(function(widgetEntry) widgetEntry.visible);
            this.forEachWindow(
                function (controller) {
                    controller.updateToolbarDefaultSet();
                });
            this._defaultPlugins = preset.pluginEntries;
        } catch (e) {
            this._logger.error("Could not update toolbars default set items. " + this.core.Lib.strutils.formatError(e));
        }
    },
    _consts: {
        PREF_DEFAULT_PRESET_URL: ".default.preset.url",
        DEF_PRESET_FILE_NAME: "default.xml",
        DOWNLOAD_HOST_NAME: "download.yandex.ru",
        BAR_HOSTS: {
            "bar.yandex.ru": 0,
            "toolbar.yandex.ru": 0
        },
        WIDGET_LIBRARY_HOST: "bar-widgets.yandex.ru",
        AUTOPRESET_COOKIE_DOMAIN: "bar.yandex.ru",
        AUTOPRESET_COOKIE_NAME: "bar-install-preset"
    },
    _barCore: null,
    _logger: null,
    _defaultWidgets: null,
    _defaultPlugins: null,
    _overlayProvider: null,
    _navigatorID: 0,
    _newWID: 0,
    _newWEID: 0,
    _localeString: null,
    _wndControllerName: undefined,
    _yandexHostsPattern: /(^|\.)yandex\.(ru|ua|by|kz|net|com)$/i,
    _dirs: {
        get XBRoot() {
            let dirFile = Cc["@mozilla.org/file/directory_service;1"]
                              .getService(Ci.nsIProperties)
                              .get("ProfD", Ci.nsIFile);
            dirFile.append(this.XBDirName);
            this._forceDir(dirFile);
            return dirFile;
        },
        
        get XBPackages() {
            let packagesDir = this.XBRoot;
            packagesDir.append("packages");
            this._forceDir(packagesDir);
            return packagesDir;
        },
        
        get XBPresets() {
            let presetsDir = this.XBRoot;
            presetsDir.append("presets");
            this._forceDir(presetsDir);
            return presetsDir;
        },
        
        get XBNativeStorage() {
            let storageDir = this.XBRoot;
            storageDir.append("native_storage");
            this._forceDir(storageDir);
            return storageDir;
        },
        
        get XBTemp() {
            let dirFile = this.XBRoot;
            dirFile.append("temp");
            this._forceDir(dirFile);
            return dirFile;
        },
        
        get XBDirName() {
            return this._xbDirName || (this._xbDirName = this._barApp.name + "-xb");
        },
        
        _barApp: null,
        
        _forceDir: function BarAppDirs_forceDir(dirFile, perm) {
            this._barApp.core.Lib.fileutils.forceDirectories(dirFile, perm);
        }
    },
    _parts: {},
    _partsNames: {
        installer:          "installer.js",
        addonFS:            "addonfs.js",
        StringBundlePart:   "strbundle.js",
        statLogger:         "statlogger.js",
        BarPlatform:        "platform.js",
        XB:                 "xb.js",
        UI:                 "uibuilder.js",
        NativeComponents:   "native_comps.js",
        pacMan:             "pacman.js",
        widgetLibrary:      "widgetlib.js",
        overlayProvider:    "overlay_prov.js",
        WindowEngine:       "wndengine.js",
        preinstaller:       "preinstaller.js",
        updater:            "update.js"
    },
    
    get _autoPresetURL() {
        const ioService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
        let autoPresetCookieURI = ioService.newURI("http://" + this._consts.AUTOPRESET_COOKIE_DOMAIN + "/", null, null);
        const cookieService = Cc["@mozilla.org/cookieService;1"].getService().QueryInterface(Ci.nsICookieService);
        let cookieString = cookieService.getCookieString(autoPresetCookieURI, null);
        
        let presetURL;
        if (cookieString) {
            let cookies = cookieString.split(";");
            for each (let cookie in cookies) {
                let separatorPos = cookie.indexOf("=");
                if (separatorPos == -1)
                    continue;
                
                let cookieName = this._barCore.Lib.strutils.trimAll( cookie.substring(0, separatorPos) );
                if (cookieName == this._consts.AUTOPRESET_COOKIE_NAME) {
                    presetURL = unescape( cookie.substring(separatorPos + 1) );
                    break;
                }
            }
        }
        
        this.__defineGetter__("_autoPresetURL", function _autoPresetURL() presetURL);
        return this._autoPresetURL;
    },
    
    _finalCleanup: function BarApp__finalCleanup() {
        this._logger.debug("Cleanup...");
        
        let dirsToRemove = [ this.directories.XBRoot ];
        for each (let dir in dirsToRemove) {
            try {
                dir.remove(true);
            } catch (e) {
                this._logger.error("Final cleanup: can't remove directory. " + this._barCore.Lib.strutils.formatError(e));
            }
        }
        
        let Preferences = this._barCore.Lib.Preferences;
        
        let prefBranches = [this._barCore.xbWidgetsPrefsPath,
                            this._barCore.nativesPrefsPath,
                            this._barCore.staticPrefsPath,
                            this.name + ".versions." ];
        for each (let prefBranch in prefBranches) {
            try {
                Preferences.resetBranch(prefBranch);
            } catch (e) {
                this._logger.error("Final cleanup: can't reset branch '" + prefBranch + "'. " +
                                   this._barCore.Lib.strutils.formatError(e));
            }
        }
        
        let prefs = [ this.name + this._consts.PREF_DEFAULT_PRESET_URL ];
        for each (let pref in prefs) {
            try {
                Preferences.reset(pref);
            } catch (e) {
                this._logger.error("Final cleanup: can't reset pref '" + pref + "'. " +
                                   this._barCore.Lib.strutils.formatError(e));
            }
        }
    },
    
    _init: function BarApp__init() {
        this._logger.config("Initializing...");
        let startTime = Date.now();
        this._loadParts();
        this._loadDefaultPreset();
        this._logger.config("Init done in " + (Date.now() - startTime) + "ms");
    },
    
    _migrateSettings: function BarApp__migrateSettings() {
        if (this._parts.installer.info.addonVersionState <= 0)
            return;
        
        this._logger.config("migration...");
        
        let Preferences = this._barCore.Lib.Preferences;
        let textonlyPluginId = "http://bar.yandex.ru/packages/yandexbar#textonly";
        let oldTextonlyPrefBranch = "yasearch.general.ui.textonly.";
        let introducedTextonly = Preferences.get(oldTextonlyPrefBranch + "introduced", false);
        if (introducedTextonly) {
            let textonlyPrefPath = this.NativeComponents.makeStaticBranchPath(textonlyPluginId);
            Preferences.set(textonlyPrefPath + "introduced", true);
            Preferences.reset(oldTextonlyPrefBranch + "introduced");
        }
        
        let enabledTextonly = Preferences.get(oldTextonlyPrefBranch + "enabled", null);
        if (enabledTextonly !== null) {
            let textonlyPlugin = this.widgetLibrary.getPlugin(textonlyPluginId);
            textonlyPlugin.enabled = enabledTextonly;
            Preferences.reset(oldTextonlyPrefBranch + "enabled");
        }
        
        if (this._parts.installer.info.addonLastVersion < "5.2.1") {
            try {
                this._parts.XB.cache.remove("http://export.yandex.ru/bar/quotelist.xml?ver=2#GET#1#100#399##");
            } catch (e) {}
        }
    },
    
    _loadParts: function BarApp__loadParts() {
        const Cu = Components.utils;
        const partsDirPath = "resource://" + this.name + "-app/parts/";
        
        for (let partName in this._partsNames) {
            let partFileName = this._partsNames[partName];
            let partPath = partsDirPath + partFileName;
            this._logger.debug("Loading " + partName + " part from " + partPath);
            Cu.import(partPath, this._parts);
            let part = this._parts[partName];
            if (!part)
                throw new Error("Part " + partName + " not loaded!");
            if (typeof part.init == "function")
                part.init(this);
        }
    },
    
    _finalizeParts: function BarApp__finalizeParts() {
        let revParts = this._barCore.Lib.misc.mapKeysToArray(this._parts).reverse();
        for each (let partName in revParts) {
            this._logger.debug("Finalizing " + partName + " part");
            let part = this._parts[partName];
            if (typeof part.finalize == "function") {
                try {
                    part.finalize();
                    this._parts[partName] = null;
                }
                catch (e) {
                    this._logger.error("Error finalizing part. " + this._barCore.Lib.strutils.formatError(e));
                    this._logger.debug(e.stack);
                }
            }
            
            delete this._parts[partName];
        }
    },
    
    _findDefaultPreset: function BarApp__findDefaultPreset() {
        let defaultPresetUrlPrefPath = this.name + this._consts.PREF_DEFAULT_PRESET_URL;
        let internalPresetPath = "$custombar/presets/" + this._consts.DEF_PRESET_FILE_NAME;
        let preferences = this.core.Lib.Preferences;
        let strutils = this.core.Lib.strutils;
        let fileutils = this._barCore.Lib.fileutils;
        let presetFile;
        try {
            let preset;
            let presetUrl = preferences.get(defaultPresetUrlPrefPath, null);
            
            if (!presetUrl)
                throw new Error("Can't get default preset preference value.");
            
            let presetFileName = encodeURIComponent(presetUrl);
            
            presetFile = this.directories.XBPresets;
            presetFile.append(presetFileName);
            
            try {
                if (this._parts.installer.info.addonVersionState != 0) {
                    let presetDoc = fileutils.xmlDocFromStream(this.addonFS.getStreamFromPath(internalPresetPath));
                    preset = new this.BarPlatform.Preset(presetDoc);
                    
                    if (preset.url == presetUrl) {
                        fileutils.removeFileSafe(presetFile);
                        this.addonFS.copySource(
                            internalPresetPath,
                            this.directories.XBPresets,
                            presetFileName,
                            parseInt("0755", 8)
                        );
                    }
                    else
                        preset = null;
                }
            }
            catch (e) {
                preset = null;
                
                this._logger.error("Could not restore default preset from extension core. " +
                                   this._barCore.Lib.strutils.formatError(e));
            }
            
            if (!preset)
                preset = new this.BarPlatform.Preset(presetFile, presetUrl);
            
            return preset;
        }
        catch (e) {
            let presetFilePath = presetFile ? presetFile.path : "no file";
            this._logger.debug( strutils.formatString(
                "Failed parsing normal default preset (%1).\n %2", [presetFilePath, strutils.formatError(e)]) );
            
            if (presetFile)
                fileutils.removeFileSafe(presetFile);
            let extPresetFile = this.directories.XBPresets;
            extPresetFile.append(this._consts.DEF_PRESET_FILE_NAME);
            try {
                let preset = new this.BarPlatform.Preset(extPresetFile);
                try {
                    extPresetFile.moveTo(null, encodeURIComponent(preset.url));
                    preferences.set(defaultPresetUrlPrefPath, preset.url);
                }
                catch (e) {
                    this._logger.error("Could not set external default preset as active. " +
                                       this._barCore.Lib.strutils.formatError(e));
                }
                return preset;
            }
            catch (e) {
                if (extPresetFile.exists()) {
                    this._logger.debug("Failed parsing external default preset.\n" + this._barCore.Lib.strutils.formatError(e));
                    fileutils.removeFileSafe(extPresetFile);
                }
                try {
                    let presetDoc = fileutils.xmlDocFromStream(this.addonFS.getStreamFromPath(internalPresetPath));
                    let preset = new this.BarPlatform.Preset(presetDoc);
                    
                    try {
                        this.addonFS.copySource(
                            internalPresetPath,
                            this.directories.XBPresets,
                            encodeURIComponent(preset.url),
                            parseInt("0755", 8)
                        );
                        preferences.set(defaultPresetUrlPrefPath, preset.url);
                    }
                    catch (e) {
                        this._logger.error("Could not extract internal preset.\n" + this._barCore.Lib.strutils.formatError(e));
                    }
                    return preset;
                }
                catch (e) {
                    this._logger.fatal("Failed parsing internal default preset.\n" + this._barCore.Lib.strutils.formatError(e));
                }
            }
        }
        
        return null;
    },
    
    _loadDefaultPreset: function BarApp__loadDefaultPreset() {
        this._logger.config("Loading application default preset");
        
        let preset = this._findDefaultPreset();
        if (preset) {
            this._defaultWidgets = preset.widgetEntries.filter(function(widgetEntry) widgetEntry.visible);
            this._defaultPlugins = preset.pluginEntries;
        }
    },
    _loadIncommingPlugins: function BarApp__loadIncommingPlugins() {
        let entries = this._parts.preinstaller.newPlugins;
        if (!entries || !entries.length)
            return;
        this._logger.debug("Loading incomming plugins:\n" + this._barCore.Lib.sysutils.dump(entries));
        let strutils = this._barCore.Lib.strutils;
        for each (let entry in entries) {
            try {
                let plugin = this.widgetLibrary.getPlugin(entry.componentID);
                plugin.applySettings(entry.settings);
                plugin.enabled = true;
            }
            catch (e) {
                this._logger.error(strutils.formatString("Could not activate incomming plugin (%1). %2",
                                                         [strutils.formatError(e)]));
            }
        }
    },
    _cleanupPreferences: function BarApp__clearPreferences() {
        let currentSetData = this.overlayProvider.currentSetIds;
        if (this._barCore.Lib.sysutils.isEmptyObject(currentSetData))
            return;
        
        let prefService = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefService);
        let settingKeyPattern = /^(.+#.+)\.(\d+)\..+$/;
        
        function checkBranch(prefBranch) {
            for each (let key in prefBranch.getChildList("", {})) {
                let keyMatch = key.match(settingKeyPattern);
                if (!keyMatch)
                    continue;
                
                let prefProtoID = keyMatch[1];
                let prefInstID = keyMatch[2];
                let instArray = currentSetData[prefProtoID];
                if (!instArray) {
                    prefBranch.deleteBranch(prefProtoID);
                }
                else {
                    if (instArray.indexOf(prefInstID) < 0) {
                        let settingKey = prefProtoID + "." + prefInstID;
                        prefBranch.deleteBranch(settingKey);
                    }
                }
            }
        }
        
        checkBranch( prefService.getBranch(this._barCore.xbWidgetsPrefsPath) );
        checkBranch( prefService.getBranch(this._barCore.nativesPrefsPath) );
    },
    
    
    _openWindow: function BarApp__openWindow(navigatorWindow, path, windowClass, focusIfOpened,
                                             resizeable, modal, windowArgs) {
        var baseNameMatch = path.match(/(\w+)\.x[um]l$/i);
        windowClass = windowClass || (this.name + baseNameMatch? (":" + baseNameMatch[0]): "");
        
        if (focusIfOpened) {
            let chromeWindow = this._barCore.Lib.misc.getTopWindowOfType(windowClass);
            if (chromeWindow) {
                chromeWindow.focus();
                return chromeWindow;
            }
        }
        
        let features = ["chrome", "titlebar", "toolbar", "centerscreen", (modal ? "modal" : "dialog=no")];
        if (resizeable) features.push("resizable");
        
        let ownerWindow = navigatorWindow || this._barCore.Lib.misc.getTopBrowserWindow();
        let openParams = [path, windowClass, features.join()].concat(windowArgs);
        
        return ownerWindow.openDialog.apply(ownerWindow, openParams);
    }
};

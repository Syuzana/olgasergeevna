let EXPORTED_SYMBOLS = ["AddonManager"];

const {
  classes: Cc,
  interfaces: Ci,
  results: Cr,
  utils: Cu
} = Components;

const OBSERVER_SERVICE = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);

function AddonItemWrapper(aAddon) {
  this._addon = aAddon;
}

AddonItemWrapper.prototype = {
  get id() {
    return this._addon.id;
  },
  get userDisabled() {
    let enabled = false;
    
    const RDFService = Cc["@mozilla.org/rdf/rdf-service;1"].getService(Ci.nsIRDFService);
    let itemResource = RDFService.GetResource("urn:mozilla:item:" + this.id);
    
    if (itemResource) {
      let ds = AddonManager._ExtensionManager.datasource;
      let target = ds.GetTarget(itemResource, RDFService.GetResource("http://www.mozilla.org/2004/em-rdf#isDisabled"), true);
      if (target && target instanceof Ci.nsIRDFLiteral)
        enabled = (target.Value != "true");
    }
    
    return !enabled;
  },
  
  set userDisabled(val) {
    if (val)
      AddonManager._ExtensionManager.enableItem(this.id);
    else
      AddonManager._ExtensionManager.disableItem(this.id);
  },
  get scope() {
    let scope = AddonManager.SCOPE_PROFILE;
    
    switch (this._addon.installLocationKey) {
      case "app-global":
        scope = AddonManager.SCOPE_APPLICATION;
        break;
      
      default:
        break;
    }
    
    return scope;
  }
}

const AddonManager = {
  SCOPE_PROFILE: 1,
  SCOPE_APPLICATION: 4,
  
  _started: false,
  
  _applyCallback: function AM__applyCallback(aCallback) {
    try {
      aCallback.apply(null, Array.slice(arguments, 1));
    } catch (e) {
      Cu.reportError(e);
    }
  },
  get _AddonManager() {
    let loader = {};
    try {
      Cu.import("resource://gre/modules/AddonManager.jsm", loader);
    } catch (e) {}
    
    delete this._AddonManager;
    
    if (!("AddonManager" in loader)) {
      this._AddonManager = null;
      
      if (!this._ExtensionManager)
        throw new Error("AddonManager: can't find native AddonManager");
      
    } else {
      this._AddonManager = loader.AddonManager;
    }
    
    return this._AddonManager;
  },
  get _ExtensionManager() {
    delete this._ExtensionManager;
    
    if (!("nsIExtensionManager" in Ci))
      return (this._ExtensionManager = null);
    
    return (this._ExtensionManager = Cc["@mozilla.org/extensions/manager;1"].getService(Ci.nsIExtensionManager));
  },
  _getInstallRdfContent: function AM__getInstallRdfContent(aAddonDirectory) {
    let installRDFFile = aAddonDirectory;
    installRDFFile.append("install.rdf");
    
    let content = "";
    
    try {
      let inputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
      inputStream.init(installRDFFile, 0x01, 0, inputStream.CLOSE_ON_EOF);
      
      let fileSize = inputStream.available();
      let cvstream = Cc["@mozilla.org/intl/converter-input-stream;1"].createInstance(Ci.nsIConverterInputStream);
      cvstream.init(inputStream, "UTF-8", fileSize, Ci.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER);
      
      let data = {};
      cvstream.readString(fileSize, data);
      content = data.value;
      
      cvstream.close();
    } catch (e) {
      Cu.reportError(e);
    }
    
    return content;
  },
  getAddonVersion: function AM_getAddonVersion(aAddonDirectory) {
    let installRdfContent = this._getInstallRdfContent(aAddonDirectory);
    
    if (installRdfContent) {
      let version = installRdfContent.match(/<em:version>([^<]*)<\/em:version>/);
      
      if (version && /^\d+\.\d+\.\d+/.test(version[1]))
        return version[1];
    }
    
    throw new Error("AddonManager: can't get addon version from install.rdf");
  },
  getAddonId: function AM_getAddonId(aAddonDirectory) {
    let installRdfContent = this._getInstallRdfContent(aAddonDirectory);
    
    if (installRdfContent) {
      let addonId = installRdfContent.match(/<em:id>([^<]*)<\/em:id>/);
      
      if (addonId && addonId[1])
        return addonId[1];
    }
    
    throw new Error("AddonManager: can't get addon id from install.rdf");
  },
  
  disableAddonByID: function AM_disableAddonByID(aAddonId) {
    if (this._AddonManager) {
      this._AddonManager.getAddonByID(aAddonId, function AM_disableAddonByID_callback(aAddon) {
        if (aAddon)
          aAddon.userDisabled = true;
      });
    } else {
      this._ExtensionManager.disableItem(aAddonId);
    }
  },
  
  getAddonsByIDs: function AM_getAddonsByIDs(aIds, aCallback) {
    if (this._AddonManager) {
      this._AddonManager.getAddonsByIDs(aIds, aCallback);
    } else {
      let addons = this._ExtensionManager
                       .getItemList(Ci.nsIUpdateItem.TYPE_EXTENSION, {})
                       .filter(function(aAddon) aIds.indexOf(aAddon.id) != -1)
                       .map(function(aAddon) new AddonItemWrapper(aAddon));
      
      this._applyCallback(aCallback, addons);
    }
  },
  
  EM_ACTION_REQUESTED_TOPIC: "em-action-requested",
  _addonListeners: [],
  
  startup: function AM_startup() {
    if (this._started)
      return;
    
    this._started = true;
    
    let me = this;
    
    ["onEnabling", "onEnabled", "onDisabling", "onDisabled",
     "onUninstalling", "onUninstalled", "onInstalled",
     "onOperationCancelled", "onUpdateAvailable", "onUpdateFinished",
     "onCompatibilityUpdateAvailable"
    ].forEach(function(aEvent) {
      me[aEvent] = function() {
        me._notifyAddonListeners.apply(me, [aEvent].concat(Array.slice(arguments)));
      };
    });
    
    if (this._AddonManager)
      this._AddonManager.addAddonListener(this);
    else
      OBSERVER_SERVICE.addObserver(this, this.EM_ACTION_REQUESTED_TOPIC, false);
  },
  
  shutdown: function AM_shutdown() {
    if (!this._started)
      return;
    
    this._started = false;
    
    if (this._AddonManager)
      this._AddonManager.removeAddonListener(this);
    else
      OBSERVER_SERVICE.removeObserver(this, this.EM_ACTION_REQUESTED_TOPIC);
    
    this._addonListeners = null;
    this._watchingAddons = null;
  },
  
  observe: function AM_observe(aSubject, aTopic, aData) {
    switch (aTopic) {
      case this.EM_ACTION_REQUESTED_TOPIC:
        const EVENTS_MAP = {
          __proto__: null,
          "item-installed": "onInstalled",//?
          "item-upgraded": "onUpdateFinished",//?
          "item-uninstalled": "onUninstalling",//?
          "item-enabled": "onEnabling",//true
          "item-disabled": "onDisabling",//?
          "item-cancel-action": "onOperationCancelled"//false (undefined)
        };
        
        let extEvent = EVENTS_MAP[aData];
        if (extEvent) {
          aSubject.QueryInterface(Ci.nsIUpdateItem);
          let addon = new AddonItemWrapper(aSubject);
          let pendingRestart = !(aTopic == "item-cancel-action");
          this._notifyAddonListeners(extEvent, addon, pendingRestart);
        }
      
      case "browser-ui-startup-complete":
        OBSERVER_SERVICE.removeObserver(this, "browser-ui-startup-complete");
        OBSERVER_SERVICE.addObserver(this, "xpcom-shutdown", false);
        this.startup();
        break;
      
      case "xpcom-shutdown":
        OBSERVER_SERVICE.removeObserver(this, "xpcom-shutdown");
        this.shutdown();
        break;
      
      default:
        break;
    }
  },
  
  onAddonEvent: function AM_onAddonEvent(aEventType, aAddon, aPendingRestart) {
    let addonId = aAddon.id;
    let watchingAddon = this._watchingAddons[addonId] || null;
    
    if (!watchingAddon)
      return;
    watchingAddon.installed = !(aEventType == "onUninstalling");
  },
  
  _watchingAddons: { __proto__: null },
  
  watchAddonUninstall: function AM_watchAddonUninstall(aAddonId) {
    this._watchingAddons[aAddonId] = {
      __proto__: null,
      installed: null
    };
    
    this.addAddonListener(this);
  },
  
  isAddonUninstalling: function AM_isAddonUninstalling(aAddonId) {
    let watchingAddon = this._watchingAddons[aAddonId] || null;
    return (watchingAddon && watchingAddon.installed === false);
  },
  
  addAddonListener: function AM_addAddonListener(aListener) {
    if (!this._addonListeners.some(function(listener) listener == aListener))
      this._addonListeners.push(aListener);
  },
  
  removeAddonListener: function AM_removeAddonListener(aListener) {
    this._addonListeners = this._addonListeners.filter(function(listener) listener != aListener);
  },
  
  _notifyAddonListeners: function AM__notifyAddonListeners() {
    let args = arguments;
    
    this._addonListeners.forEach(function(aListener) {
      try {
        aListener.onAddonEvent.apply(aListener, args);
      } catch (e) {
        Cu.reportError("AddonManager._notifyAddonListeners threw exception " +
                       "when calling Listener.onAddonEvent: " + e);
      }
    });
  }
};
OBSERVER_SERVICE.addObserver(AddonManager, "browser-ui-startup-complete", false);

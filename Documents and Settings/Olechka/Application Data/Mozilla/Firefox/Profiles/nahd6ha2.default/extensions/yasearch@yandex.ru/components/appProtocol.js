const {
    classes: Cc,
    interfaces: Ci,
    results: Cr,
    utils: Cu
} = Components;
const EXTENSION_PATH = Cc["@mozilla.org/network/io-service;1"]
                           .getService(Ci.nsIIOService)
                           .newFileURI(__LOCATION__.parent.parent).spec;

Cc["@mozilla.org/moz/jssubscript-loader;1"]
    .getService(Ci.mozIJSSubScriptLoader)
    .loadSubScript(EXTENSION_PATH + "config.js");

Cu.import("resource://gre/modules/XPCOMUtils.jsm");

function CustomBarProtocol() {
    this._providers = [];
    this._observerService.addObserver(this, "quit-application", false);
};

CustomBarProtocol.prototype = {
    addDataProvider: function CBPH_addDataProvider(aProvider) {
        if (this._providers.some(function(handler) aProvider === handler))
            return;
        
        if (!("newURI" in aProvider))
            throw new Error("Bad data provider interface.");
        
        this._providers.push(aProvider);
    },
    
    removeDataProvider: function CBPH_removeDataProvider(aProvider) {
        this._providers = this._providers.filter(function(handler) aProvider !== handler);
    },
    
    get wrappedJSObject() {
        return this;
    },
    observe: function CBPH_observe(aSubject, aTopic, aData) {
        switch (aTopic) {
            case "quit-application":
                this._destroy();
                break;
        }
    },
    
    get scheme() {
        return this._scheme;
    },
    
    get protocolFlags() {
        return Ci.nsIProtocolHandler.URI_DANGEROUS_TO_LOAD;
    },
    
    get defaultPort() {
        return -1;
    },
    
    allowPort: function CBPH_allowPort(port, scheme) {
        return false;
    },
    
    newURI: function CBPH_newURI(aSpec, aOriginalCharset, baseURI) {
        let uri = this._handleNewURI(aSpec, aOriginalCharset, baseURI);
        
        if (!uri)
            throw Cr.NS_ERROR_MALFORMED_URI;
        
        return uri;
    },
    
    newChannel: function CBPH_newChannel(aURI) {
        let channel = this._ioService.newChannel("data:,", null, null);
        channel.originalURI = aURI;
        return channel;
    },
    
    _scheme: PROTOCOL_APP_SCHEME,
    _ioService: Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService),
    _observerService: Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService),
    
    _handleNewURI: function CBPH__handleNewURI(aSpec, aOriginalCharset, aBaseURI) {
        let uri = null;
        let spec = aSpec.replace(this.scheme + ":", "");
        
        this._providers.some(function(aProvider) {
            return (null !== (uri = aProvider.newURI(spec, aOriginalCharset, aBaseURI)));
        });
        
        if (uri && typeof uri == "string")
            uri = this._ioService.newURI(uri, aOriginalCharset, aBaseURI);
        
        return uri;
    },
    
    _destroy: function CBPH__destroy() {
        this._observerService.removeObserver(this, "quit-application");
        this._providers = null;
    },
    
    classDescription: "Custom Yandex bar protocol handler JS component for " + APP_NAME,
    classID: PROTOCOL_APP_CLASS_ID,
    contractID: PROTOCOL_APP_CONTRACT_ID,
    QueryInterface: XPCOMUtils.generateQI([Ci.nsIProtocolHandler]),
    
    _xpcom_categories: [{
        category: "app-startup",
        service: true
    }]
};

if (XPCOMUtils.generateNSGetFactory)
  var NSGetFactory = XPCOMUtils.generateNSGetFactory([CustomBarProtocol]);
else
  var NSGetModule = XPCOMUtils.generateNSGetModule([CustomBarProtocol]);

const {
    classes: Cc,
    interfaces: Ci,
    results: Cr,
    utils: Cu
} = Components;

const EXTENSION_PATH = Cc["@mozilla.org/network/io-service;1"]
                           .getService(Ci.nsIIOService)
                           .newFileURI(__LOCATION__.parent.parent)
                           .spec;

Cu.import("resource://gre/modules/XPCOMUtils.jsm");
Cu.import(EXTENSION_PATH + "modules/JSON.jsm");
let LastResultHack = {
    _result: null,
    
    get result() {
        return this._result;
    },
    
    set result(aValue) {
        this._result = aValue;
    },
    
    clear: function LastResultHack_clear() {
        this._result = null;
    },
    
    get lastResult() {
        let result = this.result;
        
        if (result)
            return result._results[result._lastResultIndex];
        
        return null;
    },
    
    getResultForString: function LastResultHack_getResultForString(aString) {
        let result = this.lastResult;
        
        if (result && result.value.toLowerCase() == aString.toLowerCase())
            return result;
        
        return null;
    }
};
function SimpleAutoCompleteResult(searchString, searchResult,
                                  defaultIndex, errorDescription,
                                  results, comments) {
    this._searchString = searchString;
    this._searchResult = searchResult;
    this._defaultIndex = defaultIndex;
    this._errorDescription = errorDescription;
    this._results = results;
    this._comments = comments;
    
    this._lastResultIndex = -1;
}

SimpleAutoCompleteResult.prototype = {
    _searchString: "",
    _searchResult: 0,
    _defaultIndex: 0,
    _errorDescription: "",
    _results: [],
    _comments: [],
    
    get wrappedJSObject() {
        return this;
    },
    
    /**
     * The original search string
     */
    get searchString() {
        return this._searchString;
    },
    
    /**
     * The result code of this result object, either:
     *         RESULT_IGNORED   (invalid searchString)
     *         RESULT_FAILURE   (failure)
     *         RESULT_NOMATCH   (no matches found)
     *         RESULT_SUCCESS   (matches found)
     */
    get searchResult() {
        return this._searchResult;
    },
    
    /**
     * Index of the default item that should be entered if none is selected
     */
    get defaultIndex() {
        return this._defaultIndex;
    },
    
    /**
     * A string describing the cause of a search failure
     */
    get errorDescription() {
        return this._errorDescription;
    },
    
    /**
     * The number of matches
     */
    get matchCount() {
        return this._results.length;
    },
    
    /**
     * Get the value of the result at the given index
     */
    getValueAt: function SACResult_getValueAt(aIndex) {
        this._lastResultIndex = aIndex;
        
        return this._results[aIndex].value;
    },
    
    getLabelAt: function SACResult_getLabelAt(aIndex) {
        return this.getValueAt(aIndex);
    },
    
    /**
     * Get the comment of the result at the given index
     */
    getCommentAt: function SACResult_getCommentAt(index) {
        return this._comments[index];
    },
    
    /**
     * Get the style hint for the result at the given index
     */
    getStyleAt: function SACResult_getStyleAt(index) {
        if (!this._comments[index])
            return null;
        
        if (index == 0)
            return "suggestfirst";
        
        return "suggesthint";
    },
    
    /**
     * Get the image for the result at the given index
     * The return value is expected to be an URI to the image to display
     */
    getImageAt: function SACResult_getImageAt(index) {
        return "";
    },
    
    /**
     * Remove the value at the given index from the autocomplete results.
     * If removeFromDb is set to true, the value should be removed from
     * persistent storage as well.
     */
    removeValueAt: function SACResult_removeValueAt(index, removeFromDb) {
        this._results.splice(index, 1);
        this._comments.splice(index, 1);
    },
    
    QueryInterface: XPCOMUtils.generateQI([Ci.nsIAutoCompleteResult, Ci.nsISupports])
};
function SimpleAutoCompleteSearch() {
}

SimpleAutoCompleteSearch.prototype = {
    classDescription: "Custom Yandex.Bar AutoComplete",
    classID: Components.ID("7ec0573a-0738-11df-ab80-979cccaab3b0"),
    contractID: "@mozilla.org/autocomplete/search;1?name=yacustombar-autocomplete",
    QueryInterface: XPCOMUtils.generateQI([Ci.nsIAutoCompleteSearch, Ci.nsISupports]),
    
    get wrappedJSObject() {
        return {
            lastResult: LastResultHack
        }
    },
    
    startSearch: function SACSearch_startSearch(aSearchString, aSearchParam, aResult, aListener) {
        LastResultHack.result = null;
        
        if (!aSearchParam.length)
            return;
        
        let results = [];
        let comments = [];
        
        let lowercaseSearchString = aSearchString.toLowerCase();
        
        JSON.parse(aSearchParam).forEach(function(aSearchResult) {
            if (aSearchResult.value.toLowerCase().indexOf(lowercaseSearchString) == 0) {
                results.push({
                    value: aSearchResult.value,
                    realValue: aSearchResult.realValue,
                });
                comments.push(aSearchResult.comment || null);
            }
        });
        
        let newResult = new SimpleAutoCompleteResult(aSearchString, Ci.nsIAutoCompleteResult.RESULT_SUCCESS,
                                                     0, "", results, comments);
        
        LastResultHack.result = newResult;
        
        aListener.onSearchResult(this, newResult);
    },
    
    stopSearch: function SACSearch_stopSearch() {}
};

if (XPCOMUtils.generateNSGetFactory)
  var NSGetFactory = XPCOMUtils.generateNSGetFactory([SimpleAutoCompleteSearch]);
else
  var NSGetModule = XPCOMUtils.generateNSGetModule([SimpleAutoCompleteSearch]);

UI.Behaviour.Style = UI.Behaviour.extend({
    $name: "XB_UI_Style",
    
    name: "style",
    nodeName: "html:span",
    namespaceURI: UI._consts.STR_HTML_NS,
    
    map: {
        __proto__: null,
        "color": "color",
        "font-weight": "fontWeight"
    },
    
    onAttribute: function XBUI_Style_onAttribute(event) {
        let property = this.map[event.name];
        if (property)
            this.node.style[property] = event.value;
    }
});
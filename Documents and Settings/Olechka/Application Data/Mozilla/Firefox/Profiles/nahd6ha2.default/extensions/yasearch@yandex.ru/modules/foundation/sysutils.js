const sysutils = {
    
    dump: function SysUtils_dump(what, depth) {
        function _dump(what, depth, stack) {
            stack.push(what);
            
            let res = "";
            for (let prop in what) {
                let val = what[prop];
                let valStr;
                if ((depth > stack.length) &&
                    sysutils.isObject(val) &&
                    (stack.indexOf(val) == -1))
                    valStr = _dump(val, depth, stack);
                else
                    try {
                        valStr = String(val);
                    }
                    catch (e) {
                        valStr = "{conversion error}";
                    }
                
                res += (prop + ": " + valStr + "\r\n");
            }
            
            stack.pop();
            
            return res;
        }
        
        return _dump(what, depth, []);
    },
    
    get platformInfo() {
        let info = Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULAppInfo).QueryInterface(Ci.nsIXULRuntime);
        
        let os = info.OS;
        
        if (/^win/i.test(os))
            os = "windows";
        else if (/^linux/i.test(os))
            os = "linux";
        else if (/^darwin/i.test(os))
            os = "mac";
        
        let comparator = Cc["@mozilla.org/xpcom/version-comparator;1"].getService(Ci.nsIVersionComparator);
        
        let simpleArchitecture = info.XPCOMABI.split("-")[0] || "x86";
        if (simpleArchitecture == "x86_64")
            simpleArchitecture = "x64";
        
        let platformInfo = {
            browser: {
                name: "firefox",
                version: {
                    toString: function PBVersion_toString() info.version,
                    isLessThan: function PBVersion_isLessThan(aVersion) comparator.compare(this, aVersion) < 0,
                    isGreaterThan: function PBVersion_isGreaterThan(aVersion) comparator.compare(this, aVersion) > 0,
                    isEqual: function PBVersion_isEqual(aVersion) comparator.compare(this, aVersion) == 0
                },
                architecture: info.XPCOMABI,
                simpleArchitecture: simpleArchitecture
            },
            
            os: {
                name: os
            }
        };
        
        this.__defineGetter__("platformInfo", function() platformInfo);
        return this.platformInfo;
    },
    
    isEmptyObject: function SysUtils_isEmptyObject(obj) {
        for (let n in obj)
            return false;
        return true;
    },
    
    objectsAreEqual: function SysUtils_objectsAreEqual(obj1, obj2) {
        if (typeof obj1 != "object" || typeof obj2 != "object")
            throw new TypeError("Objects expected");
        
        let props1 = [], 
            props2 = [];
        
        for (let propName in obj1)
            props1.push(propName);
        
        for (let propName in obj2)
            props2.push(propName);
        
        if (props1.length != props2.length)
            return false;
        
        let prop1, prop2;
        for each (let propName in props1) {
            if ( !(propName in obj2) )
                return false;
            
            prop1 = obj1[propName];
            prop2 = obj2[propName];
            
            if ( (typeof prop1 == "object") && (typeof prop2 == "object") ) {
                if ( !this.objectsAreEqual(prop1, prop2) )
                    return false;
            }
            else {
                if (prop1 !== prop2)
                    return false;
            }
        }
        
        return true;
    },
    
    copyObj: function SysUtils_copyObj(src, deep) {
        if (typeof src != "object" || !src)
            return src;
        
        if (this.isArray(src))
            return src.map(function(el) {return deep ? this.copyObj(el, deep) : el}, this);
        
        let result = {};
        
        for (let [name, value] in Iterator(src))
            result[name] = deep ? this.copyObj(value, deep) : value;
        
        return result;
    },
    
    isObject: function SysUtils_isObject(val) {
        return (val && Object.prototype.toString.call(val) == "[object Object]");
    },
    
    isArray: function SysUtils_isArray(val) {
        return (val && Object.prototype.toString.call(val) == "[object Array]");
    },
    
    isNumber: function SysUtils_isNumber(x) {
        return (typeof x == "number" && !isNaN(x));
    },
    
    isString: function SysUtils_isString(x) {
        return (typeof x == "string");
    },
    
    isBool: function SysUtils_isBool(x) {
        return (typeof x == "boolean");
    },
    cutNumberTo: function SysUtils_cutNumberTo(num, numDigits) {
        let intPart = Math.floor(num);
        let fracPart = num - intPart;
        let pow = Math.pow(10, numDigits);
        let cutFrac = Math.floor(fracPart * pow) / pow;
        return intPart + cutFrac;
    },
    
    defineLazyGetter: function SysUtils_defineLazyGetter(aObject, aName, aLambda) {
        aObject.__defineGetter__(aName, function SysUtils_lazyGetter() {
            delete aObject[aName];
            return aObject[aName] = aLambda.apply(aObject);
        });
    },
    
    defineLazyServiceGetter: function SysUtils_defineLazyServiceGetter(aObject, aName, aContract, aInterfaceName) {
        this.defineLazyGetter(aObject, aName, function SysUtils_lazyService() {
            return Cc[aContract].getService(Ci[aInterfaceName]);
        });
    },
    
    WeakObjectProxy: function WeakObjectProxy(object) {
        if (!sysutils.isObject(object))
            throw new CustomErrors.EArgType("object", "Object", object);
        
        let inst = (function weakRefInitializer() {
            let __storedObject = object;
            
            return {
                get isValid() {
                    return __storedObject !== undefined;
                },
                
                invoke: function WeakObjectProxy_invoke(methodName, methodArguments) {
                    if (!this.isValid)
                        throw new Error("This weak object is invalid");
                    let method = __storedObject[methodName];
                    if (typeof method !== "function")
                        throw new Error(strutils.formatError("No such method (%1)", [methodName]));
                    
                    let args = Array.slice(arguments, 1);
                    return method.apply(__storedObject, args);
                },
                
                clear: function WeakObjectProxy_clear() {
                    __storedObject = undefined;
                }
            };
        })();
        
        return inst;
    },
    
    _Ci: Components.interfaces,
    _Cc: Components.classes
};

const EXPORTED_SYMBOLS = ["statLogger"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

var Lib;
const statLogger = {
    init: function statLogger_init(aApplication) {
        this._application = aApplication;
        Lib = this._application.core.Lib;
        this.clear();
    },
    
    finalize: function statLogger_finalize() {
    },
    dump: function statLogger_dump() {
        return "Statistics:" +
            "\nshort actions:\n" + Lib.sysutils.dump(this._shortActions, 2) +
            "\nsys actions:\n" + Lib.sysutils.dump(this._sysActions, 2) +
            "\ncustom actions:\n" + Lib.sysutils.dump(this._customActions, 2);
    },
    
    clear: function statLogger_clear() {
        this._shortActions = {};
        this._sysActions = {};
        this._customActions = {};
    },
    
    logCustomAction: function statLogger_logCustomAction(componentID, actionID) {
        this._incAction(this._customActions, componentID, actionID);
    },
    
    logSysAction: function statLogger_logSysAction(componentID, actionID) {
        this._incAction(this._sysActions, componentID, actionID);
    },
    
    logShortAction: function statLogger_logShortAction(actionID) {
        if (!(actionID = this._filterActionID(actionID)))
            return;
        
        if (!(actionID in this._shortActions))
            this._shortActions[actionID] = 0;
        
        this._shortActions[actionID]++;
    },
    
    readActions: function statLogger_readActions() {
        let allChunks = [];
        
        for (let actionID in this._shortActions) {
            let actionCount = this._shortActions[actionID];
            let chunk = actionID + ((actionCount > 1) ? ("." + actionCount) : "");
            allChunks.push(chunk);
        }
        
        for (let componentHash in this._sysActions) {
            for (let actionID in this._sysActions[componentHash]) {
                let actionCount = this._sysActions[componentHash][actionID];
                let chunk = componentHash + "-" + actionID + ((actionCount > 1) ? ("." + actionCount) : "");
                allChunks.push(chunk);
            }
        }
        
        for (let componentHash in this._customActions) {
            for (let actionID in this._customActions[componentHash]) {
                let actionCount = this._customActions[componentHash][actionID];
                let chunk = componentHash + "+" + actionID + ((actionCount > 1) ? ("." + actionCount) : "");
                allChunks.push(chunk);
            }
        }
        
        this.clear();
        
        return allChunks.join(",");
    },
    
    _sysActions: null,
    _customActions: null,
    _shortActions: null,
    _filterActionID: function statLogger__filterActionID(actionID) {
        let action = Number(actionID);
        
        if (action > 0) {
            action = "" + action;
            if (action === ("" + actionID))
                return action;
        }
        
        return null;
    },
    
    _incAction: function statLogger__incAction(actionsMap, componentID, actionID) {
        if (!(actionID = this._filterActionID(actionID)))
            return;
        
        let componentHash = this._compID2Hash(componentID);
        let compActions = actionsMap[componentHash] || (actionsMap[componentHash] = {});
        
        if (!(actionID in compActions))
            compActions[actionID] = 0;
        
        compActions[actionID]++;
    },
    
    _compID2Hash: function statLogger__incAction(componentID) {
        return Lib.misc.CryptoHash.getFromString(componentID, "MD5");
    }
};

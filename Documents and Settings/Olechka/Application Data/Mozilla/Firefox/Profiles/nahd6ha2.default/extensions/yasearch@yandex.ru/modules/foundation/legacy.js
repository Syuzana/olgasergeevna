/*
	http://github.com/shergin/legacy/
	Special version for Firefox only.
*/

function Class($super, $members, $statics) {
    var $class;
    
    if ($members.constructor && (typeof $members.constructor == "function") && $members.constructor.name !== "Object") {
        $members.$constructor = $members.constructor;
        $class = $members.constructor;
        delete $members.constructor;
    }
    else {
        $class = function $class() {
            if ($class.prototype.$constructor)
                $class.prototype.$constructor.apply(this, arguments);
        }
    }
    
    var prototype = {};
    
    if ($super) {
        prototype.__proto__ = $super.prototype;
        prototype.constructor = $class;
    }
    
    $class.$super = $super;
    $class.prototype = prototype;
    
    prototype.$class = $class;
    prototype.$super = $super ? $super.prototype : null;
    prototype.$base = Class.$base;
    prototype.$name = $members.$name || ($members.$constructor ? $members.$constructor.name : '');
    
    Class.$implement($class, $members, $statics);
    
    return $class;
}

Class.$empty = function() {};

Class.$base = function $base() {
    var caller = $base.caller;
    return caller.$class.$super.prototype[caller.$name].apply(this, arguments);
};

Class.$copy = function($source, $target, $class) {
    const specials = ["$class", "$name", "$super", "$base"];
    
    for (let name in $source) {
        if (specials.indexOf(name) >= 0)
            continue;
        
        let getter = $source.__lookupGetter__(name),
            setter = $source.__lookupSetter__(name);
        
        if (getter || setter) {
            if (getter) {
                if (getter.$class)
                    getter = eval(getter.toString());
                getter.$class = $class;
                getter.$name = name;
                $target.__defineGetter__(name, getter);
            }
            
            if (setter) {
                if (setter.$class)
                    setter = eval(setter.toString());
                setter.$class = $class;
                setter.$name = name;
                $target.__defineSetter__(name, setter);
            }
        }
        else {
            let member = $source[name];
            if (typeof member == "function") {
				if (member.$class)
					member = eval("(" + member.toString() + ")");
				member.$class = $class;
				member.$name = name;
            }
            $target[name] = member;
        }
    }
};

Class.$implement = function $implement($class, $members, $statics) {
    Class.$copy($members, $class.prototype, $class);
    
    if ($statics)
        Class.$copy($statics, $class, null);
};
var Base = Class(null, {
        extend: function($members) {
            Class.$copy($members, this, this.$class);
            return this;
        },
        base: function() {
        }
    }, {
        ancestorOf: function($class) {
            while ($class) {
                $class = $class.$super;
                if ($class == this)
                    return true;
            }
            return false;
        },
        
        inherits: function($class) {
            return $class.ancestorOf(this);
        },
        extend: function $extend(members, statics) {
            statics = statics || {};
            statics.extend = this.extend;
            statics.implement = this.implement;
            statics.ancestorOf = this.ancestorOf;
            statics.inherits = this.inherits;
            var $class = Class(this, members || {}, statics);
            $class.prototype.base = $class.prototype.$base;
            return $class;
        },
        implement: function(members, statics) {
            if (members.prototype)
                Class.$implement(this, members.prototype, members);
            else
                Class.$implement(this, members, statics);
            return this;
        }
    }
);


NativeBarAPI = function NativeBarAPI(componentInfo) {
    if (!componentInfo ||
        !componentInfo.id ||
        !(componentInfo.package_ instanceof application.BarPlatform.WidgetPackage))
        throw new lib.CustomErrors.EArgType("componentInfo", "XBComponentInfo", componentInfo);
    
    this._componentInfo = componentInfo;
    this._logger = lib.Log4Moz.repository.getLogger(appCore.appName + ".NativeAPI." + componentInfo.id);
    let sysutils = lib.sysutils;
    
    [ "Environment", "Controls", "Settings", "Package",
      "Files", "XMLUtils", "StrUtils", "Browser", "YandexAuth", "Services"
    ].forEach(function inst(apiName) {
        sysutils.defineLazyGetter(this, apiName, function lazyGetter() {
            return new NativeBarAPI[apiName](this._componentInfo, this._logger);
        });
    }, this);
    
    sysutils.defineLazyGetter(this, "JSON", function lazyGetter() lib.JSON);
};

NativeBarAPI.prototype = {
    get componentID() {
        return this._componentInfo.id;
    },
    
    get logger() {
        return this._logger;
    },
    
    queryAPIVersion: function NativeAPI_queryAPIVersion(version) {
        let apiVer = Number(version);
        if ((apiVer > 0) && (apiVer <= this.Environment.barPlatform.version))
            return this;
        throw new Error(lib.strutils.formatString("API version %1 is not available", [version]));
    },
    finalize: function NativeAPI_finalize() {
        try {
            NativeComponents._unregisterServices(this.componentID);
        }
        catch (e) {
            this._logger.error("NativeComponents._unregisterServices failed. " + strutils.formatError(e));
        }
        try {
            NativeComponents._releaseServices(this.componentID);
        }
        catch (e) {
            this._logger.error("NativeComponents._releaseServices failed. " + strutils.formatError(e));
        }
        
        this.Settings._finalize();
        
        this.YandexAuth._finalize();
        
        this._componentInfo = null;
    }
};

NativeBarAPI.Services = function Services(componentInfo) {
    this._componentInfo = componentInfo;
};

NativeBarAPI.Services.prototype = {
    registerService: function NativeAPI_registerService(serviceName, serviceObject) {
        return NativeComponents.registerService(this._componentInfo.id, serviceName, serviceObject);
    },
    
    notifyServiceUsers: function NativeAPI_notifyServiceUsers(serviceName, topic, data) {
        NativeComponents.notifyServiceUsers(this._componentInfo.id, serviceName, topic, data);
    },
    
    unregisterService: function NativeAPI_unregisterService(serviceName) {
        return NativeComponents.unregisterService(this._componentInfo.id, serviceName);
    },
    obtainService: function NativeAPI_obtainService(providerID, serviceName, eventListener) {
        return NativeComponents.obtainService(providerID, serviceName, eventListener, this._componentInfo.id);
    },
    
    releaseService: function NativeAPI_releaseService(providerID, serviceName, eventListener) {
        NativeComponents.releaseService(providerID, serviceName, eventListener);
    }
};

NativeBarAPI.Environment = function Environment(componentInfo, logger) {
    this._logger = logger;
};
NativeBarAPI.Environment.prototype = {
    constructor: NativeBarAPI.Environment,
    
    os: {
        get name() {
            return lib.sysutils.platformInfo.os.name;
        }
    },
    
    browser: {
        get name() {
            return lib.sysutils.platformInfo.browser.name;
        },
        
        get version() {
            return lib.sysutils.platformInfo.browser.version;
        }
    },
    
    addon: {
        get id() {
            return lib.AddonManager.getAddonId(appCore.extensionPathFile);
        },
        
        get version() {
            return lib.AddonManager.getAddonVersion(appCore.extensionPathFile);
        },
        
        get locale() {
            let localeString = application.localeString;
            let localeComponents = lib.misc.parseLocale(localeString);
            
            let locale = {
                toString: function NativeAPI_locale_toString() localeString,
                language: localeComponents.language || "",
                country:  localeComponents.country || "",
                region:   localeComponents.region || ""
            };
            
            this.__defineGetter__("locale", function NativeAPI_locale() locale);
            return this.locale;
        }
    },
    
    barPlatform: {
        get name() {
            return appCore.appName;
        },
        
        get version() {
            return appCore.version;
        }
    }
};
NativeBarAPI.Controls = function Controls(componentInfo, logger) {
    this._componentInfo = componentInfo;
    this._logger = logger;
};

NativeBarAPI.Controls.prototype = {
    
    openSettingsDialog: function NativeAPI_openSettingsDialog(browserWindow, setupID, paneType) {
        application.openSettingsDialog(browserWindow, setupID, paneType);
    },
    
    
    openAboutDialog: function NativeApi_openAboutDialog() {
        application.openAboutDialog();
    },
    
    
    logUsageStat: function NativeAPI_logUsageStat(eventID) {
        let packageInfo = application.packageManager.getPackageInfo(this._componentInfo.package_.id);
        if (application.isTrustedPackageURL(packageInfo.uri))
            application.statLogger.logShortAction(eventID);
        else
            application.statLogger.logCustomAction(this._componentInfo.id, eventID);
    },
    
    
    navigateBrowser: function NativeAPI_navigateBrowser(navigateData) {
        return application.navigate(navigateData);
    },
    
    
    addWidget: function NativeAPI_addWidget(widgetID, browserWindow, relativeTo, placeAfter) {
        if (!browserWindow)
            browserWindow = misc.getTopBrowserWindow();
        else if (!(browserWindow instanceof Ci.nsIDOMWindow))
            throw new TypeError("XUL window required " + browserWindow);
        let widgetElement = NativeComponents._getWindowController(browserWindow).
            addWidgetItem(widgetID || this._componentInfo.id, null, relativeTo, placeAfter, undefined, true);
        return [widgetElement.wdgtInstanceID, widgetElement];
    },
    
    removeWidget: function NativeAPI_removeWidget(WIIDorElement, browserWindow) {
        if (!browserWindow) {
            if (WIIDorElement instanceof Ci.nsIDOMElement)
                browserWindow = WIIDorElement.ownerDocument.defaultView;
            else
                browserWindow = misc.getTopBrowserWindow();
        }
        if (!(browserWindow instanceof Ci.nsIDOMWindow))
            throw new TypeError("XUL window required " + browserWindow);
        NativeComponents._getWindowController(browserWindow).removeItem(WIIDorElement);
    },
    
    enablePlugin: function NativeAPI_enablePlugin(pluginID) {
        application.widgetLibrary.getPlugin(pluginID).enable();
    },
    
    disablePlugin: function NativeAPI_disablePlugin(pluginID) {
        application.widgetLibrary.getPlugin(pluginID).disable();
    }
};

NativeBarAPI.Settings = function Settings(componentInfo, logger) {
    this._componentInfo = componentInfo;
    this._logger = logger;
    this._settingsObservers = {};
};

NativeBarAPI.Settings.prototype = {
    getValue: function NativeAPI_getValue(settingName, WIID) {
        if (this._componentInfo.type == "widget")
            return this._componentInfo.component.lookupGetSettingValue(settingName, WIID);
        return this._componentInfo.component.getSettingValue(settingName);
    },
    
    
    setValue: function NativeAPI_setValue(settingName, newValue, WIID) {
        if (this._componentInfo.type == "widget")
            this._componentInfo.component.lookupSetSettingValue(settingName, WIID, newValue);
        else
            this._componentInfo.component.applySetting(settingName, newValue);
    },
    
    observeChanges: function NativeAPI_observeChanges(observer, WIID) {
        if (!sysutils.isObject(observer) || (typeof observer.onSettingChange != "function"))
            throw new TypeError("Observer must be an object with 'onSettingChange' method");
        
        let ci = this._componentInfo;
        if (ci.type == "plugin")
            WIID = ci.component.id;
        else if (WIID)
            this._watchInstSettings(WIID);
        this._watchSettings(WIID);
        
        this._settingsObservers[WIID] = observer;
    },
    
    ignoreChanges: function NativeAPI_ignoreChanges(WIID) {
        let ci = this._componentInfo;
        if (ci.type == "plugin")
            WIID = ci.component.id;
        else
            this._ignoreInstSettings(WIID);
        
        delete this._settingsObservers[WIID];
        if (sysutils.isEmptyObject(this._settingsObservers))
            this._ignoreSettings();
    },
    
    get prefService() {
        let prefService = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefService);
        this.__defineGetter__("prefService", function() prefService);
    },
    
    get PrefsModule() {
        return Preferences;
    },
    
    getPackageBranchPath: function NativeAPI_getPackageBranchPath() {
        return this._pbp || (this._pbp = NativeComponents.makePackagePrefPath(this._componentInfo.package_.id));
    }, 
    
    getComponentBranchPath: function NativeAPI_getComponentBranchPath() {
        return this._cbp || (this._cbp = NativeComponents.makeWidgetPrefPath(this._componentInfo.id));
    },
    
    getInstanceBranchPath: function NativeAPI_getInstanceBranchPath(WIID) {
        return NativeComponents.makeInstancePrefPath(this._componentInfo.id, WIID);
    },
    
    
    getStaticBranchPath: function NativeAPI_getStaticBranchPath(WIID) {
        return NativeComponents.makeStaticBranchPath(this._componentInfo.id, WIID);
    },
    
    _settingsObservers: null,
    _watchingCommonSettings: false,
    
    _finalize: function () {
        for (let WIID in this._settingsObservers) {
            try {
                this._ignoreInstSettings(WIID);
            }
            catch (e) {
                
            }
        }
        this._ignoreSettings();
        this._settingsObservers = null,
        this._watchingCommonSettings = null;
    },
    
    observe: function NativeAPI_observe(subject, topic, data) {
        try {
            switch (topic) {
                case "nsPref:changed": {
                    this._notifySettingsObservers(subject, data);
                    break;
                }
                default:
                    return;
            }
        }
        catch (e) {
            this._logger.error("nsIObserver.observe failed. " + strutils.formatError(e));
        }
    },
    
    _watchSettings: function NativeAPI__watchSettings() {
        if (this._watchingCommonSettings)
            return;
        let component = this._componentInfo.component;
        Preferences.observe2(NativeComponents.makePackagePrefPath(component.pkg.id), this);
        Preferences.observe2(NativeComponents.makeWidgetPrefPath(component.id), this);
    },
    
    _watchInstSettings: function NativeAPI__watchInstSettings(WIID) {
        if (WIID in this._settingsObservers)
            throw new Error("Widget instance " + WIID + " settings are already being watched");
        Preferences.observe2(NativeComponents.makeInstancePrefPath(this._componentInfo.component.id, WIID), this);
    },
    
    _ignoreSettings: function NativeAPI__ignoreSettings() {
        let component = this._componentInfo.component;
        Preferences.ignore2(NativeComponents.makePackagePrefPath(component.pkg.id), this);
        Preferences.ignore2(NativeComponents.makeWidgetPrefPath(component.id), this);
        this._watchingCommonSettings = false;
    },
    
    _ignoreInstSettings: function NativeAPI__ignoreInstSettings(WIID) {
        Preferences.ignore2(NativeComponents.makeInstancePrefPath(this._componentInfo.component.id, WIID), this);
    },
    
    _notifySettingsObservers: function NativeAPI__notifySettingsObservers(prefBranch, prefPath) {
        let component = this._componentInfo.component;
        let prefProperties = BarPlatform.parsePrefPath(prefPath, component.id);
        let settingName = prefProperties.settingName;
        
        if (this._componentInfo.type == "widget") {
            let widgetHasInstSetting = (settingName in component._instSettings);
            if (prefProperties.isInstancePref && !widgetHasInstSetting) 
                throw new Error(strutils.formatString("Widget %1 does not have setting named \"%2\"", [component.id, settingName]));
            if ( (!prefProperties.isInstancePref && widgetHasInstSetting) ||
                 (prefProperties.isPackagePref && (settingName in component.widgetSettings2)) )
                    return;
        }
        else {
            if (prefProperties.isInstancePref)
                throw new Error("Got instance setting change, but this component is not a widget: " + prefPath);
            if (prefProperties.isPackagePref && (settingName in component.pluginSettings2))
                return;
        }
        let newValue = this.getValue(settingName, prefProperties.instanceID);
        for (let [id, observer] in Iterator(this._settingsObservers)) {
            try {
                observer.onSettingChange(settingName, newValue, prefProperties.instanceID);
            }
            catch (e) {
                this._logger.error("Could not notify native core about setting change. " + strutils.formatError(e));
            }
        };
    }
};

NativeBarAPI.Package = function Package(componentInfo, logger) {
    this._package = componentInfo.package_;
    this._logger = logger;
};

NativeBarAPI.Package.prototype = {
    constructor: NativeBarAPI.Package,
    get id() {
        return this._package.id;
    },
    resolvePath: function NativeAPI_resolvePath(path) {
        return this._package.resolvePath(path);
    },
    getFileInputChannel: function NativeAPI_getFileInputChannel(path) {
        return this._package.newChannelFromPath(path);
    }
};

NativeBarAPI.Files = function Files(componentInfo, logger) {
    this._componentInfo = componentInfo;
    this._logger = logger;
};

NativeBarAPI.Files.prototype = {
    getWidgetStorage: function NativeAPI_getWidgetStorage(create) {
        let result = application.directories.XBNativeStorage;
        result.append(encodeURIComponent(this._componentInfo.id));
        
        this.getWidgetStorage = function NativeAPI_getWidgetStorage(inCreate) {
            if (inCreate)
                lib.fileutils.forceDirectories(result);
            return result.clone();
        };
        
        return this.getWidgetStorage(create);
    },
    
    getInstanceStorage: function NativeAPI_getInstanceStorage(WIID, create) {
        let result = this.getWidgetStorage();
        result.append(WIID);
        if (create)
            lib.fileutils.forceDirectories(result);
        return result;
    },
    
    forceDirectories: function NativeAPI_forceDirectories(file) {
        lib.fileutils.forceDirectories(file);
    },
    
    writeStreamToFile: function NativeAPI_writeStreamToFile(inputStream, destFile, accessRights, modeFlags) {
        return lib.fileutils.writeStreamToFile(inputStream, destFile, accessRights, modeFlags);
    },
    
    readTextFile: function NativeAPI_readTextFile(file) {
        return lib.fileutils.readTextFile(file);
    },
    
    writeTextFile: function NativeAPI_writeTextFile(file, text, accessRights, modeFlags) {
        return lib.fileutils.writeTextFile(file, text, accessRights, modeFlags);
    }
};

NativeBarAPI.XMLUtils = function XMLUtils(componentInfo, logger) {
    this._logger = logger;
};

NativeBarAPI.XMLUtils.prototype = {
    constructor: NativeBarAPI.XMLUtils,
    
    xmlDocFromStream: function NativeAPI_xmlDocFromStream(stream, docURI, baseURI, privileged) {
        return lib.fileutils.xmlDocFromStream(stream, docURI, baseURI, privileged);
    },
    
    xmlDocFromFile: function NativeAPI_xmlDocFromFile(localFile, privileged) {
        return lib.fileutils.xmlDocFromFile(localFile, privileged);
    },
    
    
    queryXMLDoc: function NativeAPI_queryXMLDoc(xpathExpr, contextNode, extNSResolver) {
        return misc.queryXMLDoc(xpathExpr, contextNode, extNSResolver);
    },
    
    getDOMParser: function NativeAPI_getDOMParser(docURI, baseURI, withSystemPrincipal) {
        return misc.getDOMParser(docURI, baseURI, withSystemPrincipal);
    }
};

NativeBarAPI.StrUtils = function StrUtils(componentInfo, logger) {
    this._logger = logger;
};

NativeBarAPI.StrUtils.prototype = {
    constructor: NativeBarAPI.StrUtils,
    
    readStringFromStream: function NativeAPI_readStringFromStream(inputStream) {
        return fileutils.readStringFromStream(inputStream);
    },
    formatError: function NativeAPI_formatError(error) {
        return strutils.formatError(error);
    }
};

NativeBarAPI.Browser = function Browser(componentInfo, logger) {
    this._logger = logger;
};

NativeBarAPI.Browser.prototype = {
    getWindowListener: function NativeAPI_getWindowListener(window) {
        return NativeComponents._getWindowController(window).windowListener;
    },
    
    getWindowDataIsland: function NativeAPI_getWindowDataIsland(window) {
        return NativeComponents._getWindowController(window).windowDataIsland;
    }
};

NativeBarAPI.YandexAuth = function YandexAuth(componentInfo, logger) {
    this._logger = logger;
};

NativeBarAPI.YandexAuth.prototype = {
    
    get authorized() {
        return !!this._yaAuth.isLogin;
    },
    
    
    get sessionLogin() {
        return this._yaAuth.session.login;
    },
    
    
    get username() {
        return this._yaAuth.username || "";
    },
    
    
    initLoginProcess: function YandexAuth_initLoginProcess(aLoginData) {
        return this._yaAuth.initLoginProcess(aLoginData.login,
                                             aLoginData.password,
                                             !!aLoginData.permanentAuth,
                                             !!aLoginData.rememberLoginDetails);
    },
    
    
    initLogoutProcess: function YandexAuth_logout(aLogoutData) {
        this._yaAuth.fireAfterLogout(true, !!aLogoutData.forgetLoginDetails);
    },
    
    
    openAuthDialog: function YandexAuth_openAuthDialog() {
        let topBrowser = misc.getTopBrowserWindow();
        if (!(topBrowser && "Ya" in topBrowser))
            return false;
        
        topBrowser.Ya.openWelcomeWindow(null, null, { view: "auth" });
        return true;
    },
    
    
    openMyBarDialog: function YandexAuth_openMyBarDialog() {
        let topBrowser = misc.getTopBrowserWindow();
        if (!(topBrowser && "Ya" in topBrowser))
            return false;
        
        topBrowser.Ya.openWelcomeWindow(null, null, { view: "auto" });
        return true;
    },
    
    LOGIN_STATES: {
        NO_AUTH: 0,
        ERROR: 1,
        CAPTCHA_ERROR: 2,
        NET_ERROR: 3,
        REQUEST: 4,
        AUTH: 5
    },
    
    KNOWN_TOPICS: [
        "login-state-changed",
        "login-auth-changed"
    ],
    
    
    addListener: function YandexAuth_addListener(aTopic, aListener) {
        if (this.KNOWN_TOPICS.indexOf(aTopic) == -1)
            throw new TypeError("YandexAuth.addListener: unknown topic \"" + aTopic + "\"");
        
        if (!this._listeners)
            return;
        
        if (!this._listeners[aTopic]) {
            this._listeners[aTopic] = [aListener];
        } else if (!this._listeners[aTopic].some(function(listener) listener == aListener)) {
            this._listeners[aTopic].push(aListener);
        }
        
        this._startObserve();
    },
    
    
    removeListener: function YandexAuth_removeListener(aTopic, aListener) {
        if (this.KNOWN_TOPICS.indexOf(aTopic) == -1)
            throw new TypeError("YandexAuth.removeListener: unknown topic \"" + aTopic + "\"");
        
        if (!this._listeners)
            return;
        
        if (this._listeners[aTopic]) {
            this._listeners[aTopic] = this._listeners[aTopic].filter(function(listener) listener != aListener);
        
            if (!this._listeners[aTopic].length)
                delete this._listeners[aTopic];
        }
        
        if (sysutils.isEmptyObject(this._listeners))
            this._stopObserve();
    },
    
    get _yaAuth() {
        let yaAuth = Cc["@yandex.ru/" + application.name + ";1"].getService(Ci.nsIYaSearch).wrappedJSObject.yaAuth;
        this.__defineGetter__("_yaAuth", function() yaAuth);
        return this._yaAuth;
    },
    
    _isObserving: false,
    
    _listeners: { __proto__: null },
    
    observe: function YandexAuth_observe(aSubject, aTopic, aData) {
        switch (aTopic) {
            case "yasearch-refresh-login-status":
                this._notifyListeners("login-auth-changed", {
                });
                break;
            
            case "yasearch-login-state-changed":
                this._notifyListeners("login-state-changed", {
                });
                break;
            
            default:
                break;
        }
    },
    
    
    _notifyListeners: function YandexAuth__notifyListeners(aTopic, aData) {
        if (!this._listeners)
            return;
        
        let listeners = this._listeners[aTopic];
        if (!listeners)
            return;
        
        listeners.forEach(function WindowListener_NotificatorFunc(listener) {
            try {
                if (this._listeners[aTopic].indexOf(listener) != -1)
                    listener.observe(null, aTopic, aData);
            } catch (e) {
                this._logger.error("Notify listener error: " + e);
            }
        }, this);
    },
    
    _startObserve: function YandexAuth__startObserve() {
        if (this._isObserving)
            return;
        
        const OBSERVER_SERVICE = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
        OBSERVER_SERVICE.addObserver(this, "yasearch-refresh-login-status", false);
        OBSERVER_SERVICE.addObserver(this, "yasearch-login-state-changed", false);
        this._isObserving = true;
    },
    
    _stopObserve: function YandexAuth__stopObserve() {
        if (!this._isObserving)
            return;
        
        const OBSERVER_SERVICE = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
        OBSERVER_SERVICE.removeObserver(this, "yasearch-refresh-login-status", false);
        OBSERVER_SERVICE.removeObserver(this, "yasearch-login-state-changed", false);
        this._isObserving = false;
    },
    
    _finalize: function YandexAuth__finalize() {
        delete this._listeners;
        this._stopObserve();
    },
    
    _getYandexUserObject: function YandexAuth__getYandexUserObject() {
        this._yaAuth.Users.getUser(username);
    }
};

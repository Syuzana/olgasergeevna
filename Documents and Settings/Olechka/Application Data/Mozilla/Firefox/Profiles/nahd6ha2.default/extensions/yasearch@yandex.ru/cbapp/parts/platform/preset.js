
BarPlatform.Preset = Base.extend({
    constructor: function Preset_constructor(content, address) {
        this._widgetsInfo = [];
        this._pluginsInfo = [];
        this._logger = BarPlatform._getLogger("Preset" + Date.now());
        this._applicationLocale = misc.parseLocale(BarPlatform._application.localeString);
        
        if (address !== undefined) {
            this._baseURI = this._ioService.newURI(address, null, null);
            this._address = address;
        }
        
        if (content instanceof Ci.nsIFile) {
            this._loadFromFile(content);
        } else if (content instanceof Ci.nsIDOMDocument) {
            this._loadFromDocument(content);
        } else
            throw new CustomErrors.EArgType("content", "nsIFile|nsIDOMDocument", typeof content);
    },
    get address() {
        return this._address;
    },
    
    get name() {
        return this._name;
    },
    
    get version() {
        return this._version;
    },
    
    get author() {
        return this._author;
    },
    
    get icon() {
        return this._icon;
    },
    get url() {
        return this._url;
    },
    
    
    get widgetEntries() {
        return sysutils.copyObj(this._widgetsInfo, true);
    },
    
    
    get pluginEntries() {
        return sysutils.copyObj(this._pluginsInfo, true);
    },
    
    get allEntries() {
        return sysutils.copyObj(this._pluginsInfo.concat(this._widgetsInfo), true);
    },
    get content() {
        return this._content;
    },
    _ioService: Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService),
    
    _consts: {
        STR_PRESET_ELEMENT_NAME: "preset",
        STR_SETTING_ELEMENT_NAME: "setting",
        
        WARN_COMP_IGNORED: "Component was ignored because of syntax errors",
        WARN_SETTING_IGNORED: "Widget setting was ignored because of syntax errors"
    },
    
    _address: undefined,
    _baseURI: null,
    _author: undefined,
    _name: undefined,
    _version: "1.0",
    _icon: undefined,
    _widgetsInfo: undefined,
    _pluginsInfo: undefined,
    
    _resolveUrl: function Preset__resolveUrl(spec) {
        try {
            var url = this._ioService.newURI(spec, null, this._baseURI);
        } catch(e) {
            return spec;
        }
        
        if (this._baseURI && (url.spec == this._baseURI.spec))
            return "";
        return url.spec;
    },
    
    _loadFromFile: function Preset__loadFromFile(presetFile) {
        this._loadFromDocument( fileutils.xmlDocFromFile(presetFile) );
    },
    
    _loadFromDocument: function Preset__loadFromDocument(XMLDocument) {
        let rootName = XMLDocument.documentElement.localName;
        if (rootName != this._consts.STR_PRESET_ELEMENT_NAME)
            throw new BarPlatform.Preset.EPresetSyntax(rootName);
        this._parsePreset(XMLDocument.documentElement);
        
        let serializer = Components.classes["@mozilla.org/xmlextras/xmlserializer;1"]
                                   .createInstance(Components.interfaces.nsIDOMSerializer);
        this._content = serializer.serializeToString(XMLDocument);
    },
    _findBestMatch: function Preset__findBestMatch(elementsArray) {
        if (!elementsArray || elementsArray.length < 1)
            return null;
        
        const weights = {
            language: 32,
            empty: 16,
            ru: 8,
            en: 4,
            country: 2,
            region: 1
        };
        
        let applicationLocale = this._applicationLocale;
        let locales = [];
        for each (let element in elementsArray) {
            let locale = element.getAttribute("locale") || element.getAttribute("lang") || null,
                weight = 0;
            if (locale) {
                locale = misc.parseLocale(locale);
                
                for (let space in weights) {
                    if (space in locale) {
                        let component = locale[space];
                        if (space == "language")
                            if (component in weights)
                                weight += weights[component];
                        if (component === applicationLocale[space])
                            weight += weights[space];
                    }
                }
            } else {
                weight = weights["empty"];
            }
            
            locales.push({
                element: element,
                weight: weight
            });
        }
        
        locales.sort(function rule(a, b) {
            if (a.weight == b.weight)
                return 0;
            return a.weight < b.weight ? -1 : +1;
        });
        
        return locales.pop().element.textContent;
    },
    
    _parsePreset: function Preset__parsePreset(presetElement) {
        this._version = presetElement.getAttribute("version") || "1.0";
        
        let appLang = misc.parseLocale(BarPlatform._application.localeString).language;
        let urlNodes = misc.queryXMLDoc("./url", presetElement);
        this._url = this._findBestMatch(urlNodes);
        
        if (!this._baseURI) {
            try {
                this._baseURI = this._ioService.newURI(this._url, null, null);
            }
            catch (e) {
                this._logger.warn("Preset URL in the <url> node is malformed.");
            }
        }
        let authorNodes = misc.queryXMLDoc("./author", presetElement);
        this._author = this._findBestMatch(authorNodes);
        if (!this._author)
            throw new BarPlatform.Preset.EPresetSyntax("author");
        let nameNodes = misc.queryXMLDoc("./name", presetElement);
        this._name = this._findBestMatch(nameNodes);
        if (!this._name)
            throw new BarPlatform.Preset.EPresetSyntax("name");
        let iconNodes = misc.queryXMLDoc("./icon", presetElement);
        this._icon = this._resolveUrl(this._findBestMatch(iconNodes));
        let widgetNodes = misc.queryXMLDoc("./widget", presetElement);
        for each (let widgetNode in widgetNodes) {
            let widgetInfo = this._parseWidgetElement(widgetNode);
            if (widgetInfo)
                this._widgetsInfo.push(widgetInfo);
        }
        let pluginNodes = misc.queryXMLDoc("./plugin", presetElement);
        for each (let pluginNode in pluginNodes) {
            let pluginInfo = this._parsePluginElement(pluginNode);
            if (pluginInfo)
                this._pluginsInfo.push(pluginInfo);
        }
    },
    
    _parseSettings: function Preset__parseSettings(widgetElement) {
        let settingElements = {__proto__: null};
        
        for (let settingIndex = widgetElement.childNodes.length; settingIndex--; ) {
            let settingElement = widgetElement.childNodes[settingIndex];
            if (settingElement.nodeType != settingElement.ELEMENT_NODE ||
                settingElement.localName != this._consts.STR_SETTING_ELEMENT_NAME)
                continue;
            
            let settingName = settingElement.getAttribute("name");
            if (!settingName) {
                this._logger.warn(this._consts.WARN_SETTING_IGNORED);
                continue;
            }
            
            if (!(settingName in settingElements))
                settingElements[settingName] = [];
            
            settingElements[settingName].push(settingElement);
        }
        
        let settings = {__proto__: null};
        for (let name in settingElements)
            settings[name] = this._findBestMatch(settingElements[name]);
        
        return settings;
    },
    
    _parseWidgetElement: function Preset__parseWidgetElement(widgetElement) {
        let protoID = widgetElement.getAttribute("id");
        if (!protoID) {
            this._logger.warn(this._consts.WARN_COMP_IGNORED);
            return null;
        }
        
        protoID = this._resolveUrl(protoID);
        
        let [packageID, widgetName] = BarPlatform.parseComponentID(protoID);
        let widgetInfo = {
            type: "widget",
            componentID: protoID,
            packageID: packageID,
            packageURI: packageID,
            name: widgetName,
            visible: !(widgetElement.getAttribute("visible") == "false"),
            settings: this._parseSettings(widgetElement)
        };
        
        return widgetInfo;
    },
    
    _parsePluginElement: function Preset__parseWidgetElement(pluginElement) {
        let pluginID = pluginElement.getAttribute("id");
        if (!pluginID) {
            this._logger.warn(this._consts.WARN_COMP_IGNORED);
            return null;
        }
        
        pluginID = this._resolveUrl(pluginID);
        
        let [packageID, pluginName] = BarPlatform.parseComponentID(pluginID);
        
        let pluginInfo = {
            type: "plugin",
            componentID: pluginID,
            packageID: packageID,
            packageURI: packageID,
            name: pluginName,
            enabled: !(pluginElement.getAttribute("enabled") == "false"),
            settings: this._parseSettings(pluginElement)
        };
        
        return pluginInfo;
    }
});

BarPlatform.Preset.EPresetSyntax = CustomErrors.ECustom.extend({
    $name: "EPresetSyntax",
    
    constructor: function EPresetSyntax(elementName) {
        this.base("Preset parse error");
        this._elementName = elementName.toString();
    },
    
    get _details() {
        return [this._elementName];
    },
    
    _elementName: undefined
});
BarPlatform._ParserConsts = {
    STR_BAR_API_ATTR: "bar-api",
    STR_WIDGET_ELEMENT_NAME: "widget",
    STR_PLUGIN_ELEMENT_NAME: "plugin",
    COMPONENT_TYPES: {
        plugin: 0,
        widget: 0
    }
};

BarPlatform.Unit = function XBUnit(fileName, package_, name) {
    if (!sysutils.isString(fileName))
        throw new CustomErrors.EArgType("fileName", "String", fileName);
    if ( !(package_ instanceof BarPlatform.WidgetPackage) )
        throw new CustomErrors.EArgType("package_", "WidgetPackage", package_);
    if (!sysutils.isString(name))
        throw new CustomErrors.EArgType("name", "String", name);
    if (name.length < 1)
        throw new CustomErrors.EArgRange("name", "/.+/", name);
    this._fileName = fileName;
    this._package = package_;
    this._name = name;
    this._logger = BarPlatform._getLogger("Unit_" + name);
};

BarPlatform.Unit.prototype = {
    constructor: BarPlatform.Unit,
    
    get name() {
        return this._name;
    },
    
    get unitPackage() {
        return this._package;
    },
    
    
    get componentInfo() {
        if (!this._componentInfo) {
            let channel = this._package.newChannelFromPath(this._fileName);
            try {
                let infoParser = new UnitInfoParser();
                this._componentInfo = infoParser.parseFromStream(channel.contentStream, channel.originalURI);
                this._componentInfo.id = this._package.id + "#" + this._name;
                if (this._componentInfo.iconPath)
                    this._componentInfo.iconURL = this._package.resolvePath(this._componentInfo.iconPath);
                this._componentInfo.package_ = this._package;
            }
            finally {
                channel.contentStream.close();
            }
        }
        return sysutils.copyObj( this._componentInfo );
    },
    
    
    get widgetProto() {
        if (!(this._widgetProto || this._parseError)) {
            if (this.componentInfo.type != BarPlatform._ParserConsts.STR_WIDGET_ELEMENT_NAME)
                throw new Error("This unit does not declare a widget");
            this._load();
        }
        
        if (!this._widgetProto && this._parseError)
            throw this._parseError;
        return this._widgetProto;
    },
    
    
    get plugin() {
        if (!(this._plugin || this._parseError)) {
            if (this.componentInfo.type != BarPlatform._ParserConsts.STR_PLUGIN_ELEMENT_NAME)
                throw new Error("This unit does not declare a plugin");
            this._load();
        }
        
        if (!this._plugin && this._parseError)
            throw this._parseError;
        return this._plugin;
    },
    
    checkSecurity: function XBUnit_checkSecurity() {
        if (this.componentInfo.barAPI == "native") {
            let thisPackageID = this._package.id;
            if (!BarPlatform._application.isTrustedPackageURL(thisPackageID))
                throw new CustomErrors.ESecurityViolation("Creating native component", thisPackageID);
        }
        return true;
    },
    
    check: function XBUnit_check() {
        this.checkSecurity();
        
        switch (this.componentInfo.type) {
            case "widget": {
                let widgetProto = this.widgetProto;
                break;
            }
            case "plugin": {
                let plugin = this.plugin;
                break;
            }
        }
        return true;
    },
    
    finalize: function XBUnit_finalize() {
        if (this._widgetProto) {
            this._widgetProto.finalize();
            this._widgetProto = null;
        }
        this._componentInfo = null;
        this._parseError = undefined;
        this._name = undefined;
        this._package = null;
        this._logger = null;
    },
    _load: function XBUnit__load() {
        try {
            let channel = this._package.newChannelFromPath(this._fileName);
            let unitDoc = fileutils.xmlDocFromStream(channel.contentStream, channel.originalURI, channel.originalURI);
            let docElement = unitDoc.documentElement;
            let barAPI = docElement.getAttribute(BarPlatform._ParserConsts.STR_BAR_API_ATTR) || "xb";
            if (!(barAPI in this._consts.PLATFORMS))
                throw new BarPlatform.Unit.EUnitSyntax(docElement.nodeName,
                                                       strutils.formatString("Unknown API type \"%1\"", [barAPI]));
            
            let componentType = docElement.localName;
            let parser = BarPlatform._getParser(barAPI, componentType);
            if (!parser)
                throw new Error(strutils.formatString("No \"%2\" parser registered for API \"%1\".",
                                                      [barAPI, componentType]));
            switch (componentType) {
                case BarPlatform._ParserConsts.STR_WIDGET_ELEMENT_NAME:
                    this._widgetProto = parser.parseFromDoc(unitDoc, this);
                    break;
                case BarPlatform._ParserConsts.STR_PLUGIN_ELEMENT_NAME:
                    this._plugin = parser.parseFromDoc(unitDoc, this);
                    break;
                default: throw new BarPlatform.Unit.
                    EUnitSyntax(componentType, strutils.formatString("Unknown component type \"%1\"", [componentType]));
            }
        }
        catch (e) {
            this._parseError = e;
            this._logger.error(this._consts.ERR_PARSING_COMPONENT + ". " + strutils.formatError(e));
            if (!(e instanceof BarPlatform.Unit.EWidgetSyntax))
                this._logger.debug(e.stack);
        }
    },
    
    _consts: {
        ERR_PARSING_COMPONENT: "An error occured while parsing component",
        PLATFORMS: {
            "xb": 0,
            "native": 0
        }
    },
    _name: undefined,
    _logger: null,
    _widgetProto: null,
    _parseError: undefined,
    _fileName: undefined,
    _package: null
};


BarPlatform.Unit.parseSetting = function Unit_parseSetting(settingElement, defaultScope) {
    let settingName = settingElement.getAttribute("name");
    if (!settingName)
        throw new BarPlatform.Unit.EUnitSyntax(settingElement.nodeName, this._consts.ERR_NO_SETTING_NAME);
    
    let settingScope = this.evalScope(settingElement.getAttribute("scope") || undefined, defaultScope);
    let defaultValue = settingElement.getAttribute("default");
    let settingTypes = BarPlatform.Unit.settingTypes;
    
    function evalValueType(valTypeName) {
        switch (valTypeName) {
            case "number":
            case "float":
                return settingTypes.STYPE_FLOAT;
            case "int":
                return settingTypes.STYPE_INTEGER;
            case "boolean":
                return settingTypes.STYPE_BOOLEAN;
            default:
                return settingTypes.STYPE_STRING;
        }
    }
    
    let settingType = settingTypes.STYPE_STRING;
    let controlElement = settingElement.getElementsByTagNameNS("*", "control")[0];
    if (controlElement) {
        switch (controlElement.getAttribute("type")) {
            case "checkbox":
                settingType = settingTypes.STYPE_BOOLEAN;
                break;
            case "textedit":
                settingType = evalValueType(controlElement.getAttribute("value-type"));
                break;
            case "custom":
                settingType = evalValueType(controlElement.getAttribute("fx-value-type"));
                break;
        }
    }
    
    return {
        name: settingName,
        scope: settingScope,
        defaultValue: defaultValue,
        type: settingType,
        controlElement: controlElement
    };
};

BarPlatform.Unit.evalScope = function Unit_evalScope(scopeName, defaultScope) {
    switch (scopeName) {
        case "package":
            return this.scopes.ENUM_SCOPE_PACKAGE;
        case "widget":
            return this.scopes.ENUM_SCOPE_WIDGET;
        case "plugin":
            return this.scopes.ENUM_SCOPE_PLUGIN;
        case "instance":
            return this.scopes.ENUM_SCOPE_INSTANCE;
        case undefined:
            if (defaultScope !== undefined)
                return defaultScope;
        default:
            throw new CustomErrors.EArgRange("scopeName", "package|widget|plugin|instance|{default}", scopeName);
    }
};

BarPlatform.Unit.scopes = {
    ENUM_SCOPE_PACKAGE: 0,
    ENUM_SCOPE_WIDGET: 1,
    ENUM_SCOPE_PLUGIN: 2,
    ENUM_SCOPE_INSTANCE: 3
};

BarPlatform.Unit.settingTypes = {
    STYPE_STRING: 0,
    STYPE_INTEGER: 1,
    STYPE_FLOAT: 2,
    STYPE_BOOLEAN: 3
};

BarPlatform.Unit.EUnitSyntax = CustomErrors.ECustom.extend({
    $name: "EUnitSyntax",
    
    constructor: function EUnitSyntax(elementName, explanation) {
        this.base();
        this._elementName = elementName.toString();
        this._explanation = explanation.toString();
    },
    
    get _details() {
        return [this._elementName, this._explanation];
    },
    
    _message: "Unit parse error",
    _elementName: undefined,
    _explanation: undefined
});


BarPlatform.Unit.EWidgetSyntax = BarPlatform.Unit.EUnitSyntax.extend({
    $name: "EWidgetSyntax",
    
    _message: "Widget parse error"
});

BarPlatform.Unit.EPluginSyntax = BarPlatform.Unit.EUnitSyntax.extend({
    $name: "EPluginSyntax",
    
    _message: "Bar plugin parse error"
});

function UnitInfoParser() {
    this._componentInfo = {};
    this._saxReader = Cc["@mozilla.org/saxparser/xmlreader;1"].createInstance(Ci.nsISAXXMLReader);
    this._logger = BarPlatform._getLogger("IParser");
};

UnitInfoParser.prototype = {
    constructor: UnitInfoParser,
    
    
    parseFromStream: function widgetInfoParser_parseFromStream(inputStream, baseURI) {
        this._saxReader.baseURI = baseURI;
        this._saxReader.contentHandler = this;
        this._logger.trace("Parsing component info " + baseURI.spec);
        this._saxReader.parseFromStream(inputStream, null, "text/xml");
        if (this._error)
            throw this._error;
        return this._componentInfo;
    },
    startElement: function widgetInfoParser_startElement(uri, localName, qName, attributes) {
        if (localName in BarPlatform._ParserConsts.COMPONENT_TYPES) {
            this._componentInfo.type = localName;
            this._componentInfo.name = attributes.getValueFromQName("name") || "";
            this._componentInfo.isUnique = (attributes.getValueFromQName("unique") != "false");
            this._componentInfo.barAPI = attributes.getValueFromQName("bar-api") || undefined;
            this._componentInfo.iconPath = attributes.getValueFromQName("icon") || "";
        }
        else
            this._error = new BarPlatform.Unit.EUnitSyntax(qName, "Unknown component type");
        this._saxReader.contentHandler = null;
    },
    
    startDocument: function widgetInfoParser_startDocument() {},
    endDocument: function widgetInfoParser_endDocument() {},
    endElement: function widgetInfoParser_endElement() {},
    characters: function widgetInfoParser_characters() {},
    processingInstruction: function widgetInfoParser_processingInstruction() {},
    ignorableWhitespace: function widgetInfoParser_ignorableWhitespace() {},
    startPrefixMapping: function widgetInfoParser_startPrefixMapping() {},
    endPrefixMapping: function widgetInfoParser_endPrefixMapping() {},
    QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports, Ci.nsISAXContentHandler])
};


BarPlatform.WidgetPrototypeBase = Base.extend({
    constructor: function WidgetPrototypeBase(id, name, unique, iconPath, unit) {
        if (!id)
            throw new CustomErrors.EArgRange("id", "/.+/", id);
        if ( !(unit instanceof BarPlatform.Unit) )
            throw new CustomErrors.EArgType("unit", "Unit", unit);
        this._id = id.toString();
        this._unit = unit;
        this._unique = !!unique;
        this._name = name.toString();
        this._iconPath = iconPath;
        this._spawnedIDs = {};
    },
    
    finalize: function WidgetPrototypeBase_finalize() {
        this._unit = null;
        this._spawnedIDs = null;
    },
    
    get id() {
        return this._id;
    },
    
    get name() {
        return this._name;
    },
    
    get iconURI() {
        return this._iconPath ? this.pkg.resolvePath(this._iconPath) : "";
    },
    
    get iconPath() {
        return this._iconPath;
    },
    
    get isUnique() {
        return this._unique;
    },
    
    get unit() {
        return this._unit;
    },
    
    get pkg() {
        return this._unit.unitPackage;
    },
    
    get spawnedIDs() {
        return misc.mapKeysToArray(this._spawnedIDs);
    },
    
    _name: undefined,
    _iconPath: undefined,
    _unit: null,
    _unique: false
});

const Cc = Components.classes;
const Ci = Components.interfaces;

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

function nsIYaPassManager() {
  this.wrappedJSObject = this;
};

nsIYaPassManager.prototype = {
  classDescription: "YasearchPassManager Autocomplete",
  contractID: "@mozilla.org/autocomplete/search;1?name=YasearchPassComplete",
  classID: Components.ID("{7a19ec9e-54e4-4099-ae92-ac3628c67889}"),
  _xpcom_categories: [{ category: "app-startup", service: true }],

  QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports, Ci.nsIAutoCompleteSearch]),
  
  get loginManager() {
    return Cc["@mozilla.org/login-manager;1"].getService(Ci.nsILoginManager);
  },
  
  _defaultUserPrefName: "yasearch.users.default",
  
  get onHoverTreeUglyHack() {
    return this.__onHoverTreeUglyHack;
  },
  
  set onHoverTreeUglyHack(val) {
    this.__onHoverTreeUglyHack = !!val;
    return this.__onHoverTreeUglyHack;
  },
  
  startSearch: function(aSearchString, aSearchParam, aPreviousResult, aListener) {
    let candidats = [];
    
    if (aSearchString) {
      for each (let login in this.savedLogins)
        if (login.indexOf(aSearchString) == 0)
          candidats.push(login);
    } else {
      candidats = this.savedLogins;
    }
    
    let me = this;
    
    let result = {
      QueryInterface: function(aIID) {
        if (aIID.equals(Ci.nsIAutoCompleteSearch) || aIID.equals(Ci.nsISupports))
          return this;
        
        throw Components.results.NS_NOINTERFACE;
      },
      
      results: candidats,
      defaultIndex: 0,
      errorDescription: null,
      matchCount:    candidats.length,
      searchResult:  Ci.nsIAutoCompleteResult[candidats.length ? "RESULT_SUCCESS" : "RESULT_NOMATCH"],
      searchString:  aSearchString,
      getCommentAt:  function(index) { return ""; },
      getStyleAt:    function(index) {
        return me.onHoverTreeUglyHack ? "commentColumnHover" : "";
      },
      getImageAt: function(index) { return ""; },
      getValueAt: function(index) { return this.results[index]; },
      getLabelAt: function(index) { return this.getValueAt(index); },
      removeValueAt: function(rowIndex, removeFromDb) {}
    }
    
    aListener.onSearchResult(this, result);
  },
  
  stopSearch: function() {},
  
  _getLoginChromeURL: function(aType) {
    if (aType == "mfd")
      return "chrome://yasearch/mfd/";
    
    return "chrome://yasearch/";
  },
  
  _savedLogins: null,
  
  resetSavedLogins: function() {
    this.savedLogins = null;
  },
  
  set savedLogins(val) {
    if (val !== null)
      throw "YasearchPassComplete.savedLogins must be null";
    
    this._savedLogins = null;
  },
  
  get savedLogins() {
    return this._savedLogins || (this._savedLogins = this._getSavedLogins());
  },
  
  _getSavedLogins: function(aTypesArray) {
    let logins = [];
    
    let types = aTypesArray || ["mfd", "yandex"];
    
    types.forEach(function(aType) {
      let chromeURL = this._getLoginChromeURL(aType);
      logins = logins.concat(this.loginManager.findLogins({}, chromeURL, chromeURL, null)
                                 .map(function(login) login.username));
    }, this);
    
    return logins.sort();
  },
  
  get hasSavedLogins() {
    return !!this.savedLogins.length;
  },
  _prepeareUsername: function(aUsername, aType) {
    if (aType == "mfd")
      return aUsername;
    
    function toLowerWithoutDots(aString) {
      return aString.toLowerCase().replace(/\./g, "-");
    }
    
    let usernameLower = toLowerWithoutDots(aUsername);
    let savedLogins = this.savedLogins;
    
    return savedLogins.filter(function(aLogin) {
             return aLogin === aUsername;
           })[0] ||
           savedLogins.filter(function(aLogin) {
             return toLowerWithoutDots(aLogin) === usernameLower;
           })[0] ||
           savedLogins.filter(function(aLogin) {
             return aLogin.split("@")[0] === aUsername;
           })[0] ||
           savedLogins.filter(function(aLogin) {
             return toLowerWithoutDots(aLogin).split("@")[0] === usernameLower;
           })[0] ||
           aUsername;
  },
  
  _loginManagerFindLogin: function(aUsername, aType) {
    let types = aType ? [aType] : ["mfd", "yandex"];
    
    for each (let [index, type] in Iterator(types)) {
      let chromeURL = this._getLoginChromeURL(type);
      let storedLogins = this.loginManager.findLogins({}, chromeURL, chromeURL, null);
      
      for (let i = 0, len = storedLogins.length; i < len; i++) {
        let username = storedLogins[i].username;
        if (username == aUsername || (type == "yandex" && username.split("@")[0] == aUsername)) {
          return storedLogins[i];
        }
      }
    }
    
    return null;
  },
  
  _removeUserData: function(aUsername, aType) {
    let login = this._loginManagerFindLogin(aUsername, aType);
    
    if (!login)
      return false;
    
    this.loginManager.removeLogin(login);
    
    return true;
  },
  
  notifyObservers: function() {
    Cc["@mozilla.org/observer-service;1"]
        .getService(Ci.nsIObserverService)
        .notifyObservers(null, "yasearch-refresh-data", "login");
  },
  
  removeUserData: function(aUsername, aType) {
    let username = this._prepeareUsername(aUsername, aType);
    
    this.resetSavedLogins();
    
    if (this._removeUserData(username, aType))
      this.notifyObservers();
    
    if (aType == "yandex") {
      try {
        const prefBranch = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch);
        if (username == prefBranch.getCharPref(this._defaultUserPrefName))
          prefBranch.clearUserPref(this._defaultUserPrefName);
      } catch(e) {}
    }
  },
  
  getStoredPassword: function(aUsername) {
    let login = this._loginManagerFindLogin(this._prepeareUsername(aUsername, "mfd"), "mfd") ||
                this._loginManagerFindLogin(this._prepeareUsername(aUsername, "yandex"), "yandex");
    
    return login ? login.password : null;
  },
  
  getSavedLoginInfo: function(aUsername) {
    let result = {
      password: null,
      type: "yandex"
    };
    
    let login = this._loginManagerFindLogin(this._prepeareUsername(aUsername, "mfd"), "mfd");
    if (login)
      result.type = "mfd";
    else
      login = this._loginManagerFindLogin(this._prepeareUsername(aUsername, "yandex"), "yandex");
    
    if (!login)
      return null;
    
    result.password = login.password;
    
    return result;
  },
  
  getSavedLoginsInfo: function(aTypesArray) {
    let logins = [];
    
    let types = aTypesArray || ["yandex", "mfd"];
    
    types.forEach(function(aType) {
      let chromeURL = this._getLoginChromeURL(aType);
      let storedLogins = this.loginManager.findLogins({}, chromeURL, chromeURL, null);
      
      for (let i = 0, len = storedLogins.length; i < len; i++) {
        let l = storedLogins[i];
        logins.push({
          username: l.username,
          password: l.password,
          type: aType
        });
      }
    }, this);
    
    return logins;
  },
  
  removeAllUsersData: function() {
    this.savedLogins.forEach(function(aUsername) {
      this._removeUserData(aUsername);
    }, this);
    
    try {
      const prefBranch = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch);
      prefBranch.clearUserPref(this._defaultUserPrefName);
    } catch(e) {}
    
    this.resetSavedLogins();
  },
  
  storeLoginDetails: function(aUsername, aPassword, aStore, aType) {
    if (aType == "mfd")
      aUsername = aUsername.toLowerCase();
    
    this.removeUserData(aUsername, aType);
    
    if (aStore) {
      const nsLoginInfo = new Components.Constructor("@mozilla.org/login-manager/loginInfo;1", Ci.nsILoginInfo, "init");
      
      let chromeURL = this._getLoginChromeURL(aType);
      let loginInfo = new nsLoginInfo(chromeURL, chromeURL, null, aUsername, aPassword, "", "");
      
      this.loginManager.addLogin(loginInfo);
      
      if (aType == "yandex") {
        const prefBranch = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch);
        prefBranch.setCharPref(this._defaultUserPrefName, aUsername);
      }
      
      this.resetSavedLogins();
      
      this.notifyObservers();
    }
  }
};

if (XPCOMUtils.generateNSGetFactory)
  var NSGetFactory = XPCOMUtils.generateNSGetFactory([nsIYaPassManager]);
else
  var NSGetModule = XPCOMUtils.generateNSGetModule([nsIYaPassManager]);

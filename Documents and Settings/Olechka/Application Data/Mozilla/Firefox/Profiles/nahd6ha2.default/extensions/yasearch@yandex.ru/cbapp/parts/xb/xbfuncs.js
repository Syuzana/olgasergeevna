
XB._functions = {};

XB._functions.CN_event = XB._calcNodes.FuncNode.extend({
    $name: "CN_event",
    
    expectedArgNames: ["condition", "action"],
    
    
    constructor: function FEvent() {
        this.base.apply(this, arguments);
        this._timer = XB._Cc["@mozilla.org/timer;1"].createInstance(XB._Ci.nsITimer);
    },
    
    QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports, Ci.nsIObserver, Ci.nsITimerCallback]),
    notify: function Fevent_notify() {
        try {
            this._argManager.getValByName(this.expectedArgNames[1]);
        }
        catch (e) {
            this._logger.error("CN_event.notify() failed. " + e.message);
        }
    },
    
    _calculate: function FEvent__calculate(changedArgs) {
        let cond = this._argManager.getValByNameDef(this.expectedArgNames[0], "Bool", false);
        if (cond) {
            this._timer.cancel();
            this._timer.initWithCallback(this, 10, this._timer.TYPE_ONE_SHOT);
        }
    }
});

XB._functions.CN_assign = XB._calcNodes.ProcNode.extend({
    $name: "CN_assign",
    
    expectedArgNames: ["name", "value"],
    
    _proc: function Fassign__proc(eventInfo) {
        let refName = this._argManager.getValByName("name", "String");
        let refNode = this._parentWidget.findVariable(refName);
        if (!refNode)
            refNode = this._parentWidget.findSetting(refName);
        if (!refNode)
            throw new Error(XB._base.consts.ERR_DATA_UNDEFINED + " (" + refName + ")");
        let newValue = this._argManager.getValByName("value");
        refNode.setValue(newValue);
    }
});

XB._functions.CN_cast = XB._calcNodes.FuncNode.extend({
    $name: "CN_cast",
    
    expectedArgNames: ["type", "value"],
    
    _calculate: function Fcast__calculate(changedArgs) {
        let preferedType = this._argManager.getValByName("type", "String");
        let value = this._argManager.getValByName("value");
        
        switch (preferedType) {
            case "empty":
                return XB.types.empty;
            case "bool":
                return XB._base.runtime.xToBool(value);
            case "number":
                return XB._base.runtime.xToNumber(value);
            case "string":
                return XB._base.runtime.xToString(value);
            default:
                throw new Error("Unknown cast type: " + preferedType);
        }
    }
});
XB._functions.RegEx = XB._calcNodes.FuncNode.extend({
    $name: "RegEx",
    
    _calculate: function FRegex__calculate(changedArgs) {
        if (!this._compiledRegExp || this._argManager.argInArray("expression", changedArgs))
            this._compiledRegExp = new RegExp(this._argManager.getValByName("expression", "String"), "m");
    },
    
    _compiledRegExp: null
});
XB._functions["CN_regex-test"] = XB._functions.RegEx.extend({
    $name: "CN_regex-test",
    
    expectedArgNames: ["expression", "value"],
    
    _calculate: function FRegexTest__calculate(changedArgs) {
        this.base(changedArgs);
        let string = this._argManager.getValByName("value", "String");
        return this._compiledRegExp.test(string);
    }
});
XB._functions["CN_regex-search"] = XB._functions.RegEx.extend({
    $name: "CN_regex-search",
    
    expectedArgNames: ["expression", "value"],
    
    _calculate: function FRegexSearch__calculate(changedArgs) {
        this.base(changedArgs);
        
        let string = this._argManager.getValByName("value", "String");
        let match = string.match(this._compiledRegExp);
        if (!match)
            return XB.types.empty;
        return match.length > 1 ? match[1] : match[0];
    }
});
XB._functions["CN_regex-escape"] = XB._functions.RegEx.extend({
    $name: "CN_regex-escape",
    
    expectedArgNames: ["value"],
    
    _calculate: function FRegexEscape__calculate(changedArgs) {
        let string = this._argManager.getValByName("value", "String");
        return strutils.escapeRE(string);
    }
});
XB._functions["CN_substring"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_substring",
    
    expectedArgNames: ["value", "start", "length"],
    
    _calculate: function FSubstring__calculate(changedArgs) {
        let str = this._argManager.getValByName("value", "String");
        let start = this._argManager.getValByName("start", "Number");
        let len = this._argManager.getValByNameDef("length", "Number", XB.types.empty);
        if (len === XB.types.empty)
            return str.substr(start);
        return str.substr(start, len);
    }
});
XB._functions["CN_string-length"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_string-length",
    
    expectedArgNames: ["value"],
    
    _calculate: function FStringLength_calculate(changedArgs) {
        return this._argManager.getValByName("value", "String").length;
    }
});
XB._functions["CN_trim-string"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_trim-string",
    
    expectedArgNames: ["value", "left", "right"],
    
    _calculate: function FTrimString__calculate(changedArgs) {
        let str = this._argManager.getValByName("value", "String");
        let left = this._argManager.getValByNameDef("left", "Bool", true);
        let right = this._argManager.getValByNameDef("right", "Bool", true);
        if (left && right)
            return strutils.trimAll(str);
        else if (left)
            return strutils.trimAllLeft(str);
        else if (right)
            return strutils.trimAllRight(str);
        else
            return str;
    }
});
XB._functions["CN_lowercase-string"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_lowercase-string",
    
    expectedArgNames: ["value"],
    
    _calculate: function FLowercaseString__calculate(changedArgs) {
        return this._argManager.getValByName("value", "String").toLowerCase();
    }
});
XB._functions["CN_uppercase-string"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_uppercase-string",
    
    expectedArgNames: ["value"],
    
    _calculate: function FLowercaseString__calculate(changedArgs) {
        return this._argManager.getValByName("value", "String").toUpperCase();
    }
});
XB._functions["CN_digest"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_digest",
    
    expectedArgNames: ["value", "type"],
    
    _calculate: function FDigest__calculate(changedArgs) {
        let value = this._argManager.getValByName("value", "String");
        let type = this._argManager.getValByNameDef("type", "String", "md5");
        return misc.CryptoHash.getFromString(value, type);
    }
});
XB._functions["CN_encode"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_encode",
    
    expectedArgNames: ["value", "method"],
    
    _calculate: function FEncode__calculate(changedArgs) {
        let value = this._argManager.getValByName("value", "String");
        let method = this._argManager.getValByNameDef("method", "String", "url");
        switch (method) {
            case "url":
                return encodeURIComponent(value);
            
            case "base64":
                return btoa(value);
            
            default:
                throw new Error("Unknown method " + method);
        }
    }
});
XB._functions["CN_decode"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_decode",
    
    expectedArgNames: ["value", "method"],
    
    _calculate: function FDecode__calculate(changedArgs) {
        let value = this._argManager.getValByName("value", "String");
        let method = this._argManager.getValByNameDef("method", "String", "url");
        switch (method) {
            case "url":
                return decodeURIComponent(value);
            
            case "base64":
                return atob(value);
            
            default:
                throw new Error("Unknown method " + method);
        }
    }
});
XB._functions["CN_unescape-html"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_unescape-html",
    
    expectedArgNames: ["source"],
    
    _calculate: function FUnescapeHTML__calculate(changedArgs) {
        return this._unescaper.unescape(this._argManager.getValByName(this.expectedArgNames[0], "String"));
    },
    
    get _unescaper() {
        let unescaper = Cc["@mozilla.org/feed-unescapehtml;1"].getService(Ci.nsIScriptableUnescapeHTML);
        this.__defineGetter__("_unescaper", function() unescaper);
        return unescaper;
    }
});
XB._functions.Arithmetical = XB._calcNodes.FuncNode.extend({
    $name: "Arithmetical",
    
    _calculate: function FArithmetical__calculate(changedArgs) {
        if (this._argManager.argsCount < 2)
            return new XB.types.Exception(this._getHumanReadableID(), XB.types.Exception.types.E_SYNTAX,
                                          "Arithmetic functions take at least 2 arguments");
        return this._applyOp(this._operation);
    },
    
    _applyOp: function FArithmetical__applyOp(op) {
        let result = undefined;
        for (let valIndex = 0, len = this._argManager.argsCount; valIndex < len; valIndex++) {
            let num = this._argManager.getValByIndex(valIndex, "Number");
            if (result == undefined)
                result = num;
            else
                result = op(result, num);
        }
        return result;
    }
});

XB._functions.Arithmetical.PRECISION = 6;
XB._functions.CN_add = XB._functions.Arithmetical.extend({
    $name: "CN_add",
    
    _operation: function Fadd__operation(p1, p2) {
        return p1 + p2;
    }
});
XB._functions.CN_sub = XB._functions.Arithmetical.extend({
    $name: "CN_sub",
    
    _operation: function Fsub__operation(p1, p2) {
        return p1 - p2;
    }
});
XB._functions.CN_mul = XB._functions.Arithmetical.extend({
    $name: "CN_mul",
    
    _operation: function Fmul__operation(p1, p2) {
        return p1 * p2;
    }
});
XB._functions.CN_div = XB._functions.Arithmetical.extend({
    $name: "CN_div",
    
    _operation: function Fdiv__operation(p1, p2) {
        if (p2 == 0)
            throw new Error("Division by zero");
        return p1 / p2;
    }
});
XB._functions.CN_mod = XB._functions.Arithmetical.extend({
    $name: "CN_mod",
    
    _operation: function Fmod__operation(p1, p2) {
        if (p2 == 0)
            throw new Error("Division by zero");
        return p1 % p2;
    }
});
XB._functions["CN_format-number"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_format-number",
    
    expectedArgNames: ["value", "precision", "separate-groups", "positive-sign"],
    
    _calculate: function FFormatNumber__calculate(changedArgs) {
        return strutils.formatNumber(this._argManager.getValByName("value", "Number"),
                                     this._argManager.getValByNameDef("precision", "Number", undefined),
                                     this._argManager.getValByNameDef("positive-sign", "Bool", false),
                                     this._argManager.getValByNameDef("separate-groups", "Bool", true));
    }
});
XB._functions.Periodic = XB._calcNodes.FuncNode.extend({
    $name: "Periodic",
    
    expectedArgNames: ["interval"],
    
    constructor: function FPeriodic() {
        this.base.apply(this, arguments);
        this._timer = XB._Cc["@mozilla.org/timer;1"].createInstance(XB._Ci.nsITimer);
    },
    
    _resetTimer: function FPeriodic__resetTimer() {
        let expireTime = this._argManager.getValByNameDef(this.expectedArgNames[0], "Number", Number.POSITIVE_INFINITY);
        if (expireTime == Number.POSITIVE_INFINITY) {
            this._timerSet = true;
            return;
        }
        
        if (expireTime <= 0)
            throw new RangeError(this._consts.ERR_INVALID_INTERVAL);
        expireTime = Math.max(expireTime, XB._functions.Periodic.MIN_INTERVAL);
        
        this._timer.initWithCallback(this, expireTime * 1000, this._timer.TYPE_REPEATING_SLACK);
        this._timerSet = true;
        this._timerInterval = expireTime;
    },
    
    _cancelTimer: function FPeriodic__cancelTimer() {
        this._timer.cancel();
        this._timerSet = false;
    },
    
    _calculate: function FPeriodic__calculate(changedArgs) {
        if (this.hasSubscribers() &&
            (!this._timerSet || this._argManager.argInArray(this.expectedArgNames[0], changedArgs))) {
            this._resetTimer();
        }
        else {
            this._cancelTimer();
        }
        return this._storedValue;
    },
    
    _notNeeded: function FPeriodic__notNeeded() {
        this._cancelTimer();
    },
    
    _timer: null,
    _timerInterval: undefined,
    _timerSet: false
}, {
    MIN_INTERVAL: 1
});
XB._functions.CN_timestamp = XB._functions.Periodic.extend({
    $name: "CN_timestamp",
    
    QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports, Ci.nsITimerCallback]),
    notify: function Ftimestamp_notify() {
        try {
            this._setNewVal( parseInt(Date.now() / 1000, 10) );
            this._notifyDeps();
        }
        catch (e) {
            this._logger.error("CN_timestamp.notify() failed. " + e.message);
        }
    },
    
    _calculate: function Ftimestamp__calculate(changedArgs) {
        this.base(changedArgs);
        return parseInt(Date.now() / 1000, 10);
    }
});
XB._functions["CN_time-arrived"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_time-arrived",
    
    expectedArgNames: ["time"],
    
    constructor: function FTimeArrived() {
        this.base.apply(this, arguments);
        this._timer = XB._Cc["@mozilla.org/timer;1"].createInstance(XB._Ci.nsITimer);
    },
    
    QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports, Ci.nsITimerCallback]),
    notify: function Fwait_notify() {
        try {
            if (!this.hasSubscribers())
                return;
            if (this._setNewVal(true))
                this._notifyDeps();
        }
        catch (e) {
            this._logger.error("CN_time-arrived.notify() failed. " + e.message);
        }
    },
    
    _timer: null,
    
    _calculate: function Ftimearrived__calculate(changedArgs) {
        let moreToWait = this._argManager.getValByName("time", "Number") - parseInt(Date.now() / 1000, 10);
        if (moreToWait > 0)
            this._timer.initWithCallback(this, moreToWait * 1000, this._timer.TYPE_ONE_SHOT);
        return moreToWait <= 0;
    }
});

XB._functions["CN_format-time"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_format-time",
    
    expectedArgNames: ["value", "format"],
    
    _calculate: function FFormatTime__calculate(changedArgs) {
        return this._formatTimestamp(this._argManager.getValByName("value", "Number") * 1000,
                                     this._argManager.getValByName("format", "String"));
    },
    
    _formatTimestamp: function FFormatTime__formatTimestamp(timestamp, format) {
        let date = new Date(timestamp);
        if (date == "Invalid Date")
            throw new RangeError("Invalid date timestamp (" + timestamp + ")");
        
        function leadZero(str) {
            return str.length > 1 ? str : "0" + str;
        }
        
        function formatCode(match, code) {
            switch (code) {
                case "d":
                    return date.getDate();
                case "D":
                    return leadZero("" + date.getDate());
                case "m":
                    return date.getMonth() + 1;
                case "M":
                    return leadZero("" + (date.getMonth() + 1));
                case "y":
                    return ("" + date.getFullYear()).substr(2, 2);
                case "Y":
                    return date.getFullYear();
                case "h":
                    return date.getHours();
                case "H":
                    return leadZero("" + date.getHours());
                case "n":
                    return date.getMinutes();
                case "N":
                    return leadZero("" + date.getMinutes());
                case "s":
                    return date.getSeconds();
                case "S":
                    return leadZero("" + date.getSeconds());
                case "%":
                    return "%";
                default:
                    return code;
            }
        }
        
        return format.replace(/%([dDmMyYhHnNsS%])/g, formatCode);
    }
});
XB._functions.CN_if = XB._calcNodes.FuncNode.extend({
    $name: "CN_if",
    
    expectedArgNames: ["condition", "ontrue", "onfalse"],
    
    _calculate: function Fif__calculate(changedArgs) {
        if ( this._argManager.getValByName("condition", "Bool") )
            return this._argManager.getValByNameDef("ontrue", undefined, XB.types.empty);
        return this._argManager.getValByNameDef("onfalse", undefined, XB.types.empty);
    }
});
XB._functions.CN_concat = XB._calcNodes.FuncNode.extend({
    $name: "CN_concat",
    
    _calculate: function Fconcat__calculate(changedArgs) {
        let numArgs = this._argManager.argsCount;
        if (numArgs > 0)
            return XB._functions.CN_concat.concatArgs(this, 0, numArgs - 1);
        return XB.types.empty;
    }
});
XB._functions.Data = XB._functions.CN_concat.extend({
    $name: "Data"
});

XB._functions.CN_concat.concatArgs = function ConcatArgs(funcNode, start, end) {
    if (end - start < 1)
        return funcNode._argManager.getValByIndex(start);
    
    let resultIsXML = false;
    let argXValues = [];
    
    for (; start <= end; start++) {
        let xv = funcNode._argManager.getValByIndex(start);
        argXValues.push(xv);
        resultIsXML = resultIsXML || XB._base.runtime.isXML(xv);
    }
    
    return XB._functions.CN_concat.glue(argXValues, resultIsXML);
};

XB._functions.CN_concat.glue = function ConcatGlue(valuesArray, xmlOutput) {
    if (!xmlOutput) {
        let result = "";
        
        for each (let value in valuesArray) {
            result += XB._base.runtime.xToString(value);
        }
        
        return result;
    }
    else {
        let result = new XB.types.XML();
        
        for each (let value in valuesArray) {
            if (XB._base.runtime.isXML(value))
                result.append(value);
            else {
                let node = XB._base.runtime.tempXMLDoc.createTextNode(XB._base.runtime.xToString(value));
                result.append(node);
            }
        }
        
        return result;
    }
};
XB._functions.CN_xpath = XB._calcNodes.FuncNode.extend({
    $name: "CN_xpath",
    
    expectedArgNames: ["expression", "source"],
    
    _ioService: Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService),
    
    _calculate: function Fxpath__calculate(changedArgs) {
        let expr = this._argManager.getValByName("expression", "String");
        let srcXML = this._getSrc();
        
        let namespaces = this._argManager.getValsByClass("xmlns", "String");
        let nsResolver = sysutils.isEmptyObject(namespaces)? null :
            function XPathNSResolver(ns) {
                return namespaces[ns] || null;
            };
        
        let result = srcXML.query(expr, nsResolver);
        if (srcXML.owner == this)
            srcXML.dispose();
        return result;
    },
    
    _getSrc: function Fxpath__getSrc() {
        let src = this._argManager.getValByName("source");
        if (XB._base.runtime.isXML(src))
            return src;
        
        if (src === XB.types.empty)
            throw new CustomErrors.EArgType("source", "XML|String", "Empty");
        
        let srcURL = this._parentWidget.prototype.unit.unitPackage.resolvePath(XB._base.runtime.xToString(src));
        let srcChannel = this._ioService.newChannel(srcURL, null, null).QueryInterface(Ci.nsIInputStreamChannel);
        let srcURI = srcChannel.URI;
        let retXML = new XB.types.XML( fileutils.xmlDocFromStream(srcChannel.contentStream, srcURI, srcURI) );
        retXML.owner = this;
        return retXML;
    }
});
XB._functions.CN_xslt = XB._calcNodes.FuncNode.extend({
    $name: "CN_xslt",
    
    expectedArgNames: ["stylesheet", "source"],
    
    _ioService: Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService),
    
    _calculate: function Fxslt__calculate(changedArgs) {
        let srcXML = this._getSrc();
        let stylesheetDoc = this._getStylesheet();
        
        if (this._debugMode) {
            this._owner.logger.debug(this._getHumanReadableID() + ": transforming source:\n" + srcXML);
        }
        
        let result = srcXML.transform(stylesheetDoc);
        if (srcXML.owner == this)
            srcXML.dispose();
        
        return result;
    },
    
    _getSrc: function Fxslt__getSrc() {
        let src = this._argManager.getValByName("source");
        if (XB._base.runtime.isXML(src))
            return src;
        
        if (src === XB.types.empty)
            throw new CustomErrors.EArgType("source", "XML|String", "Empty");
        
        let srcURL = this._parentWidget.prototype.unit.unitPackage.resolvePath(XB._base.runtime.xToString(src));
        let srcChannel = this._ioService.newChannel(srcURL, null, null).QueryInterface(Ci.nsIInputStreamChannel);
        let srcURI = srcChannel.URI;
        let retXML = new XB.types.XML( fileutils.xmlDocFromStream(srcChannel.contentStream, srcURI, srcURI) );
        retXML.owner = this;
        return retXML;
    },
    
    _getStylesheet: function Fxslt__getStylesheet() {
        let ssPath = this._argManager.getValByName("stylesheet", "String");
        let ssURL = this._parentWidget.prototype.unit.unitPackage.resolvePath(XB._base.runtime.xToString(ssPath));
        let ssChannel = this._ioService.newChannel(ssURL, null, null).QueryInterface(Ci.nsIInputStreamChannel);
        let ssURI = ssChannel.URI;
        return fileutils.xmlDocFromStream(ssChannel.contentStream, ssURI, ssURI);
    }
});

XB._functions.CN_attribute = XB._calcNodes.FuncNode.extend({
    $name: "CN_attribute",
    
    expectedArgNames: ["name", "value"],
    
    _calculate: function FAttribute__calculate(changedArgs) {
        let name = this._argManager.getValByName(this.expectedArgNames[0], "String");
        let value = this._argManager.getValByName(this.expectedArgNames[1], "String");
        let result = new XB.types.XML();
        result.addAttribute("", name, value);
        return result;
    }
});
XB._functions["CN_value-of"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_value-of",
    
    expectedArgNames: ["name"],
    
    _calculate: function Fvalueof__calculate(changedArgs) {
        if (!this._refNode || this._argManager.argInArray("name", changedArgs))
            this._findRefNode();
        return this._refNode.getValue(this.hasSubscribers()? this: null);
    },
    
    _notNeeded: function Fvalueof__notNeeded() {
        if (this._refNode) {
            this._refNode.unsubscribe(this);
            this._refNode = null;
            this._refName = undefined;
        }
    },
    
    _findRefNode: function Fvalueof__findRefNode() {
        let refName = this._argManager.getValByName("name", "String");
        let refNode = XB._functions["CN_value-of"].findValByName(this._parentWidget, refName);
        
        if (this._refNode != refNode) {
            if (this._refNode)
                this._refNode.unsubscribe(this);
            this._refNode = refNode;
            this._refName = refName;
        }
    },
    
    _getHumanReadableID: function FValueOf__getHumanReadableID() {
        return this.base() + (this._refName ? (" [" + this._refName + "]") : "");
    },
    
    _refNode: null,
    _refName: undefined
});
XB._functions["CN_value-of"].findValByName = function XB_findValByName(parentWidget, refName) {
    let refNode = parentWidget.findData(refName);
    
    if (!refNode)
        refNode = parentWidget.findVariable(refName);
    
    if (!refNode)
        refNode = parentWidget.findSetting(refName);
    
    if (!refNode)
        throw new Error(XB._base.consts.ERR_DATA_UNDEFINED + " (" + refName + ")");
    
    return refNode;
};
XB._functions["CN_is-empty"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_is-empty",
    
    expectedArgNames: ["value", "mode"],
    
    _calculate: function Fisempty__calculate(changedArgs) {
        let what = this._argManager.getValByName("value");
        let modeStr = this._argManager.getValByNameDef("mode", "String", "default");
        let strict = false;
        switch (modeStr) {
            case "strict":
                strict = true;
                break;
            case "default":
                strict = false;
                break;
            default:
                throw new SyntaxError("Unknown mode \"" + modeStr + "\"");
        }
        return XB._functions["CN_is-empty"].test(what, strict);
    }
});

XB._functions["CN_is-empty"].test = function EmptyTest(what, strict) {
    if (strict)
        return what === XB.types.empty;
    
    return ( strutils.compareStrings(XB._base.runtime.xToString(what), "") == 0 );
};
XB._functions.Comparison = XB._calcNodes.FuncNode.extend({
    $name: "Comparison",
    
    expectedArgNames: ["left", "right", "mode"],
    
    _calculate: function FComparision__calculate(changedArgs) {
        let mode = this._argManager.getValByNameDef("mode", "String", "default");
        let cmpMode;
        switch (mode) {
            case "strict":
                cmpMode = XB._base.runtime.cmpModes.CMP_STRICT;
                break;
            case "default":
                cmpMode = XB._base.runtime.cmpModes.CMP_FREE;
                break;
            default:
                throw new Error("Unknown mode \"" + mode + "\"");
        }
        
        let left = this._argManager.getValByName("left");
        let right = this._argManager.getValByName("right");
        return this._op( XB._base.runtime.compareValues(left, right, cmpMode) );
    }
});
XB._functions.CN_eq = XB._functions.Comparison.extend({
    $name: "CN_eq",
    
    _op: function Feq__op(compResult) {
        return compResult == 0;
    }
});

XB._functions.CN_equal = XB._functions.CN_eq;
XB._functions.CN_neq = XB._functions.CN_eq.extend({
    $name: "CN_neq",
    
    _op: function Fneq__op(compResult) {
        return !this.base(compResult);
    }
});

XB._functions["CN_not-equal"] = XB._functions.CN_neq;
XB._functions.Relation = XB._calcNodes.FuncNode.extend({
    $name: "Relation",
    
    expectedArgNames: ["left", "right", "mode"],
    
    _calculate: function FRelation__calculate(changedArgs) {
        let convType = undefined;
        let mode = this._argManager.getValByNameDef("mode", "String", "number");
        switch (mode) {
            case "number":
                convType = "Number";
                break;
            case "string":
                convType = "String";
                break;
            case "smart":
                break;
            default:
                throw new Error("Unknown mode \"" + mode + "\"");
        }
        
        let left = this._argManager.getValByName("left", convType);
        let right = this._argManager.getValByName("right", convType);
        let cmpMode = XB._base.runtime.cmpModes[!!convType ? "CMP_STRICT" : "CMP_SMART"];
        return this._op( XB._base.runtime.compareValues(left, right, cmpMode, true) );
    }
});
XB._functions.CN_lt = XB._functions.Relation.extend({
    $name: "CN_lt",
    
    _op: function Flt__op(compResult) {
        return compResult < 0;
    }
});
XB._functions.CN_lte = XB._functions.Relation.extend({
    $name: "CN_lte",
    
    _op: function Flte__op(compResult) {
        return compResult <= 0;
    }
});
XB._functions.CN_gt = XB._functions.Relation.extend({
    $name: "CN_gt",
    
    _op: function Fgt__op(compResult) {
        return compResult > 0;
    }
});
XB._functions.CN_gte = XB._functions.Relation.extend({
    $name: "CN_gte",
    
    _op: function Fgte__op(compResult) {
        return compResult >= 0;
    }
});
XB._functions.CN_not = XB._calcNodes.FuncNode.extend({
    $name: "CN_not",
    
    _calculate: function Fnot__calculate(changedArgs) {
        return !(this._argManager.getValByIndex(0, "Bool"));
    }
});
XB._functions.CN_or = XB._calcNodes.FuncNode.extend({
    $name: "CN_or",
    
    _calculate: function For__calculate(changedArgs) {
        if (this._argManager.argsCount < 1)
            throw new Error("No arguments");
        for (let i = 0, len = this._argManager.argsCount; i < len; i++) {
            let val = this._argManager.getValByIndex(i);
            if (XB._base.runtime.xToBool(val))
                return true;
        }
        return false;
    }
});
XB._functions.CN_coalesce = XB._calcNodes.FuncNode.extend({
    $name: "CN_coalesce",
    
    _calculate: function Fcoalesce__calculate(changedArgs) {
        let strictMode = false;
        let modeStr = this._argManager.getValByNameDef("mode", "String", "default");
        switch (modeStr) {
            case "default":
                strictMode = false;
                break;
            case "strict":
                strictMode = true;
                break;
            default:
                throw new SyntacError("Unknown mode \"" + modeStr + "\"");
        }
        
        for each (let argName in this._argManager.argsNames) {
            if (argName == "mode")
                continue;
            
            let val = this._argManager.getValByName(argName);
            if (!XB._functions["CN_is-empty"].test(val, strictMode))
                return val;
        }
        
        return XB.types.empty;
    }
});
XB._functions.CN_and = XB._calcNodes.FuncNode.extend({
    $name: "CN_and",
    
    _calculate: function Fand__calculate(changedArgs) {
        if (this._argManager.argsCount < 1)
            throw new Error("No arguments");
        for (let i = 0, len = this._argManager.argsCount; i < len; i++) {
            let val = this._argManager.getValByIndex(i);
            let asBool = XB._base.runtime.xToBool(val);
            
            if (!asBool)
                return false;
        }
        return true;
    }
});
XB._functions.CN_try = XB._calcNodes.FuncNode.extend({
    $name: "CN_try",
    
    expectedArgNames: ["expression", "..."],
    
    _calculate: function Ftry__calculate(changedArgs) {
        try {
            return this._argManager.getValByName(this.expectedArgNames[0]);
        }
        catch (origException) {
            if (!(origException instanceof XB.types.Exception))
                throw origException;
            
            if (origException.type == XB.types.Exception.types.E_RETHROW) {
                origException = new XB.types.Exception(origException.srcNodeUid, XB.types.Exception.types.E_GENERIC, origException.message);
            }
            if (this._debugMode)
                this._logger.debug(this._getHumanReadableID() + " caught exception: " + origException.toString());
            
            this._findHandler(origException.type);
            if (this._usedHandler) {
                let handlerVal = this._usedHandler.getValue(this.hasSubscribers()? this: null);
                if (handlerVal instanceof XB.types.Exception) {
                    if (this._debugMode)
                        this._logger.debug(this._getHumanReadableID() + " exception rethrown with type " + handlerVal.type);
                    switch (handlerVal.type) {
                        case XB.types.Exception.types.E_RETHROW:
                            handlerVal = origException;
                            break;
                        case XB.types.Exception.types.E_LASTVALUE:
                            handlerVal = this._storedValue;
                            break;
                    }
                }
                if (this._debugMode)
                    this._logger.debug(this._getHumanReadableID() + " exception handler returned " + handlerVal);
                return handlerVal;
            }
            
            return origException;
        }
    },
    
    _findHandler: function Ftry__findHandler(exceptionType) {
        let handler = this._argManager.findNodeByName(exceptionType) ||
                      this._argManager.findNodeByName(this._consts.STR_ALL_EXCEPTIONS);
        if (this._usedHandler != handler) {
            if (this._usedHandler)
                this._usedHandler.unsubscribe(this);
            this._usedHandler = handler;
        }
    },
    
    _notNeeded: function Ftry__notNeeded() {
        if (this._usedHandler) {
            this._usedHandler.unsubscribe(this);
            this._usedHandler = null;
        }
    },
    
    _usedHandler: null,
    
    _consts: {
        STR_ALL_EXCEPTIONS: "..."
    }
});
XB._functions.CN_throw = XB._calcNodes.FuncNode.extend({
    $name: "CN_throw",
    
    expectedArgNames: ["type"],
    
    _calculate: function Fthrow__calculate(changedArgs) {
        let eType = this._argManager.getValByNameDef("type", "String", XB.types.Exception.types.E_RETHROW);
        
        let exception = new XB.types.Exception(this._getHumanReadableID(), eType, "XBTHROW");
        let excPropVal;
        let excPropsNames = this._argManager.argsNames;
        for each (let propName in excPropsNames) {
            if (propName == "type")
                continue;
            excPropVal = this._argManager.getValByName(propName);
            exception[propName] = excPropVal;
        }
        
        return exception;
    }
});
XB._functions.CN_lastvalue = XB._calcNodes.FuncNode.extend({
    $name: "CN_lastvalue",
    
    _calculate: function Flastvalue__calculate(changedArgs) {
        return new XB.types.Exception(this._getHumanReadableID(), XB.types.Exception.types.E_LASTVALUE, "XBLASTVAL");
    }
});
XB._functions.CN_optional = XB._calcNodes.FuncNode.extend({
    $name: "CN_optional",
    
    expectedArgNames: ["condition"],
    
    _calculate: function Foptional__calculate(changedArgs) {
        let numArgs = this._argManager.argsCount;
        if (numArgs > 1 && this._argManager.getValByName("condition", "Bool") ) {
            return XB._functions.CN_concat.concatArgs(this, 1, numArgs - 1);
        }
        return XB.types.empty;
    }
});
XB._functions["CN_page-location"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_page-location",
    
    expectedArgNames: ["mode"],
    
    _calculate: function Fpagelocation__calculate(changedArgs) {
        let windowListener = this._owner.host._overlayController.windowListener;
        let modeStr = this._argManager.getValByNameDef("mode", "String", this._modes[0]);
        
        if (!this._observing || this._argManager.argInArray("mode", changedArgs)) {
            if (this._observing)
                this._removeObservers();
            
            if (this._modes.indexOf(modeStr) == -1)
                return new XB.types.Exception(this._getHumanReadableID(), XB.types.Exception.types.E_SYNTAX,
                                              "Unknow mode \"" + modeStr + "\"");
            
            let watcherTopic = (modeStr == this._modes[0]) ? "WindowLocationChange" : "PageLoad";
            windowListener.addListener(watcherTopic, this);
            this._observing = true;
        }
        
        return (modeStr == this._modes[0] ? windowListener.windowLocation : null);
    },
    
    _notNeeded: function Fpagelocation__notNeeded() {
        if (this._observing) {
            this._removeObservers();
        }
    },
    
    _removeObservers: function Fpagelocation__removeObservers() {
        let windowListener = this._owner.host._overlayController.windowListener;
        windowListener.removeListener("WindowLocationChange", this);
        windowListener.removeListener("PageLoad", this);
        this._observing = false;
    },
    
    observe: function Fpagelocation_observe(aSubject, aTopic, aData) {
        if (!this._observing) {
            this._owner.logger.warn(this._getHumanReadableID() + " is NOT observing!");
            return;
        }
        
        switch (aTopic) {
            case "WindowLocationChange":
                if (this._setNewVal(aData.url))
                    this._notifyDeps();
                break;
                
            case "PageLoad":
                this._setNewVal(XB.types.empty);
                this._notifyDeps();
                
                this._setNewVal(aData.url);
                this._notifyDeps();
                
                break;
            
            default:
                break;
        }
    },
    
    _modes: ["on switch", "on load"],
    _observing: null
});
XB._functions["CN_page-title"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_page-title",
    
    _calculate: function Fpagetitle__calculate(changedArgs) {
        if (!this._observing) {
            this._observing = true;
            
            let windowListener = this._owner.host._overlayController.windowListener;
            windowListener.addListener("WindowTitleChange", this);
            this._title = windowListener.windowTitle;
        }
        
        return this._title;
    },
    
    _notNeeded: function Fpagetitle__notNeeded() {
        if (this._observing) {
            this._observing = false;
            this._owner.host._overlayController.windowListener.removeListener("WindowTitleChange", this);
        }
        
        this._title = null;
    },
    
    observe: function Fpagetitle_observe(aSubject, aTopic, aData) {
        switch (aTopic) {
            case "WindowTitleChange":
                if (this._setNewVal(this._title = aData.title))
                    this._notifyDeps();
                break;
            
            default:
                break;
        }
    },
    
    _observing: null,
    _title: null
});
XB._functions.CN_cookie = XB._calcNodes.FuncNode.extend({
    $name: "CN_cookie",
    
    expectedArgNames: ["url", "name", "include-httponly"],
    
    constructor: function FCookie() {
        this.base.apply(this, arguments);
        this._timer = XB._Cc["@mozilla.org/timer;1"].createInstance(XB._Ci.nsITimer);
    },
    
    QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports, Ci.nsIObserver, Ci.nsITimerCallback]),
    observe: function Fcookie_observe(subject, topic, data) {
        if (!this.hasSubscribers()) return;
        switch (topic) {
            case "cookie-changed":
                if (subject instanceof Ci.nsICookie && subject.name != this._cookieName)
                    return;
                
                this._timer.cancel();
                this._timer.initWithCallback(this, 10, this._timer.TYPE_ONE_SHOT);
                break;
            default:
                break;
        }
    },
    notify: function Fcookie_notify() {
        try {
            if (!this.hasSubscribers()) return;
            if (this._setNewVal(this._cookie()))
                this._notifyDeps();
        }
        catch (e) {
            this._logger.error("CN_cookie.notify() failed. " + e.message);
        }
    },
    
    _ioService: Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService),
    _cookieService: Cc["@mozilla.org/cookieService;1"].getService().QueryInterface(Ci.nsICookieService),
    _observerService: Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService),
    _observing: false,
    _uri: null,
    _cookieName: undefined,
    _url: undefined,
    _timer: null,
    _httpOnly: undefined,
    
    _cookie: function Fcookie__cookie() {
        let cookieString = this._httpOnly ? this._cookieService.getCookieStringFromHttp(this._uri, null, null)
                                          : this._cookieService.getCookieString(this._uri, null);
        
        if (!cookieString)
            return XB.types.empty;
        
        let cookies = cookieString.split(";");
        for (let i = 0, len = cookies.length; i < len; i++) {
            let cookie = cookies[i];
            
            let separatorPos = cookie.indexOf("=");
            if (separatorPos == -1)
                continue;
            
            let cookieName = strutils.trimAll( cookie.substring(0, separatorPos) );
            if (cookieName == this._cookieName)
                return unescape( cookie.substring(separatorPos + 1) );
        }
        
        return XB.types.empty;
    },
    
    _calculate: function Fcookie__calculate(changedArgs) {
        try {
            if (this._argManager.argInArray("url", changedArgs) || this._url == undefined)
                this._uri = this._ioService.newURI(this._argManager.getValByName("url", "String"), null, null);
            if (this._argManager.argInArray("name", changedArgs) || this._cookieName == undefined) {
                this._cookieName = this._argManager.getValByName("name", "String");
            if (this._argManager.argInArray(this.expectedArgNames[2], changedArgs) || this._httpOnly == undefined)
                this._httpOnly = this._argManager.getValByNameDef(this.expectedArgNames[2], "Bool", false);
            }
        }
        catch (e) {
            this._notNeeded();
            throw e;
        }
        
        if (!this._observing) {
            this._observerService.addObserver(this, "cookie-changed", false);
            this._observing = true;
        }
        
        return this._cookie();
    },
    
    _notNeeded: function Fcookie__notNeeded() {
        if (this._observing) {
            this._observerService.removeObserver(this, "cookie-changed");
            this._observing = false;
        }
    }
});
XB._functions.CN_request = XB._calcNodes.FuncNode.extend({
    $name: "CN_request",
    
    expectedArgNames: ["url", "update", "expire", "format", "valid-status", "valid-xpath"],
    
    _specialParamPattern: /^(\w+)\.(.+)$/,
    
    _calculate: function Frequest__calculate(changedArgs) {
        let format;
        let formatStr = this._argManager.getValByNameDef("format", "String", "xml");
        switch (formatStr) {
            case "text":
                format = XB.types.RequestData.Format.FMT_TEXT;
                break;
            case "xml":
                format = XB.types.RequestData.Format.FMT_XML;
                break;
            case "json":
                format = XB.types.RequestData.Format.FMT_JSON;
                break;
            default:
                throw new Error("Unknown format specification: " + formatStr);
        }
        
        let validStatusRange = {start: 100, end: 399};
        if (this._argManager.argExists("valid-status")) {
            let statusesExpr = this._argManager.getValByName("valid-status", "String");
            let exprMatch = statusesExpr.match(/^(\d+)?\.\.(\d+)?$/);
            if (exprMatch == null)
                throw new RangeError("Could not parse request status validation expression (" + statusesExpr + ")");
            validStatusRange.start = parseInt(exprMatch[1], 10);
            validStatusRange.end = parseInt(exprMatch[2], 10);
        }
        
        let updateInterval = this._argManager.getValByNameDef("update", "Number", 0)
                             || Number.POSITIVE_INFINITY;
        
        let specialParams = {hidden: {}};
        let argsNames = this._argManager.argsNames;
        for each (let argName in argsNames) {
            let match = argName.match(this._specialParamPattern);
            if (!match) continue;
            let paramType = match[1];
            let paramName = match[2];
            if (paramType in specialParams)
                specialParams[paramType][paramName] = this._argManager.getValByName(argName);
        }
        
        let reqData = new XB.types.RequestData(this._argManager.getValByName("url", "String"),
                                               "GET",
                                               updateInterval,
                                               this._argManager.getValByNameDef("expire", "Number", 0),
                                               format,
                                               validStatusRange,
                                               this._argManager.getValByNameDef("valid-xpath", "String"),
                                               specialParams);
        return reqData;
    }
});

XB._functions.NetworkData = XB._calcNodes.FuncNode.extend({
    $name: "NetworkData",
    
    expectedArgNames: ["request"],
    
    update: function FNetworkData_update(netResource, netData) {
        if (netResource !== this._resource) {
            this._logger.warn("Update notification from a martian network resource! " + this._getHumanReadableID());
            return;
        }
        if (this._setNewVal(this._processResData(netData)))
            this._notifyDeps();
    },
    
    _calculate: function FNetworkData__calculate(changedArgs) {
        try {
            var reqData = this._argManager.getValByName("request", "RequestData");
            var netResource = XB.network.getResource(reqData);
        }
        catch (e) {
            this._notNeeded();
            throw e;
        }
        
        if (this._resource != netResource) {
            if (this._resource)
                try {
                    this._resource.unsubscribe(this, this._dataType);
                }
                catch (e) {
                    this._logger.error("Could not unsubscribe from a network resource. " +
                                                    strutils.formatError(e));
                    this._logger.debug(e.stack);
                }
            this._resource = netResource;
        }
        
        let rawData = this._resource.requestData(this, this._dataType);
        this._logger.trace("FNetworkData_calculate raw data is " + rawData);
        return this._processResData(rawData) || XB.types.empty;
    },
    
    _resource: null,
    
    _notNeeded: function FNetworkData__notNeeded() {
        if (this._resource) {
            this._resource.unsubscribe(this, this._dataType);
            this._resource = null;
        }
    }
});
XB._functions.CN_content = XB._functions.NetworkData.extend({
    $name: "CN_content",
    
    _dataType: XB.network.ResourceDataType.NRDT_CONTENT,
    
    _processResData: function Fcontent__processResData(content) {
        if (!content)
            return XB.types.empty;
        
        if (content instanceof XB._Ci.nsIDOMDocument)
            return new XB.types.XML(content);
        
        if (!sysutils.isString(content))
            throw new TypeError("Network resource sent not an XML document, nor a String");
        
        return content;
    }
});
XB._functions["CN_http-header"] = XB._functions.NetworkData.extend({
    $name: "CN_http-header",
    
    expectedArgNames: ["request", "name"],
    
    _dataType: XB.network.ResourceDataType.NRDT_HEADERS,
    
    _calculate: function Fheader__calculate(changedArgs) {
        if (this._argManager.argInArray("name", changedArgs) || !this._headerName) {
            this._headerName = this._argManager.getValByName("name", "String");
        }
        return this.base(changedArgs);
    },
    
    _processResData: function Fheader__processResData(resHeaders) {
        if (resHeaders)
            return resHeaders[this._headerName];
    },
    
    _headerName: undefined
});
XB._functions["CN_http-status"] = XB._functions.NetworkData.extend({
    $name: "CN_http-status",
    
    expectedArgNames: ["request"],
    
    _dataType: XB.network.ResourceDataType.NRDT_STATUS,
    
    _processResData: function FhttpStatus__processResData(httpStatus) {
        return httpStatus;
    }
});

XB._functions.CN_finished = XB._calcNodes.FuncNode.extend({
    $name: "CN_finished",
    
    expectedArgNames: ["try-id"],
    
    onRequestFinished: function Ffinished_onRequestFinished(requestID) {
        if (this._requestID == requestID)
            if (this._setNewVal(true))
                this._notifyDeps();
        this._requestID = undefined;
    },
    
    _calculate: function Ffinished__calculate(changedArgs) {
        let requestID = this._argManager.getValByName(this.expectedArgNames[0], "Number");
        if (XB.network.requestFinished(requestID))
            return true;
        
        XB.network.subscribeToRequestFinish(requestID, this);
        this._requestID = requestID;
        return false;
    },
    
    _requestID: undefined
});
XB._functions.CN_update = XB._calcNodes.ProcNode.extend({
    $name: "CN_update",
    
    expectedArgNames: ["request"],
    
    _proc: function Fupdate__proc(eventInfo) {
        let requestData = this._argManager.getValByName(this.expectedArgNames[0], "RequestData");
        let invCache = true;
        let netResource = XB.network.getResource(requestData);
        return netResource.update(true, invCache);
    }
});

XB._functions["CN_parse-uri"] = XB._calcNodes.FuncNode.extend({
    $name:"CN_parse-uri",
    
    expectedArgNames: ["uri"],
    
    _ioService: Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService),
    
    _calculate: function FparseUrl__calculate(changedArgs) {
        let uriStr = this._argManager.getValByName("uri", "String");
        let stdURL = Cc["@mozilla.org/network/standard-url;1"].createInstance(Ci.nsIStandardURL);
        stdURL.init(Ci.nsIStandardURL.URLTYPE_STANDARD, -1, uriStr, null, null);
        let uri = stdURL.QueryInterface(Components.interfaces.nsIURI);
        
        let uriAttributes = {scheme: uri.scheme};
        if (uri.username)
            uriAttributes.username = uri.username;
        if (uri.password)
            uriAttributes.password = uri.password;
        if (uri.host)
            uriAttributes.host = uri.host;
        if (uri.port != -1)
            uriAttributes.port = uri.port;
        if (uri.path)
            uriAttributes.path = uri.path;
        
        let elemJson = {name: "uri", attributes: uriAttributes};
        return XB._base.runtime.makeXML(elemJson);
    }
});

XB._functions.CN_file = XB._calcNodes.FuncNode.extend({
    $name: "CN_file",
    
    expectedArgNames: ["path", "type"],
    
    _calculate: function Ffile__calculate(changedArgs) {
        let path = this._argManager.getValByName("path", "String");
        let type = this._argManager.getValByNameDef("type", "String", "text");
        let file = this._parentWidget.prototype.unit.unitPackage.findFile(path);
        
        switch (type) {
            case "xml":
                return new XB.types.XML( fileutils.xmlDocFromFile(file) );
            case "text":
                return fileutils.readTextFile(file);
            default:
                throw new Error("Unsupported file output type");
        }
    }
});

XB._functions["CN_copy-to-clipboard"] = XB._calcNodes.ProcNode.extend({
    $name: "CN_copy-to-clipboard",
    
    constructor: function FCopyToClipboard() {
        this.base.apply(this, arguments);
        this._helper = XB._Cc["@mozilla.org/widget/clipboardhelper;1"].getService(XB._Ci.nsIClipboardHelper);
    },
    
    _helper: null,
    
    _proc: function FcopyToClipboard__proc(eventInfo) {
        this._helper.copyString(this._argManager.getValByIndex(0, "String"));
    }
});

XB._functions.CN_navigate = XB._calcNodes.ProcNode.extend({
    $name: "CN_navigate",
    
    expectedArgNames: ["url", "target"],
    
    _proc: function Fnavigate__proc(eventInfo) {
        let url = this._argManager.getValByName("url", "String");
        let target = this._argManager.getValByNameDef("target", "String", "");
        let action = this._argManager.getValByNameDef("action", "String", "");
        let windowWidth = this._argManager.getValByNameDef("width", "Number", 0);
        let windowHeight = this._argManager.getValByNameDef("height", "Number", 0);
        let title = this._argManager.getValByNameDef("title", undefined, XB.types.empty);
        if (title === XB.types.empty)
            title = null;
        else
            title = XB._base.runtime.xToString(title);
        
        let navData = {
            unsafeURL: url,
            target: target,
            eventInfo: eventInfo,
            windowProperties: { width: windowWidth, height: windowHeight, title: title }
        };
        
        if (action) {
            let parentProto = this._parentWidget.prototype;
            let componentInfo = {id: parentProto.id, packageID: parentProto.unit.unitPackage.id};
            this._parentWidget.host.navigate(navData, action, componentInfo);
        }
        else
            this._parentWidget.host.navigate(navData);
    }
});

XB._functions["CN_setup-widget"] = XB._calcNodes.ProcNode.extend({
    $name: "CN_setup-widget",
    
    _proc: function FSetupWidget__proc(eventInfo) {
        this._parentWidget.host.setupWidget(this._parentWidget);
    }
});

XB._functions["CN_add-widget"] = XB._calcNodes.ProcNode.extend({
    $name: "CN_add-widget",
    
    _proc: function FAddWidget__proc(eventInfo) {
        this._parentWidget.host.addWidget(this._parentWidget.prototype, undefined, this._parentWidget, true);
    }
});

XB._functions["CN_remove-widget"] = XB._calcNodes.ProcNode.extend({
    $name: "CN_remove-widget",
    
    _proc: function FRemoveWidget__proc(eventInfo) {
        this._parentWidget.host.removeWidget(this._parentWidget.id);
    }
});

XB._functions["CN_reload-package"] = XB._calcNodes.ProcNode.extend({
    $name: "CN_reload-package",
    
    expectedArgNames: ["package-id"],
    
    _proc: function FReloadPackage__proc() {
        let packageID = this._argManager.getValByNameDef(this.expectedArgNames[0], "String", undefined);
        XB._base.application.restartComponents(packageID || this._owner.prototype.unit.unitPackage.id);
    }
});

XB._functions["CN_serialize-xml"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_serialize-xml",
    
    expectedArgNames: ["source"],
    
    _calculate: function FSerializeXml__calculate(changedArgs) {
        let src = this._argManager.getValByName("source");
        if (!XB._base.runtime.isXML(src))
            return new XB.types.Exception(this._getHumanReadableID(), XB.types.Exception.types.E_TYPE,
                                          "Not XML, " + XB._base.runtime.describeValue(src));
        return src.serialize();
    }
});

XB._functions.CN_meander = XB._functions.Periodic.extend({
    $name: "CN_meander",
    notify: function Fmeander_notify() {
        try {
            this._setNewVal(!this._storedValue);
            this._notifyDeps();
        }
        catch (e) {
            this._logger.error("CN_meander.notify() failed. " + e.message);
        }
    },
    
    _calculate: function Fmeander__calculate(changedArgs) {
        return !!this.base.apply(this, arguments);
    }
});

XB._functions["CN_type-of"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_type-of",
    
    expectedArgNames: ["value"],
    
    _calculate: function FValueOf__calculate() {
        try {
            let rank = XB._base.runtime.rankValue( this._argManager.getValByName(this.expectedArgNames[0]) );
            return this._typeNames[rank];
        }
        catch (e) {
            return this._typeNames[6];
        }
    },
    
    _typeNames: {
        0: "empty",
        1: "bool",
        2: "number",
        3: "string",
        4: "xml",
        5: "request",
        6: "exception"
    }
});

XB._functions["CN_value-used"] = XB._calcNodes.FuncNode.extend({
    $name: "CN_value-used",
    
    expectedArgNames: ["name"],
    
    _calculate: function FValueUsed__calculate() {
        let valName = this._argManager.getValByName(this.expectedArgNames[0], "String");
        let valNode = XB._functions["CN_value-of"].findValByName(this._parentWidget, valName);
        return valNode.hasSubscribers;
    }
});
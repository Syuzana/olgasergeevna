
XB.Database = function XBDB() {
    this._connection = null;
    this._logger = XB._base.getLogger("DB");
}

XB.Database.prototype.open = function XBDB_open(fileName) {
    if (!fileName)
        throw new Error("Filename is required");
    
    this.close();
    
    let dbFile = XB._base.application.directories.XBRoot;
    dbFile.append(fileName);
    
    const storageService = XB._Cc["@mozilla.org/storage/service;1"].getService(XB._Ci.mozIStorageService);
    
    try {
        this._connection = storageService.openDatabase(dbFile);
    } catch(e) {
        let journalDBFile = XB._base.application.directories.XBRoot;
        journalDBFile.append(fileName + "-journal");
        
        try { journalDBFile.remove(false); } catch(ex) {}
        
        if (e.result == XB._Cr.NS_ERROR_FILE_CORRUPTED)
            dbFile.remove(false);
        
        this._connection = storageService.openDatabase(dbFile);
    }
}

XB.Database.prototype.close = function XBDB_close() {
    if (this._connection) {
        try {
            this._connection.close();
        } catch (e) {
            this._logger.error("DB connection close error. " + strutils.formatError(e));
        }
        
        this._connection = null;
    }
}

XB.Database.prototype._createStatement = function XBDB__createStatement(query, parameters) {
    let statement = this._connection.createStatement(query);
    if (statement && !("executeAsync" in statement)) {
        let wrappedStatement = XB._Cc["@mozilla.org/storage/statement-wrapper;1"].createInstance(XB._Ci.mozIStorageStatementWrapper);
        wrappedStatement.initialize(statement);
        statement = wrappedStatement;
    }
    
    if (parameters)
      for (let name in parameters)
        statement.params[name] = parameters[name];
    
    return statement;
}

XB.Database.prototype.exec = function XBDB_exec(query, parameters) {
    let statement,
        result = [];
    try {
        statement = this._createStatement(query, parameters);
        let unwrappedStatement = statement.statement || statement;
        
        let columns = [];
        for (let i = unwrappedStatement.columnCount; i--;)
            columns.push(unwrappedStatement.getColumnName(i));
        
        while (statement.step()) {
            let row = {};
            for (let i = columns.length; i--;)
                row[columns[i]] = statement.row[columns[i]];

            result.push(row);
        }
    } catch(e) {
        this._logger.error(strutils.formatString("Could not query DB. %1", [strutils.formatError(e)]));
        this._logger.debug("Query was:\n" + query);
        this._logger.debug(e.stack);
    } finally {
        if (statement)
            statement.reset();
    }
    
    return result;
}
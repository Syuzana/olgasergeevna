
XB.types = {};

XB.types.empty = null;

XB.types.XML = function XBXML() {
    let arg0 = arguments[0];
    if (arguments.length == 1 && arg0 instanceof XB._Ci.nsIDOMDocument) {
        this._isADocument = true;
        this._rootNode = arg0.documentElement;
    }
    else {
        this._initAsList();
        this.append.apply(this, arguments);
    }
};

XB.types.XML.prototype = {
    
    append: function XML_append() {
        if (this._isADocument) {
            let oldRoot = this._rootNode;
            this._initAsList();
            this.append(oldRoot);
        }
        
        for (let argIndex = 0, argsLen = arguments.length; argIndex < argsLen; argIndex++) {
            let arg = arguments[argIndex];
            
            if (sysutils.isArray(arg)) {
                this.append.apply(this, arg);
                continue;
            }
            
            if (arg instanceof XB._Ci.nsIDOMNode) {
                this._appendAnyNode(arg);
                continue;
            }
            
            let isThisType = (arg instanceof this.constructor);
            if (arg instanceof XB._Ci.nsIDOMNodeList || isThisType) {
                for (let nodeIndex = 0, nodesLen = arg.length; nodeIndex < nodesLen; nodeIndex++) {
                    this._appendAnyNode(arg.item(nodeIndex));
                }
                if (isThisType) {
                    let argAttrs = arg.attributes;
                    for (let i = 0, len = argAttrs.length; i < len; i++) {
                        this._appendAnyNode(argAttrs.item(i));
                    }
                }
                continue;
            }
            
            throw new TypeError(XB._base.consts.ERR_DOM_NODES_EXPECTED);
        }
    },
    
    addAttribute: function XML_addAttribute(ns, name, value) {
        if (this.disposed)
            throw new Error(XB._base.consts.ERR_DISPOSED_XML);
        this._rootNode.setAttributeNS(ns, name, value);
    },
    
    toString: function XML_toString() {
        let role = this._isADocument? "document": "nodes";
        if (this.disposed)
            return "[XBXML " + role + " (disposed)]";
        return "[XBXML "+ role + " length: " + this.length + ", content:\n" +
            strutils.trimSpaces(this.serialize().substr(0, 800)) + "...\n]";
    },
    appendChild: function XML_appendChild(node) {
        this._rootNode.appendChild(node);
    },
    
    item: function XML_item(at) {
        if (!this._isADocument)
            return this._rootNode.childNodes[at];
        if (at > 0)
            throw new RangeError("Document has only one root node");
        return this._rootNode;
    },
    
    get attributes() {
        if (this.disposed)
            throw new Error(XB._base.consts.ERR_DISPOSED_XML);
        return this._rootNode.attributes;
    },
    
    get childNodes() {
        return this._rootNode.childNodes;
    },
    
    get length() {
        if (this.disposed)
            return 0;
        if (this._isADocument)
            return 1;
        return this._rootNode.childNodes.length;
    },
    
    get disposed() {
        return !this._rootNode;
    },
    dispose: function XML_dispose() {
        if (this._isADocument) {
            this._isADocument = false;
        }
        else {
            if (this._frag)
                this._frag.removeChild(this._rootNode);
            this._frag = null;
        }
        
        if (this.disposed)
            XB._base.logger.warn("Attemp to dispose an already disposed XML object.");
        else {
            if (XB._base.logger.level <= XB._base.application.core.Lib.Log4Moz.Level.Trace)
                XB._base.logger.trace("Disposing XML:\n" + this.serialize().substr(0, 400) + "\n...");
            this._rootNode = null;
        }
    },
    
    serialize: function XML_serialize() {
        let result = "";
        for (let nodeIndex = 0, len = this.length; nodeIndex < len; nodeIndex++) {
            result += XB._base.runtime.serializeXML(this.item(nodeIndex));
        }
        return result;
    },
    
    get textContent() {
        return this._rootNode.textContent;
    },
    
    equalsTo: function XML_equalsTo(other) {
        if ( !(other instanceof this.constructor) )
            throw new TypeError("XML type expected");
        if (other === this)
            return true;
        if ( (this._isADocument ^ other._isADocument) )
            return false;
        
        if (this.disposed || other.disposed)
            return false;
        return this._rootNode.isEqualNode(other._rootNode);
    },
    
    query: function XML_query(expr, NSResolver) {
        if (this.disposed)
            throw new Error("Querying from disposed XML object (expression: \"" + expr + "\")");
        
        if (XB._base.logger.level <= XB._base.application.core.Lib.Log4Moz.Level.Trace) {
            XB._base.logger.trace("Query from:\n" + this.serialize().substr(0, 400) + "\n...");
        }
        let queryNode = this._isADocument? this._rootNode.ownerDocument: this._rootNode;
        let queryResult = XB._base.runtime.queryXMLDoc(expr, queryNode, NSResolver);
        return XB.types.XML.handleQueryResult(queryResult);
    },
    
    transform: function XML_transform(stylesheet) {
        let newFragment = XB._base.runtime.transformXML((this._isADocument) ? this._rootNode.ownerDocument : this._rootNode, stylesheet);
        let newXML = new XB.types.XML(newFragment);
        return newXML;
    },
    
    clone: function XML_clone() {
        return new this.constructor(this);
    },
    
    get owner() {
        return this._owner;
    },
    set owner(owner) {
        this._owner = owner;
    },
    
    _isADocument: false,
    _frag: null,
    _rootNode: null,
    _owner: null,
    
    _initAsList: function XML__initAsList() {
        this._frag = XB._base.runtime.tempXMLDoc.createDocumentFragment();
        this._rootNode = this._frag.appendChild(XB._base.runtime.tempXMLDoc.createElement("xml"));
        if (!this._rootNode)
            throw new Error("Couldn't create root node");
        this._isADocument = false;
    },
    
    _appendAnyNode: function XML__appendAnyNode(node) {
        let newNode = null;
        if (node.ownerDocument == this._rootNode.ownerDocument) {
            newNode = node.cloneNode(true);
        } else {
            try {
                newNode = this._rootNode.ownerDocument.importNode(node, true);
            }
            catch(e) {
                XB._base.logger.debug("Native importNode failed.");
                newNode = this._importNodeEx(this._rootNode.ownerDocument, node, true);
            }
        }
        
        if (newNode.nodeType == newNode.ATTRIBUTE_NODE)
            this._rootNode.setAttributeNode(newNode);
        else
            this._rootNode.appendChild(newNode);
    },
    
    _importNodeEx: function XML__importNodeEx(document, node, deep) {
        let clone = null;
        switch (node.nodeType) {
            case node.DOCUMENT_NODE:
                clone = this._importNodeEx(document, node.documentElement, deep);
                break;
            case node.ELEMENT_NODE:
                clone = document.createElementNS(node.namespaceURI, node.nodeName);
                if (node.localName == XB._base.consts.STR_VAL_REF_ELEM_NAME &&
                    node.namespaceURI == XB._base.consts.STR_UI_NS) {
                    clone.setUserData(XB._base.consts.STR_VAL_REF_ID_KEY_NAME,
                                      node.getUserData(XB._base.consts.STR_VAL_REF_ID_KEY_NAME), this._userDataHandler);
                }
                let attributes = node.attributes;
                for (let i = 0, len = attributes.length; i < len; i++) {
                    let source = attributes[i];
                    let target = document.createAttribute(source.nodeName);
                    target.nodeValue = source.nodeValue;
                    clone.setAttributeNode(target);
                }
                if (deep) {
                    let child = node.firstChild;
                    while (child) {
                        clone.appendChild(this._importNodeEx(document, child, deep));
                        child = child.nextSibling;
                    }
                }
                break;
            case node.TEXT_NODE:
                clone = document.createTextNode(node.nodeValue);
                break;
            case node.CDATA_SECTION_NODE:
                clone = document.createCDATASection(node.nodeValue);
                break;
            case node.COMMENT_NODE:
                clone = document.createComment(node.nodeValue);
                break;
        }
        
        return clone;
    },
    _userDataHandler: {
        handle: function XML_UDH_handle(operation, key, data, srcNode, dstNode) {
            try {
                dstNode.setUserData(key, data, this);
            }
            catch (e) {
                XB._base.logger.error("Failed setting userData " + strutils.formatError(e));
            }
        }
    }
};

XB.types.XML.handleQueryResult = function XBXML_handleQueryResult(queryResult) {
    if (sysutils.isArray(queryResult)) {
        XB._base.logger.trace("XML query returned an array of nodes " + queryResult.length);
        if (!queryResult.length)
            return XB.types.empty;
        queryResult = new XB.types.XML((queryResult.length == 1) ? queryResult[0] : queryResult);
        XB._base.logger.trace("Constructed a new XML " + queryResult.length);
    }
    
    return queryResult;
};

XB.types.Exception = function XBException(srcNodeUid, eType, msg) {
    this._type = eType || this._type;
    this._msg = msg;
    this._srcNodeUid = srcNodeUid;
};

XB.types.Exception.types = {
    E_GENERIC: "Exception",
    E_SYNTAX: "Syntax",
    E_TYPE: "Type",
    E_RUNTIME: "Runtime",
    E_RETHROW: "Rethrow",
    E_LASTVALUE: "LastValue"
};

XB.types.Exception.prototype = {
    get type() {
        return this._type;
    },
    
    get srcNodeUid() {
        return this._srcNodeUid;
    },
    
    get message() {
        return this._msg;
    },
    
    equalsTo: function XBException_equalsTo(other) {
        return sysutils.objectsAreEqual(this, other);
    },
    
    toString: function XBException_toString() {
        return this._type + "@" + this._srcNodeUid + ": " + this._msg;
    },
    
    _type: XB.types.Exception.types.E_GENERIC,
    _srcNodeUid: undefined
};

XB.types.RequestData = function XBRequestData(url, method, update, expire, format, validStatusRange, validXpath, specials) {
    XB._base.runtime.ensureValueTypeIs(url, "String");
    let uri = XB._Cc["@mozilla.org/network/io-service;1"].getService(XB._Ci.nsIIOService).newURI(url, null, null);
    if (uri.scheme !== "http" && uri.scheme !== "https")
        throw new Error("Invalid URL scheme '" + uri.scheme + "'");
    this._url = url;
    
    XB._base.runtime.ensureValueTypeIs(method, "String");
    this._method = method;
    
    XB._base.runtime.ensureValueTypeIs(update, "Number");
    if (update < 1)
        throw new RangeError("Invalid update interval " + update);
    this._updateInterval = update;
    
    XB._base.runtime.ensureValueTypeIs(expire, "Number");
    if (expire < 0)
        throw new RangeError("Invalid expiration time " + expire);
    this._expirationInterval = expire;
    
    XB._base.runtime.ensureValueTypeIs(format, "Number");
    if (format < XB.types.RequestData.Format.FMT_TEXT || format > XB.types.RequestData.Format.FMT_JSON)
        throw new RangeError("Unknown format type");
    this._format = format;
    
    if (validStatusRange && sysutils.isNumber(validStatusRange.start) && sysutils.isNumber(validStatusRange.end) &&
        validStatusRange.start >= 100 && validStatusRange.end <= 599 && validStatusRange.start <= validStatusRange.end) {
        this._statusMin = validStatusRange.start;
        this._statusMax = validStatusRange.end;
    }
    else
        throw new TypeError("Invalid status range parameter");
    
    if (validXpath) {
        XB._base.runtime.ensureValueTypeIs(validXpath, "String");
        this._checkXpathExpr = validXpath;
    }
    
    if (specials) {
        this._hiddenParams = specials.hidden;
    }
};

XB.types.RequestData.Format = {
    FMT_TEXT: 0,
    FMT_XML: 1,
    FMT_JSON: 2
};

XB.types.RequestData.prototype = {
    constructor: XB.types.RequestData,
    
    get url() {
        return this._url;
    },
    
    get method() {
        return this._method;
    },
    
    get updateInterval() {
        return this._updateInterval;
    },
    
    get expirationInterval() {
        return this._expirationInterval;
    },
    
    get format() {
        return this._format;
    },
    
    get statusRange() {
        return {start: this._statusMin, end: this._statusMax};
    },
    
    get xpathExpression() {
        return this._checkXpathExpr;
    },
    
    get hiddenParams() {
        return this._hiddenParams;
    },
    get specialsHash() {
        if (this._spHash)
            return this._spHash;
        let hiddenParamsHash = "";
        for (let hp in this._hiddenParams) {
            hiddenParamsHash += (hp + ":" + this._hiddenParams[hp]);
        }
        return (this._spHash = hiddenParamsHash);
    },
    
    get hash() {
        return this._hash || (this._hash =
            [this._url, this._method, this.updateInterval, this.expirationInterval, this._format, this._statusMin,
             this._statusMax, this._checkXpathExpr, this.specialsHash].join("#"));
    },
    
    equalsTo: function ReqData_equalsTo(other) {
        if ( !(other instanceof this.constructor) )
            throw new TypeError("RequestData required");
        return this._fields.every(function(field) { return this[field] == other[field]; }, this);
    },
    
    _spHash: undefined,
    _hash: undefined,
    _url: undefined,
    _method: "GET",
    _updateInterval: 0,
    _expirationInterval: 0,
    _format: XB.types.RequestData.Format.FMT_TEXT,
    _statusMin: 0,
    _statusMax: 0,
    _checkXpathExpr: undefined,
    _hiddenParams: null,
    
    _fields: ["_url", "_method", "_updateInterval", "_expirationInterval", "_format", "_statusMin", "_statusMax", "_checkXpathExpr", "specialsHash"]
};

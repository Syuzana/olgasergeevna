let EXPORTED_SYMBOLS = ["ProtocolHandler"];

const Cc = Components.classes;
const Ci = Components.interfaces;

const IOS = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);

var gHandlers = [];

var ProtocolHandler = {
  _scheme: "",
  
  get scheme() {
    return this._scheme;
  },
  
  set scheme(val) {
    this._scheme = val;
  },
  
  addHandler: function ProtocolHandler_addHandler(aHandler) {
    if (gHandlers.some(function(handler) aHandler === handler))
      return;
    
    gHandlers.push(aHandler);
  },
  
  removeHandler: function ProtocolHandler_removeHandler(aHandler) {
    gHandlers = gHandlers.filter(function(handler) aHandler !== handler);
  },
  newURI: function ProtocolHandler_newURI(aSpec, aOriginalCharset, aBaseURI) {
    let uri = null;
    
    let spec = aSpec.replace(this.scheme + ":", "");
    
    gHandlers.some(function(handler) {
      return (null !== (uri = handler.newURI(spec, aOriginalCharset, aBaseURI)));
    });
    
    if (uri && typeof uri == "string")
      uri = IOS.newURI(uri, aOriginalCharset, aBaseURI);
    
    return uri;
  },
  
  newChannel: function ProtocolHandler_newChannel(aURI) {
    return null;
  }
};
const strutils = {
    
    insertBreaksInString: function StrUtils_insertBreaksInString(s) {
        return ("" + s || "").replace(/^\s+/, "")
                             .replace(/\s+$/, "")
                             .replace(/([\/\-&\?\.])/g, "$1\u200B")
                             .replace(/(\S{5})(\S{5})/g, "$1\u200B$2");
    },
    
    normalizeString: function StrUtils_normalizeString(s) {
        return s ? this.trimSpaces(s).replace(/[\u0020\n\r\t]+/g, " ") : "";
    },
    
    trimSpaces: function StrUtils_trimSpaces(s) {
        return s ? s.replace(/^[\u0020\n\r\t]*/, "").replace(/[\u0020\n\r\t]*$/, "") : "";
    },
    
    trimAll: function StrUtils_trimAll(s) {
        if (String.hasOwnProperty("trim"))
            this.trimAll = function StrUtils_trimAll(s) s ? s.trim() : "";
        else
            this.trimAll = function StrUtils_trimAll(s) s ? s.replace(/^\s\s*/, "").replace(/\s\s*$/, "") : "";
        
        return this.trimAll(s);
    },
    
    trimAllLeft: function StrUtils_trimAllLeft(s) {
        return s.replace(/^\s\s*/, "");
    },
    
    trimAllRight: function StrUtils_trimAllRight(s) {
        return s.replace(/\s\s*$/, "");
    },
    
    compareStrings: function StrUtils_compareStrings(str1, str2) {
        str1 = str1.toString();
        str2 = str2.toString();
        return ( ( str1 == str2 ) ? 0 : ( ( str1 > str2 ) ? 1 : -1 ) );
    },
    
    repeatString: function StrUtils_repeatString(str, num) {
        return (new Array(++num)).join(str);
    },
    
    camelize: function StrUtils_camelize(string) {
        function camelizeFunction(match, chr) (chr || "").toUpperCase();
        return string.replace(/\-+(.)?/g, camelizeFunction);
    },
    
    formatString: function StrUtils_formatString(template, values) {
        for (let i = values.length; i--;)
            template = template.replace(new RegExp("%" + (i + 1), "g"), values[i]);
        return template;
    },
    
    formatError: function StrUtils_formatError(e) {
        
        if (!(e instanceof this._Ci.nsIException))
            return ("" + e);
        
        let text = e.name + ": " + e.message;
        if (e.fileName)
            text += "\nin " + e.fileName + "\nat line " + e.lineNumber;
        
        return text;
    },
    
    formatNumber: function StrUtils_formatNumber(num, precision, posSign, separateGroups) {
        if (!sysutils.isNumber(num))
            throw new TypeError("Number expected");
        
        if (precision !== undefined) {
            precision = parseInt(precision, 10);
            if (precision < 0)
                throw new RangeError("Precision must be positive");
        }
        
        let sign = "";
        if (num < 0)
            sign = "-";
        else {
            if (posSign && (num > 0))
                sign = "+";
        }
        
        let result;
        
        if (precision == undefined) {
            result = sign + Math.abs(num).toLocaleString();
        } else {
            let order = Math.pow(10, precision);
            let rounded = Math.round(Math.abs(num) * order);
            
            result = sign + parseInt(rounded / order, 10).toLocaleString();
            if (precision > 0) {
                let fracStr = rounded.toString().substr(-precision);
                fracStr = this.repeatString("0", precision - fracStr.length) + fracStr;
                result += this._STR_LOCALE_DECIMAL_SEPARATOR + fracStr;
            }
        }
        
        if (separateGroups === false)
            result = result.replace(this._STR_LOCALE_GROUP_SEPARATOR_RE, "");
        
        return result;
    },
    
    escapeRE: function StrUtils_escapeRE(aString) {
        return String(aString).replace(this._ESCAPE_RE, "\\$1");
    },
    
    _Ci: Components.interfaces,
    _Cc: Components.classes,
    _ESCAPE_RE: /([.*+?^=!:${}()|[\]\/\\])/g,
    _STR_LOCALE_DECIMAL_SEPARATOR: (1.1).toLocaleString()[1],
    get _STR_LOCALE_GROUP_SEPARATOR_RE() {
        delete this._STR_LOCALE_GROUP_SEPARATOR_RE;
        
        let separator = "\u00A0";
        
        let nmbLocaleStr = (1000).toLocaleString();
        if (nmbLocaleStr.length > 4 && nmbLocaleStr[1] != "0")
            separator = nmbLocaleStr[1];
        
        return this._STR_LOCALE_GROUP_SEPARATOR_RE = new RegExp(this.escapeRE(separator), "g");
    }
};

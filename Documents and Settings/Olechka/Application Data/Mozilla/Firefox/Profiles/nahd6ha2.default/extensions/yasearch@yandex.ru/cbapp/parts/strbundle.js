const EXPORTED_SYMBOLS = ["StringBundlePart"];

const { classes: Cc, interfaces: Ci, utils: Cu } = Components;

const StringBundlePart = {
    init: function StringBundlePart_init(application) {
        this.defaultPrefixForURL = "chrome://" + application.name + "/locale/";
        
        let globalPropsBundle = Cc["@mozilla.org/intl/stringbundle;1"]
                                    .getService(Ci.nsIStringBundleService)
                                    .createBundle(this.defaultPrefixForURL + "global.properties");
        
        let pluralRule = parseInt(globalPropsBundle.GetStringFromName("pluralRule"), 10);
        Cu.import("resource://gre/modules/PluralForm.jsm", this._pluralFormModule);
        [this.getPluralForm, this.getNumForms] = this._pluralFormModule.PluralForm.makeGetter(pluralRule);
    },
    
    defaultPrefixForURL: undefined,
    getPluralForm: undefined,
    getNumForms: undefined,
    
    _pluralFormModule: {}
};

StringBundlePart.StringBundle = function StringBundle(aURL) {
    this._url = this._createURL(aURL);
};

StringBundlePart.StringBundle.prototype = {
    get: function StringBundle_get(key, args) {
        if (args)
            return this._stringBundle.formatStringFromName(key, args, args.length);
        
        return this._stringBundle.GetStringFromName(key);
    },
    
    getPlural: function StringBundle_getPlural(key, pluralData, args) {
        
        if (typeof pluralData == "number")
            return this._getPluralString(key, pluralData);
        
        let str = this.get(key, args);
        
        for (let i = pluralData.length; i--;) {
            let purIndex = i + 1;
            let data = pluralData[i];
            
            let plurStringKey = (typeof data == "number" || !("key" in data))
                                    ? [key, purIndex, "Plur"].join("")
                                    : data.key;
            
            let plurNumber = (typeof data == "number" ? data : data.number) || 0;
            let plurString = this._getPluralString(plurStringKey, plurNumber);
            
            str = str.replace(new RegExp("#" + purIndex, "g"), plurString);
        }
        
        return str;
    },
    
    tryGet: function StringBundle_tryGet(key, args, default_) {
        try {
            return this.get(key, args);
        }
        catch (e) {
            return default_;
        }
    },
    
    
    _createURL: function StringBundle__createURL(aURL) {
        return (/^chrome:\/\//.test(aURL) ? "" : StringBundlePart.defaultPrefixForURL) + aURL;
    },
    
    get _stringBundle() {
        let stringBundle = Cc["@mozilla.org/intl/stringbundle;1"]
                               .getService(Ci.nsIStringBundleService)
                               .createBundle(this._url);
        
        this.__defineGetter__("_stringBundle", function() stringBundle);
        return this._stringBundle;
    },
    
    _getPluralString: function StringBundle__getPluralString(aStringKey, aNumber) {
        let plurStrings = this.get(aStringKey).split(";");
        let plurStringNone = StringBundlePart.getNumForms() < plurStrings.length ? plurStrings.shift() : null;
        
        return (aNumber === 0 && plurStringNone !== null)
                   ? plurStringNone
                   : StringBundlePart.getPluralForm(aNumber, plurStrings.join(";"));
    }
};


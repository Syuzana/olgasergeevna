const misc = {
    
    getBrowserWindows: function misc_getBrowserWindows() {
        let windows = [];
        let wndEnum = Cc["@mozilla.org/appshell/window-mediator;1"]
                        .getService(Ci.nsIWindowMediator)
                        .getEnumerator("navigator:browser");
        
        while (wndEnum.hasMoreElements())
            windows.push(wndEnum.getNext());
        
        return windows;
    },
    
    getTopBrowserWindow: function misc_getTopBrowserWindow() {
        return this.getTopWindowOfType("navigator:browser");
    },
    
    getTopWindowOfType: function misc_getTopWindowOfType(windowType) {
        let mediator = Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator);
        return mediator.getMostRecentWindow(windowType);
    },
    openWindow: function misc_openWindow(parameters) {
        let window;
        
        if ("name" in parameters && parameters.name) {
            const WM = Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator);
            if ((window = WM.getMostRecentWindow(parameters.name))) {
                window.focus();
                return window;
            }
        }
        
        let parent;
        let features = parameters.features || "";
        
        if (features.indexOf("__popup__") != -1) {
            let featuresHash = { __proto__: null };
            
            features.replace(/(^|,)__popup__($|,)/, "").split(",")
            .forEach(function(aFeatureString) {
                if (aFeatureString) {
                    let [name, value] = aFeatureString.split("=");
                    if (name)
                        featuresHash[name] = value;
                }
            });
            function addFeature(aFeatureString) {
                let [name, value] = aFeatureString.split("=");
                if (!(name in featuresHash))
                    featuresHash[name] = value;
            }
            
            addFeature("chrome");
            addFeature("dependent=yes");
            
            if (!Ci.nsIDOMGeoPositionAddress)
                addFeature("titlebar=no");
            
            if (sysutils.platformInfo.os.name != "windows")
                addFeature("popup=yes");
            let featuresMod = [];
            for (let [name, value] in Iterator(featuresHash))
                featuresMod.push(name + (value ? "=" + value : ""));
            
            features = featuresMod.join(",");
            if (!("parent" in parameters))
                parent = this.getTopBrowserWindow();
        }
        
        parent = parent || parameters.parent || null;
        
        const WW = Cc["@mozilla.org/embedcomp/window-watcher;1"].getService(Ci.nsIWindowWatcher);
        window = WW.openWindow(
            parent,
            parameters.url,
            parameters.name || "_blank",
            features,
            parameters.arguments
        );
        
        window.parameters = parameters;
        
        return window;
    },
    
    createFixupURIFromString: function misc_createFixupURIFromString(aString) {
        let uri = null;
        if (aString) {
            let URIFixup = Cc["@mozilla.org/docshell/urifixup;1"].getService(Ci.nsIURIFixup);
            try {
                uri = URIFixup.createFixupURI(aString, URIFixup.FIXUP_FLAG_NONE);
            } catch (e) {}
        }
        
        return uri;
    },
    
    mapKeysToArray: function misc_mapKeysToArray(map, filter) {
        return filter ? [key for (key in map) if (filter(key))]
                      : [key for (key in map)];
    },
    
    mapValsToArray: function misc_mapValsToArray(map, filter) {
        return filter ? [val for each (val in map) if (filter(key))]
                      : [val for each (val in map)];
    },
    get CryptoHash() {
        let CryptoHash = {
            getFromString: function CryptoHash_getFromString(aString, aAlgorithm) {
                let hash = this._createHash(aAlgorithm);
                let stream = this._utf8Converter.convertToInputStream(aString);
                this._updateHashFromStream(hash, stream);
                return this._binaryToHex(hash.finish(false));
            },
            get _utf8Converter() {
                let converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"]
                                    .createInstance(Ci.nsIScriptableUnicodeConverter);
                converter.charset = "UTF-8";
                
                delete this._utf8Converter;
                return (this._utf8Converter = converter);
            },
            
            _createHash: function CryptoHash__createHash(aAlgorithm) {
                let hash = Cc["@mozilla.org/security/hash;1"].createInstance(Ci.nsICryptoHash);
                hash.initWithString(aAlgorithm);
                return hash;
            },
            
            _binaryToHex: function CryptoHash__binaryToHex(aInput) {
                return [("0" + aInput.charCodeAt(i).toString(16)).slice(-2)
                            for (i in aInput)].join("");
            },
            
            _updateHashFromStream: function CryptoHash__updateHashFromStream(aHash, aStream) {
                let streamSize = aStream.available();
                if (streamSize)
                    aHash.updateFromStream(aStream, streamSize);
            }
        };
        
        delete this.CryptoHash;
        return (this.CryptoHash = CryptoHash);
    },
    
    parseLocale: function misc_parseLocale(localeString) {
        let components = localeString.match(this._localePattern);
        if (components)
            return {
                language: components[1],
                country: components[3],
                region: components[5]
            };
        return null;
    },
    
    queryXMLDoc: function misc_queryXMLDoc(expr, contextNode, extNSResolver) {
        if ( !(contextNode instanceof Ci.nsIDOMNode) )
            throw new CustomErrors.EArgType("contextNode", "nsIDOMNode", contextNode);
        let doc = (contextNode instanceof Ci.nsIDOMDocument)? contextNode: contextNode.ownerDocument;
        let NSResolver = extNSResolver || this._xpathEvaluator.createNSResolver(doc.documentElement);
        let rawResult = this._xpathEvaluator.evaluate(expr, contextNode, NSResolver, 0, null);
        
        let result;
        switch (rawResult.resultType) {
            case rawResult.NUMBER_TYPE:
                result = rawResult.numberValue;
                break;
            case rawResult.STRING_TYPE:
                result = rawResult.stringValue;
                break;
            case rawResult.BOOLEAN_TYPE:
                result = rawResult.booleanValue;
                break;
            case rawResult.UNORDERED_NODE_ITERATOR_TYPE:
                let nodesArray = [];
                let node;
                while (node = rawResult.iterateNext())
                    nodesArray.push(node);
                result = nodesArray;
                break;
            default:
                break;
        }
        return result;
    },
    
    getDOMParser: function misc_getDOMParser(docURI, baseURI, withSystemPrincipal) {
        let domParser = Cc["@mozilla.org/xmlextras/domparser;1"].createInstance(Ci.nsIDOMParser);
        
        if (docURI || baseURI) {
            let systemPrincipal = null;
            try {
                if (!!withSystemPrincipal)
                    systemPrincipal = Cc["@mozilla.org/scriptsecuritymanager;1"]
                                          .getService(Ci.nsIScriptSecurityManager)
                                          .getSystemPrincipal();
            } catch (ex) {}
            
            domParser.init(systemPrincipal, docURI, baseURI);
            
        } else {
            try {
                domParser.init();
            } catch (ex) {
                domParser = Cc["@mozilla.org/xmlextras/domparser;1"].getService(Ci.nsIDOMParser);
            }
        }
        
        return domParser;
    },
    
    _xpathEvaluator: Cc["@mozilla.org/dom/xpath-evaluator;1"].getService(Ci.nsIDOMXPathEvaluator),
    _localePattern: /^([a-z]{2})(-([A-Z]{2})(-(\w{2,5}))?)?$/
};

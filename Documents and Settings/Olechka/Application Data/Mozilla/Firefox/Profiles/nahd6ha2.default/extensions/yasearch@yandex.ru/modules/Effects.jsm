let EXPORTED_SYMBOLS = ["Effects"];

const Cc = Components.classes;
const Ci = Components.interfaces;

var gAnimators = [];

var observerServiceWrapper = {
  _observerService: Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService),
  
  addObserver: function(aObject, aTopic) {
    return this._observerService.addObserver(aObject, aTopic, false);
  },
  
  removeObserver: function(aObject, aTopic) {
    return this._observerService.removeObserver(aObject, aTopic, false);
  }
};

var gCleaner = {
  init: function() {
    observerServiceWrapper.addObserver(this, "xpcom-shutdown");
  },
  
  shutdown: function() {
    while (gAnimators.length) {
      gAnimators.pop().finish();
    }
    
    gAnimators = null;
    
    observerServiceWrapper.removeObserver(this, "xpcom-shutdown");
  },
  
  observe: function(aSubject, aTopic, aData) {
    if (aTopic === "xpcom-shutdown")
      this.shutdown();
  }
};

gCleaner.init();

function Animator(aNode, aParams, aOptions, aCallback, maxTimes) {
  this._node = aNode;
  
  this._params = aParams;
  this._options = aOptions;
  this._callback = aCallback;
  
  this._timer = null;
  this._timerCounter = 0;
  this._maxTimes = maxTimes;
  
  this._timerInterval = parseInt(this._options.duration / this._maxTimes, 10);
  
  gAnimators.push(this);
  
  this.start();
}

Animator.prototype = {
  _destroy: function() {
    if (!this._timer)
      return;
    
    let timer = this._timer;
    gAnimators = gAnimators.filter(function(anim) anim._timer !== timer);
    
    this._timer.cancel();
    this._timer = null;
    this._timerCounter = null;
    this._maxTimes = null;
    this._timerInterval = null;
    
    this._node = null;
    this._params = null;
    this._options = null;
    this._callback = null;
  },
  
  start: function() {
    this._timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
    this._timer.initWithCallback(this, this._timerInterval, Ci.nsITimer.TYPE_REPEATING_SLACK);
  },
  
  stop: function() {
    if (this._callback)
      this._callback();
    
    this._destroy();
  },
  
  finish: function() {
    if (this._timer) {
      this._timerCounter = this._maxTimes;
      this.notify(this._timer);
    }
  },
  
  notify: function(aTimer) {
    this._timerCounter++;
    
    try {
      let style = this._node.style;
      for (let [paramName, paramValue] in Iterator(this._params)) {
        style[paramName] = paramValue.from + (paramValue.step * this._timerCounter) + paramValue.unit;
      }
    } catch(e) {
      this._timerCounter = this._maxTimes;
    }
    
    if (this._timerCounter >= this._maxTimes)
      this.stop();
  }
}

var Effects = {
  hide: function(aElement) {},
  
  show: function(aElement) {},
  animate: function(aNode, aParams, aOptions, aCallback) {
    let options = aOptions && typeof aOptions == "object" ? aOptions : {};
    if (!("duration" in options)) {
      options.duration = typeof aOptions == "number" ? aOptions : 200;
    }
    
    let maxTimes = 8;
    
    let params = {};
    let nodeStyle = aNode.ownerDocument.defaultView.getComputedStyle(aNode, "");
    for (let [paramName, paramValue] in Iterator(aParams)) {
      let currValue = parseFloat(nodeStyle[paramName]);
      let step = (paramValue - currValue) / maxTimes;
      
      if (step) {
        params[paramName] = {
          from: currValue,
          to: paramValue,
          step: step,
          unit: /([a-z]+)/.test(nodeStyle[paramName]) ? RegExp.$1 : ""
        };
      }
    }
    
    return new Animator(aNode, params, options, aCallback, maxTimes);
  }
};
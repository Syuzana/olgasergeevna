const EXPORTED_SYMBOLS = ["DataURI"];
const PERMS_FILE = 0644;

const Cc = Components.classes;
const Ci = Components.interfaces;

const FileInputStream = Cc["@mozilla.org/network/file-input-stream;1"];
const BinInputStream = Cc["@mozilla.org/binaryinputstream;1"];

const mimeService = Cc["@mozilla.org/mime;1"].getService(Ci.nsIMIMEService);

const DataURI = function DataURI(headers,content) {
	if (!(this instanceof DataURI)) return new DataURI(headers, content);
	this.headers = {};
	for (let key in headers) this.headers[key] = headers[key];
	this.content = content;
	return this;
};

DataURI.fromFile = function DataURI_fromFile(aFile, contentType) {
	if (!contentType) contentType = mimeService.getTypeFromFile(aFile);
	const headers = { contentType: contentType, base64: true };
	const content = DataURI.encodeFile(aFile);
	const dataURI = DataURI(headers, content);
	return dataURI;
};

DataURI.prototype.headers = { contentType: "text/plain", base64: false };
DataURI.prototype.content = "";

DataURI.prototype.toString = function DataURI_toString() {
	let headers = this.headers;
	let base64 = headers.base64;

	let content = this.content;
	if (!base64) content = encodeURIComponent(content);

	let uri = "data:";
	uri += headers.contentType || "";
	if (headers.base64) uri += ";base64";
	uri += "," + content;

	return uri;
};

DataURI.encodeFile = function DataURI_encodeFile(aFile) {
	const fileStream = FileInputStream.createInstance(Ci.nsIFileInputStream);
	fileStream.init(aFile, 0x01, PERMS_FILE, 0);
	const encoded = DataURI.encodeFileStream(fileStream);
	fileStream.close();
	return encoded;
};

DataURI.encodeFileStream = function DataURI_encodeFileStream(aFileStream) {
	const binStream = BinInputStream.createInstance(Ci.nsIBinaryInputStream);
	binStream.setInputStream(aFileStream);
	const encoded = DataURI.encodeBinStream(binStream);
	binStream.close();
	return encoded;
};

DataURI.encodeBinStream = function DataURI_encodeBinStream(aBinStream) {
	const bytes = aBinStream.readByteArray(aBinStream.available());
	const encoded = btoa(String.fromCharCode.apply(null, bytes));
	return encoded;
};

/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Google Suggest Autocomplete Implementation for Firefox.
 *
 * The Initial Developer of the Original Code is Google Inc.
 * Portions created by the Initial Developer are Copyright (C) 2006
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Ben Goodger <beng@google.com>
 *   Mike Connor <mconnor@mozilla.com>
 *   Joe Hughes  <joe@retrovirus.com>
 *   Pamela Greene <pamg.bugs@gmail.com>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

const SEARCH_RESPONSE_SUGGESTION_JSON = "application/x-suggestions+json";

const BROWSER_SUGGEST_PREF = "browser.search.suggest.enabled";
const XPCOM_SHUTDOWN_TOPIC              = "xpcom-shutdown";
const NS_PREFBRANCH_PREFCHANGE_TOPIC_ID = "nsPref:changed";

const SEARCH_BUNDLE = "chrome://yasearch/locale/searchbox/searchbox.properties";

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cr = Components.results;

const HTTP_OK                    = 200;
const HTTP_INTERNAL_SERVER_ERROR = 500;
const HTTP_BAD_GATEWAY           = 502;
const HTTP_SERVICE_UNAVAILABLE   = 503;

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

function SuggestAutoCompleteResult(searchString,
                                   searchResult,
                                   defaultIndex,
                                   errorDescription,
                                   results,
                                   comments,
                                   formHistoryResult) {
  this._searchString = searchString;
  this._searchResult = searchResult;
  this._defaultIndex = defaultIndex;
  this._errorDescription = errorDescription;
  this._results = results;
  this._comments = comments;
  this._formHistoryResult = formHistoryResult;
}
SuggestAutoCompleteResult.prototype = {
  _searchString: "",
  _searchResult: 0,
  _defaultIndex: 0,
  _errorDescription: "",
  _results: [],
  _comments: [],
  _formHistoryResult: null,
  get searchString() {
    return this._searchString;
  },
  get searchResult() {
    return this._searchResult;
  },
  get defaultIndex() {
    return this._defaultIndex;
  },
  get errorDescription() {
    return this._errorDescription;
  },
  get matchCount() {
    return this._results.length;
  },
  getValueAt: function(index) {
    return this._results[index];
  },
  getLabelAt: function(index) {
    return this.getValueAt(index);
  },
  getCommentAt: function(index) {
    return this._comments[index];
  },
  getStyleAt: function(index) {
    if (!this._comments[index])
      return null;

    if (index == 0)
      return "suggestfirst";

    return "suggesthint";
  },
  getImageAt: function(index) {
    return "";
  },
  removeValueAt: function(index, removeFromDatabase) {
    if (removeFromDatabase && this._formHistoryResult &&
        index < this._formHistoryResult.matchCount) {
      this._formHistoryResult.removeValueAt(index, true);
    }
    this._results.splice(index, 1);
    this._comments.splice(index, 1);
  },
  QueryInterface: XPCOMUtils.generateQI([Ci.nsIAutoCompleteResult, Ci.nsISupports])
};

function SuggestAutoComplete() {
  this._init();
}

SuggestAutoComplete.prototype = {
  _init: function() {
    Components.utils.import("resource://yasearch-mod/JSON.jsm");
    this._addObservers();
    this._loadSuggestPref();
  },
  
  get _strings() {
    if (!this.__strings) {
      var sbs = Cc["@mozilla.org/intl/stringbundle;1"].getService(Ci.nsIStringBundleService);
      this.__strings = sbs.createBundle(SEARCH_BUNDLE);
    }
    return this.__strings;
  },
  
  __strings: null,

  _loadSuggestPref: function SAC_loadSuggestPref() {
    var prefService = Cc["@mozilla.org/preferences-service;1"].
                      getService(Ci.nsIPrefBranch);
    this._suggestEnabled = prefService.getBoolPref(BROWSER_SUGGEST_PREF);
  },
  
  _suggestEnabled: null,
  _serverErrorLog: [],
  _maxErrorsBeforeBackoff: 3,
  _serverErrorPeriod: 600000,
  _serverErrorTimeoutIncrement: 600000,
  _serverErrorTimeout: 0,
  _nextRequestTime: 0,
  _serverErrorEngine: null,
  _request: null,
  _listener: null,
  _includeFormHistory: true,
  _sentSuggestRequest: false,
  
  notify: function SAC_notify(timer) {
    this._formHistoryTimer = null;
    
    if (!this._listener)
      return;
    
    this._listener.onSearchResult(this, this._formHistoryResult);
    this._reset();
  },
  
  _suggestionTimeout: 500,
  
  onSearchResult: function SAC_onSearchResult(search, result) {
    this._formHistoryResult = result;
    
    if (this._request) {
      this._formHistoryTimer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
      this._formHistoryTimer.initWithCallback(this, this._suggestionTimeout, Ci.nsITimer.TYPE_ONE_SHOT);
    } else if (!this._sentSuggestRequest) {
      this._listener.onSearchResult(this, this._formHistoryResult);
      this._reset();
    }
  },
  
  _suggestURI: null,
  _formHistoryResult: null,
  _formHistoryTimer: null,
  
  _reset: function SAC_reset() {
    if (!this._formHistoryTimer) {
      this._listener = null;
      this._formHistoryResult = null;
    }
    this._request = null;
  },
  
  _startHistorySearch: function SAC_SHSearch(searchString, searchParam) {
    var formHistory = Cc["@mozilla.org/autocomplete/search;1?name=form-history"].createInstance(Ci.nsIAutoCompleteSearch);
    formHistory.startSearch(searchString, searchParam, this._formHistoryResult, this);
  },
  
  _noteServerError: function SAC__noteServeError() {
    var currentTime = Date.now();

    this._serverErrorLog.push(currentTime);
    if (this._serverErrorLog.length > this._maxErrorsBeforeBackoff)
      this._serverErrorLog.shift();

    if ((this._serverErrorLog.length == this._maxErrorsBeforeBackoff) &&
        ((currentTime - this._serverErrorLog[0]) < this._serverErrorPeriod)) {
      this._serverErrorTimeout = (this._serverErrorTimeout * 2) +
                                 this._serverErrorTimeoutIncrement;
      this._nextRequestTime = currentTime + this._serverErrorTimeout;
    }
  },
  
  _clearServerErrors: function SAC__clearServerErrors() {
    this._serverErrorLog = [];
    this._serverErrorTimeout = 0;
    this._nextRequestTime = 0;
  },
  
  _okToRequest: function SAC__okToRequest() {
    return Date.now() > this._nextRequestTime;
  },
  
  _checkForEngineSwitch: function SAC__checkForEngineSwitch(engine) {
    if (engine == this._serverErrorEngine)
      return;

    this._serverErrorEngine = engine;
    this._clearServerErrors();
  },

  _isBackoffError: function SAC__isBackoffError(status) {
    return ((status == HTTP_INTERNAL_SERVER_ERROR) ||
            (status == HTTP_BAD_GATEWAY) ||
            (status == HTTP_SERVICE_UNAVAILABLE));
  },

  onReadyStateChange: function() {
    if (!this._request || this._request.readyState != 4)
      return;

    try {
      var status = this._request.status;
    } catch (e) {
      return;
    }

    if (this._isBackoffError(status)) {
      this._noteServerError();
      return;
    }

    var responseText = this._request.responseText;
    
    if (status != HTTP_OK || responseText == "")
      return;

    this._clearServerErrors();
    
    var serverResults = JSON.parse(responseText);
    var searchString = serverResults[0] || "";
    var results = serverResults[1] || [];

    var comments = [];
    var historyResults = [];
    var historyComments = [];

    if (this._includeFormHistory && this._formHistoryResult &&
        (this._formHistoryResult.searchResult == Ci.nsIAutoCompleteResult.RESULT_SUCCESS)) {
      for (var i = 0; i < this._formHistoryResult.matchCount; ++i) {
        var term = this._formHistoryResult.getValueAt(i);

        var dupIndex = results.indexOf(term);
        if (dupIndex != -1)
          results.splice(dupIndex, 1);
        
        historyResults.push(term);
        historyComments.push("");
      }
      
      if (historyComments.length > 0)
        historyComments[0] = this._strings.GetStringFromName("history_label");
    }

    for (var i = 0; i < results.length; ++i)
      comments.push("");

    if (comments.length > 0)
      comments[0] = this._strings.GetStringFromName("suggestion_label");

    var finalResults = historyResults.concat(results);
    var finalComments = historyComments.concat(comments);

    this.onResultsReady(searchString, finalResults, finalComments,
                        this._formHistoryResult);

    this._reset();
  },

  onResultsReady: function(searchString, results, comments,
                           formHistoryResult) {
    
    if (this._listener) {
      var result = new SuggestAutoCompleteResult(
          searchString,
          Ci.nsIAutoCompleteResult.RESULT_SUCCESS,
          0,
          "",
          results,
          comments,
          formHistoryResult);

      this._listener.onSearchResult(this, result);
      this._listener = null;
    }
  },
  
  startSearch: function(searchString, searchParam, previousResult, listener) {
    if (!previousResult)
      this._formHistoryResult = null;
    
    this.stopSearch();
    
    this._listener = listener;
    
    var engine;
    
    if (/yasearch\-history2/.test(searchParam)) {
      engine = Cc["@yandex.ru/yasearch;1"].getService(Ci.nsIYaSearch).wrappedJSObject.defaultYandexSearchEngine;
      searchParam = "yasearch-history";
    } else {
      engine = Cc["@yandex.ru/yasearch;1"].getService(Ci.nsIYaSearch).wrappedJSObject.currentSearchEngine;
    }
    
    if (!searchString ||
        !this._suggestEnabled ||
        !engine.supportsResponseType(SEARCH_RESPONSE_SUGGESTION_JSON) ||
        !this._okToRequest()) {
      this._sentSuggestRequest = false;
      this._startHistorySearch(searchString, searchParam, previousResult);
      
      return;
    }

    this._request = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].
                    createInstance(Ci.nsIXMLHttpRequest);
    var submission = engine.getSubmission(searchString,
                                          SEARCH_RESPONSE_SUGGESTION_JSON);
    this._suggestURI = submission.uri;
    var method = (submission.postData ? "POST" : "GET");
    this._request.open(method, this._suggestURI.spec, true);
    
    var self = this;
    function onReadyStateChange() {
      self.onReadyStateChange();
    }
    
    this._request.onreadystatechange = onReadyStateChange;
    this._request.send(submission.postData);

    if (this._includeFormHistory) {
      this._sentSuggestRequest = true;
      this._startHistorySearch(searchString, searchParam, previousResult);
    }
  },

  stopSearch: function() {
    if (this._request) {
      this._request.abort();
      this._reset();
    }
  },

  observe: function SAC_observe(aSubject, aTopic, aData) {
    switch (aTopic) {
      case NS_PREFBRANCH_PREFCHANGE_TOPIC_ID:
        this._loadSuggestPref();
        break;
      case XPCOM_SHUTDOWN_TOPIC:
        this._removeObservers();
        break;
    }
  },

  _addObservers: function SAC_addObservers() {
    var prefService2 = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch2);
    prefService2.addObserver(BROWSER_SUGGEST_PREF, this, false);

    var os = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
    os.addObserver(this, XPCOM_SHUTDOWN_TOPIC, false);
  },

  _removeObservers: function SAC_removeObservers() {
    var prefService2 = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch2);
    prefService2.removeObserver(BROWSER_SUGGEST_PREF, this);

    var os = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
    os.removeObserver(this, XPCOM_SHUTDOWN_TOPIC);
  },

  QueryInterface: XPCOMUtils.generateQI([Ci.nsIAutoCompleteSearch, Ci.nsIAutoCompleteObserver, Ci.nsISupports])
};

function SearchSuggestAutoComplete() {
  this._init();
}

SearchSuggestAutoComplete.prototype = {
  __proto__: SuggestAutoComplete.prototype,
  serviceURL: "",
  
  classDescription: "Remote Search Suggestions (yandex mod)",
  classID: Components.ID("{79AF2508-6216-44e0-A28E-275DC7878BD7}"),
  contractID: "@mozilla.org/autocomplete/search;1?name=yasearch-autocomplete"
};

if (XPCOMUtils.generateNSGetFactory)
  var NSGetFactory = XPCOMUtils.generateNSGetFactory([SearchSuggestAutoComplete]);
else
  var NSGetModule = XPCOMUtils.generateNSGetModule([SearchSuggestAutoComplete]);

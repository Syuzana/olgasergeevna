
XB.network = {
    init: function XBNet_init() {
        this._resources = {};
        this._downloaders = {};
        this._logger = XB._base.getLogger("Network");
        this._logger.level = XB._base.application.core.Lib.Log4Moz.Level.Config;
    },
    
    finalize: function XBNet_finalize() {
        for each (let resource in this._resources)
            try {
                resource.finalize();
            }
            catch (e) {
                this._logger.error("Error finalizing resource. " + strutils.formatError(e));
            }
        
        for each (let downloader in this._downloaders)
            try {
                downloader.finalize();
            }
            catch (e) {
                this._logger.error("Error finalizing downloader. " + strutils.formatError(e));
            }
        
        this._resources = null;
        this._downloaders = null;
    },
    
    getResource: function XBNet_getResource(requestData) {
        XB._base.runtime.ensureValueTypeIs(requestData, "RequestData");
        
        let resHash = requestData.hash;
        this._logger.debug("Looking for resource with hash \"" + resHash + "\"");
        let resource = this._resources[resHash];
        if (!resource) {
            this._logger.debug("Creating new resource");
            resource = new XB.network.Resource(requestData);
            this._resources[resHash] = resource;
        }
        return resource;
    },
    
    requestFinished: function XBNet_requestFinished(requestID) {
        requestID = parseInt(requestID, 10);
        return !!requestID && (requestID < this._requestID) && !(requestID in this._reqFinishSubs);
    },
    
    subscribeToRequestFinish: function XBNet_subscribeToRequestFinish(requestID, subscriber) {
        let subsArray = this._reqFinishSubs[requestID];
        if (!subsArray)
            this._reqFinishSubs[requestID] = [subscriber];
        else
            subsArray.push(subscriber);
    },
    
    ResourceDataType: {
        NRDT_CONTENT: 0,
        NRDT_HEADERS: 1,
        NRDT_STATUS: 2
    },
    
    _reqFinishSubs: {},
    _resources: null,
    _downloaders: null,
    _logger: null,
    _requestID: 1,
    
    _disposeResource: function XBNet__disposeResource(netResource) {
        let resHash = netResource.requestDataHash;
        netResource.finalize();
        delete this._resources[resHash];
    },
    
    _getDownloader: function XBNet__getDownloader(requestData) {
        XB._base.runtime.ensureValueTypeIs(requestData, "RequestData");
        
        let dlHash = this._dlHash(requestData);
        let downloader = this._downloaders[dlHash];
        if (!downloader) {
            downloader = new XB.network._ResourceDownloader(requestData);
            this._downloaders[dlHash] = downloader;
        }
        return downloader;
    },
    
    _getNewRequestID: function XBNet__getNewRequestID() {
        let requestID = this._requestID++;
        this._reqFinishSubs[requestID] = undefined;
        return requestID;
    },
    
    _dlHash: function XBNet__dlHash(requestData) {
        return [requestData.url, requestData.method, requestData.specialsHash].join("#");
    },
    
    _onDownloadFinished: function XBNet__onDownloadFinished(requestID) {
        let subscribers = this._reqFinishSubs[requestID];
        if (subscribers) {
            for each (let subscriber in subscribers)
                try {
                    subscriber.onRequestFinished(requestID);
                }
                catch (e) {
                    XB._base.logger.error("subscriber.onRequestFinished failed. " + strutils.formatError(e));
                }
        }
        
        delete this._reqFinishSubs[requestID];
    }
};

XB.network.Resource = function XBNetResource(requestData) {
    this._logger = XB._base.getLogger(this._makeLoggerID(requestData));
    this._logger.debug("Created");
    this._storageKey = this._makeStorageKey(requestData);
    this._dataExpTimer = XB._Cc["@mozilla.org/timer;1"].createInstance(XB._Ci.nsITimer);
    this._subscribtions = {};
    this._downloader = XB.network._getDownloader(requestData);
    this._requestData = requestData;
    this._data = {};
    this._reqFlags = {};
    
    if (this._loadFromCache()) {
        if (this._currDataTime !== undefined) {
            let dataExpirationInterval = (this._currDataTime + this._requestData.expirationInterval) * 1000 - Date.now();
            dataExpirationInterval = Math.max(dataExpirationInterval, 0);
            this._dataExpTimer.initWithCallback(this, dataExpirationInterval, this._dataExpTimer.TYPE_REPEATING_SLACK);
            
            let extraUpdateInterval = (this._currDataTime + this._requestData.updateInterval) * 1000 - Date.now();
            extraUpdateInterval = Math.max(extraUpdateInterval, 0);
            this._logger.debug("Resource set extra request timer to " + extraUpdateInterval);
            this._extraUpdTimer = XB._Cc["@mozilla.org/timer;1"].createInstance(XB._Ci.nsITimer);
            this._extraUpdTimer.initWithCallback(this, extraUpdateInterval, this._extraUpdTimer.TYPE_ONE_SHOT);
        }
    }
};

XB.network.Resource.prototype = {
    constructor: XB.network.Resource,
    requestData: function XBNetResource_requestData(subscriber, dataType) {
        this._checkSubscription(subscriber, dataType);
        return this._getDataPart(dataType);
    },
    
    update: function XBNetResource_update(bypassBrowserCache, invalidateXBCache) {
        let requestId = this._downloader.update(bypassBrowserCache);
        if (invalidateXBCache)
            this._reqFlags[requestId] = this._REQ_FLAG_INVALIDATE_CACHE;
        return requestId;
    },
    
    unsubscribe: function XBnetResource_unsubscribe(subscriber, dataType) {
        if (!subscriber || !subscriber.effectiveID || (dataType === undefined) )
            throw new TypeError(this._consts.ERR_WRONG_SUBSCRIBER_INTF);
        let subHash = subscriber.effectiveID + "#" + dataType;
        delete this._subscribtions[subHash];
        if (sysutils.isEmptyObject(this._subscribtions))
            XB.network._disposeResource(this);
    },
    finalize: function XBNetResource_finalize() {
        if (this._dataExpTimer)
            this._dataExpTimer.cancel();
        this._dataExpTimer = null;
        if (this._extraUpdTimer)
            this._extraUpdTimer.cancel();
        this._extraUpdTimer = null;
        this._downloader.unsubscribe(this);
        this._downloader = null;
        this._requestData = null;
        this._logger = null;
    },
    onDownloaderFinished: function XBNetResource_onDownloaderFinished(requestId) {
        try {
            if (this._updateFromDownloader()) {
                try {
                    this._resetTimer();
                    
                    if (this._requestData.expirationInterval == 0)
                        return;
                    
                    let cacheData = {
                            status: this._downloader.data.status,
                            headers: this._downloader.data.headers,
                            body: this._downloader.data.bodyText
                        };
                    XB.cache.store(this._storageKey, cacheData);
                }
                finally {
                    this._notifySubscribers();
                }
            }
            else if ((this._reqFlags[requestId] || 0) & this._REQ_FLAG_INVALIDATE_CACHE) {
                try {
                    XB.cache.remove(this._storageKey);
                }
                finally {
                    this._data = {};
                    this._currDataTime = undefined;
                    this._notifySubscribers();
                }
            }
        }
        catch (e) {
            this._logger.error("XBNetResource_onDownloaderFinished failed. " + strutils.formatError(e));
        }
        finally {
            delete this._reqFlags[requestId];
        }
    },
    
    get requestDataHash() {
        return this._requestData.hash;
    },
    
    _consts: {
        ERR_WRONG_SUBSCRIBER_INTF: "Wrong subscriber interface",
        ERR_LOAD_CACHE: "Couldn't load cached response",
        ERR_UNKNOWN_DATA_TYPE: "Unknown data type requested",
        ERR_NOTIFYING: "Could not notify resource subscriber"
    },
    _logger: null,
    _dataExpTimer: null,
    _extraUpdTimer: null,
    _subscribtions: null,
    _requestData: null,
    _storageKey: undefined,
    _downloader: null,
    _data: null,
    _subscribed: false,
    _currDataTime: undefined,
    _reqFlags: undefined,
    
    _REQ_FLAG_INVALIDATE_CACHE: 1,
    
    _loadFromCache: function XBNetResource__loadFromCache() {
        try {
            let storeTime = {};
            let responseData = XB.cache.findData(this._storageKey, this._requestData.expirationInterval, storeTime);
            if (responseData) {
                responseData = XB._base.application.core.Lib.JSON.parse(responseData);
                let content = this._isXML? this._tryConvertToXML(responseData.body): responseData.body;
                if (this._validate(responseData.status, content, responseData.headers)) {
                    this._data.headers = responseData.headers;
                    this._data.content = content;
                    this._data.status = responseData.status;
                    
                    this._currDataTime = storeTime.timestamp;
                    return true;
                }
            }
        }
        catch (e) {
            this._logger.error(this._consts.ERR_LOAD_CACHE + ". " + strutils.formatError(e));
        }
        
        return false;
    },
    
    _getDataPart: function XBNetResource__getDataPart(dataType) {
        switch (dataType) {
            case XB.network.ResourceDataType.NRDT_CONTENT:
                return this._data.content || undefined;
            case XB.network.ResourceDataType.NRDT_HEADERS:
                return this._data.headers || undefined;
            case XB.network.ResourceDataType.NRDT_STATUS:
                return this._data.status;
        }
        this._logger.warn(this._consts.ERR_UNKNOWN_DATA_TYPE + ": " + dataType);
        return undefined;
    },
    
    _checkSubscription: function XBNetResource__checkSubscription(subscriber, dataType) {
        this._subscribe(subscriber, dataType);
        if (!this._subscribed) {
            if (this._downloader.data.status != -1) {
                this._updateFromDownloader();
            }
            this._downloader.subscribe(this, this._requestData.updateInterval);
            if (sysutils.isEmptyObject(this._data)) {
                this._logger.debug("Data is empty. Requesting...");
                this._downloader.update();
            }
            this._subscribed = true;
        }
    },
    
    _subscribe: function XBNetResource__subscribe(subscriber, dataType) {
        if (typeof subscriber.update !== "function")
            throw new TypeError(this._consts.ERR_WRONG_SUBSCRIBER_INTF);
        let subHash = subscriber.effectiveID + "#" + dataType;
        this._subscribtions[subHash] = {subscriber: subscriber, dataType: dataType};
    },
    
    _updateFromDownloader: function XBNetResource__updateFromDownloader() {
        let data = this._downloader.data;
        let content = this._isXML? this._tryConvertToXML(data.bodyText): data.bodyText;
        if (this._validate(data.status, content, data.headers)) {
            this._data.content = content;
            this._data.headers = data.headers;
            this._data.status = data.status;
            
            this._currDataTime = parseInt(Date.now() / 1000, 10);
            return true;
        }
        return false;
    },
    
    _validate: function XBNetResource__validate(status, content, headers) {
        let minStatus = this._requestData.statusRange.start,
            maxStatus = this._requestData.statusRange.end;
        let statusIsValid = (minStatus <= status && status <= maxStatus);
        let structIsValid = true;
        if (this._isXML && this._requestData.xpathExpression)
            try {
                if (!content)
                    throw new Error("Content is not XML");
                this._logger.trace("Content xml root: " + content.documentElement.nodeName);
                let queryResult = XB._base.runtime.queryXMLDoc(this._requestData.xpathExpression, content);
                if ( (sysutils.isArray(queryResult) && !queryResult.length) || !queryResult )
                    throw new Error("Empty validation query result");
            }
            catch (e) {
                this._logger.debug("Content validation failed. " + strutils.formatError(e));
                structIsValid = false;
            }
        
        this._logger.trace(strutils.formatString("Status is %1 (valid is %2..%3); structure is %4",
                                                 [status, minStatus, maxStatus, structIsValid ? "OK" : "BAD"]));
        return (statusIsValid && structIsValid);
    },
    
    _resetTimer: function XBNetResource__resetTimer() {
        this._dataExpTimer.cancel();
        if (this._requestData.expirationInterval)
            this._dataExpTimer.initWithCallback(this, this._requestData.expirationInterval * 1000,
                                                this._dataExpTimer.TYPE_REPEATING_SLACK);
    },
    
    _notifySubscribers: function XBNetResource__notifySubscribers() {
        for each (let subscription in this._subscribtions) {
            try {
                subscription.subscriber.update(this, this._getDataPart(subscription.dataType));
            }
            catch (e) {
                this._logger.error(this._consts.ERR_NOTIFYING + ". " + strutils.formatError(e));
            }
        }
    },
    
    _tryConvertToXML: function XBNetResource__tryConvertToXML(text) {
        if (!text)
            return null;
        
        let xmlDoc = XB._base.domParser.parseFromString(text, "text/xml");
        
        if (xmlDoc.documentElement.localName == "parsererror") {
            this._logger.debug("Text is not XML");
            
            try {
                throw new Error("JSON to XML conversion is not implemented");
                
                return xmlDoc;
            }
            catch (e) {
                this._logger.debug("Failed converting from JSON to XML. " + strutils.formatError(e));
            }
            
            try {
                throw new Error("CSV to XML conversion is not implemented");
                
                return xmlDoc;
            }
            catch (e) {
                this._logger.debug("Failed converting from CSV to XML. " + strutils.formatError(e));
            }
            
            return null;
        }
        
        return xmlDoc;
    },
    
    _makeStorageKey: function XBNetResource__makeStorageKey(requestData) {
        return [requestData.url, requestData.method, requestData.format,
                requestData.statusRange.start, requestData.statusRange.end, requestData.xpathExpression,
                requestData.specialsHash].join("#");
    },
    
    _makeLoggerID: function XBNetResource__makeLoggerID(requestData) {
        let url = requestData.url;
        if (url.length > 16)
            url = url.substring(0, 15) + "...";
        return "NetRes." + url + "." + Date.now();
    },
    
    get _isXML() {
        return this._requestData.format !== XB.types.RequestData.Format.FMT_TEXT;
    },
    notify: function XBNetResource_notify(timer) {
        if (timer === this._extraUpdTimer) {
            this.update();
        }
        else {
            this._dataExpTimer.cancel();
            this._data = {};
            this._currDataTime = undefined;
            this._notifySubscribers();
        }
    }
};

XB.network._ResourceDownloader = function XBResDownloader(requestData) {
    XB._base.runtime.ensureValueTypeIs(requestData, "RequestData");
    this._requestData = requestData;
    this._data = {
        status: -1,
        bodyText: "",
        headers: null
    };
    this._subscribtions = [];
    this._timer = XB._Cc["@mozilla.org/timer;1"].createInstance(XB._Ci.nsITimer);
    this._logger = XB._base.getLogger("HTTPDL." + Date.now());
    this._logger.debug("Downloader created");
}

XB.network._ResourceDownloader.prototype = {
    constructor: XB.network._ResourceDownloader,
    update: function XBResDownloader_update(bypassCache) {
        let requestId = this._sendRequest(!!bypassCache);
        this._timer.cancel();
        return requestId;
    },
    subscribe: function XBResDownloader_subscribe(subscriber, interval) {
        if ( !(subscriber instanceof XB.network.Resource) )
            throw new TypeError(this._consts.ERR_WRONG_SUBSCRIBER_INTF);
        if (interval < this._consts.MIN_INTERVAL)
            throw new Error(this._consts.ERR_SMALL_INTERVAL);
        
        let subscribtion = null;
        for (let i = this._subscribtions.length; i--; )
            if (this._subscribtions[i].subscriber == subscriber) {
                subscribtion = this._subscribtions[i];
                break;
            }
        
        if (!subscribtion) {
            subscribtion = {
                subscriber: subscriber,
                interval: interval
            };
            
            this._subscribtions.push(subscribtion);
        } else {
            subscribtion.interval = interval;
        }
        
        if (interval < this._currentInterval)
            this._currentInterval = interval;
        
        if (this._subscribtions.length == 1) {
            if (this._currentInterval != Number.POSITIVE_INFINITY)
                this._resetTimer(this._currentInterval);
        }
        
        this._logger.debug("Added subscriber. New interval is " + this._currentInterval + " s");
    },
    unsubscribe: function XBResDownloader_unsubscribe(subscriber) {
        for (let i = this._subscribtions.length; i--; )
            if (this._subscribtions[i].subscriber === subscriber) {
                this._subscribtions.splice(i, 1);
                this._updateInterval();
                this._logger.debug("Removed subscriber. New interval is " + this._currentInterval +
                                   " s. Subscriptions: " + this._subscribtions.length);
                break;
            }
    },
    
    get numSubscribers() {
        return this._subscribtions.length;
    },
    
    get data() {
        return this._data;
    },
    
    finalize: function XBResDownloader_finalize() {
        this._timer.cancel();
        this._requestData = null;
        this._subscribtions = null;
    },
    
    _consts: {
        MIN_INTERVAL: 5,
        
        ERR_SMALL_INTERVAL: "The requested resource update interval is too small",
        ERR_WRONG_SUBSCRIBER_INTF: "Subscriber does not support the required interface"
    },
    _currRequestID: undefined,
    _requestData: null,
    _data: null,
    _subscribtions: null,
    _timer: null,
    _logger: null,
    
    _currentInterval: Number.POSITIVE_INFINITY,
    
    _resetTimer: function XBResDownloader__resetTimer(newInterval) {
        this._timer.cancel();
        if (newInterval != undefined && newInterval != Number.POSITIVE_INFINITY) {
            this._timer.initWithCallback(this, newInterval * 1000, this._timer.TYPE_ONE_SHOT);
        }
    },
    
    _updateInterval: function XBResDownloader__updateInterval() {
        let interval = Number.POSITIVE_INFINITY;
        for (let i = this._subscribtions.length; i--; )
            if (this._subscribtions[i].interval < interval)
                interval = this._subscribtions[i].interval;
        
        this._resetTimer(interval);
        this._currentInterval = interval;
    },
    
    _sendRequest: function XBResDownloader__sendRequest(bypassCache) {
        try {
            let requestId = XB.network._getNewRequestID();
            this._logger.debug("Sending request #" + requestId + ": " + [this._requestData.method, this._requestData.url]);
            
            let request = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Ci.nsIXMLHttpRequest);
            request.mozBackgroundRequest = true;
            request.open(this._requestData.method, this._requestData.url, true);
            if (bypassCache) {
                request.channel.loadFlags |= Ci.nsIRequest.LOAD_BYPASS_CACHE;
            }
            
            let target = request.QueryInterface(Ci.nsIDOMEventTarget);
            let requestHandler = this._createRequestHandler(requestId);
            target.addEventListener("load", requestHandler, false);
            target.addEventListener("error", requestHandler, false);
            request.send(null);
            return requestId;
        }
        catch (e) {
            this._resetTimer(this._currentInterval);
            this._logger.error("Failed sending request. " + strutils.formatError(e));
        }
    },
    
    _handleResponse: function XBResDownloader__handleResponse(request, requestId) {
        try {
            this._logger.debug("Handling response #" + requestId);
            this._data.status = request.status;
            this._data.bodyText = request.responseText;
            
            let headers = { __proto__: null };
            try {
                request.channel.QueryInterface(Ci.nsIHttpChannel);
                request.channel.visitResponseHeaders({
                    visitHeader: function XBResDownloader_visitHeader(name, value) headers[name] = value
                });
            } catch (ex) {}
            
            this._data.headers = headers;
            
            for (let i = this._subscribtions.length; i--; )
                try {
                    this._subscribtions[i].subscriber.onDownloaderFinished(requestId);
                }
                catch (e) {
                    this._logger.error("Failed notifying downloader subscriber. " + strutils.formatError(e));
                    if (e.stack)
                        this._logger.debug(e.stack);
                }
        }
        catch(e) {
            this._logger.error("Error in _ResourceDownloader::_handleResponse. " + strutils.formatError(e));
        }
        finally {
            this._resetTimer(this._currentInterval);
            XB.network._onDownloadFinished(requestId);
        }
    },
    notify: function XBResDownloader_notify() {
        this._sendRequest();
    },
    _createRequestHandler: function XBResDownloader__createRequestHandler(requestId) {
        let me = this;
        
        function XBResDownloader__CRH_handleEvent(event) {
            if (me && ("_handleResponse") in me)
                me._handleResponse(event.target, requestId);
        };
        
        return XBResDownloader__CRH_handleEvent;
    }
}

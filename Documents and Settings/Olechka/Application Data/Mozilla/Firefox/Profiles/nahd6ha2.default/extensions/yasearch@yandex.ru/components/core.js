const {
    classes: Cc,
    interfaces: Ci,
    results: Cr,
    utils: Cu
} = Components;
const EXTENSION_DIR = __LOCATION__.parent.parent;
const EXTENSION_PATH = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI(EXTENSION_DIR).spec;
Cc["@mozilla.org/moz/jssubscript-loader;1"].getService(Ci.mozIJSSubScriptLoader).loadSubScript(EXTENSION_PATH + "config.js");

Cu.import("resource://gre/modules/XPCOMUtils.jsm");
function CustomBarCore() {
    this._loadModules();
    this._setupLogging();
    this._logger.info("Hello from " + this.contractID);
    
    this._observerService.addObserver(this, "profile-after-change", false);
    this._observerService.addObserver(this, "profile-before-change", false);
    this._observerService.addObserver(this, "quit-application", false);
};

CustomBarCore.prototype = {
    
    Lib: {},
    
    get appName() {
        return this._appName;
    },
    
    get version() {
        return this._version;
    },
    
    get xbWidgetsPrefsPath() {
        return XB_WIDGETS_PREFS_PATH;
    },
    
    get nativesPrefsPath() {
        return NATIVES_PREFS_PATH;
    },
    
    get staticPrefsPath() {
        return STATIC_PREFS_PATH;
    },
    
    get application() {
        return this._appObj;
    },
    
    get xbProtocol() {
        return Cc[PROTOCOL_XB_CONTRACT_ID].getService(Ci.nsIProtocolHandler).QueryInterface(Ci.nsIXBProtocolHandler);
    },
    
    get extensionPathFile() {
        return EXTENSION_DIR.clone();
    },
    get wrappedJSObject() {
        return this;
    },
    
    get eventTopics() {
        return this._globalEvents;
    },
    observe: function CustomBarCore_observe(subject, topic, data) {
        switch (topic) {
            case "http-on-modify-request":
                try {
                    this._checkPresetRequest(subject.QueryInterface(Ci.nsIHttpChannel))
                }
                catch (e) {
                    this._logger.error("Error observing 'http-on-modify-request'. " + this._formatError(e));
                    this._logger.debug(e.stack);
                }
                break;
            
            case "profile-after-change":
                try {
                    let oldBarComponent = Cc["@yandex.ru/" + this.appName + ";1"].getService(Ci.nsIYaSearch).wrappedJSObject;
                    if (oldBarComponent && !oldBarComponent.initialized) {
                        this._observerService.addObserver(this, this.appName + "-state-changed", false);
                        return;
                    }
                } catch (ex) {}
                
                this._initApp();
                break;
                
            case "profile-before-change":
                this._destroyApp();
                break;
            
            case "quit-application":
                this._shutdown();
                break;
            
            case "nsPref:changed":
                this._updateLoggers(data == this._dumpLogLevelPrefName, data == this._consoleLogLevelPrefName);
                break;
            
            case this.appName + "-state-changed":
                if (data == "initialized") {
                    this._observerService.removeObserver(this, this.appName + "-state-changed");
                    this._initApp();
                }
                break;
        }
    },
    _version: PLATFORM_VERSION,
    _moduleFileNames: ["Log4moz.jsm", "Preferences.jsm", "JSON.jsm", "WindowListener.jsm", "AddonManager.jsm", "Foundation.jsm"],
    _modulesPath: "resource://" + APP_NAME + "-mod/",
    _appResPath: "resource://" + APP_NAME + "-app/",
    _appChromePath: "chrome://" + APP_NAME + "/",
    _dumpLogLevelPrefName: APP_NAME + ".xbcore.logging.stdout.level",
    _consoleLogLevelPrefName: APP_NAME + ".xbcore.logging.console.level",
    _appName: APP_NAME,
    _appObj: null,
    _dumpAppender: null,
    _consoleAppender: null,
    _observerService: Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService),
    _globalEvents: {
        EVT_PLUGIN_BEFORE_ENABLED: APP_NAME + "-platform-plugin-before_enabled",
        EVT_PLUGIN_ENABLED: APP_NAME + "-platform-plugin-enabled",
        EVT_PLUGIN_BEFORE_DISABLED: APP_NAME + "-platform-plugin-before_disabled",
        EVT_PLUGIN_DISABLED: APP_NAME + "-platform-plugin-disabled",
        EVT_BEFORE_GLOBAL_RESET: APP_NAME + "-platform-components-before_reset",
        EVT_AFTER_GLOBAL_RESET: APP_NAME + "-platform-components-after_reset",
    },
    _presetURLPattern: /^.+\.xb\.xml$/,
    
    _loadModules: function CustomBarCore__loadModules() {
        const Lib = this.Lib;
        const prePath = this._modulesPath;
        this._moduleFileNames.forEach(function loadModule(fileName) {
            Cu.import(prePath + fileName, Lib);
        });
    },
    
    _setupLogging: function CustomBarCore__setupLogging() {
        const Log4Moz = this.Lib.Log4Moz;
        
        let root = Log4Moz.repository.rootLogger;
        root.level = Log4Moz.Level.All;
        
        let formatter = new Log4Moz.BasicFormatter();
        this._consoleAppender = new Log4Moz.ConsoleAppender(formatter);
        this._consoleAppender.level = Log4Moz.Level.Error;
        root.addAppender(this._consoleAppender);
        this._dumpAppender = new Log4Moz.DumpAppender(formatter);
        this._dumpAppender.level = 100;
        root.addAppender(this._dumpAppender);
        this._logger = Log4Moz.repository.getLogger(this.appName + ".Core");
        this._logger.level = Log4Moz.Level.Debug;
    },
    
    _updateLoggers: function CustomBarCore__updateLoggers(checkDumpSetting, checkConsoleSetting) {
        if (checkDumpSetting) {
            let dumpLevel = parseInt( this.Lib.Preferences.get(this._dumpLogLevelPrefName, -1), 10 );
            if (!isNaN(dumpLevel)) {
                if (dumpLevel < 0)
                    dumpLevel = 100;
                this._dumpAppender.level = dumpLevel;
            }
        }
        
        if (checkConsoleSetting) {
            let consoleLevel = parseInt( this.Lib.Preferences.get(this._consoleLogLevelPrefName, -1), 10 );
            if (!isNaN(consoleLevel)) {
                if (consoleLevel < 0)
                    consoleLevel = 100;
                this._consoleAppender.level = consoleLevel;
            }
        }
    },
    
    _initApp: function CustomBarCore__initApp() {
        this._updateLoggers(true, true);
        this.Lib.Preferences.observe(this._dumpLogLevelPrefName, this);
        this.Lib.Preferences.observe(this._consoleLogLevelPrefName, this);
        
        try {
            let appModule = {};
            Cu.import(this._appResPath + "bar.js", appModule);
            this._appObj = appModule.barApplication;
            this._appObj.init(this);
        }
        catch (e) {
            this._logger.fatal("Couldn't initialize application. " + this._formatError(e));
            dump(e)
            dump(e.fileName + "; " + e.lineNumber)
            if (e.stack)
                this._logger.debug(e.stack);
        }
        
        this._observerService.addObserver(this, "http-on-modify-request", false);
    },
    
    _destroyApp: function CustomBarCore__destroyApp() {
        this._observerService.removeObserver(this, "http-on-modify-request");
        
        this.Lib.Preferences.ignore(this._dumpLogLevelPrefName, this);
        this.Lib.Preferences.ignore(this._consoleLogLevelPrefName, this);
        
        try {
            this._appObj.finalize();
            this._appObj = null;
        } catch(e) {
            this._logger.error(this._formatError(e));
        }
    },
    
    _shutdown: function CustomBarCore__shutdown() {
        //
    },
    
    _formatError: function CustomBarCore__formatError(e) {
        if (!(e instanceof Ci.nsIException))
            return ("" + e);
        
        let text = e.name + ": " + e.message;
        if (e.fileName)
            text += "\nin " + e.fileName + "\nat line " + e.lineNumber;
        
        return text;
    },
    
    _checkPresetRequest: function CustomBarCore__checkPresetRequest(channel) {
        if (!channel.URI.path.split(/\?|#|;/)[0].match(this._presetURLPattern))
            return;
        let presetURL = channel.URI.spec;
        try {
            channel.notificationCallbacks.QueryInterface(Ci.nsIWebBrowserPersist);
            return;
        } catch (e) {}
        
        try {
            channel.QueryInterface(Ci.nsIPropertyBag2);
            if (channel.getPropertyAsBool("isXBInstallRequest"))
                return;
        } catch (e) {}
        
        this._logger.debug("XB preset request caught " + presetURL);
        
        channel.cancel(Cr.NS_BINDING_ABORTED);
        this.application.installPreset(presetURL);
    },
    
    classDescription: "Custom Yandex bar core JS component for " + APP_NAME,
    classID: CORE_CLASS_ID,
    contractID: CORE_CONTRACT_ID,
    QueryInterface: XPCOMUtils.generateQI([Ci.nsISupports, Ci.nsIObserver]),
    _xpcom_categories: [{ category: "app-startup", service: true }]
};

if (XPCOMUtils.generateNSGetFactory)
  var NSGetFactory = XPCOMUtils.generateNSGetFactory([CustomBarCore]);
else
  var NSGetModule = XPCOMUtils.generateNSGetModule([CustomBarCore]);

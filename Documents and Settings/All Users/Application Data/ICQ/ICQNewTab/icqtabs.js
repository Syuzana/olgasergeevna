﻿function keyword_focus()
{
	var tmp = document.getElementById("search_keyword");
	tmp.style.color = "black";
	if ( tmp.value == tmp.org_val)
		tmp.value = "";
}

function keyword_blur()
{
	var tmp = document.getElementById("search_keyword");
	
	if ( tmp.value == "")
		tmp.value = tmp.org_val;
		
	if ( tmp.value == tmp.org_val)
		tmp.style.color = "gray";
}
function resize_divs()
{
	var btn_width = document.getElementById("sub_btn").offsetWidth;
	var max_width = document.getElementById("d2-2").offsetWidth-100;
	//document.getElementById("d2-2").style.width = max_width;
	document.getElementById("search_keyword").style.width = max_width - (btn_width+25+75);
	document.getElementById("inp_div").style.width = max_width - (btn_width+25+75);
}
function keyword_change(e, obj)
{
	if(e.keyCode>45||e.keyCode==8)
	{
		var tdiv = document.getElementById("d2");
		var val = obj.value;
		var val_lng = val.length;
		if(val_lng >= 1)
		{
			tdiv.className = "d2-b";
			document.getElementById("btns").style.display = "block";
			var sfor_btn = document.getElementById("search_for");
			var gto_btn = document.getElementById("go_to");
			var max_width = document.getElementById("btns").offsetWidth;
			sfor_btn.value = sfor_btn.stat_val + val;
			if(val_lng > 3)
			{
				val = check_start(val);
			}
			gto_btn.value = gto_btn.stat_val + "http://www." + val + ".com";
			gto_btn.go_to_val = val;
			gto_btn.style.width = "auto";
			if(gto_btn.offsetWidth>max_width)
			{
				gto_btn.style.width = max_width - 10;
			}
			sfor_btn.style.width = "auto";
			if(sfor_btn.offsetWidth>max_width)
			{
				sfor_btn.style.width = max_width - 10;
			}
		}
		else
		{
			document.getElementById("btns").style.display = "none";
			tdiv.className = "d2";
		}
	}
}

function is_expression(full_text, expr, is_start)
{
	if(is_start)
	{
		if ( full_text.substr(0,expr.length) == expr)
			full_text = full_text.substr(expr.length, full_text.length);
	}
	else
	{
		if( full_text.substr(full_text.length - expr.length, full_text.length) == expr )
			full_text = full_text.substr(0, full_text.length-expr.length);
	}
	return full_text;
}

function check_start(txt)
{
	txt = is_expression(txt, "http://", true);
	txt = is_expression(txt, "www.", true);
	txt = is_expression(txt, ".com", false);
	return txt;
}
function submitSrch(page)
{	
	var tmp = document.getElementById("search_keyword");
	var keyword = tmp.value;
	var search_mode = '';
	
	if (keyword == tmp.org_val)
	{
		document.location.href = "http://yandex.ru/yandsearch?clid=122936&text="+keyword;
		return false;
	}
	
	if	(keyword != '' )
		document.getElementById("search").submit();

	return false;
}
function go_to_page(obj)
{
	var loc = obj.go_to_val;
	if(loc!="")
	{
		document.location.href = "http://www."+escape(encodeURIComponent(loc))+".com";
		return false;
	}
	else
	{
		document.location.href = "http://search.icq.com/";
		return false;
	}
}